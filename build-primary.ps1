#!/usr/bin/env bash

# $ dotnet build --framework netstandard2.0  ./src/Base2art.Standard.DataStorage.Odbc/Base2art.Standard.DataStorage.Odbc.csproj

$src = Get-ChildItem -Path "src"
$test = Get-ChildItem -Path "test"
$version = "1.0.0.0"
$configType = "Release"

echo "cleaning local packages..."

foreach ($d in $src)
{
    if (Test-Path "$( $d.FullName )/bin/$( $configType )")
    {
        Remove-Item -Path "$( $d.FullName )/bin/$( $configType )/*.nupkg"
    }
}


echo "cleaning..."
dotnet clean --configuration "$( $configType )" -v q src/Base2art.Standard.DataStorage/Base2art.Standard.DataStorage.csproj

if (-not $?)
{
    exit $LASTEXITCODE
}

echo "building 1..."
dotnet build --configuration "$( $configType )" -v q src/Base2art.Standard.DataStorage/Base2art.Standard.DataStorage.csproj

if (-not $?)
{
    exit $LASTEXITCODE
}

echo "building 2..."
dotnet build --configuration "$( $configType )" -v q src/Base2art.Standard.DataStorage/Base2art.Standard.DataStorage.csproj

if (-not $?)
{
    exit $LASTEXITCODE
}


echo "packing..."
dotnet pack --include-symbols --include-source  -v q --configuration "$( $configType )" /p:PackageVersion="$( $version )"  src/Base2art.Standard.DataStorage/Base2art.Standard.DataStorage.csproj

if (-not $?)
{
    exit $LASTEXITCODE
}

echo "pushing to local..."

mkdir ./nuget-repository/

foreach ($d in $src)
{
    if (Test-Path "$( $d.FullName )/bin/$( $configType )")
    {
        $pkgs = Get-ChildItem -Filter "*.nupkg" -Path "$($d.FullName)/bin/Release/"  
        foreach ($pkg in $pkgs)
        {
            if ($pkg.Name.Contains(".symbols.")) {
                  cp "$($pkg.FullName)" ./nuget-repository/$($pkg.Name.Replace(".symbols.", "."))
                  
                  #cp "$($pkg.FullName)" ~/nuget-repository/
            }
        }
    }
}

