namespace Base2art.DataStorage.Provider.SQLite
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DataManipulation;
    using DataStorage.SQLite;
    using DynamicDataManipulation;

    public class DynamicExecutionEngine : IDynamicExecutionEngine
    {
        private readonly IExecutionEngine dapperExecutionEngine;

        public DynamicExecutionEngine(IExecutionEngine dapperExecutionEngine) => this.dapperExecutionEngine = dapperExecutionEngine;

        public bool SupportsDynamicQueries { get; } = true;

        public bool SupportsDynamicProcedureQueries { get; } = false;

        public Task<IEnumerable<TOutput>> ExecuteReader<TOutput>(string sql, object data, bool isProcedure)
        {
            var builderMap = new BuilderMaps(new SQLiteManipulationCommandBuilderFactory(), new DataManipulatorSupport());
            return isProcedure
                       ? this.dapperExecutionEngine.QueryProcedureReaderAsync<TOutput>(sql, data, builderMap, TimeSpan.Zero)
                       : this.dapperExecutionEngine.QueryReaderAsync<TOutput>(sql, data, builderMap, TimeSpan.Zero);
        }

        public Task<int> Execute(string sql, object data, bool isProcedure)
            => isProcedure
                   ? this.dapperExecutionEngine.ExecuteProcedure(sql, data, TimeSpan.Zero)
                   : this.dapperExecutionEngine.Execute(sql, data, TimeSpan.Zero);
    }
}