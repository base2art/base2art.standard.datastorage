﻿namespace Base2art.DataStorage.Provider.SQLite
{
    using Base2art.Dapper;
    using Base2art.Dapper.OdbcClient;
    using Dapper;
    using DataStorage.SQLite;
    using Interception.DataDefinition;
    using Interception.DataManipulation;
    using Microsoft.Data.Sqlite;

    public class SQLiteProvider : DataStorageProviderBase
    {
        public SQLiteProvider(
            string[] definerInterceptorNames,
            string[] manipulatorInterceptorNames,
            IDataDefinerInterceptor[] definerInterceptors,
            IDataManipulatorInterceptor[] manipulatorInterceptors) : base(
                                                                          definerInterceptorNames,
                                                                          manipulatorInterceptorNames,
                                                                          definerInterceptors,
                                                                          manipulatorInterceptors)
        {
        }

        public override IDynamicDataStore CreateDynamicDataStoreAccess(NamedConnectionString named)
            => new DynamicDataStore(new DynamicExecutionEngine(DapperExecutionEngine(named)));

        protected override IDataDefinerAndManipulatorProvider CreateActualStore(NamedConnectionString named)
        {
            var dapperExecutionEngine = DapperExecutionEngine(named);
            return new SQLiteFormatterProvider(dapperExecutionEngine);
        }

        private static DapperExecutionEngine DapperExecutionEngine(NamedConnectionString named)
        {
            var cstr = named.ConnectionString;

            var factory = new OdbcConnectionFactory<DelegatedConnection<SqliteConnection>>(cstr);

            var dapperExecutionEngine = new DapperExecutionEngine(factory);
            return dapperExecutionEngine;
        }
    }
}