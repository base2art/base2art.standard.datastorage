﻿namespace Base2art.DataStorage
{
    using System;

    public interface IDataConverter
    {
        object ConvertDataFromStorage(object value, Type propertyType);
    }
}