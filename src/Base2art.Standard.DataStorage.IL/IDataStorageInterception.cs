﻿namespace Base2art.DataStorage
{
    using System;

    public interface IDataStorageInterception : IDisposable
    {
    }
}