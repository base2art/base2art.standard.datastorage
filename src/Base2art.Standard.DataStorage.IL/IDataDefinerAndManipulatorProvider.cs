﻿namespace Base2art.DataStorage
{
    using DataDefinition;
    using DataManipulation;

    public interface IDataDefinerAndManipulatorProvider : IDataDefinerProvider, IDataManipulatorProvider
    {
    }
}