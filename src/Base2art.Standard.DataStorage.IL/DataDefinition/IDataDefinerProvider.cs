﻿namespace Base2art.DataStorage.DataDefinition
{
    public interface IDataDefinerProvider
    {
        IDataDefiner DataDefiner { get; }
    }
}