﻿namespace Base2art.DataStorage.DataDefinition.Builders
{
    public interface IDropTableBuilder
    {
        string BuildSql(
            string schemaName,
            string typeName,
            bool ifExists);
    }
}