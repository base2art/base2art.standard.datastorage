﻿namespace Base2art.DataStorage.DataDefinition.Builders
{
    using System.Linq;

    public interface ICreateTableKeysBuilder
    {
        string BuildSql(
            string schemaName,
            string typeName,
            ILookup<string, string> keys);

        string BuildSql(
            string schemaName,
            string typeName,
            string key,
            IGrouping<string, string> key2);
    }
}