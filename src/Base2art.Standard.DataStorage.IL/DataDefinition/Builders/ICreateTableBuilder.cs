﻿namespace Base2art.DataStorage.DataDefinition.Builders
{
    using System;
    using System.Collections.Generic;

    public interface ICreateTableBuilder
    {
        string BuildSql(
            string schemaName,
            string typeName,
            bool ifNotExists,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns);
    }
}