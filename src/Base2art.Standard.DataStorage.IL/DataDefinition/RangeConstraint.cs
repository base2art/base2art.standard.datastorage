﻿namespace Base2art.DataStorage.DataDefinition
{
    using System;

    public class RangeConstraint
    {
        public IComparable Min { get; set; }

        public IComparable Max { get; set; }

        public static RangeConstraint Create(IComparable min, IComparable max) => new RangeConstraint
                                                                                  {
                                                                                      Min = min,
                                                                                      Max = max
                                                                                  };

        public static RangeConstraint<T> Create<T>(T min, T max) where T : struct, IComparable<T> => new RangeConstraint<T>(min, max);
    }

    public class RangeConstraint<T> : RangeConstraint
        where T : struct, IComparable<T>
    {
        public RangeConstraint(T? min, T? max)
        {
            this.Min = min;
            this.Max = max;
        }

        public new T? Min { get; set; }
        public new T? Max { get; set; }
    }
}