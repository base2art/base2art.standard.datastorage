﻿namespace Base2art.DataStorage.DataDefinition
{
    public interface IStorageTypeMap
    {
        string ColumnType(DataTypes item2, bool item3, RangeConstraint item4);
    }
}