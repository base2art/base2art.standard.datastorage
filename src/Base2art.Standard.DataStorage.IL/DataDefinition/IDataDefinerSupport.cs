﻿namespace Base2art.DataStorage.DataDefinition
{
    public interface IDataDefinerSupport
    {
        bool DroppingTables { get; }

        string NativeExceptionTypeName { get; }

        bool ConstraintEnforcement { get; }
    }
}