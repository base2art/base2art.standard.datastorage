﻿namespace Base2art.DataStorage.DataDefinition
{
    using Builders;

    public interface IDefinerCommandBuilderFactory
    {
        ICreateTableBuilder CreateTableBuilder(IStorageTypeMap commonBuilder, IEscapeCharacters escapeCharacters);

        ICreateTableAltererBuilder CreateTableAltererBuilder(IStorageTypeMap commonBuilder, IEscapeCharacters escapeCharacters);

        ICreateTableIndexesBuilder CreateTableIndexesBuilder(IStorageTypeMap commonBuilder, IEscapeCharacters escapeCharacters);

        ICreateTableKeysBuilder CreateTableKeysBuilder(IStorageTypeMap commonBuilder, IEscapeCharacters escapeCharacters);

        IDropTableBuilder CreateDropTableBuilder(IEscapeCharacters escapeCharacters);
    }
}