﻿namespace Base2art.DataStorage.DataDefinition
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public interface IDataDefiner
    {
        IDataDefinerSupport Supports { get; }

        Task CreateTable(
            string schemaName,
            string tableName,
            bool allowUpdate,
            bool ifNotExists,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes);

        Task DropTable(string schemaName, string tableName, bool ifExists);
    }
}