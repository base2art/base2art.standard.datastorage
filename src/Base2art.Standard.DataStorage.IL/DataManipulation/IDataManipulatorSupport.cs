﻿namespace Base2art.DataStorage.DataManipulation
{
    public interface IDataManipulatorSupport
    {
        bool MultipleStatementsPerCommand { get; }

        OverflowHandlingStyle OverflowHandling { get; }

        UnderflowHandlingStyle UnderflowHandling { get; }

        string NativeExceptionTypeName { get; }
        bool InnerJoin { get; }
        bool RightJoin { get; }
        bool LeftJoin { get; }
        bool FullJoin { get; }
        bool CrossJoin { get; }

        bool TextConcatenationOperator { get; }
    }
}