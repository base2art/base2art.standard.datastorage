﻿namespace Base2art.DataStorage.DataManipulation
{
    public interface IDataManipulatorProvider
    {
        IDataManipulator DataManipulator { get; }
    }
}