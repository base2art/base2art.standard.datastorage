﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Data;

    public interface ISharedBuilderMaps
    {
        IDataManipulatorSupport Supports { get; }

        string TextConcatenationOperator { get; }

        object ConvertDataForSql(object input);

        void AddWhereClause(
            StringBuilder sb,
            WhereClause whereClause,
            IDictionary<string, object> data,
            string defaultSchema,
            string table,
            string prefix,
            string suffix);

        Tuple<bool, string> ConvertColumn(string tableAlias, Tuple<Column, OperatorType?, Column> y);

        string OperatorConversion(OperatorType operatorType);

        string RandomSyntax();
    }
}