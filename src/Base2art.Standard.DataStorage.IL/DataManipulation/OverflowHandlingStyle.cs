﻿namespace Base2art.DataStorage.DataManipulation
{
    public enum OverflowHandlingStyle
    {
        Truncate = 0,
        Expand = 1,
        ThrowException = 2
    }
}