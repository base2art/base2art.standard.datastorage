﻿namespace Base2art.DataStorage.DataManipulation.Builders
{
    using System.Collections.Generic;
    using Data;

    public interface IUpdateBuilder
    {
        string BuildSql(
            string defaultSchema,
            string schemaName,
            string typeName,
            SetsList sets,
            WhereClause wheres,
            string suffix,
            IDictionary<string, object> data);
    }
}