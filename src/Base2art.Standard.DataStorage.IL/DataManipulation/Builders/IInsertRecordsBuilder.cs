﻿namespace Base2art.DataStorage.DataManipulation.Builders
{
    using System.Collections.Generic;
    using Data;

    public interface IInsertRecordsBuilder
    {
        string BuildSql(
            string schemaName,
            string typeName,
            IReadOnlyList<SetsList> sets,
            string suffix,
            IDictionary<string, object> data);
    }
}