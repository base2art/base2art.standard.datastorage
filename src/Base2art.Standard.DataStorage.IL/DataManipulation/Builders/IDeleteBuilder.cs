﻿namespace Base2art.DataStorage.DataManipulation.Builders
{
    using System.Collections.Generic;
    using Data;

    public interface IDeleteBuilder
    {
        string BuildSql(
            string defaultSchema,
            string schemaName,
            string typeName,
            WhereClause wheres,
            string suffix,
            IDictionary<string, object> data);
    }
}