﻿namespace Base2art.DataStorage.DataManipulation.Builders
{
    using System.Collections.Generic;
    using Data;

    public interface IInsertSelectBuilder
    {
        string BuildSql(
            string defaultSchema,
            string schemaName,
            string typeName,
            FieldList list,
            SelectData records,
            string suffix,
            IDictionary<string, object> data);
    }
}