﻿namespace Base2art.DataStorage.DataManipulation.Builders
{
    using System.Collections.Generic;
    using Data;

    public interface ISelectBuilder
    {
        string BuildSql(
            SelectData selectData,
            string defaultSchema,
            string suffix,
            IDictionary<string, object> data,
            bool end,
            bool includeMarkers);
    }
}