﻿namespace Base2art.DataStorage.DataManipulation
{
    using Builders;

    public interface IManipulationCommandBuilderFactory
    {
        IInsertSelectBuilder CreateInsertSelectBuilder(ISharedBuilderMaps maps, IEscapeCharacters escapeCharacters);

        IInsertRecordsBuilder CreateInsertRecordsBuilder(ISharedBuilderMaps maps, IEscapeCharacters escapeCharacters);

        ISelectBuilder CreateSelectBuilder(ISharedBuilderMaps maps, IEscapeCharacters escapeCharacters);

        IUpdateBuilder CreateUpdateBuilder(ISharedBuilderMaps maps, IEscapeCharacters escapeCharacters);

        IDeleteBuilder CreateDeleteBuilder(ISharedBuilderMaps maps, IEscapeCharacters escapeCharacters);
    }
}