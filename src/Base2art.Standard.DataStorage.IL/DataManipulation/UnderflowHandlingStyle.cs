﻿namespace Base2art.DataStorage.DataManipulation
{
    public enum UnderflowHandlingStyle
    {
        None = 0,
        Pad = 1,
        ThrowException = 2
    }
}