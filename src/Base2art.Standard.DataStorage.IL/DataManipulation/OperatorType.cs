﻿namespace Base2art.DataStorage.DataManipulation
{
    public enum OperatorType
    {
        NoOp,

        Add,
        Subtract,
        Multiply,
        Divide,
        Modulus,
        IdentityInequality,
        IdentityEquality,

//        ValueEquality,
        BitwiseOr,
        BitwiseAnd,
        BooleanOr,
        BooleanAnd,
        LessThan,
        LessThanOrEqual,
        GreaterThan,
        GreaterThanOrEqual,

        In,
        NotIn,
        Like,
        NotLike
    }
}