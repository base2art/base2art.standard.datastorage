﻿namespace Base2art.DataStorage.DataManipulation
{
    public enum JoinType
    {
        None,
        Aggregate,
        AggregateGrouped,
        Inner,
        Left,
        Right,
        Full,
        Cross
    }
}