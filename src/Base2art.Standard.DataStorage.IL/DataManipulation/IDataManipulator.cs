﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Data;

    public interface IDataManipulator
    {
        IDataManipulatorSupport Supports { get; }

        Task InsertAsync(
            string schemaName,
            string tableName,
            IReadOnlyList<SetsList> records);

        Task InsertAsync(
            string schemaName,
            string tableName,
            FieldList list,
            SelectData records);

        Task<IEnumerable<T>> SelectAsync<T>(SelectData selectData);

        IEnumerable<T> Select<T>(SelectData selectData);

        Task<IEnumerable<Tuple<T1, T2>>> SelectAsync<T1, T2>(SelectData selectData);

        IEnumerable<Tuple<T1, T2>> Select<T1, T2>(SelectData selectData);

        Task<IEnumerable<Tuple<T1, T2, T3>>> SelectAsync<T1, T2, T3>(SelectData selectData);

        IEnumerable<Tuple<T1, T2, T3>> Select<T1, T2, T3>(SelectData selectData);

        Task<IEnumerable<Tuple<T1, T2, T3, T4>>> SelectAsync<T1, T2, T3, T4>(SelectData selectData);

        IEnumerable<Tuple<T1, T2, T3, T4>> Select<T1, T2, T3, T4>(SelectData selectData);

        Task<IEnumerable<Tuple<T1, T2, T3, T4, T5>>> SelectAsync<T1, T2, T3, T4, T5>(SelectData selectData);

        IEnumerable<Tuple<T1, T2, T3, T4, T5>> Select<T1, T2, T3, T4, T5>(SelectData selectData);

        Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>>> SelectAsync<T1, T2, T3, T4, T5, T6>(SelectData selectData);

        IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>> Select<T1, T2, T3, T4, T5, T6>(SelectData selectData);

        Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>>> SelectAsync<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData);

        IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>> Select<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData);

        Task UpdateAsync(
            string schemaName,
            string tableName,
            SetsList sets,
            WhereClause wheres);

        Task DeleteAsync(
            string schemaName,
            string tableName,
            WhereClause wheres);
    }
}