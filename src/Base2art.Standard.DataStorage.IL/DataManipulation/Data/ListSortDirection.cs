﻿namespace Base2art.DataStorage.DataManipulation.Data
{
    public enum ListSortDirection
    {
        Ascending,

        Descending
    }
}