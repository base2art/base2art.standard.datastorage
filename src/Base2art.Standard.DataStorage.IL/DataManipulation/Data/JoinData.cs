﻿namespace Base2art.DataStorage.DataManipulation.Data
{
    using System;

    public class JoinData
    {
        public JoinType JoinType { get; set; }

        public Tuple<int, Column, OperatorType, int, Column>[] OnData { get; set; }

        public SelectData SelectData { get; set; }
    }
}