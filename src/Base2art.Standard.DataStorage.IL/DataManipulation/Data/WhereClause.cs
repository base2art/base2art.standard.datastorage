﻿namespace Base2art.DataStorage.DataManipulation.Data
{
    using System;

    public class WhereClause
    {
        private readonly Tuple<Column, Tuple<OperatorType, Column>[], OperatorType, Column>[] data;

        public WhereClause(Tuple<Column, Tuple<OperatorType, Column>[], OperatorType, Column>[] data) => this.data = data;

        public Tuple<Column, Tuple<OperatorType, Column>[], OperatorType, Column>[] GetNames() =>
            this.data ?? new Tuple<Column, Tuple<OperatorType, Column>[], OperatorType, Column>[0];
    }
}