﻿namespace Base2art.DataStorage.DataManipulation.Data
{
    using System;

    public class FieldList
    {
        private readonly Tuple<Column, OperatorType?, Column>[] names;

        public FieldList(Tuple<Column, OperatorType?, Column>[] names) => this.names = names;

        public Tuple<Column, OperatorType?, Column>[] GetNames() => this.names ?? new Tuple<Column, OperatorType?, Column>[0];
    }
}