﻿namespace Base2art.DataStorage.DataManipulation.Data
{
    using System.Collections.Generic;
    using System.Linq;

    public class SetsList
    {
        private readonly IDictionary<string, object> dataLookup;
        private readonly IEnumerable<string> names;

        public SetsList(IEnumerable<string> names, IDictionary<string, object> dataLookup)
        {
            this.dataLookup = dataLookup ?? new Dictionary<string, object>();
            this.names = names;
        }

        public string[] GetNames() => (this.names ?? new string[0]).ToArray();

        public object GetFieldData(string fieldName)
        {
            object data;
            if (!this.dataLookup.TryGetValue(fieldName, out data))
            {
                data = null;
            }

            return data;
        }
    }
}