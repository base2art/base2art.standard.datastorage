﻿namespace Base2art.DataStorage.DataManipulation.Data
{
    public enum SpecialColumns
    {
        All,
        Count,
        NewUUID
    }
}