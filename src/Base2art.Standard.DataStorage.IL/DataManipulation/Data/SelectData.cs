﻿namespace Base2art.DataStorage.DataManipulation.Data
{
    public class SelectData
    {
        public string SchemaName { get; set; }
        public string TableName { get; set; }
        public bool WithNoLock { get; set; }
        public bool Distinct { get; set; }
        public FieldList FieldList { get; set; }
        public WhereClause WhereClause { get; set; }
        public int? Offset { get; set; }
        public int? Limit { get; set; }
        public JoinData JoinData { get; set; }
        public OrderingData[] Ordering { get; set; }
        public Column[] Grouping { get; set; }

        public string GetGroupByAggregateType()
        {
            var jd = this.JoinData;
            while (jd != null)
            {
                if (jd.JoinType == JoinType.AggregateGrouped)
                {
                    return jd.SelectData.TableName;
                }

                jd = jd?.SelectData?.JoinData;
            }

            return null;
        }
    }
}