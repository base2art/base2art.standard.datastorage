namespace Base2art.DataStorage.DataManipulation.Data
{
    public class ColumnMetaData
    {
        public string Prefix { get; set; }

        public string Suffix { get; set; }
    }
}