﻿namespace Base2art.DataStorage.DataManipulation.Data
{
    using System;
    using System.Collections;

    public class Column
    {
//        private readonly IEnumerable enumerable;

        private readonly string tableName;

        private Column(IEnumerable data) => this.ColumnData = data;

        private Column(SelectData data) => this.SelectData = data;

        private Column(SpecialColumns columnType) => this.SpecialColumn = columnType;

        public Column(SpecialColumns columnType, string columnName)
        {
            this.SpecialColumn = columnType;
            this.ColumnName = columnName;
        }

        private Column(string tableName, string data, Type dataType)
        {
            this.tableName = tableName;
            this.ColumnName = data;
            this.DataType = dataType;
        }

        private Column(object data, Type dataType)
        {
            this.ColumnData = data;
            this.DataType = dataType;
        }

        public SelectData SelectData { get; }

//        public string TableName
//        {
//            get { return tableName; }
//        }

        public string ColumnName { get; }

        public object ColumnData { get; }

        public SpecialColumns? SpecialColumn { get; }

        public Type DataType { get; }

        public ColumnMetaData ColumnMetaData { get; } = new ColumnMetaData();

        public static Column Simple(string data, Type dataType) => new Column(string.Empty, data, dataType);

        public static Column Simple(string tableName, string data, Type dataType) => new Column(string.Empty, data, dataType);

        public static Column Custom(SpecialColumns data) => new Column(data);

        public static Column CustomWithAlias(SpecialColumns column, string columnName) => new Column(column, columnName);

        public static Column InnerSelect(SelectData data) => new Column(data);

        public static Column Data(object data, Type dataType) => new Column(data, dataType);

        public static Column List(IEnumerable data) => new Column(data);

        public string FieldNameOrNull() => this.ColumnName;

        public string AsColumnText()
        {
            var printData = $"{this.ColumnName}{this.SelectData}{this.ColumnData}";
            if (!string.IsNullOrWhiteSpace(printData))
            {
                return printData;
            }

            return this.DataType == typeof(string) ? $"'{this.ColumnData}'" : $"{this.ColumnData}";
        }

        public override string ToString() => this.AsColumnText();
    }
}