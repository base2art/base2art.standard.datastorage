﻿namespace Base2art.DataStorage.DataManipulation.Data
{
    public class OrderingData
    {
        public Column Column { get; set; }

        public ListSortDirection Direction { get; set; }
    }
}