﻿namespace Base2art.DataStorage
{
    public enum DataTypes
    {
        Boolean,
        Decimal,
        Double,
        Float,
        Int,
        Long,
        Object,
        Short,
        String,
        Binary,
        DateTime,
        DateTimeOffset,

//        DateTime,
        TimeSpan,
        Guid,

        Xml
//        Object,
    }
}