﻿namespace Base2art.DataStorage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using ComponentModel;
    using DataDefinition;
    using DataManipulation;
    using DataManipulation.Data;
    using Interception.DataDefinition;
    using Interception.DataManipulation;

    public class WrappingDataStorage : IDataDefinerAndManipulator
    {
        private readonly IDataDefinerInterceptor[] definerInterceptors;
        private readonly IDataManipulatorInterceptor[] manipulatorInterceptors;

        public WrappingDataStorage(
            IDataDefinerInterceptor[] definerInterceptors,
            IDataManipulatorInterceptor[] manipulatorInterceptors,
            IDataDefiner definer,
            IDataManipulator manipulator)
        {
            this.definerInterceptors = definerInterceptors;
            this.manipulatorInterceptors = manipulatorInterceptors;
            this.Definer = definer;
            this.Manipulator = manipulator;
        }

        protected IDataDefiner Definer { get; }

        protected IDataManipulator Manipulator { get; }

        IDataManipulatorSupport IDataManipulator.Supports => this.Manipulator.Supports;

        IDataDefinerSupport IDataDefiner.Supports => this.Definer.Supports;

        public Task CreateTable(
            string schemaName,
            string tableName,
            bool allowUpdate,
            bool ifNotExists,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes) =>
            this.definerInterceptors.InvokeWrapped(x => x.OnCreateTable(
                                                                        schemaName,
                                                                        tableName,
                                                                        allowUpdate,
                                                                        ifNotExists,
                                                                        columns,
                                                                        indexes,
                                                                        keyes),
                                                   () => this.Definer.CreateTable(
                                                                                  schemaName,
                                                                                  tableName,
                                                                                  allowUpdate,
                                                                                  ifNotExists,
                                                                                  columns,
                                                                                  indexes,
                                                                                  keyes));

        public Task DropTable(string schemaName, string tableName, bool ifExists) =>
            this.definerInterceptors.InvokeWrapped(x => x.OnDropTable(schemaName, tableName, ifExists),
                                                   () => this.Definer.DropTable(schemaName, tableName, ifExists));

        public Task InsertAsync(string schemaName, string tableName, IReadOnlyList<SetsList> records) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnInsertAsync(schemaName, tableName, records),
                                                       () => this.Manipulator.InsertAsync(schemaName, tableName, records));

        public Task InsertAsync(string schemaName, string tableName, FieldList list, SelectData records) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnInsertAsync(schemaName, tableName, list, records),
                                                       () => this.Manipulator.InsertAsync(schemaName, tableName, list, records));

        public Task<IEnumerable<T>> SelectAsync<T>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelectAsync<T>(selectData),
                                                       () => this.Manipulator.SelectAsync<T>(selectData));

        public IEnumerable<T> Select<T>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelect<T>(selectData),
                                                       () => this.Manipulator.Select<T>(selectData));

        public Task<IEnumerable<Tuple<T1, T2>>> SelectAsync<T1, T2>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelectAsync<T1, T2>(selectData),
                                                       () => this.Manipulator.SelectAsync<T1, T2>(selectData));

        public IEnumerable<Tuple<T1, T2>> Select<T1, T2>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelect<T1, T2>(selectData),
                                                       () => this.Manipulator.Select<T1, T2>(selectData));

        public Task<IEnumerable<Tuple<T1, T2, T3>>> SelectAsync<T1, T2, T3>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelectAsync<T1, T2, T3>(selectData),
                                                       () => this.Manipulator.SelectAsync<T1, T2, T3>(selectData));

        public IEnumerable<Tuple<T1, T2, T3>> Select<T1, T2, T3>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelect<T1, T2, T3>(selectData),
                                                       () => this.Manipulator.Select<T1, T2, T3>(selectData));

        public Task<IEnumerable<Tuple<T1, T2, T3, T4>>> SelectAsync<T1, T2, T3, T4>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelectAsync<T1, T2, T3, T4>(selectData),
                                                       () => this.Manipulator.SelectAsync<T1, T2, T3, T4>(selectData));

        public IEnumerable<Tuple<T1, T2, T3, T4>> Select<T1, T2, T3, T4>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelect<T1, T2, T3, T4>(selectData),
                                                       () => this.Manipulator.Select<T1, T2, T3, T4>(selectData));

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5>>> SelectAsync<T1, T2, T3, T4, T5>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelectAsync<T1, T2, T3, T4, T5>(selectData),
                                                       () => this.Manipulator.SelectAsync<T1, T2, T3, T4, T5>(selectData));

        public IEnumerable<Tuple<T1, T2, T3, T4, T5>> Select<T1, T2, T3, T4, T5>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelect<T1, T2, T3, T4, T5>(selectData),
                                                       () => this.Manipulator.Select<T1, T2, T3, T4, T5>(selectData));

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>>> SelectAsync<T1, T2, T3, T4, T5, T6>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelectAsync<T1, T2, T3, T4, T5, T6>(selectData),
                                                       () => this.Manipulator.SelectAsync<T1, T2, T3, T4, T5, T6>(selectData));

        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>> Select<T1, T2, T3, T4, T5, T6>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelect<T1, T2, T3, T4, T5, T6>(selectData),
                                                       () => this.Manipulator.Select<T1, T2, T3, T4, T5, T6>(selectData));

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>>> SelectAsync<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelectAsync<T1, T2, T3, T4, T5, T6, T7>(selectData),
                                                       () => this.Manipulator.SelectAsync<T1, T2, T3, T4, T5, T6, T7>(selectData));

        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>> Select<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnSelect<T1, T2, T3, T4, T5, T6, T7>(selectData),
                                                       () => this.Manipulator.Select<T1, T2, T3, T4, T5, T6, T7>(selectData));

        public Task UpdateAsync(string schemaName, string tableName, SetsList sets, WhereClause wheres) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnUpdateAsync(schemaName, tableName, sets, wheres),
                                                       () => this.Manipulator.UpdateAsync(schemaName, tableName, sets, wheres));

        public Task DeleteAsync(string schemaName, string tableName, WhereClause wheres) =>
            this.manipulatorInterceptors.InvokeWrapped(x => x.OnDeleteAsync(schemaName, tableName, wheres),
                                                       () => this.Manipulator.DeleteAsync(schemaName, tableName, wheres));
    }
}