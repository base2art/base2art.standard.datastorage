﻿namespace Base2art.DataStorage.ComponentModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public static class Actions
    {
        public static Exception TryCatchAction<TIn>(this TIn arg, Action<TIn> run)
        {
            try
            {
                run(arg);
            }
            catch (Exception e)
            {
                return e;
            }

            return null;
        }

        public static async Task InvokeWrapped<T>(
            this IEnumerable<T> interceptors,
            Func<T, Task<IDataStorageInterception>> logger,
            Func<Task> runner)
        {
            var itemTasks = interceptors.Select(logger);
            var items = await Task.WhenAll(itemTasks);
            using (new AggregateDisposable<IDataStorageInterception>(items))
            {
                await runner();
            }
        }

        public static async Task<IEnumerable<TOut>> InvokeWrapped<T, TOut>(
            this IEnumerable<T> interceptors,
            Func<T, Task<IDataStorageInterception>> logger,
            Func<Task<IEnumerable<TOut>>> runner)
        {
            var itemTasks = interceptors.Select(logger);
            var items = await Task.WhenAll(itemTasks);
            using (new AggregateDisposable<IDataStorageInterception>(items))
            {
                return await runner();
            }
        }

        public static IEnumerable<TOut> InvokeWrapped<T, TOut>(
            this IEnumerable<T> interceptors,
            Func<T, Task<IDataStorageInterception>> logger,
            Func<IEnumerable<TOut>> runner)
        {
            var itemTasks = interceptors.Select(logger);
            var items = Task.WhenAll(itemTasks).GetAwaiter().GetResult();
            using (new AggregateDisposable<IDataStorageInterception>(items))
            {
                return runner();
            }
        }
    }
}