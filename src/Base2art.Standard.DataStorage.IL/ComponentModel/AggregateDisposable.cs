﻿namespace Base2art.DataStorage.ComponentModel
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class AggregateDisposable<T> : Component
        where T : IDisposable
    {
        private readonly IReadOnlyList<T> items;

        public AggregateDisposable(IReadOnlyList<T> items) => this.items = items ?? new List<T>();

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (!disposing)
            {
                return;
            }

            var exceptions = this.items
                                 .Select(x => x.TryCatchAction(y => y.Dispose()))
                                 .Where(x => x != null)
                                 .ToList();
            if (exceptions.Count > 0)
            {
                throw new AggregateException("Problems disposing", exceptions);
            }
        }
    }
}