﻿namespace Base2art.DataStorage.ComponentModel
{
    using System;

    public class Component : IDisposable
    {
        // Flag: Has Dispose already been called?
        private bool disposed;

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            this.DisposeInternal(true);
            GC.SuppressFinalize(this);
        }

        ~Component()
        {
            this.DisposeInternal(false);
        }

        protected virtual void Dispose(bool disposing)
        {
        }

        // Protected implementation of Dispose pattern.
        private void DisposeInternal(bool disposing)
        {
            if (this.disposed)
            {
                return;
            }

            this.Dispose(disposing);

            // Free any unmanaged objects here.
            //
            this.disposed = true;
        }
    }
}