﻿namespace Base2art.DataStorage
{
    using DataDefinition;
    using DataManipulation;

    public interface IDataDefinerAndManipulator : IDataDefiner, IDataManipulator
    {
    }
}