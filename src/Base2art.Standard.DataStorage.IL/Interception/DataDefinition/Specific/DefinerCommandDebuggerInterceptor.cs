﻿namespace Base2art.DataStorage.Interception.DataDefinition.Specific
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using ComponentModel;
    using DataStorage.DataDefinition;

    public class DefinerCommandDebuggerInterceptor : IDataDefinerInterceptor
    {
        private readonly string basePath;
        private readonly IDefinerCommandBuilderFactory definer;
        private readonly IEscapeCharacters escapeCharacters;
        private readonly string extension;
        private readonly IStorageTypeMap maps;

        public DefinerCommandDebuggerInterceptor(
            string basePath,
            string extension,
            IDefinerCommandBuilderFactory definer,
            IStorageTypeMap maps,
            IEscapeCharacters escapeCharacters)
        {
            this.basePath = basePath;
            this.extension = extension;
            this.definer = definer;
            this.maps = maps;
            this.escapeCharacters = escapeCharacters;
        }

        public Task<IDataStorageInterception> OnCreateTable(
            string schemaName,
            string tableName,
            bool allowUpdate,
            bool ifNotExists,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes)
        {
            var interceptions = new List<IDataStorageInterception>();

            var item1 = this.definer.CreateTableBuilder(this.maps, this.escapeCharacters)
                            .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(schemaName, tableName, ifNotExists, columns));

            interceptions.Add(item1);
            interceptions.AddRange(indexes.Select(idx => this.definer.CreateTableIndexesBuilder(this.maps, this.escapeCharacters)
                                                             .CreateLogger(this.basePath, this.extension,
                                                                           (x, data) => x.BuildSql(schemaName, tableName, idx.Key, idx))));

            interceptions.AddRange(keyes.Select(key => this.definer.CreateTableKeysBuilder(this.maps, this.escapeCharacters)
                                                           .CreateLogger(this.basePath, this.extension,
                                                                         (x, data) => x.BuildSql(schemaName, tableName, key.Key, key))));

            return Task.FromResult<IDataStorageInterception>(new AggregateDataStorageInterception(interceptions));
        }

        public Task<IDataStorageInterception> OnDropTable(string schemaName, string tableName, bool ifExists)
        {
            return Task.FromResult(this.definer.CreateDropTableBuilder(this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(
                                                                                                            schemaName,
                                                                                                            tableName,
                                                                                                            ifExists)));
        }

        private class AggregateDataStorageInterception : Component, IDataStorageInterception
        {
            private readonly AggregateDisposable<IDataStorageInterception> interceptions;

            public AggregateDataStorageInterception(List<IDataStorageInterception> interceptions) =>
                this.interceptions = new AggregateDisposable<IDataStorageInterception>(interceptions);

            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);
                if (disposing)
                {
                    this.interceptions.Dispose();
                }
            }
        }
    }
}