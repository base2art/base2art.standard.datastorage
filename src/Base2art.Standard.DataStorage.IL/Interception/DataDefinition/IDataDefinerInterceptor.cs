﻿namespace Base2art.DataStorage.Interception.DataDefinition
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DataStorage.DataDefinition;

    public interface IDataDefinerInterceptor
    {
        Task<IDataStorageInterception> OnCreateTable(
            string schemaName,
            string tableName,
            bool allowUpdate,
            bool ifNotExists,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes);

        Task<IDataStorageInterception> OnDropTable(string schemaName, string tableName, bool ifExists);
    }
}