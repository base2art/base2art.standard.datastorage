﻿namespace Base2art.DataStorage.Interception
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using ComponentModel;

    public static class Interceptor
    {
        public static IDataStorageInterception CreateLogger<T>(this T createInsertSelectBuilder,
                                                               string basePath,
                                                               string extension,
                                                               Func<T, Dictionary<string, object>, string> sqlProvider)
        {
            var data = new Dictionary<string, object>();
            var sql = sqlProvider(createInsertSelectBuilder, data);

            if (string.IsNullOrWhiteSpace(sql))
            {
                Log(basePath, extension, "No SQL: ", data);
                return new NullDataStorageInterception();
            }

            Log(basePath, extension, sql, data);
            return new NullDataStorageInterception();
        }

        private static void Log(string basePath, string extension, string sql, Dictionary<string, object> data)
        {
            extension = string.IsNullOrWhiteSpace(extension)
                            ? ".sql-log"
                            : extension;

            if (!extension.StartsWith("."))
            {
                extension = "." + extension;
            }

            var output = (string.IsNullOrEmpty(basePath)
                              ? () => Console.Out
                              : new Func<TextWriter>(() =>
                              {
                                  Directory.CreateDirectory(basePath);
                                  var path = Path.Combine(basePath, DateTime.UtcNow.ToString("yyyy-MM-dd") + extension);
                                  var stream = new FileStream(path, FileMode.Append, FileAccess.Write, FileShare.ReadWrite, 4096, false);
                                  return new StreamWriter(stream);
                              }))();

            output.WriteLine(sql);
            output.WriteLine("{");
            foreach (var key in data.Keys)
            {
                output.WriteLine("  {0} : `{1}`", key, data[key]);
            }

            output.WriteLine("}");
            output.Flush();
        }

        private class NullDataStorageInterception : Component, IDataStorageInterception
        {
        }
    }
}