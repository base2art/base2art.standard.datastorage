﻿namespace Base2art.DataStorage.Interception.DataManipulation.Specific
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DataStorage.DataManipulation;
    using DataStorage.DataManipulation.Data;

    public class ManipulatorCommandDebuggerInterceptor : IDataManipulatorInterceptor
    {
        private readonly string basePath;
        private readonly IEscapeCharacters escapeCharacters;
        private readonly string extension;
        private readonly IManipulationCommandBuilderFactory manipulator;
        private readonly ISharedBuilderMaps maps;

        public ManipulatorCommandDebuggerInterceptor(
            string basePath,
            string extension,
            IManipulationCommandBuilderFactory manipulator,
            ISharedBuilderMaps maps,
            IEscapeCharacters escapeCharacters)
        {
            this.basePath = basePath;
            this.extension = extension;
            this.manipulator = manipulator;
            this.maps = maps;
            this.escapeCharacters = escapeCharacters;
        }

        public Task<IDataStorageInterception> OnInsertAsync(string schemaName, string tableName, IReadOnlyList<SetsList> records)
        {
            return Task.FromResult(this.manipulator.CreateInsertRecordsBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(schemaName,
                                                                                                            tableName,
                                                                                                            records,
                                                                                                            "",
                                                                                                            data)));
        }

        public Task<IDataStorageInterception> OnInsertAsync(string schemaName, string tableName, FieldList records, SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateInsertSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(schemaName,
                                                                                                            schemaName,
                                                                                                            tableName,
                                                                                                            records,
                                                                                                            selectData,
                                                                                                            "",
                                                                                                            data)));
        }

        public Task<IDataStorageInterception> OnSelectAsync<T>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnSelectAsync<T1, T2>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnSelectAsync<T1, T2, T3>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnSelectAsync<T1, T2, T3, T4>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnSelectAsync<T1, T2, T3, T4, T5>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnSelectAsync<T1, T2, T3, T4, T5, T6>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnSelectAsync<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnSelect<T>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnSelect<T1, T2>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnSelect<T1, T2, T3>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnSelect<T1, T2, T3, T4>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnSelect<T1, T2, T3, T4, T5>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnSelect<T1, T2, T3, T4, T5, T6>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnSelect<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData)
        {
            return Task.FromResult(this.manipulator.CreateSelectBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql(selectData,
                                                                                                            "dbo",
                                                                                                            string.Empty,
                                                                                                            data,
                                                                                                            true,
                                                                                                            true)));
        }

        public Task<IDataStorageInterception> OnUpdateAsync(string schemaName, string tableName, SetsList sets, WhereClause wheres)
        {
            return Task.FromResult(this.manipulator.CreateUpdateBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql("dbo",
                                                                                                            schemaName,
                                                                                                            tableName,
                                                                                                            sets,
                                                                                                            wheres,
                                                                                                            string.Empty,
                                                                                                            data)));
        }

        public Task<IDataStorageInterception> OnDeleteAsync(string schemaName, string tableName, WhereClause wheres)
        {
            return Task.FromResult(this.manipulator.CreateDeleteBuilder(this.maps, this.escapeCharacters)
                                       .CreateLogger(this.basePath, this.extension, (x, data) => x.BuildSql("dbo",
                                                                                                            schemaName,
                                                                                                            tableName,
                                                                                                            wheres,
                                                                                                            string.Empty,
                                                                                                            data)));
        }
    }
}