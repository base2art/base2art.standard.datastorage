﻿namespace Base2art.DataStorage.Interception.DataManipulation
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DataStorage.DataManipulation.Data;

    public interface IDataManipulatorInterceptor
    {
        Task<IDataStorageInterception> OnInsertAsync(string schemaName, string tableName, IReadOnlyList<SetsList> records);

        Task<IDataStorageInterception> OnInsertAsync(string schemaName, string tableName, FieldList records, SelectData selectData);

        Task<IDataStorageInterception> OnSelectAsync<T>(SelectData selectData);
        Task<IDataStorageInterception> OnSelectAsync<T1, T2>(SelectData selectData);
        Task<IDataStorageInterception> OnSelectAsync<T1, T2, T3>(SelectData selectData);
        Task<IDataStorageInterception> OnSelectAsync<T1, T2, T3, T4>(SelectData selectData);
        Task<IDataStorageInterception> OnSelectAsync<T1, T2, T3, T4, T5>(SelectData selectData);
        Task<IDataStorageInterception> OnSelectAsync<T1, T2, T3, T4, T5, T6>(SelectData selectData);
        Task<IDataStorageInterception> OnSelectAsync<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData);

        Task<IDataStorageInterception> OnSelect<T>(SelectData selectData);
        Task<IDataStorageInterception> OnSelect<T1, T2>(SelectData selectData);
        Task<IDataStorageInterception> OnSelect<T1, T2, T3>(SelectData selectData);
        Task<IDataStorageInterception> OnSelect<T1, T2, T3, T4>(SelectData selectData);
        Task<IDataStorageInterception> OnSelect<T1, T2, T3, T4, T5>(SelectData selectData);
        Task<IDataStorageInterception> OnSelect<T1, T2, T3, T4, T5, T6>(SelectData selectData);
        Task<IDataStorageInterception> OnSelect<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData);

        Task<IDataStorageInterception> OnUpdateAsync(string schemaName, string tableName, SetsList sets, WhereClause wheres);
        Task<IDataStorageInterception> OnDeleteAsync(string schemaName, string tableName, WhereClause wheres);
    }
}