﻿namespace Base2art.DataStorage
{
    public interface IEscapeCharacters
    {
        string OpeningEscapeSequence { get; }

        string ClosingEscapeSequence { get; }

        string Text { get; }
    }
}