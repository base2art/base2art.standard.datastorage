﻿namespace Base2art.Dapper
{
    using System;
    using System.Data.Common;

    public class OdbcConnectionStringFactory : ConnectionFactory
    {
        public OdbcConnectionStringFactory(string connectionString, string odbcProviderTypeName) : base(connectionString,
                                                                                                        x => Create(x, odbcProviderTypeName))
        {
        }

        private static DbConnection Create(string connectionString, string odbcProviderTypeName)
        {
            var type = Type.GetType(odbcProviderTypeName, true);
            var factory = (DbProviderFactory) Activator.CreateInstance(type);
            var conn = factory.CreateConnection();
            conn.ConnectionString = connectionString;
            return conn;
        }
    }
}