﻿namespace Base2art.Dapper.OdbcClient
{
    using System.Data;
    using System.Data.Common;

    public class DelegatedConnection<T> : DbConnection
        where T : DbConnection, IDbConnection, new()
    {
        protected T Connection { get; } = new T();

        public override string ConnectionString
        {
            get => this.Connection.ConnectionString;
            set => this.Connection.ConnectionString = value;
        }

        public override string Database => this.Connection.Database;

        public override ConnectionState State => this.Connection.State;

        public override string DataSource => this.Connection.DataSource;

        public override string ServerVersion => this.Connection.ServerVersion;

        public override void ChangeDatabase(string databaseName)
        {
            this.Connection.ChangeDatabase(databaseName);
        }

        public override void Close()
        {
            this.Connection.Close();
        }

        public override void Open()
        {
            this.Connection.Open();
        }

        protected override DbTransaction BeginDbTransaction(IsolationLevel isolationLevel) => this.Connection.BeginTransaction(isolationLevel);

        protected override DbCommand CreateDbCommand() => new DelegatedCommand(this.Connection.CreateCommand());
    }
}