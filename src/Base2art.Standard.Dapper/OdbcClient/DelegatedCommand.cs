﻿namespace Base2art.Dapper.OdbcClient
{
    using System.Data;
    using System.Data.Common;

    public class DelegatedCommand : DbCommand
    {
        private readonly DbCommand dbCommandImplementation;

        public DelegatedCommand(DbCommand createCommand) => this.dbCommandImplementation = createCommand;

        public override string CommandText
        {
            get => this.dbCommandImplementation.CommandText;
            set => this.dbCommandImplementation.CommandText = value;
        }

        public override int CommandTimeout
        {
            get => this.dbCommandImplementation.CommandTimeout;
            set => this.dbCommandImplementation.CommandTimeout = value;
        }

        public override CommandType CommandType
        {
            get => this.dbCommandImplementation.CommandType;
            set => this.dbCommandImplementation.CommandType = value;
        }

        public override UpdateRowSource UpdatedRowSource
        {
            get => this.dbCommandImplementation.UpdatedRowSource;
            set => this.dbCommandImplementation.UpdatedRowSource = value;
        }

        protected override DbConnection DbConnection
        {
            get => this.dbCommandImplementation.Connection;
            set => this.dbCommandImplementation.Connection = value;
        }

        protected override DbParameterCollection DbParameterCollection => this.dbCommandImplementation.Parameters;

        protected override DbTransaction DbTransaction
        {
            get => this.dbCommandImplementation.Transaction;
            set => this.dbCommandImplementation.Transaction = value;
        }

        public override bool DesignTimeVisible
        {
            get => this.dbCommandImplementation.DesignTimeVisible;
            set => this.dbCommandImplementation.DesignTimeVisible = value;
        }

        public override void Cancel()
        {
            this.dbCommandImplementation.Cancel();
        }

        public override int ExecuteNonQuery() => this.dbCommandImplementation.ExecuteNonQuery();

        public override object ExecuteScalar() => this.dbCommandImplementation.ExecuteScalar();

        public override void Prepare()
        {
            this.dbCommandImplementation.Prepare();
        }

        protected override DbParameter CreateDbParameter() => this.dbCommandImplementation.CreateParameter();

        protected override DbDataReader ExecuteDbDataReader(CommandBehavior behavior) => this.dbCommandImplementation.ExecuteReader(behavior);

        protected override void Dispose(bool disposing)
        {
            if (!disposing)
            {
                base.Dispose(false);
            }
        }
    }
}