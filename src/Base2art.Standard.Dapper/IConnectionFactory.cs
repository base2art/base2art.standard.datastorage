﻿namespace Base2art.Dapper
{
    using System.Data.Common;
    using System.Threading.Tasks;

    public interface IConnectionFactory
    {
        DbConnection Connection();

        Task<DbConnection> ConnectionAsync();
    }
}