﻿namespace Base2art.Dapper
{
    using System.Data.Common;

    public class OdbcConnectionFactory<T> : ConnectionFactory
        where T : DbConnection, new()
    {
        public OdbcConnectionFactory(string connectionString)
            : base(connectionString, x => Create(connectionString))
        {
        }

        private static DbConnection Create(string connectionString)
        {
            var conn = new T();
            conn.ConnectionString = connectionString;
            return conn;
        }
    }
}