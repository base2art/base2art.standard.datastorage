﻿namespace Base2art.Dapper
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using global::Dapper;

    public static class DapperSync
    {
        public static int ExecuteStoredProc(this IConnectionFactory connectionFactory, string sprocName, object param, TimeSpan? commandTimeout) =>
            connectionFactory.ExecuteInternal(sprocName, param, CommandType.StoredProcedure, commandTimeout);

        public static IEnumerable<T> QueryStoredProc<T>(this IConnectionFactory connectionFactory, string sprocName, object param,
                                                        TimeSpan? commandTimeout) =>
            connectionFactory.QueryInternal<T>(sprocName, param, CommandType.StoredProcedure, commandTimeout);

        public static IEnumerable<Dictionary<string, object>> QueryStoredProc(
            this IConnectionFactory connectionFactory,
            string sprocName,
            object param,
            TimeSpan? commandTimeout)

        {
            using (var conn = connectionFactory.Connection())
            {
                var rez = conn.Query(
                                     sprocName,
                                     param,
                                     commandType: CommandType.StoredProcedure,
                                     transaction: null,
                                     commandTimeout: commandTimeout.AsTimeout(),
                                     buffered: true)
                              .Cast<IDictionary<string, object>>()
                              .Select(x => new Dictionary<string, object>(x ?? new Dictionary<string, object>(), StringComparer.OrdinalIgnoreCase));
                return rez;
            }
        }

        public static T SingleOfStoredProc<T>(this IConnectionFactory connectionFactory, string sprocName, object param, TimeSpan? commandTimeout) =>
            connectionFactory.SingleOfInternal<T>(sprocName, param, CommandType.StoredProcedure, commandTimeout);

        public static int Execute(this IConnectionFactory connectionFactory, string sql, object param, TimeSpan? commandTimeout) =>
            connectionFactory.ExecuteInternal(sql, param, CommandType.Text, commandTimeout);

        public static IEnumerable<T> Query<T>(this IConnectionFactory connectionFactory, string sql, object param, TimeSpan? commandTimeout) =>
            connectionFactory.QueryInternal<T>(sql, param, CommandType.Text, commandTimeout);

        public static T SingleOf<T>(this IConnectionFactory connectionFactory, string sql, object param, TimeSpan? commandTimeout) =>
            connectionFactory.SingleOfInternal<T>(sql, param, CommandType.Text, commandTimeout);

        public static IEnumerable<T> QueryReader<T>(this IConnectionFactory connectionFactory, string sql, object param,
                                                    Func<IDataRecord, T> buildObject, TimeSpan? commandTimeout) =>
            connectionFactory.QueryInternal(sql, param, buildObject, CommandType.Text, commandTimeout);

        public static T SingleOfReader<T>(this IConnectionFactory connectionFactory, string sql, object param, Func<IDataRecord, T> buildObject,
                                          TimeSpan? commandTimeout) =>
            connectionFactory.SingleOfInternal(sql, param, buildObject, CommandType.Text, commandTimeout);

        private static IEnumerable<T> QueryInternal<T>(
            this IConnectionFactory connectionFactory,
            string content,
            object parm,
            CommandType commandType,
            TimeSpan? commandTimeout)
        {
            using (var conn = connectionFactory.Connection())
            {
                var rez = conn.Query<T>(content, parm, commandType: commandType, transaction: null, commandTimeout: commandTimeout.AsTimeout(), buffered: true);
                return rez;
            }
        }

        private static T SingleOfInternal<T>(this IConnectionFactory connectionFactory, string content, object parm, CommandType commandType,
                                             TimeSpan? commandTimeout)
        {
            using (var conn = connectionFactory.Connection())
            {
                var rez = conn.Query<T>(content, parm, commandType: commandType, transaction: null, commandTimeout: commandTimeout.AsTimeout(), buffered: true);
                return rez.FirstOrDefault();
            }
        }

        private static int ExecuteInternal(this IConnectionFactory connectionFactory, string content, object parm, CommandType commandType,
                                           TimeSpan? commandTimeout)
        {
            using (var conn = connectionFactory.Connection())
            {
                return conn.Execute(content, parm, commandType: commandType, transaction: null, commandTimeout: commandTimeout.AsTimeout());
            }
        }

        private static IEnumerable<T> QueryInternal<T>(this IConnectionFactory connectionFactory, string content, object parm,
                                                       Func<IDataRecord, T> buildObject, CommandType commandType, TimeSpan? commandTimeout)
        {
            using (var conn = connectionFactory.Connection())
            {
                var reader = conn.ExecuteReader(content, parm, commandType: commandType, transaction: null,
                                                commandTimeout: commandTimeout.AsTimeout());
                var items = new List<T>();
                while (reader.Read())
                {
                    items.Add(buildObject(reader));
                }

                return items;
            }
        }

        private static T SingleOfInternal<T>(this IConnectionFactory connectionFactory, string content, object parm, Func<IDataRecord, T> buildObject,
                                             CommandType commandType, TimeSpan? commandTimeout)
        {
            using (var conn = connectionFactory.Connection())
            {
                var reader = conn.ExecuteReader(content, parm, commandType: commandType, transaction: null,
                                                commandTimeout: commandTimeout.AsTimeout());
                if (reader.Read())
                {
                    return buildObject(reader);
                }

                return default;
            }
        }

        internal static int? AsTimeout(this TimeSpan? commandTimeout)
        {
            if (commandTimeout.HasValue)
            {
                return (int) commandTimeout.Value.TotalSeconds;
            }

            return null;
        }
    }
}