﻿namespace Base2art.Dapper
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Linq;
    using System.Reflection;

    public static class Parser
    {
        public static T Parse<T>(IDataRecord x, Func<object, Type, object> commonBuilder)
        {
            var item = Parse1<T>(x, 0, commonBuilder);
            return item.Item1;
        }

        //, x => Parse<T>(x)
        //, x => ParseTuple<T1, T2>(x)
        //, x => ParseTuple<T1, T2>(x)
        //, x => ParseTuple3<T1, T2, T3>(x)
        //, x => ParseTuple3<T1, T2, T3>(x)
        //, x => Parse<T>(x)
        public static Tuple<T1, T2> ParseTuple<T1, T2>(IDataRecord x, Func<object, Type, object> commonBuilder)
        {
            var item1 = Parse1<T1>(x, 0, commonBuilder);
            var item2 = Parse1<T2>(x, item1.Item2, commonBuilder);

            return Tuple.Create(item1.Item1, item2.Item1);
        }

        public static Tuple<T1, T2, T3> ParseTuple<T1, T2, T3>(IDataRecord x, Func<object, Type, object> commonBuilder)
        {
            var item1 = Parse1<T1>(x, 0, commonBuilder);
            var item2 = Parse1<T2>(x, item1.Item2, commonBuilder);
            var item3 = Parse1<T3>(x, item2.Item2, commonBuilder);

            return Tuple.Create(item1.Item1, item2.Item1, item3.Item1);
        }

        public static Tuple<T1, T2, T3, T4> ParseTuple<T1, T2, T3, T4>(IDataRecord x, Func<object, Type, object> commonBuilder)
        {
            var item1 = Parse1<T1>(x, 0, commonBuilder);
            var item2 = Parse1<T2>(x, item1.Item2, commonBuilder);
            var item3 = Parse1<T3>(x, item2.Item2, commonBuilder);
            var item4 = Parse1<T4>(x, item3.Item2, commonBuilder);

            return Tuple.Create(item1.Item1, item2.Item1, item3.Item1, item4.Item1);
        }

        public static Tuple<T1, T2, T3, T4, T5> ParseTuple<T1, T2, T3, T4, T5>(IDataRecord x, Func<object, Type, object> commonBuilder)
        {
            var item1 = Parse1<T1>(x, 0, commonBuilder);
            var item2 = Parse1<T2>(x, item1.Item2, commonBuilder);
            var item3 = Parse1<T3>(x, item2.Item2, commonBuilder);
            var item4 = Parse1<T4>(x, item3.Item2, commonBuilder);
            var item5 = Parse1<T5>(x, item4.Item2, commonBuilder);

            return Tuple.Create(item1.Item1, item2.Item1, item3.Item1, item4.Item1, item5.Item1);
        }

        public static Tuple<T1, T2, T3, T4, T5, T6> ParseTuple<T1, T2, T3, T4, T5, T6>(IDataRecord x, Func<object, Type, object> commonBuilder)
        {
            var item1 = Parse1<T1>(x, 0, commonBuilder);
            var item2 = Parse1<T2>(x, item1.Item2, commonBuilder);
            var item3 = Parse1<T3>(x, item2.Item2, commonBuilder);
            var item4 = Parse1<T4>(x, item3.Item2, commonBuilder);
            var item5 = Parse1<T5>(x, item4.Item2, commonBuilder);
            var item6 = Parse1<T6>(x, item5.Item2, commonBuilder);

            return Tuple.Create(item1.Item1, item2.Item1, item3.Item1, item4.Item1, item5.Item1, item6.Item1);
        }

        public static Tuple<T1, T2, T3, T4, T5, T6, T7> ParseTuple<T1, T2, T3, T4, T5, T6, T7>(
            IDataRecord x, Func<object, Type, object> commonBuilder)
        {
            var item1 = Parse1<T1>(x, 0, commonBuilder);
            var item2 = Parse1<T2>(x, item1.Item2, commonBuilder);
            var item3 = Parse1<T3>(x, item2.Item2, commonBuilder);
            var item4 = Parse1<T4>(x, item3.Item2, commonBuilder);
            var item5 = Parse1<T5>(x, item4.Item2, commonBuilder);
            var item6 = Parse1<T6>(x, item5.Item2, commonBuilder);
            var item7 = Parse1<T7>(x, item6.Item2, commonBuilder);

            return Tuple.Create(item1.Item1, item2.Item1, item3.Item1, item4.Item1, item5.Item1, item6.Item1,
                                item7.Item1);
        }

        private static Tuple<T, int> Parse1<T>(IDataRecord x, int start, Func<object, Type, object> commonBuilder)
        {
            var type = typeof(T);
            T data;
            Dictionary<string, object> dict = null;

            if (type.GetTypeInfo().IsClass)
            {
                data = (T) Activator.CreateInstance(type);
            }
            else
            {
                dict = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
                data = new TypedDictionaryProxy<T>(dict).Data;
            }

            var members = type.GetPublicProperties()
                              .Select<PropertyInfo, MemberInfo>(y => y)
                              .Union(type.GetFields(BindingFlags.FlattenHierarchy | BindingFlags.Instance | BindingFlags.Public))
                              .ToArray();

            var i = start;
            for (i = start; i < x.FieldCount; i++)
            {
                var name = x.GetName(i);
                if (name.StartsWith("__FIELD__SPLIT", StringComparison.OrdinalIgnoreCase))
                {
                    return Tuple.Create(data, i + 1);
                }

                var member = members.FirstOrDefault(y => string.Equals(name, y.Name, StringComparison.OrdinalIgnoreCase));

#if DEBUG
                object value;
                try
                {
                    value = x.GetValue(i);
                }
                catch (Exception)
                {
                    Console.WriteLine(name);
                    throw;
                }
#else
                var value = x.GetValue(i);

#endif

                if (dict == null)
                {
                    if (value == DBNull.Value)
                    {
                        value = null;
                    }

                    if (member is PropertyInfo pi)
                    {
                        if (value == null && pi.PropertyType.GetTypeInfo().IsValueType)
                        {
                            value = Activator.CreateInstance(pi.PropertyType);
                        }

                        pi.SetValue(data, commonBuilder(value, pi.PropertyType));
                    }

                    if (member is FieldInfo fi)
                    {
                        if (value == null && fi.FieldType.GetTypeInfo().IsValueType)
                        {
                            value = Activator.CreateInstance(fi.FieldType);
                        }

                        fi.SetValue(data, commonBuilder(value, fi.FieldType));
                    }
                }
                else
                {
                    if (value == DBNull.Value)
                    {
                        value = null;
                    }

                    if (member is PropertyInfo pi)
                    {
                        if (value == null && pi.PropertyType.GetTypeInfo().IsValueType)
                        {
                            value = Activator.CreateInstance(pi.PropertyType);
                        }

                        dict[name] = commonBuilder(value, pi.PropertyType);
                    }

                    if (member is FieldInfo fi)
                    {
                        if (value == null && fi.FieldType.GetTypeInfo().IsValueType)
                        {
                            value = Activator.CreateInstance(fi.FieldType);
                        }

                        dict[name] = commonBuilder(value, fi.FieldType);
                    }
                }
            }

            return Tuple.Create(data, i + 1);
        }
    }
}