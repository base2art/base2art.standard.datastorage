﻿namespace Base2art.Dapper
{
    using System;
    using System.Data.Common;
    using System.Threading.Tasks;

    public class ConnectionFactory : IConnectionFactory
    {
        private readonly Func<string, DbConnection> connectionCreator;
        private readonly string connectionString;

        protected ConnectionFactory(
            string connectionString,
            Func<string, DbConnection> connectionCreator)
        {
            this.connectionCreator = connectionCreator;
            this.connectionString = connectionString;
        }

        public DbConnection Connection()
        {
            var connection = this.connectionCreator(this.connectionString);
            connection.Open();
            return connection;
        }

        public async Task<DbConnection> ConnectionAsync()
        {
            var connection = this.connectionCreator(this.connectionString);
            await connection.OpenAsync();
            return connection;
        }
    }
}