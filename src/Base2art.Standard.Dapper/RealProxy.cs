namespace System.Runtime.Remoting.Messaging
{
    public interface IMethodReturnMessage : IMethodMessage
    {
        int OutArgCount { get; }
        object[] OutArgs { get; }
        Exception Exception { get; }
        object ReturnValue { get; }
        string GetOutArgName(int index);
        object GetOutArg(int argNum);
    }
}

namespace System.Runtime.Remoting.Messaging
{
    using Reflection;

    public interface IMethodMessage : IMessage
    {
        string Uri { get; }

        string MethodName { get; }

        string TypeName { get; }

        object MethodSignature { get; }

        int ArgCount { get; }

        object[] Args { get; }

        bool HasVarArgs { get; }

        LogicalCallContext LogicalCallContext { get; }

        MethodBase MethodBase { get; }

        string GetArgName(int index);

        object GetArg(int argNum);
    }
}

namespace System.Runtime.Remoting.Messaging
{
    public interface IMethodCallMessage : IMethodMessage
    {
        int InArgCount { get; }

        object[] InArgs { get; }

        string GetInArgName(int index);

        object GetInArg(int argNum);
    }
}

namespace System.Runtime.Remoting.Messaging
{
    using Reflection;

    public class ReturnMessage : IMethodReturnMessage
    {
        private readonly LogicalCallContext callCtx;
        private readonly IMethodCallMessage mcm;

        public ReturnMessage(
            Exception e,
            IMethodCallMessage mcm)
        {
            this.mcm = mcm;
            this.Exception = e;
        }

        public ReturnMessage(
            object returnValue,
            object[] outArgs,
            int outArgsCount,
            LogicalCallContext callCtx,
            IMethodCallMessage mcm)
        {
            this.OutArgs = outArgs;
            this.OutArgCount = outArgsCount;
            this.callCtx = callCtx;
            this.mcm = mcm;
            this.ReturnValue = returnValue;
        }

        public int OutArgCount { get; }
        public object[] OutArgs { get; }
        public Exception Exception { get; }
        public object ReturnValue { get; }

        public string Uri => this.mcm.Uri;

        public string MethodName => this.mcm.MethodName;

        public string TypeName => this.mcm.TypeName;

        public object MethodSignature => this.mcm.MethodSignature;

        public int ArgCount => this.mcm.ArgCount;

        public object[] Args => this.mcm.Args;

        public bool HasVarArgs => this.mcm.HasVarArgs;

        public LogicalCallContext LogicalCallContext => this.mcm.LogicalCallContext;

        public MethodBase MethodBase => this.mcm.MethodBase;

        public string GetArgName(int index) => this.mcm.GetArgName(index);

        public object GetArg(int argNum) => this.mcm.GetArg(argNum);

        public string GetOutArgName(int index) => this.mcm.GetArgName(index);

        public object GetOutArg(int argNum) => this.mcm.GetArg(argNum);
    }
}

namespace System.Runtime.Remoting.Messaging
{
    public interface IMessage
    {
    }
}

namespace System.Runtime.Remoting.Messaging
{
    public class LogicalCallContext
    {
    }
}

namespace System.Runtime.Remoting.Proxies.Internals
{
    using Messaging;
    using Reflection;

    internal sealed class MethodCallMessage : IMethodCallMessage
    {
        public MethodCallMessage(MethodInfo targetMethod, object[] args, LogicalCallContext logicalCallContext)
        {
            this.MethodBase = targetMethod;
            this.Args = args;
            this.LogicalCallContext = logicalCallContext;
        }

        public object[] Args { get; }

        public LogicalCallContext LogicalCallContext { get; }

        public MethodBase MethodBase { get; }

        public string Uri => throw new NotImplementedException();

        public string MethodName => this.MethodBase.Name;

        public string TypeName => this.MethodBase.DeclaringType.Name;

        public object MethodSignature => this.MethodBase;

        public int ArgCount => this.Args.Length;

        public int InArgCount => this.Args.Length;

        public object[] InArgs => this.Args;

        public bool HasVarArgs => false;

        public string GetArgName(int index) => this.MethodBase.GetParameters()[index].Name;

        public object GetArg(int argNum) => this.Args[argNum];

        public string GetInArgName(int index) => this.MethodBase.GetParameters()[index].Name;

        public object GetInArg(int argNum) => this.Args[argNum];
    }
}

namespace System.Runtime.Remoting.Proxies.Internals
{
    using Messaging;
    using Reflection;

    public class Wrapper : DispatchProxy
    {
        public RealProxy Target { get; set; }

        protected override object Invoke(MethodInfo targetMethod, object[] args)
        {
            var target = this.Target;
            if (target == null)
            {
                return null;
            }

            var message = new MethodCallMessage(targetMethod, args, new LogicalCallContext());
            var result = target.Invoke(message);
            if (!(result is IMethodReturnMessage rm))
            {
                return null;
            }

            if (rm.Exception != null)
            {
                // new TargetInvocationException(rm.Exception);
                throw rm.Exception;
            }

            return rm.ReturnValue;
        }
    }
}

namespace System.Runtime.Remoting.Proxies
{
    using Internals;
    using Messaging;
    using Reflection;

    public abstract class RealProxy
    {
        private readonly Type type;

        protected RealProxy(Type type) => this.type = type;

        public abstract IMessage Invoke(IMessage msg);

        public object GetTransparentProxy()
        {
            var method = typeof(RealProxy).GetRuntimeMethod(nameof(Create), new Type[0]);
            var generic = method.MakeGenericMethod(this.type);
            var proxy = generic.Invoke(null, null);

            ((Wrapper) proxy).Target = this;

            return proxy;
        }

        public static object Create<T>() => DispatchProxy.Create<T, Wrapper>();
    }
}