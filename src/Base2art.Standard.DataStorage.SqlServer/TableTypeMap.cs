﻿namespace Base2art.DataStorage.SqlServer
{
    using DataStorage.DataDefinition;

    public class TableTypeMap : ITypeMap
    {
        public string StringType(int? min, int? max)
        {
            if (max.HasValue)
            {
                if (min.GetValueOrDefault(-1) == max.Value)
                {
                    return "NCHAR(" + max + ")";
                }

                if (max.Value > 8000)
                {
                    return "NVARCHAR(MAX)";
                }
            }

            return "NVARCHAR(" + (max.HasValue ? "" + max : "MAX") + ")";
        }

        public string ObjectType(int? min, int? max) => "NVARCHAR(" + (max.HasValue ? "" + max : "MAX") + ")";

        public string BinaryType(int? min, int? max) => "VARBINARY(" + (max.HasValue ? "" + max : "MAX") + ")";

        public string XmlType() => "XML";

        public string ShortType() => "SMALLINT";

        public string TimeSpanType() => "TIME";

        public string LongType() => "BIGINT";

        public string IntType() => "INT";

        public string GuidType() => "UNIQUEIDENTIFIER";

        public string FloatType() => "REAL";

        public string DoubleType() => "FLOAT";

        public string DecimalType() => "DECIMAL";

        public string DateTimeOffsetType() => "DATETIMEOFFSET";

        public string DateTimeType() => "DATETIME";

        public string BooleanType() => "BIT";
    }
}