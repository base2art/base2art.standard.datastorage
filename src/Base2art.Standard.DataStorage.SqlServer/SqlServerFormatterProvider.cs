﻿namespace Base2art.DataStorage.SqlServer
{
    using System;
    using DataStorage.DataDefinition;
    using DataStorage.DataManipulation;

    public class SqlServerFormatterProvider : IDataDefinerAndManipulatorProvider
    {
        private const string ExceptionName = "System.Data.SqlClient.SqlException";
        private static readonly IManipulationCommandBuilderFactory manipulatorBuilderFactory;
        private static readonly DefaultStorageTypeMap typeMap;
        private static readonly SharedBuilderMaps sharedBuilderMaps;
        private readonly string defaultSchema;
        private readonly TimeSpan defaultTimeout;
        private readonly IExecutionEngine engine;

        static SqlServerFormatterProvider()
        {
            manipulatorBuilderFactory = new SqlServerManipulationCommandBuilderFactory();

            ITypeMap tableTypeMap = new TableTypeMap();
            typeMap = new DefaultStorageTypeMap(tableTypeMap);
            sharedBuilderMaps = new BuilderMaps(manipulatorBuilderFactory, DataManipulatorSupport);
        }

        public SqlServerFormatterProvider(IExecutionEngine engine, string defaultSchema, TimeSpan defaultTimeout)
        {
            this.engine = engine;
            this.defaultSchema = defaultSchema;
            this.defaultTimeout = defaultTimeout;
        }

        public static IDataManipulatorSupport DataManipulatorSupport { get; } = new DataManipulatorSupport
                                                                                {
                                                                                    MultipleStatementsPerCommand = false,
                                                                                    OverflowHandling = OverflowHandlingStyle.ThrowException,
                                                                                    UnderflowHandling = UnderflowHandlingStyle.Pad,
                                                                                    NativeExceptionTypeName = ExceptionName
                                                                                };

        public IDataDefiner DataDefiner => new DataDefinerFormatter(
                                                                    this.engine,
                                                                    this.defaultSchema,
                                                                    new DataDefinerSupport
                                                                    {
                                                                        DroppingTables = true,
                                                                        NativeExceptionTypeName = ExceptionName,
                                                                        ConstraintEnforcement = true
                                                                    },
                                                                    DataManipulatorSupport,
                                                                    sharedBuilderMaps,
                                                                    sharedBuilderMaps,
                                                                    sharedBuilderMaps,
                                                                    typeMap,
                                                                    this.defaultTimeout,
                                                                    new SqlServerDefinerCommandBuilderFactory(),
                                                                    manipulatorBuilderFactory);

        public IDataManipulator DataManipulator => new DataManipulatorFormatter(
                                                                                this.engine,
                                                                                this.defaultSchema,
                                                                                DataManipulatorSupport,
                                                                                sharedBuilderMaps,
                                                                                sharedBuilderMaps,
                                                                                sharedBuilderMaps,
                                                                                this.defaultTimeout,
                                                                                manipulatorBuilderFactory);
    }
}