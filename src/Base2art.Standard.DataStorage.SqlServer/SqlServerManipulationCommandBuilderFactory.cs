﻿namespace Base2art.DataStorage.SqlServer
{
    using DataStorage.DataManipulation;
    using DataStorage.DataManipulation.Builders;
    using SelectBuilder = DataManipulation.SelectBuilder;

    public class SqlServerManipulationCommandBuilderFactory : IManipulationCommandBuilderFactory
    {
        public IDeleteBuilder CreateDeleteBuilder(ISharedBuilderMaps maps, IEscapeCharacters escapeCharacters) =>
            new DeleteBuilder(maps, escapeCharacters);

        public IInsertSelectBuilder CreateInsertSelectBuilder(ISharedBuilderMaps maps, IEscapeCharacters escapeCharacters) =>
            new InsertSelectBuilder(this, maps, escapeCharacters);

        public IInsertRecordsBuilder CreateInsertRecordsBuilder(ISharedBuilderMaps maps, IEscapeCharacters escapeCharacters) =>
            new InsertRecordsBuilder(maps, escapeCharacters);

        public ISelectBuilder CreateSelectBuilder(ISharedBuilderMaps maps, IEscapeCharacters escapeCharacters) =>
            new SelectBuilder(maps, escapeCharacters);

        public IUpdateBuilder CreateUpdateBuilder(ISharedBuilderMaps maps, IEscapeCharacters escapeCharacters) =>
            new UpdateBuilder(maps, escapeCharacters);
    }
}