﻿namespace Base2art.DataStorage.SqlServer.DataManipulation
{
    using System;
    using System.Text;
    using DataStorage.DataManipulation;
    using DataStorage.DataManipulation.Data;

    public class SelectBuilder : DataStorage.DataManipulation.SelectBuilder
    {
        public SelectBuilder(ISharedBuilderMaps commonBuilder, IEscapeCharacters escapeCharacters) : base(commonBuilder, escapeCharacters)
        {
        }

        protected override string TableLevelNoLockHint => "WITH (NOLOCK)";

        protected override void LimitOffset(StringBuilder sb, SelectData selectData)
        {
            var offset = selectData.Offset;
            var count = selectData.Limit;
            Func<SelectData, bool> hasOrderBy = null;

            hasOrderBy = x =>
            {
                if (x.Ordering != null && x.Ordering.Length > 0)
                {
                    return true;
                }

                if (x.JoinData?.SelectData == null)
                {
                    return false;
                }

                return hasOrderBy(x.JoinData.SelectData);
            };

            if (count.HasValue)
            {
                if (!hasOrderBy(selectData))
                {
                    sb.AppendLine("ORDER BY NEWID()");
                }

                this.Offset(sb, offset.GetValueOrDefault(0));
                this.Limit(sb, count.Value);
                return;
            }

            if (offset.HasValue)
            {
                if (!hasOrderBy(selectData))
                {
                    sb.AppendLine("ORDER BY NEWID()");
                }

                this.Offset(sb, offset.Value);
            }
        }

        protected override void Offset(StringBuilder sb, int offset)
        {
            sb.Append("OFFSET ");
            sb.Append(offset);
            sb.Append(" ROWS ");
            sb.AppendLine();
        }

        protected override void Limit(StringBuilder sb, int count)
        {
            sb.Append("FETCH NEXT ");
            sb.Append(count);
            sb.Append(" ROWS ONLY ");
            sb.AppendLine();
        }
    }
}