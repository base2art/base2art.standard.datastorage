﻿namespace Base2art.DataStorage.SqlServer.DataDefinition
{
    using System.Text;

    public class DropTableBuilder : DataStorage.DataDefinition.DropTableBuilder
    {
        public DropTableBuilder(IEscapeCharacters escapeChars) : base(escapeChars)
        {
        }

        public override string BuildSql(string schemaName, string typeName, bool ifExists)
        {
            var sb = new StringBuilder();

            if (ifExists)
            {
                if (string.IsNullOrWhiteSpace(schemaName))
                {
                    sb.AppendLine("IF OBJECT_ID('" + typeName + "', 'U') IS NOT NULL");
                }
                else
                {
                    sb.AppendLine("IF OBJECT_ID('" + schemaName + "." + typeName + "', 'U') IS NOT NULL");
                }
            }

            sb.Append("DROP TABLE ");

            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }

            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append(typeName);
            sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            sb.AppendLine(";");
            return sb.ToString();
        }
    }
}