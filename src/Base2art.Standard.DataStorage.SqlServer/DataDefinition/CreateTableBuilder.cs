﻿namespace Base2art.DataStorage.SqlServer.DataDefinition
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using DataStorage.DataDefinition;

    public class CreateTableBuilder : DataStorage.DataDefinition.CreateTableBuilder
    {
        public CreateTableBuilder(IStorageTypeMap convertData, IEscapeCharacters escapeCharacters) : base(convertData, escapeCharacters)
        {
        }

        public override string BuildSql(string schemaName, string typeName, bool ifNotExists,
                                        IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns)
        {
            var sb = new StringBuilder();

//            if (!string.IsNullOrWhiteSpace(schemaName))
//            {
//                sb.AppendLine("IF NOT EXISTS (SELECT  schema_name FROM    information_schema.schemata WHERE schema_name = '" + schemaName + "' ) ");
//                sb.Append("CREATE SCHEMA ");
//                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
//                sb.Append(schemaName);
//                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
//                sb.AppendLine(";");
//            }

            if (ifNotExists)
            {
                if (string.IsNullOrWhiteSpace(schemaName))
                {
                    sb.AppendLine("IF OBJECT_ID('" + typeName + "', 'U') IS NULL");
                }
                else
                {
                    sb.AppendLine("IF OBJECT_ID('" + schemaName + "." + typeName + "', 'U') IS NULL");
                }
            }

            columns = columns ?? new Tuple<string, DataTypes, bool, RangeConstraint>[0];

            sb.AppendLine("CREATE TABLE ");
            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }

            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append(typeName);
            sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            sb.AppendLine("(");
            sb.AppendJoined(", ", columns.ToArray(), (item, i) => this.BuildField(sb, item));
            sb.AppendLine(");");

            return sb.ToString();
        }
    }
}