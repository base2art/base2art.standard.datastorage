namespace Base2art.DataStorage.GoogleDocs
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Xml;
    using System.Xml.Linq;
    using DataStorage;
    using DataDefinition;
    using HolisticPersistence;
    using HolisticPersistence.Specialized;
    using Google.Apis.Sheets.v4;
    using Google.Apis.Sheets.v4.Data;
    using Google.Apis.Util;

    public class StateManager : IStateManager
    {
        private readonly SheetsService sheetsService;
        private readonly string databaseId;

        private Dictionary<Guid, List<(int col, string name, DataTypes type, bool nullable, RangeConstraint constraint)>> sessionMetadataCache
            = new Dictionary<Guid, List<(int col, string name, DataTypes type, bool nullable, RangeConstraint constraint)>>();

        public StateManager(SheetsService sheetsService, string databaseId)
        {
            this.sheetsService = sheetsService;
            this.databaseId = databaseId;
        }

        public IIntermediateMapper Mapper { get; } = new GoogleDocsDataMapper();

        public ITransactionalSession Session()
        {
            return new TransactionalSession(Guid.NewGuid());
        }

        protected virtual StringComparer Comp => StringComparer.OrdinalIgnoreCase;

        // Task<IEnumerable<(string objectId, IReadOnlyDictionary<string, object>)>> IItemsProducer.GetItems(string tableName)
        //     => this.AllRows(tableName);

        public async Task<Dictionary<string, object>> CreateRowFromTemplate(string tableName, Guid sessionId)
        {
            var cols = await this.GetMetadata(tableName, sessionId);

            var template = new Dictionary<string, object>(Comp);

            foreach (var col in cols)
            {
                template[col.name] = col.nullable ? null : col.type.CreateDefault();
            }

            return template;
        }

        public async Task<IReadOnlyDictionary<string, Tuple<bool, RangeConstraint>>> GetConstraint(string tableName, Guid sessionId)
        {
            var cols = await this.GetMetadata(tableName, sessionId);

            var constraint = new Dictionary<string, Tuple<bool, RangeConstraint>>(Comp);

            foreach (var col in cols)
            {
                constraint[col.name] = (!col.nullable, col.constraint).ToTuple();
            }

            return constraint;
        }

        public async Task AddItemsToTable(string tableName, Dictionary<string, object>[] dictionaryItems, Guid sessionId)
        {
            ValueRange valuesRange = new ValueRange
                                     {
                                         Values = new List<IList<object>>()
                                     };

            var cols = await this.GetMetadata(tableName, sessionId);

            foreach (var dictionary in dictionaryItems)
            {
                var objects = new List<object>();
                objects.AddRange(dictionary.Values.Select((x, i) => MapToInput(cols.FirstOrDefault(y => y.col == i), x)));
                valuesRange.Values.Add(objects);
            }

            string rangeToUpdate = $"{tableName}!A3:{(RangeCreator(valuesRange.Values.First().Count))}{3 + valuesRange.Values.Count}";

            var request = sheetsService.Spreadsheets.Values.Append(valuesRange, this.databaseId, rangeToUpdate);
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;
            await request.ExecuteAsync();
        }

        private object MapToInput((int col, string name, DataTypes type, bool nullable, RangeConstraint constraint) firstOrDefault, object x)
        {
            var (_, _, type, _, _) = firstOrDefault;
            switch (type)
            {
                case DataTypes.Boolean:
                    break;
                case DataTypes.Decimal:
                    break;
                case DataTypes.Double:
                    break;
                case DataTypes.Float:
                    break;
                case DataTypes.Int:
                    break;
                case DataTypes.Long:
                    break;
                case DataTypes.Object:
                    return NullOrEmpty(x, () => this.Mapper.SerializeAsJson(x));
                case DataTypes.Short:
                    break;
                case DataTypes.String:
                    return NullOrNull(x, () => x);

                case DataTypes.Binary:
                    break;
                case DataTypes.DateTime:
                    return NullOrEmpty(x, () => ((DateTime) x).ToString("s"));
                case DataTypes.DateTimeOffset:
                    return NullOrEmpty(x, () => ((DateTimeOffset) x).ToString("s"));
                case DataTypes.TimeSpan:
                    break;
                case DataTypes.Guid:
                    break;
                case DataTypes.Xml:
                    return NullOrEmpty(x, () => ConvertXml(x));
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return x;
        }

        private string ConvertXml(object o)
        {
            if (o is XmlDocument xmlDocument)
            {
                return xmlDocument.OuterXml;
            }

            if (o is XContainer xDocument)
            {
                return xDocument.ToString();
            }

            return "";
        }

        private object NullOrNull(object o, Func<object> p0) => o == null ? "__NULL__" : p0();
        private object NullOrEmpty(object o, Func<object> p0) => o == null ? "" : p0();

        public Task<IReadOnlyDictionary<string, string[]>> GetKeys(string tableName, Guid sessionId)
            => Task.FromResult<IReadOnlyDictionary<string, string[]>>(new Dictionary<string, string[]>());

        public async Task RemoveTable(string tableName, Guid sessionId)
        {
            var sheets = await this.sheetsService.Spreadsheets.Get(this.databaseId).ExecuteAsync();
            var sheetId = sheets.Sheets.FirstOrDefault(x => x.Properties.Title == tableName)?.Properties?.SheetId;

            if (!sheetId.HasValue)
            {
                return;
            }

            var body = new BatchUpdateSpreadsheetRequest
                       {
                           Requests = new List<Request>
                                      {
                                          new Request {DeleteSheet = new DeleteSheetRequest {SheetId = sheetId}}
                                      }
                       };
            var request = this.sheetsService.Spreadsheets.BatchUpdate(body, this.databaseId);
            await request.ExecuteAsync();
        }

        public async Task<bool> ContainsTable(string tableName, Guid sessionId)
        {
            var sheets = await this.sheetsService.Spreadsheets.Get(this.databaseId).ExecuteAsync();
            var sheetId = sheets.Sheets.FirstOrDefault(x => x.Properties.Title == tableName)?.Properties?.SheetId;

            return sheetId.HasValue;
        }

        public async Task AddTable(string tableName, Guid sessionId)
        {
            var store = this.sheetsService;

            var body = new BatchUpdateSpreadsheetRequest
                       {
                           Requests = new List<Request>
                                      {
                                          new Request {AddSheet = new AddSheetRequest {Properties = new SheetProperties {Title = tableName}}}
                                      }
                       };
            var request = store.Spreadsheets.BatchUpdate(body, this.databaseId);
            await request.ExecuteAsync();
        }

        public async Task AddTemplateAndConstraints(string tableName, IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns, Guid sessionId)
        {
            await this.AppendColumnsToTable(tableName, columns, 1);
        }

        public async Task AddOrUpdateTemplateAndConstraints(
            string tableName,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns,
            Guid sessionId)
        {
            var currentCols = await this.GetMetadata(tableName, sessionId);
            var currentSet = new HashSet<string>(currentCols.Select(x => x.name), Comp);
            var itemsToAdd = columns.Where((x) => !currentSet.Contains(x.Item1));
            await AppendColumnsToTable(tableName, itemsToAdd.ToArray(), currentCols.Count + 1);
        }

        public Task AddOrUpdateKey(string tableName, string columnName, string[] refCols, Guid sessionId)
            => Task.CompletedTask;

        public async Task RemoveRecords(string tableName, string[] records, Guid sessionId)
        {
            var store = this.sheetsService;

            var body = new BatchClearValuesRequest()
                       {
                           Ranges = new List<string>(records)
                       };
            var request = store.Spreadsheets.Values.BatchClear(body, this.databaseId);
            await request.ExecuteAsync();
        }

        public async Task UpdateItems(string tableName, IEnumerable<(string objectId, Dictionary<string, object> newRow)> itemsToUpdate, Guid sessionId)
        {
            var store = this.sheetsService;

            var valueRanges = new List<ValueRange>();
            foreach (var row in itemsToUpdate)
            {
                ValueRange valueRange = new ValueRange {Values = new List<IList<object>>()};
                var objects = new List<object>();
                objects.AddRange(row.newRow.Values);
                valueRange.Values.Add(objects);

                string rangeToUpdate = $"{tableName}!{'A'}{row.objectId}:{RangeCreator(valueRange.Values.First().Count)}{row.objectId}";
                valueRange.Range = rangeToUpdate;
                valueRanges.Add(valueRange);
            }

            var body = new BatchUpdateValuesRequest()
                       {
                           Data = valueRanges,
                           ValueInputOption = StringValue(SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.USERENTERED)
                       };
            var request = store.Spreadsheets.Values.BatchUpdate(body, this.databaseId);
            await request.ExecuteAsync();
        }

        private string StringValue<TEnum>(TEnum value)
            where TEnum : struct
        {
            var enumType = typeof(TEnum);
            var memberInfos = enumType.GetMember(value.ToString());
            var enumValueMemberInfo = memberInfos.FirstOrDefault(m => m.DeclaringType == enumType);
            var valueAttributes = enumValueMemberInfo.GetCustomAttributes(typeof(StringValueAttribute), true);
            var description = ((StringValueAttribute) valueAttributes[0]).Text;
            return description;
        }

        public async Task<IEnumerable<(string objectId, IReadOnlyDictionary<string, object> data)>> AllRows(string tableName, Guid sessionId)
        {
            var store = await sheetsService.Spreadsheets.Values.Get(this.databaseId, $"{tableName}").ExecuteAsync();

            var cols = this.GetMetadata(store, sessionId);

            return store.Values.Select((x, i) => (x, i))
                        .Skip(3)
                        .Select((x) => ((x.i + 1).ToString(), ToDictionary(cols, x.x)));
        }

        private async Task<List<(int col, string name, DataTypes type, bool nullable, RangeConstraint constraint)>> GetMetadata(
            string tableName, Guid sessionId)
        {
            if (this.sessionMetadataCache.TryGetValue(sessionId, out var parms))
            {
                return parms;
            }

            var store = await sheetsService.Spreadsheets.Values.Get(this.databaseId, $"{tableName}!1:3").ExecuteAsync();

            return this.GetMetadata(store, sessionId);
        }

        private async Task AppendColumnsToTable(string tableName, IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns, int startPosition)
        {
            var store = this.sheetsService;
            var valuesRange = new ValueRange();
            valuesRange.Values = new List<IList<object>>();

            IList<object> cols1 = new List<object>();
            IList<object> cols2 = new List<object>();
            IList<object> cols3 = new List<object>();
            foreach (var col in columns)
            {
                cols1.Add(col.Item2 + (col.Item3 ? "" : "?"));
                cols2.Add(col.Item4.Min + ":-:" + col.Item4.Max);
                cols3.Add(col.Item1);
            }

            valuesRange.Values.Add(cols1);
            valuesRange.Values.Add(cols2);
            valuesRange.Values.Add(cols3);

            var range = $"{tableName}!{RangeCreator(startPosition)}1:{RangeCreator(startPosition + cols1.Count)}3";
            var request1 = store.Spreadsheets.Values.Update(valuesRange, this.databaseId, range);
            request1.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.USERENTERED;
            await request1.ExecuteAsync();
        }

        private List<(int col, string name, DataTypes type, bool nullable, RangeConstraint constraint)> GetMetadata(ValueRange store, Guid sessionId)
        {
            var parms = new List<(int col, string name, DataTypes type, bool, RangeConstraint)>();
            var types = store.Values[0];
            var constraints = store.Values[1];
            var names = store.Values[2];
            for (int j = 0; j < types.Count; j++)
            {
                var (type, nullable) = ParseDataType(types[j].ToString());
                var constraintValue = constraints[j]?.ToString();
                if (string.IsNullOrWhiteSpace(constraintValue) && !(constraintValue ?? "").Contains(":-:"))
                {
                    parms.Add((j, names[j].ToString(), type, nullable, null));
                }

                // pretty much the only time it should ever be none.
                // as you need to parse both the lft and right side even if empty.
                var parts = constraintValue.Split(new[] {":-:"}, StringSplitOptions.None);

                var minStr = parts[0];
                var maxStr = parts[1];
                var min = IntValue(minStr);
                var max = IntValue(maxStr);
                parms.Add((j, names[j].ToString(), type, nullable, RangeConstraint.Create(min, max)));
            }

            this.sessionMetadataCache[sessionId] = parms;
            return parms;
        }

        private int? IntValue(string valStr)
            => int.TryParse(valStr, out var val) ? (int?) val : null;

        // public static string RangeCreator(int count)
        // {
        //     return RangeCreator('A', count);
        // }

        public static string RangeCreator(int count)
        {
            if (count == 0)
            {
                return "";
            }

            count -= 1;

            var firstCounter = count / 26;
            var secondCounter = count % 26;
            char startPosition = 'A';
            var first = firstCounter == 0 ? "" : new string((char) (startPosition + (firstCounter - 1)), 1);
            var second = (char) (startPosition + secondCounter);

            return $"{first}{second}";
        }

        private static (DataTypes type, bool nullable) ParseDataType(string value)
        {
            var required = true;
            if (value.Trim().EndsWith("?"))
            {
                required = false;
                value = value.Trim().TrimEnd('?');
            }

            return ((DataTypes) Enum.Parse(typeof(DataTypes), value, true), !required);
        }

        private IReadOnlyDictionary<string, object> ToDictionary(
            List<(int col, string name, DataTypes type, bool nullable, RangeConstraint constraint)> cols,
            IList<object> objects)
            => this.Parse(cols.ToArray(), objects);

        private Dictionary<string, object> Parse((int col, string name, DataTypes type, bool nullable, RangeConstraint constraint)[] headers, IList<object> row)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>(this.Comp);
            for (int i = 0; i < row.Count; i++)
            {
                var header = headers[i];
                dict[header.name] = CleanObject(row[i], header);
            }

            return dict;
        }

        private object CleanObject(object o, (int col, string name, DataTypes type, bool nullable, RangeConstraint constraint) header)
        {
            return this.Mapper.Cleanup(header.type, o);
        }
    }
}