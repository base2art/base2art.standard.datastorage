﻿namespace Base2art.DataStorage.GoogleDocs
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using DataDefinition;
    using DataManipulation;
    using DataManipulation.Data;
    using HolisticPersistence;
    using HolisticPersistence.Specialized;

    public class DataDefiner : StateBasedDataDefiner
    {
        public DataDefiner(DirectoryInfo credentialsFolder, string databaseId)
            : base(new StateManager( SheetServiceProvider.GetService(credentialsFolder), databaseId))
        {
        }


        protected override IDataDefinerSupport GetDefinerSupport() => new DataDefinerSupport
                                                                      {
                                                                          DroppingTables = true,
                                                                          NativeExceptionTypeName = typeof(InvalidOperationException).FullName,
                                                                          ConstraintEnforcement = false
                                                                      };

        protected override IDataManipulatorSupport GetManipulatorSupports() => new DataManipulatorSupport
                                                                               {
                                                                                   MultipleStatementsPerCommand = true,
                                                                                   OverflowHandling = OverflowHandlingStyle.Truncate,
                                                                                   UnderflowHandling = UnderflowHandlingStyle.Pad,
                                                                                   NativeExceptionTypeName = typeof(InvalidOperationException).FullName,
                                                                                   CrossJoin = true,
                                                                                   RightJoin = true,
                                                                                   LeftJoin = true,
                                                                                   InnerJoin = true,
                                                                                   FullJoin = true,
                                                                                   TextConcatenationOperator = true
                                                                               };

        public override object CreateDefault(DataTypes dataType) => dataType.CreateDefault();
    }
}