namespace Base2art.DataStorage.GoogleDocs
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Xml;
    using Google.Apis.Upload;
    using HolisticPersistence;

    public class GoogleDocsDataMapper : SimpleIntermediateMapper
    {
        public Dictionary<DataTypes, Func<object, object>> mapping = new Dictionary<DataTypes, Func<object, object>>();

        public GoogleDocsDataMapper()
        {
            this.mapping[DataTypes.Decimal] = (x) => ParseNullableDecimal(StringOf(x));
            this.mapping[DataTypes.Float] = (x) => ParseNullableSingle(StringOf(x));
            this.mapping[DataTypes.Double] = (x) => ParseNullableDouble(StringOf(x));
            this.mapping[DataTypes.Boolean] = (x) => ParseNullableBool(StringOf(x));
            // this.mapping[typeof(bool)] = (x) => ParseNullableBool(StringOf(x)).GetValueOrDefault();

            this.mapping[DataTypes.Short] = (x) => ParseNullableInt16(StringOf(x));
            this.mapping[DataTypes.Int] = (x) => ParseNullableInt32(StringOf(x));
            this.mapping[DataTypes.Long] = (x) => ParseNullableInt64(StringOf(x));
            // this.mapping[DataTypes.] = (x) => ParseNullableInt64(StringOf(x));
            // this.mapping[typeof(int)] = (x) => ParseNullableInt32(StringOf(x)).GetValueOrDefault();

            this.mapping[DataTypes.Guid] = (x) => ParseNullableGuid(StringOf(x));
            // this.mapping[typeof(Guid)] = (x) => ParseNullableGuid(StringOf(x)).GetValueOrDefault();
            this.mapping[DataTypes.DateTime] = (x) => ParseNullableDateTime(StringOf(x)).GetValueOrDefault();
            this.mapping[DataTypes.DateTimeOffset] = (x) => ParseNullableDateTimeOffset(StringOf(x)).GetValueOrDefault();
            this.mapping[DataTypes.String] = (x) => ParseString(StringOf(x));
            this.mapping[DataTypes.Object] = (x) => ParseObject(StringOf(x));
            this.mapping[DataTypes.Binary] = (x) => ParseBinary(StringOf(x));
            this.mapping[DataTypes.TimeSpan] = (x) => ParseNullableTimeSpan(StringOf(x));
            this.mapping[DataTypes.Xml] = (x) => ParseXmlDocument(StringOf(x));
        }

        private byte[] ParseBinary(string value)
            => string.IsNullOrWhiteSpace(value) ? new byte[0] : Convert.FromBase64String(value);

        private object ParseObject(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return null;
            }

            return this.DeserializeAsJson(value);
        }

        private string ParseString(string stringValue)
        {
            if (stringValue == "__NULL__")
            {
                return null;
            }

            return stringValue;
        }

        protected override object CleanupValue(DataTypes dataType, object value)
        {
            var obj = base.CleanupValue(dataType, value);
            return !this.mapping.ContainsKey(dataType) ? obj : this.mapping[dataType](obj);
        }

        private static TimeSpan? ParseNullableTimeSpan(string arg)
            => TimeSpan.TryParse(arg, out var valueOf) ? (TimeSpan?) valueOf : null;

        private static XmlDocument ParseXmlDocument(string arg)
        {
            if (string.IsNullOrWhiteSpace(arg))
            {
                return null;
            }

            var doc = new XmlDocument();
            doc.LoadXml(arg);
            return doc;
        }

        private static DateTime? ParseNullableDateTime(string arg)
            => DateTime.TryParse(arg, out var valueOf) ? (DateTime?) valueOf : null;

        private static DateTimeOffset? ParseNullableDateTimeOffset(string arg)
            => DateTimeOffset.TryParse(arg, out var valueOf) ? (DateTimeOffset?) valueOf : null;

        private static bool? ParseNullableBool(string arg)
            => bool.TryParse(arg, out var valueOf) ? (bool?) valueOf : null;

        private static Guid? ParseNullableGuid(string arg)
            => Guid.TryParse(arg, out var valueOf) ? (Guid?) valueOf : null;

        private static short? ParseNullableInt16(string arg)
            => short.TryParse(arg, out var valueOf) ? (short?) valueOf : null;
        private static int? ParseNullableInt32(string arg)
            => int.TryParse(arg, out var valueOf) ? (int?) valueOf : null;
        private static long? ParseNullableInt64(string arg)
            => long.TryParse(arg, out var valueOf) ? (long?) valueOf : null;
        
        private static decimal? ParseNullableDecimal(string arg)
            => decimal.TryParse(arg, out var valueOf) ? (decimal?) valueOf : null;
        
        private static double? ParseNullableDouble(string arg)
            => double.TryParse(arg, out var valueOf) ? (double?) valueOf : null;
        
        private static float? ParseNullableSingle(string arg)
            => float.TryParse(arg, out var valueOf) ? (float?) valueOf : null;

        private static string StringOf(object o)
            => o is string str ? str : o?.ToString();
    }
}

// protected override T MapObject<T>(IReadOnlyDictionary<string, object> dict)
// {
//     var newDict = dict.CloneReadable(StringComparer.OrdinalIgnoreCase);
//     // this.CleanObject<T>(newDict);
//
//     return base.MapObject<T>(newDict);
// }
//
// private void CleanObject<T>(Dictionary<string, object> dict)
// {
//     var type = typeof(T);
//     var props = type.GetProperties();
//     foreach (var propertyInfo in props)
//     {
//         if (this.mapping.ContainsKey(propertyInfo.PropertyType))
//         {
//             if (dict.ContainsKey(propertyInfo.Name))
//             {
//                 dict[propertyInfo.Name] = this.mapping[propertyInfo.PropertyType](dict[propertyInfo.Name]);
//             }
//         }
//     }
// }