namespace Base2art.DataStorage.GoogleDocs
{
    using System;
    using System.IO;
    using System.Threading;
    using Google.Apis.Auth.OAuth2;
    using Google.Apis.Services;
    using Google.Apis.Sheets.v4;
    using Google.Apis.Util.Store;

    public static class SheetServiceProvider
    {
        public static SheetsService GetService(DirectoryInfo credentialsFolder)
        {
            var credential = GetUserCredentials(credentialsFolder);
            var service = GetSheetsService(credential);

            return service;
        }

        private static SheetsService GetSheetsService(UserCredential credential)
        {
            string ApplicationName = "Scott's app";
            // Create Google Sheets API service.
            var service = new SheetsService(new BaseClientService.Initializer()
                                            {
                                                HttpClientInitializer = credential,
                                                ApplicationName = ApplicationName
                                            });
            return service;
        }

        private static UserCredential GetUserCredentials(DirectoryInfo credentialFolder)
        {
            string[] Scopes = {SheetsService.Scope.Spreadsheets};

            string credentialsPathFolder = credentialFolder.FullName;
            using (var stream = new FileStream(Path.Combine(credentialsPathFolder, "credentials.json"), FileMode.Open, FileAccess.Read))
            {
                // The file token.json stores the user's access and refresh tokens, and is created
                // automatically when the authorization flow completes for the first time.
                string credPath = Path.Combine(credentialsPathFolder, "token.json");
                var credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                                                                             GoogleClientSecrets.FromStream(stream).Secrets,
                                                                             Scopes,
                                                                             "user",
                                                                             CancellationToken.None,
                                                                             new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
                return credential;
            }
        }
    }
}