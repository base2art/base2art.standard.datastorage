namespace Base2art.DataStorage.GoogleDocs
{
    using System;
    using ComponentModel;
    using HolisticPersistence.Specialized;

    public class TransactionalSession : Component, ITransactionalSession
    {
        public TransactionalSession(Guid sessionId)
        {
            this.Id = sessionId;
        }

        public Guid Id { get; }
    }
}