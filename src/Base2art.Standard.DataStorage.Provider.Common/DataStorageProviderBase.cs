﻿namespace Base2art.DataStorage.Provider
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Interception.DataDefinition;
    using Interception.DataManipulation;

    public abstract class DataStorageProviderBase : IDataStorageProvider
    {
        private readonly HashSet<string> definerInterceptorNames;
        private readonly IDataDefinerInterceptor[] definerInterceptors;
        private readonly HashSet<string> manipulatorInterceptorNames;
        private readonly IDataManipulatorInterceptor[] manipulatorInterceptors;

        protected DataStorageProviderBase(
            string[] definerInterceptorNames,
            string[] manipulatorInterceptorNames,
            IDataDefinerInterceptor[] definerInterceptors,
            IDataManipulatorInterceptor[] manipulatorInterceptors)
        {
            this.definerInterceptorNames = new HashSet<string>(definerInterceptorNames ?? new string[0]);
            this.manipulatorInterceptorNames = new HashSet<string>(manipulatorInterceptorNames ?? new string[0]);
            this.definerInterceptors = definerInterceptors;
            this.manipulatorInterceptors = manipulatorInterceptors;
        }

        public IDataStore CreateDataStoreAccess(NamedConnectionString named) => new DataStore(this.Backing(named));

        public IDbms CreateDbmsAccess(NamedConnectionString named) => new Dbms(this.Backing(named));

        public abstract IDynamicDataStore CreateDynamicDataStoreAccess(NamedConnectionString named);

        protected IDataDefinerAndManipulator Backing(NamedConnectionString named)
        {
            var actual = this.CreateActualStore(named);

            return new WrappingDataStorage(
                                           this.definerInterceptors
                                               .Where(x => IsMatch(this.definerInterceptorNames, x.GetType()))
                                               .ToArray(),
                                           this.manipulatorInterceptors
                                               .Where(x => IsMatch(this.manipulatorInterceptorNames, x.GetType()))
                                               .ToArray(),
                                           actual.DataDefiner,
                                           actual.DataManipulator);
        }

        protected abstract IDataDefinerAndManipulatorProvider CreateActualStore(NamedConnectionString named);

        private static bool IsMatch(HashSet<string> searchSet, Type type) =>
            searchSet.Contains("*") || searchSet.Contains(type.Name) || searchSet.Contains(type.FullName);
    }
}