﻿namespace Base2art.DataStorage.MySql
{
    using System;
    using System.Collections.Generic;
    using DataStorage.DataManipulation;

    public class BuilderMaps : SharedBuilderMaps
    {
        public BuilderMaps(IManipulationCommandBuilderFactory builderFactory, IDataManipulatorSupport dataManipulatorSupport)
            : base(builderFactory, dataManipulatorSupport)
        {
        }

        public override string OpeningEscapeSequence => "`";

        public override string ClosingEscapeSequence => "`";

        public static Guid FromMySql(byte[] bytearray)
        {
            var ba = new byte[16];
            ba[3] = bytearray[0];
            ba[2] = bytearray[1];
            ba[1] = bytearray[2];
            ba[0] = bytearray[3];

            ba[5] = bytearray[4];
            ba[4] = bytearray[5];

            ba[7] = bytearray[6];
            ba[6] = bytearray[7];

            ba[8] = bytearray[8];
            ba[9] = bytearray[9];

            ba[10] = bytearray[10];
            ba[11] = bytearray[11];
            ba[12] = bytearray[12];
            ba[13] = bytearray[13];
            ba[14] = bytearray[14];
            ba[15] = bytearray[15];

            return new Guid(ba);
        }

        public static byte[] ToMySql(Guid value)
        {
            var bytearray = value.ToByteArray();
            var ba = new byte[16];
            ba[3] = bytearray[0];
            ba[2] = bytearray[1];
            ba[1] = bytearray[2];
            ba[0] = bytearray[3];

            ba[5] = bytearray[4];
            ba[4] = bytearray[5];

            ba[7] = bytearray[6];
            ba[6] = bytearray[7];

            ba[8] = bytearray[8];
            ba[9] = bytearray[9];

            ba[10] = bytearray[10];
            ba[11] = bytearray[11];
            ba[12] = bytearray[12];
            ba[13] = bytearray[13];
            ba[14] = bytearray[14];
            ba[15] = bytearray[15];

            return ba;
        }

        protected override object MapGuid(Guid value) => ToMySql(value);

        protected override Guid MapFromNonNullGuid(object value) => FromMySql((byte[]) value);

        protected override object MapDateTime(DateTime value) => value.ToString("yyyyMMddHHmmss");

        protected override TimeSpan MapFromNonNullTimeSpan(object value)
        {
            var result = base.MapFromNonNullTimeSpan(value);
            return result;
        }

        protected override object MapTimeSpan(TimeSpan value)
        {
            var result = string.Format("{0:hh}:{0:mm}:{0:ss}", value);
            return result;
        }

        public override string NewUUIDSyntax() => "unhex(REPLACE(UUID(),'-',''))";
        public override string RandomSyntax() => "RAND()";

        protected override object MapObjectToSql(Dictionary<string, object> value) => base.MapObjectToSql(value);

        protected override Dictionary<string, object> MapFromNonNullObject(object value) => base.MapFromNonNullObject(value);
    }
}