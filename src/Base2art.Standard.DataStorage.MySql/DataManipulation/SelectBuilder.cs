﻿namespace Base2art.DataStorage.MySql.DataManipulation
{
    using DataStorage.DataManipulation;

    public class SelectBuilder : DataStorage.DataManipulation.SelectBuilder
    {
        public SelectBuilder(ISharedBuilderMaps maps, IEscapeCharacters escapeCharacters) : base(maps, escapeCharacters)
        {
        }

        protected override string TableLevelNoLockHint => "";
    }
}