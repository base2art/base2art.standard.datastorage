﻿namespace Base2art.DataStorage.MySql
{
    using DataDefinition;

    public class TableTypeMap : ITypeMap
    {
        public string StringType(int? min, int? max)
        {
            if (!max.HasValue)
            {
                return "TEXT";
            }

            if (min.GetValueOrDefault(-1) == max.Value)
            {
                return "CHAR(" + max + ")";
            }

            if (max.Value > 16777214)
            {
                return "LONGTEXT";
            }

            if (max.Value > 65532)
            {
                return "MEDIUMTEXT";
            }

            return "VARCHAR(" + max + ")";
        }

        public string ObjectType(int? min, int? max) => "TEXT";

        public string BinaryType(int? min, int? max)
        {
            if (!max.HasValue)
            {
                return "BLOB";
            }

            return "VARBINARY(" + max + ")";
        }

        public string XmlType() => "TEXT";

        public string ShortType() => "SMALLINT";

        public string TimeSpanType() => "TIME";

        public string LongType() => "BIGINT";

        public string IntType() => "INT";

        public string GuidType() => "BINARY(16)";

        public string FloatType() => "DOUBLE";

        public string DoubleType() => "DOUBLE";

        public string DecimalType() => "DECIMAL";

        public string DateTimeOffsetType() => "DATETIME";

        public string DateTimeType() => "DATETIME";

        public string BooleanType() => "BIT";
    }
}