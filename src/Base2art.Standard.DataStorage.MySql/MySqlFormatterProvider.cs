﻿namespace Base2art.DataStorage.MySql
{
    using System;
    using DataDefinition;
    using DataStorage.DataManipulation;

    public class MySqlFormatterProvider : IDataDefinerAndManipulatorProvider
    {
        private const string ExceptionName = "MySqlConnector.MySqlException";
        private static readonly IManipulationCommandBuilderFactory manipulatorBuilderFactory;
        private static readonly DefaultStorageTypeMap typeMap;
        private static readonly SharedBuilderMaps sharedBuilderMaps;
        private readonly TimeSpan defaultTimeout;
        private readonly IExecutionEngine engine;

        static MySqlFormatterProvider()
        {
            manipulatorBuilderFactory = new MySqlManipulationCommandBuilderFactory();

            ITypeMap tableTypeMap = new TableTypeMap();
            typeMap = new DefaultStorageTypeMap(tableTypeMap);
            sharedBuilderMaps = new BuilderMaps(manipulatorBuilderFactory, DataManipulatorSupport);
        }

        public MySqlFormatterProvider(IExecutionEngine engine, TimeSpan defaultTimeout)
        {
            this.engine = engine;
            this.defaultTimeout = defaultTimeout;
        }

        private static IDataManipulatorSupport DataManipulatorSupport { get; } = new DataManipulatorSupport
                                                                                 {
                                                                                     MultipleStatementsPerCommand = true,
                                                                                     OverflowHandling = OverflowHandlingStyle.ThrowException,
                                                                                     UnderflowHandling = UnderflowHandlingStyle.None,
                                                                                     NativeExceptionTypeName = ExceptionName,
                                                                                     CrossJoin = false,
                                                                                     TextConcatenationOperator = false
                                                                                 };

        public IDataDefiner DataDefiner => new DataDefinerFormatter(
                                                                    this.engine,
                                                                    null,
                                                                    new DataDefinerSupport
                                                                    {
                                                                        DroppingTables = true,
                                                                        NativeExceptionTypeName = ExceptionName,
                                                                        ConstraintEnforcement = true
                                                                    },
                                                                    DataManipulatorSupport,
                                                                    sharedBuilderMaps,
                                                                    sharedBuilderMaps,
                                                                    sharedBuilderMaps,
                                                                    typeMap,
                                                                    this.defaultTimeout,
                                                                    new MySqlDefinerCommandBuilderFactory(),
                                                                    manipulatorBuilderFactory);

        public IDataManipulator DataManipulator => new DataManipulatorFormatter(
                                                                                this.engine,
                                                                                null,
                                                                                DataManipulatorSupport,
                                                                                sharedBuilderMaps,
                                                                                sharedBuilderMaps,
                                                                                sharedBuilderMaps,
                                                                                this.defaultTimeout,
                                                                                manipulatorBuilderFactory);
    }
}