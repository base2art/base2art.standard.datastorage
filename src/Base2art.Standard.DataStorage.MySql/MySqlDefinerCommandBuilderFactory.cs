﻿namespace Base2art.DataStorage.MySql
{
    using DataDefinition;
    using DataDefinition.Builders;

    public class MySqlDefinerCommandBuilderFactory : IDefinerCommandBuilderFactory
    {
        public ICreateTableBuilder CreateTableBuilder(IStorageTypeMap commonBuilder, IEscapeCharacters escapeCharacters) =>
            new CreateTableBuilder(commonBuilder, escapeCharacters);

        public IDropTableBuilder CreateDropTableBuilder(IEscapeCharacters escapeCharacters) => new DropTableBuilder(escapeCharacters);

        public ICreateTableAltererBuilder CreateTableAltererBuilder(IStorageTypeMap commonBuilder, IEscapeCharacters escapeCharacters) =>
            new CreateTableAltererBuilder(commonBuilder, escapeCharacters);

        public ICreateTableIndexesBuilder CreateTableIndexesBuilder(IStorageTypeMap commonBuilder, IEscapeCharacters escapeCharacters) =>
            new CreateTableIndexesBuilder(commonBuilder, escapeCharacters);

        public ICreateTableKeysBuilder CreateTableKeysBuilder(IStorageTypeMap commonBuilder, IEscapeCharacters escapeCharacters) =>
            new CreateTableKeysBuilder(commonBuilder, escapeCharacters);
    }
}