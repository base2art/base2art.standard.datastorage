namespace Base2art.DataStorage.InMemory
{
    using System;
    using ComponentModel;
    using HolisticPersistence.Specialized;

    public class TransactionalSession : Component, ITransactionalSession
    {
        public TransactionalSession(Guid id) => this.Id = id;

        public Guid Id { get; }
    }
}