﻿namespace Base2art.DataStorage.InMemory
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DataDefinition;
    using DataManipulation;
    using DataManipulation.Data;
    using HolisticPersistence;
    using HolisticPersistence.Specialized;

    public class DataDefinerV2 : StateBasedDataDefiner
    {
        private static readonly Dictionary<string, HashSet<string>> commonSett1 =
            new Dictionary<string, HashSet<string>>(StringComparer.OrdinalIgnoreCase);

        private static readonly Dictionary<string, Dictionary<string, object>> commonSett2 =
            new Dictionary<string, Dictionary<string, object>>(StringComparer.OrdinalIgnoreCase);

        private static readonly Dictionary<string, Dictionary<string, object>> commonTemplates
            = new Dictionary<string, Dictionary<string, object>>(StringComparer.OrdinalIgnoreCase);

        private static readonly Dictionary<string, Dictionary<string, Tuple<bool, RangeConstraint>>> commonConstraints =
            new Dictionary<string, Dictionary<string, Tuple<bool, RangeConstraint>>>(StringComparer.OrdinalIgnoreCase);

        private static readonly Dictionary<string, Dictionary<string, string[]>> commonKeys
            = new Dictionary<string, Dictionary<string, string[]>>(StringComparer.OrdinalIgnoreCase);

        public DataDefinerV2(bool isolate)
            : base(isolate ? new StateManager() : new StateManager(commonSett1, commonSett2, commonTemplates, commonConstraints, commonKeys))
        {
        }


        protected override IDataDefinerSupport GetDefinerSupport() => new DataDefinerSupport
                                                                      {
                                                                          DroppingTables = true,
                                                                          NativeExceptionTypeName = typeof(InvalidOperationException).FullName,
                                                                          ConstraintEnforcement = true
                                                                      };

        protected override IDataManipulatorSupport GetManipulatorSupports() => new DataManipulatorSupport
                                                                               {
                                                                                   MultipleStatementsPerCommand = true,
                                                                                   OverflowHandling = OverflowHandlingStyle.Truncate,
                                                                                   UnderflowHandling = UnderflowHandlingStyle.Pad,
                                                                                   NativeExceptionTypeName = typeof(InvalidOperationException).FullName,
                                                                                   CrossJoin = true,
                                                                                   RightJoin = true,
                                                                                   LeftJoin = true,
                                                                                   InnerJoin = true,
                                                                                   FullJoin = true,
                                                                                   TextConcatenationOperator = true
                                                                               };

        public override object CreateDefault(DataTypes dataType) => dataType.CreateDefault();
    }
}