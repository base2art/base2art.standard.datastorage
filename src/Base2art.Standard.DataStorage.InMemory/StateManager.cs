namespace Base2art.DataStorage.InMemory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DataDefinition;
    using HolisticPersistence;
    using HolisticPersistence.Specialized;

    public class StateManager : IStateManager
    {
        private readonly Dictionary<string, Dictionary<string, Tuple<bool, RangeConstraint>>> constraints =
            new Dictionary<string, Dictionary<string, Tuple<bool, RangeConstraint>>>(StringComparer.OrdinalIgnoreCase);

        private readonly Dictionary<string, Dictionary<string, string[]>> keys
            = new Dictionary<string, Dictionary<string, string[]>>(StringComparer.OrdinalIgnoreCase);

        private readonly Dictionary<string, HashSet<string>> sett1
            = new Dictionary<string, HashSet<string>>(StringComparer.OrdinalIgnoreCase);

        private readonly Dictionary<string, Dictionary<string, object>> sett2
            = new Dictionary<string, Dictionary<string, object>>(StringComparer.OrdinalIgnoreCase);

        private readonly Dictionary<string, Dictionary<string, object>> templates
            = new Dictionary<string, Dictionary<string, object>>(StringComparer.OrdinalIgnoreCase);

        public StateManager()
        {
        }

        public IIntermediateMapper Mapper { get; } = new SimpleIntermediateMapper();

        public ITransactionalSession Session()
        {
            return new TransactionalSession(Guid.NewGuid());
        }

        public StateManager(
            Dictionary<string, HashSet<string>> sett1,
            Dictionary<string, Dictionary<string, object>> sett2,
            Dictionary<string, Dictionary<string, object>> templates,
            Dictionary<string, Dictionary<string, Tuple<bool, RangeConstraint>>> constraints,
            Dictionary<string, Dictionary<string, string[]>> keys)
        {
            this.sett1 = sett1;
            this.sett2 = sett2;
            this.templates = templates;
            this.constraints = constraints;
            this.keys = keys;
        }
        //
        // Task<IEnumerable<(string objectId, IReadOnlyDictionary<string, object>)>> IItemsProducer.GetItems(string tableName)
        //     => this.AllRows(tableName);

        public Task<Dictionary<string, object>> CreateRowFromTemplate(string tableName, Guid sessionId)
            => Task.FromResult(new Dictionary<string, object>(this.templates[tableName], StringComparer.OrdinalIgnoreCase));

        public Task<IReadOnlyDictionary<string, Tuple<bool, RangeConstraint>>> GetConstraint(string tableName, Guid sessionId)
            => Task.FromResult<IReadOnlyDictionary<string, Tuple<bool, RangeConstraint>>>(this.constraints[tableName]);

        public async Task AddItemsToTable(string tableName, Dictionary<string, object>[] rows, Guid sessionId)
        {
            foreach (var row in rows)
            {
                await this.AddItemToTable(tableName, row, sessionId);
            }
        }

        public Task AddItemToTable(string tableName, Dictionary<string, object> dictionary, Guid sessionId)
        {
            var proxyKey = Guid.NewGuid().ToString("N");
            this.sett1[tableName].Add(proxyKey);
            this.sett2.Add(proxyKey, dictionary);
            return Task.CompletedTask;
        }

        public Task<IReadOnlyDictionary<string, string[]>> GetKeys(string tableName, Guid sessionId)
            => Task.FromResult<IReadOnlyDictionary<string, string[]>>(this.keys[tableName]);

        public Task RemoveTable(string tableName, Guid sessionId)
            => Wrap(() =>
            {
                // ADD SETT2
                this.sett1.Remove(tableName);
                this.templates.Remove(tableName);
            });

        public Task<bool> ContainsTable(string tableName, Guid sessionId)
            => Task.FromResult(this.templates.ContainsKey(tableName));

        public Task AddTable(string tableName, Guid sessionId)
            => this.Wrap(() =>
            {
                this.sett1.Add(tableName, new HashSet<string>());
                // ADD SETT2?
                this.keys[tableName] = new Dictionary<string, string[]>();
            });

        public Task AddTemplateAndConstraints(string tableName, IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns, Guid sessionId)
        {
            var template = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var constraint = new Dictionary<string, Tuple<bool, RangeConstraint>>();
            foreach (var col in columns)
            {
                template[col.Item1] = col.Item3 ? col.Item2.CreateDefault() : null;
                constraint[col.Item1] = Tuple.Create(col.Item3, col.Item4);
            }

            this.templates.Add(tableName, template);
            this.constraints.Add(tableName, constraint);
            return Task.CompletedTask;
        }

        public async Task AddOrUpdateTemplateAndConstraints(string tableName, IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns, Guid sessionId)
        {
            foreach (var col in columns)
            {
                await this.AddOrUpdateTemplate(tableName, col.Item1, col.Item3 ? col.Item2.CreateDefault() : null, sessionId);
                await this.AddOrUpdateConstraint(tableName, col.Item1, col.Item3, col.Item4, sessionId);
            }
        }

        public Task AddOrUpdateTemplate(string tableName, string columnName, object defaultObj, Guid sessionId)
            => Wrap(() => this.templates[tableName][columnName] = defaultObj);

        public Task AddOrUpdateConstraint(string tableName, string columnName, bool hasValue, RangeConstraint constraint, Guid sessionId)
            => Wrap(() => this.constraints[tableName][columnName] = Tuple.Create(hasValue, constraint));

        public Task AddOrUpdateKey(string tableName, string columnName, string[] refCols, Guid sessionId)
            => Wrap(() => this.keys[tableName][columnName] = refCols);

        public Task RemoveRecords(string tableName, string[] records, Guid sessionId)
        {
            // var recordsToRemove = new HashSet<IReadOnlyDictionary<string, object>>(records);
            // this.sett[tableName].RemoveAll(recordsToRemove.Contains);

            // ADD SETT2
            var items = this.sett1[tableName];
            foreach (var record in records)
            {
                if (items.Contains(record))
                {
                    items.Remove(record);
                }
            }

            return Task.CompletedTask;
        }

        public Task UpdateItems(string tableName, IEnumerable<(string objectId, Dictionary<string, object> newRow)> itemsToUpdate, Guid sessionId)
        {
            var updatedItems = itemsToUpdate.ToList();

            foreach (var item in updatedItems)
            {
                this.sett2[item.objectId] = item.newRow;
            }

            return Task.CompletedTask;
        }

        public Task<IEnumerable<(string objectId, IReadOnlyDictionary<string, object> data)>> AllRows(string tableName, Guid sessionId)
        {
            var table = this.sett1[tableName];

            return Task.FromResult(table.Select(x => Create(x, this.sett2[x])));
        }

        private (string key, IReadOnlyDictionary<string, object>) Create(string s, IReadOnlyDictionary<string, object> dictionary) => (s, dictionary);

        private Task Wrap(Action action)
        {
            action();
            return Task.CompletedTask;
        }
    }
}