namespace Base2art.DataStorage.InMemory
{
    using System;
    using System.Collections.Generic;

    public static class ObjectTypeFactory
    {
        public static object CreateDefault(this DataTypes dt)
        {
            var lookup = new Dictionary<DataTypes, Func<object>>();

//            lookup[DataTypes.Binary] = () => new byte[0];
            lookup[DataTypes.Boolean] = () => false;
            lookup[DataTypes.DateTime] = () => DateTime.MinValue;
            lookup[DataTypes.DateTimeOffset] = () => DateTimeOffset.MinValue;
            lookup[DataTypes.Decimal] = () => 0.0m;
            lookup[DataTypes.Double] = () => 0D;
            lookup[DataTypes.Float] = () => 0f;
            lookup[DataTypes.Guid] = () => Guid.Empty;
            lookup[DataTypes.Int] = () => 0;
            lookup[DataTypes.Long] = () => 0L;
//            lookup[DataTypes.Object] = () => new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            lookup[DataTypes.Short] = () => (short) 0;
            lookup[DataTypes.String] = () => string.Empty;
            lookup[DataTypes.TimeSpan] = () => TimeSpan.Zero;
//            lookup[DataTypes.Xml] = () =>
//            {
//                throw new NotSupportedException(dt.ToString("G"));
//            };
            if (lookup.ContainsKey(dt))
            {
                return lookup[dt]();
            }

            throw new NotSupportedException(dt.ToString("G"));
        }
    }
}