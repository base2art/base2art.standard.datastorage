﻿namespace Base2art.DataStorage.InMemory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using DataManipulation;
    using DataManipulation.Data;
    using HolisticPersistence;

    public static class SelectHelper
    {
        public static IEnumerable<RecordRowSlice> FromData(
            SelectData selectData,
            Dictionary<string, List<Dictionary<string, object>>> sett)
        {
            var fields = selectData.FieldList ?? new FieldList(null);
            var tableName = selectData.TableName;

            var newMatchesArray = sett[tableName].ToArray();
            return newMatchesArray
                   .Select(record => new RecordRowSlice(tableName, record, new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase)))
                   .ToArray();
        }

        public static IEnumerable<RecordRow> LimitData(IEnumerable<RecordRow> matches, SelectData records)
        {
            if (records.Offset.HasValue)
            {
                matches = matches.Skip(records.Offset.Value);
            }

            if (records.Limit.HasValue)
            {
                matches = matches.Take(records.Limit.Value);
            }

            return matches;
        }

        public static IEnumerable<RecordRow> JoinData(
            IEnumerable<RecordRowSlice> matches,
            Dictionary<string, List<Dictionary<string, object>>> sett,
            SelectData selectData)
        {
            Func<IEnumerable<RecordRowSlice[]>, JoinData, int, IEnumerable<RecordRowSlice[]>> mapper = null;
            mapper = (msi, jd, idx) =>
            {
                Func<RecordRowSlice[], RecordRowSlice, bool> isJoined = (ms, next)
                    => jd.OnData.All(
                                     on =>
                                     {
                                         var key0 = on.Item2.ColumnName;
                                         var key1 = on.Item5.ColumnName;

                                         var reverse = on.Item1 > on.Item4;
                                         var tablePos = reverse ? on.Item4 : on.Item1;

                                         var firstOrDefault = ms.Skip(tablePos).FirstOrDefault();
                                         if (firstOrDefault != null)
                                         {
                                             var last = firstOrDefault.Original;
                                             var nextTable = next.Original;

                                             if (reverse)
                                             {
                                                 if (last.ContainsKey(key1) && nextTable.ContainsKey(key0))
                                                 {
                                                     return Comper.Comp(
                                                                        on.Item3,
                                                                        nextTable[key0],
                                                                        (IComparable) last[key1],
                                                                        on.Item5.ColumnMetaData);
                                                 }
                                             }
                                             else
                                             {
                                                 if (last.ContainsKey(key0) && nextTable.ContainsKey(key1))
                                                 {
                                                     return Comper.Comp(
                                                                        on.Item3,
                                                                        last[key0],
                                                                        (IComparable) nextTable[key1],
                                                                        on.Item5.ColumnMetaData);
                                                 }
                                             }
                                         }

                                         return false;
                                     });

                if (jd == null || jd.SelectData == null)
                {
                    return msi;
                }

                // null, columns

                if (jd.JoinType == JoinType.Aggregate)
                {
                    return msi;
                }

                if (jd.JoinType == JoinType.AggregateGrouped)
                {
                    return msi;
                }

                var records = new List<RecordRowSlice[]>();

                var innerData = FromData(jd.SelectData, sett);

                if (jd.JoinType == JoinType.Cross)
                {
                    foreach (var ms in msi)
                    {
                        foreach (var next in innerData)
                        {
                            records.Add(ms.Union(new[] {next}).ToArray());
                        }
                    }
                }

                if (jd.JoinType == JoinType.Inner)
                {
                    foreach (var ms in msi)
                    {
                        foreach (var next in innerData)
                        {
                            if (isJoined(ms, next))
                            {
                                records.Add(ms.Union(new[] {next}).ToArray());
                            }
                        }
                    }
                }

                if (jd.JoinType == JoinType.Left)
                {
                    foreach (var ms in msi)
                    {
                        var found = false;
                        foreach (var next in innerData)
                        {
                            if (isJoined(ms, next))
                            {
                                found = true;
                                records.Add(ms.Union(new[] {next}).ToArray());
                            }
                        }

                        if (!found)
                        {
                            // <T1, T2>
                            records.Add(ms.Union(new[]
                                                 {
                                                     new RecordRowSlice(
                                                                        jd.SelectData.TableName,
                                                                        new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase),
                                                                        new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase))
                                                 }).ToArray());
                        }
                    }
                }

                if (jd.JoinType == JoinType.Right)
                {
                    foreach (var next in innerData)
                    {
                        var found = false;
                        foreach (var ms in msi)
                        {
                            if (isJoined(ms, next))
                            {
                                found = true;
                                records.Add(ms.Union(new[] {next}).ToArray());
                            }
                        }

                        if (!found)
                        {
                            // <T1, T2>
                            records.Add(new RecordRowSlice[] {null}.Reverse().Skip(1).Reverse().Union(new[] {next}).ToArray());
                        }
                    }
                }

                //                return records;

                return mapper(records, jd.SelectData.JoinData, 0);
            };

            return mapper(matches.Select(x => new[] {x}), selectData.JoinData, 0).Select(x => new RecordRow(x.ToArray()));
        }

        public static IEnumerable<RecordRow> WhereData(IEnumerable<RecordRow> matches, DataDefiner definer, SelectData selectData)
        {
            Func<RecordRow, int, SelectData, bool> matchInner = (r, idx, sd) =>
            {
                foreach (var clause in (sd.WhereClause ?? new WhereClause(null)).GetNames())
                {
                    var slice = r.Skip(idx).FirstOrDefault();
                    var original = slice == null ? new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase) : slice.Original;
                    
                    var data = clause.Item4.SelectData != null
                                   ? HeldValue.Create(definer.SelectInternal(clause.Item4.SelectData, null).Result)
                                   : HeldValue.Create<List<RecordRow>>();
                    
                    if (!Comper.IsMatch(original, clause, data))
                    {
                        return false;
                    }
                }

                return true;
            };

            Func<RecordRow, bool> matcher = r =>
            {
                var i = 0;
                var sd = selectData;
                while (sd != null)
                {
                    if (!matchInner(r, i, sd))
                    {
                        return false;
                    }

                    i += 1;
                    sd = (sd.JoinData ?? new JoinData()).SelectData;
                }

                return true;
            };

            return matches.Where(matcher).ToArray();
        }

        public static IEnumerable<RecordRow> OrderData(IEnumerable<RecordRow> matches, SelectData selectData)
        {
            var sd = selectData;
            var i = 0;

            while (sd != null)
            {
                var orderByClauses = sd.Ordering ?? new OrderingData[0];

                foreach (var clause in orderByClauses.Reverse())
                {
                    if (clause.Direction == ListSortDirection.Ascending)
                    {
                        matches = matches.OrderBy(record =>
                        {
                            if (clause.Column.SpecialColumn.HasValue)
                            {
                                var col = clause.Column.SpecialColumn.GetValueOrDefault();
                                if (col == SpecialColumns.NewUUID)
                                {
                                    return Guid.NewGuid();
                                }

                                if (col == SpecialColumns.Count)
                                {
                                    return record.Items.Length;
                                }

                                throw new InvalidOperationException("Unsupported Order Type: " + col.ToString("G"));
                            }

                            return record.Skip(i).FirstOrDefault().Original[clause.Column.ColumnName];
                        }).ToArray();
                    }
                    else
                    {
                        matches = matches.OrderByDescending(record =>
                                         {
                                             if (clause.Column.SpecialColumn.HasValue)
                                             {
                                                 var col = clause.Column.SpecialColumn.GetValueOrDefault();
                                                 if (col == SpecialColumns.NewUUID)
                                                 {
                                                     return Guid.NewGuid();
                                                 }

                                                 if (col == SpecialColumns.Count)
                                                 {
                                                     return record.Items.Length;
                                                 }

                                                 throw new InvalidOperationException("Unsupported Order Type: " + col.ToString("G"));
                                             }

                                             return record.Skip(i).FirstOrDefault().Original[clause.Column.ColumnName];
                                         })
                                         .ToArray();
                    }
                }

                sd = (sd.JoinData ?? new JoinData()).SelectData;
                i += 1;
            }

            return matches.ToArray();
        }

        public static IEnumerable<RecordRow> GroupData(IEnumerable<RecordRow> matches, SelectData selectData)
        {
            var sd = selectData;
            var i = 0;
            var shouldAddAggregates = false;
            var items = new List<Tuple<LinkedList<object>, RecordRow>>();
            while (sd != null)
            {
                var oldItems = items;

                var groupByClauses = sd.Grouping ?? new Column[0];
                var names = groupByClauses;
                if (names.Length > 0)
                {
                    items = new List<Tuple<LinkedList<object>, RecordRow>>();

                    if (oldItems.Count == 0)
                    {
                        foreach (var record in matches)
                        {
                            var list = new LinkedList<object>();
                            foreach (var column in names)
                            {
                                var key = record.Skip(i).FirstOrDefault().Original[column.ColumnName];
                                list.AddLast(key);

                                shouldAddAggregates = true;
                            }

                            items.Add(Tuple.Create(list, record));
                        }
                    }
                    else
                    {
                        foreach (var oldItem in oldItems)
                        {
                            var list = new LinkedList<object>(oldItem.Item1);

                            var record = oldItem.Item2;

                            foreach (var column in names)
                            {
                                var key = record.Skip(i).FirstOrDefault().Original[column.ColumnName];
                                list.AddLast(key);

                                shouldAddAggregates = true;
                            }

                            items.Add(Tuple.Create(list, record));
                        }
                    }
                }
                else if (items.Count == 0)
                {
                    items = oldItems;
                }

                sd = (sd.JoinData ?? new JoinData()).SelectData;
                i += 1;
            }

            if (!shouldAddAggregates)
            {
                return matches;
            }

            var grouped = items.GroupBy(x => x.Item1, new LinkedListEqualityComparer());

            sd = selectData;
            while (sd != null)
            {
                if (sd.FieldList != null && sd.FieldList.GetNames().Any(x => x.Item1.SpecialColumn.GetValueOrDefault() == SpecialColumns.Count))
                {
                    break;
                }

                sd = (sd.JoinData ?? new JoinData()).SelectData;
                i += 1;
            }

            return grouped.ToArray()
                          .Select(x =>
                          {
                              var record = x.FirstOrDefault().Item2;

                              var original = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
                              var viewable = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

                              foreach (var field in sd.FieldList.GetNames())
                              {
                                  if (field.Item1.SpecialColumn.GetValueOrDefault() == SpecialColumns.Count)
                                  {
                                      original[field.Item1.ColumnName ?? "__count__"] = x.Count();
                                      viewable[field.Item1.ColumnName ?? "__count__"] = x.Count();
                                  }
                              }

                              var aggregates = new RecordRowSlice(sd.TableName, original, viewable);
                              var newRecord = new RecordRow(record.Slices.Union(new[] {aggregates}).ToArray());

                              return newRecord;
                          })
                          .ToArray();
        }

        public static IEnumerable<RecordRow> AggregateData(IEnumerable<RecordRow> matches, SelectData selectData)
        {
            var sd = selectData;
            var i = 0;
            SelectData aggSd = null;

            while (sd != null && aggSd == null)
            {
                var jd = sd.JoinData ?? new JoinData();
                if (jd.JoinType == JoinType.Aggregate)
                {
                    aggSd = jd.SelectData;
                }

                sd = jd.SelectData;
                i += 1;
            }

            if (aggSd == null)
            {
                return matches;
            }

            return matches
                   .Select(x =>
                   {
                       var record = x;

                       var original = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
                       var viewable = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);

                       foreach (var field in sd.FieldList.GetNames())
                       {
                           if (field.Item1.SpecialColumn.GetValueOrDefault() == SpecialColumns.Count)
                           {
                               original[field.Item1.ColumnName ?? "__count__"] = matches.Count();
                               viewable[field.Item1.ColumnName ?? "__count__"] = matches.Count();
                           }
                       }

                       var aggregates = new RecordRowSlice(sd.TableName, original, viewable);
                       var slices = record.Slices.Take(i)
                                          .Union(new[] {aggregates})
                                          .Union(record.Slices.Skip(i))
                                          .ToArray();
                       var newRecord = new RecordRow(slices);

                       return newRecord;
                   })
                   .ToArray();
        }

        public static void ViewData(RecordRow record, int sliceNumber, SelectData selectData)
        {
            RecordRowSlice recordRowSlice = null;

            if (record.Slices == null || record.Slices.Length <= sliceNumber)
            {
                recordRowSlice = new RecordRowSlice(
                                                    selectData.TableName,
                                                    new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase),
                                                    new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase));
            }

            if (recordRowSlice == null)
            {
                recordRowSlice = record.Slices[sliceNumber] ?? new RecordRowSlice(
                                                                                  selectData.TableName,
                                                                                  new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase),
                                                                                  new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase));
            }

            var newData = recordRowSlice.Viewable;

            var fields = selectData.FieldList ?? new FieldList(null);

            var fieldList = fields.GetNames();
            var counter = new MutableStruct<int>();
            if (fieldList.Any(x => x.Item1.SpecialColumn.HasValue && x.Item1.SpecialColumn.Value == SpecialColumns.All))
            {
                foreach (var pair in recordRowSlice.Original)
                {
                    newData[pair.Key] = pair.Value;
                    record.AddObject(pair.Value);
                }
            }

            for (var i = 0; i < fieldList.Length; i++)
            {
                var field = fieldList[i];

                var fieldName = field.Item1.ColumnName;

                if (string.IsNullOrWhiteSpace(fieldName))
                {
                    counter.Value = counter.Value + 1;
                    fieldName = "NoName" + counter.Value;
                }

                if (field.Item1.SpecialColumn.HasValue)
                {
                    if (field.Item1.SpecialColumn.Value == SpecialColumns.NewUUID)
                    {
                        var newColumnData = Guid.NewGuid();
                        newData[fieldName] = newColumnData;
                        record.AddObject(newColumnData);
                    }

                    if (field.Item1.SpecialColumn.Value == SpecialColumns.Count)
                    {
                        var newColumnData = newData["count"];
                        newData[fieldName] = newColumnData;
                        record.AddObject(newColumnData);
                    }
                }
                else
                {
                    var data = record[selectData.TableName].Original[field.Item1.ColumnName];
                    var op = field.Item2;
                    if (op.HasValue)
                    {
                        Expression expr = null;
                        if (op == OperatorType.Subtract)
                        {
                            expr = Expression.Subtract(Expression.Constant(data), Expression.Constant(field.Item3.ColumnData));
                        }

                        if (op == OperatorType.Add)
                        {
                            if (field.Item3.ColumnData is string)
                            {
                                var c = field.Item3.ColumnData.GetType();
                                var concatMethod = typeof(string).GetMethod("Concat", new[]
                                                                                      {
                                                                                          typeof(string),
                                                                                          typeof(string)
                                                                                      });
                                expr = Expression.Add(Expression.Constant(data), Expression.Constant(field.Item3.ColumnData), concatMethod);
                            }
                            else
                            {
                                expr = Expression.Add(Expression.Constant(data), Expression.Constant(field.Item3.ColumnData));
                            }
                        }

                        expr = Expression.Convert(expr, typeof(object));
                        data = Expression.Lambda<Func<object>>(expr).Compile()();
                        newData[field.Item1.ColumnName] = data;
                        record.AddObject(data);
                    }
                    else
                    {
                        newData[field.Item1.ColumnName] = data;
                        record.AddObject(data);
                    }
                }
            }

            if (selectData.JoinData != null && selectData.JoinData.SelectData != null)
            {
                ViewData(record, sliceNumber + 1, selectData.JoinData.SelectData);
            }
        }

        public static void ViewData(IEnumerable<RecordRow> records, SelectData selectData)
        {
            foreach (var record in records)
            {
                ViewData(record, 0, selectData);
            }
        }

        public static IEnumerable<RecordRow> DistinctData(IEnumerable<RecordRow> matches, SelectData data)
        {
            if (data.Distinct)
            {
                return matches.Distinct(new RecordRowComparer());
            }

            return matches;
        }
    }
}