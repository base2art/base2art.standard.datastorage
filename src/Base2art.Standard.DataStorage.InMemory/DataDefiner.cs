﻿namespace Base2art.DataStorage.InMemory
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DataDefinition;
    using DataManipulation;
    using DataManipulation.Data;
    using HolisticPersistence;

    public class DataDefiner : IHolisticDataDefiner, IDataDefinerAndManipulator
    {
        private static readonly Dictionary<string, List<Dictionary<string, object>>> commonSett =
            new Dictionary<string, List<Dictionary<string, object>>>();

        private static readonly Dictionary<string, Dictionary<string, object>> commonTemplates = new Dictionary<string, Dictionary<string, object>>();

        private static readonly Dictionary<string, Dictionary<string, Tuple<bool, RangeConstraint>>> commonConstraints =
            new Dictionary<string, Dictionary<string, Tuple<bool, RangeConstraint>>>();

        private static readonly Dictionary<string, Dictionary<string, string[]>> commonKeys = new Dictionary<string, Dictionary<string, string[]>>();

        private readonly DictionaryComparer comp = new DictionaryComparer();
        private readonly Dictionary<string, Dictionary<string, Tuple<bool, RangeConstraint>>> constraints;
        private readonly Dictionary<string, Dictionary<string, string[]>> keys;

        private readonly Dictionary<string, List<Dictionary<string, object>>> sett;
        private readonly Dictionary<string, Dictionary<string, object>> templates;

        public DataDefiner(bool isolate)
        {
            this.sett = !isolate ? commonSett : new Dictionary<string, List<Dictionary<string, object>>>();
            this.templates = !isolate ? commonTemplates : new Dictionary<string, Dictionary<string, object>>();
            this.constraints = !isolate ? commonConstraints : new Dictionary<string, Dictionary<string, Tuple<bool, RangeConstraint>>>();
            this.keys = !isolate ? commonKeys : new Dictionary<string, Dictionary<string, string[]>>();
        }

        IDataDefinerSupport IDataDefiner.Supports { get; } = new DataDefinerSupport
                                                             {
                                                                 DroppingTables = true,
                                                                 NativeExceptionTypeName = typeof(InvalidOperationException).FullName,
                                                                 ConstraintEnforcement = true
                                                             };

        IDataManipulatorSupport IDataManipulator.Supports { get; } = new DataManipulatorSupport
                                                                     {
                                                                         MultipleStatementsPerCommand = true,
                                                                         OverflowHandling = OverflowHandlingStyle.Truncate,
                                                                         UnderflowHandling = UnderflowHandlingStyle.Pad,
                                                                         NativeExceptionTypeName = typeof(InvalidOperationException).FullName,
                                                                         CrossJoin = true,
                                                                         RightJoin = true,
                                                                         LeftJoin = true,
                                                                         InnerJoin = true,
                                                                         FullJoin = true,
                                                                         TextConcatenationOperator = true
                                                                     };

        public Task CreateTable(
            string schemaName,
            string tableName,
            bool allowUpdate,
            bool ifNotExist,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes)
        {
            return Task.Factory.StartNew(() => this.CreateTableSync(tableName, allowUpdate, ifNotExist, columns, indexes, keyes));
        }

        public Task DropTable(string schemaName, string tableName, bool ifExists)
        {
            return Task.Factory.StartNew(() => this.DropTableSync(tableName, ifExists));
        }

        public Task InsertAsync(string schemaName, string tableName, IReadOnlyList<SetsList> records)
        {
            return Task.Factory.StartNew(
                                         () =>
                                         {
                                             foreach (var record in records)
                                             {
                                                 var dictionary =
                                                     new Dictionary<string, object>(this.templates[tableName], StringComparer.OrdinalIgnoreCase);
                                                 var constraint = this.constraints[tableName];
                                                 foreach (var fieldName in record.GetNames())
                                                 {
                                                     if (dictionary.ContainsKey(fieldName))
                                                     {
                                                         dictionary[fieldName] =
                                                             this.CorrectValue(record.GetFieldData(fieldName), constraint, fieldName);
                                                     }
                                                 }

                                                 this.VerifyKey(tableName, dictionary);
                                                 this.sett[tableName].Add(dictionary);
                                             }
                                         });
        }

        public Task InsertAsync(string schemaName, string tableName, FieldList list, SelectData records)
        {
            return Task.Factory.StartNew(() => this.Insert(schemaName, tableName, list, records));
        }

        public Task<IEnumerable<T>> SelectAsync<T>(SelectData selectData)
        {
            return Task.Factory.StartNew(() => this.Select<T>(selectData));
        }

        public IEnumerable<T> Select<T>(SelectData selectData)
        {
            var dicts = this.SelectInternal(selectData, null).Result;
            return dicts.Select(x => new ComplexRecordRow<T>(this, x.FirstOrDefault()).Data.Item1);
        }

        public Task<IEnumerable<Tuple<T1, T2>>> SelectAsync<T1, T2>(SelectData selectData)
        {
            return Task.Factory.StartNew(() => this.Select<T1, T2>(selectData));
        }

        public IEnumerable<Tuple<T1, T2>> Select<T1, T2>(SelectData selectData)
        {
            var result = this.SelectInternal(selectData, null).Result;
            return result.Select(x => new ComplexRecordRow<T1, T2>(
                                                                   this,
                                                                   x.FirstOrDefault(),
                                                                   x.Skip(1).FirstOrDefault()).Data);
        }

        public Task<IEnumerable<Tuple<T1, T2, T3>>> SelectAsync<T1, T2, T3>(SelectData selectData)
        {
            return Task.Factory.StartNew(() => this.Select<T1, T2, T3>(selectData));
        }

        public IEnumerable<Tuple<T1, T2, T3>> Select<T1, T2, T3>(SelectData selectData)
        {
            var result = this.SelectInternal(selectData, null).Result;
            return result.Select(x => new ComplexRecordRow<T1, T2, T3>(
                                                                       this,
                                                                       x.FirstOrDefault(),
                                                                       x.Skip(1).FirstOrDefault(),
                                                                       x.Skip(2).FirstOrDefault()).Data);
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4>>> SelectAsync<T1, T2, T3, T4>(SelectData selectData)
        {
            return Task.Factory.StartNew(() => this.Select<T1, T2, T3, T4>(selectData));
        }

        public IEnumerable<Tuple<T1, T2, T3, T4>> Select<T1, T2, T3, T4>(SelectData selectData)
        {
            var result = this.SelectInternal(selectData, null).Result;
            return result.Select(x => new ComplexRecordRow<T1, T2, T3, T4>(
                                                                           this,
                                                                           x.FirstOrDefault(),
                                                                           x.Skip(1).FirstOrDefault(),
                                                                           x.Skip(2).FirstOrDefault(),
                                                                           x.Skip(3).FirstOrDefault()).Data);
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5>>> SelectAsync<T1, T2, T3, T4, T5>(SelectData selectData)
        {
            return Task.Factory.StartNew(() => this.Select<T1, T2, T3, T4, T5>(selectData));
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5>> Select<T1, T2, T3, T4, T5>(SelectData selectData)
        {
            var result = this.SelectInternal(selectData, null).Result;
            return result.Select(x => new ComplexRecordRow<T1, T2, T3, T4, T5>(
                                                                               this,
                                                                               x.FirstOrDefault(),
                                                                               x.Skip(1).FirstOrDefault(),
                                                                               x.Skip(2).FirstOrDefault(),
                                                                               x.Skip(3).FirstOrDefault(),
                                                                               x.Skip(4).FirstOrDefault()).Data);
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>>> SelectAsync<T1, T2, T3, T4, T5, T6>(SelectData selectData)
        {
            return Task.Factory.StartNew(() => this.Select<T1, T2, T3, T4, T5, T6>(selectData));
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>> Select<T1, T2, T3, T4, T5, T6>(SelectData selectData)
        {
            var result = this.SelectInternal(selectData, null).Result;
            return result.Select(x => new ComplexRecordRow<T1, T2, T3, T4, T5, T6>(
                                                                                   this,
                                                                                   x.FirstOrDefault(),
                                                                                   x.Skip(1).FirstOrDefault(),
                                                                                   x.Skip(2).FirstOrDefault(),
                                                                                   x.Skip(3).FirstOrDefault(),
                                                                                   x.Skip(4).FirstOrDefault(),
                                                                                   x.Skip(5).FirstOrDefault()).Data);
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>>> SelectAsync<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData)
        {
            return Task.Factory.StartNew(() => this.Select<T1, T2, T3, T4, T5, T6, T7>(selectData));
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>> Select<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData)
        {
            var result = this.SelectInternal(selectData, null).Result;
            return result.Select(x => new ComplexRecordRow<T1, T2, T3, T4, T5, T6, T7>(
                                                                                       this,
                                                                                       x.FirstOrDefault(),
                                                                                       x.Skip(1).FirstOrDefault(),
                                                                                       x.Skip(2).FirstOrDefault(),
                                                                                       x.Skip(3).FirstOrDefault(),
                                                                                       x.Skip(4).FirstOrDefault(),
                                                                                       x.Skip(5).FirstOrDefault(),
                                                                                       x.Skip(6).FirstOrDefault()).Data);
        }

        public Task UpdateAsync(string schemaName, string tableName, SetsList sets, WhereClause wheres)
        {
            return Task.Factory.StartNew(() => this.Update(schemaName, tableName, sets, wheres));
        }

        public Task DeleteAsync(string schemaName, string tableName, WhereClause wheres)
        {
            return Task.Factory.StartNew(() => this.Delete(tableName, wheres));
        }

        private void Insert(string schemaName, string tableName, FieldList insertList, SelectData selectData)
        {
            var data = this.SelectInternal(selectData, null).Result;
            var dictionary = new Dictionary<string, object>(this.templates[tableName], StringComparer.OrdinalIgnoreCase);
            var constraint = this.constraints[tableName];
            if (insertList == null)
            {
                throw new InvalidOperationException("Must specific Insert columns");
            }

            var insertNames = insertList.GetNames();
            foreach (var record in data)
            {
                for (var i = 0; i < insertNames.Length; i++)
                {
                    var field = insertNames[i];
                    var keyName = field.Item1.ColumnName;
                    if (dictionary.ContainsKey(keyName))
                    {
                        dictionary[keyName] = this.CorrectValue(record.Items[i], constraint, keyName);
                    }
                }

                this.VerifyKey(tableName, dictionary);
                var coll = this.sett[tableName];
                coll.Add(dictionary);
            }
        }

        private void Update(string schemaName, string tableName, SetsList sets, WhereClause wheres)
        {
            var whereClauses = wheres ?? new WhereClause(null);
            var matches = Comper.Where(this, this.sett[tableName], whereClauses).Result;
            foreach (var match in matches)
            {
                foreach (var fieldName in sets.GetNames())
                {
                    if (match.ContainsKey(fieldName))
                    {
                        match[fieldName] = sets.GetFieldData(fieldName);
                    }
                }
            }
        }

        private void Delete(string tableName, WhereClause wheres)
        {
            var matches = Comper.Where(this, this.sett[tableName], wheres ?? new WhereClause(null)).Result;
            var recordsToRemove = new HashSet<Dictionary<string, object>>(matches.ToArray());
            this.sett[tableName].RemoveAll(recordsToRemove.Contains);
        }

        public Task<List<RecordRow>> SelectInternal(SelectData selectData, FieldList fieldsWithAugmentation)
        {
            var orderByClauses = selectData.Ordering ?? new OrderingData[0];
            var data = SelectHelper.FromData(selectData, this.sett);
            var records = SelectHelper.JoinData(data, this.sett, selectData);
            records = SelectHelper.WhereData(records, this, selectData);
            records = SelectHelper.OrderData(records, selectData);
            records = SelectHelper.GroupData(records, selectData);
            records = SelectHelper.AggregateData(records, selectData);
            SelectHelper.ViewData(records.ToArray(), selectData);
            records = SelectHelper.DistinctData(records, selectData);
            records = SelectHelper.LimitData(records, selectData);
            return Task.FromResult(records.ToList());
        }

        private void DropTableSync(string tableName, bool ifExists)
        {
            if (this.sett.ContainsKey(tableName) || !ifExists)
            {
                this.sett.Remove(tableName);
            }

            if (this.templates.ContainsKey(tableName) || !ifExists)
            {
                this.templates.Remove(tableName);
            }
        }

        private void CreateTableSync(
            string tableName,
            bool allowUpdate,
            bool ifNotExist,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes)
        {
            if (!allowUpdate)
            {
                if (this.sett.ContainsKey(tableName) && !ifNotExist)
                {
                    throw new InvalidOperationException();
                }

                if (this.templates.ContainsKey(tableName) && !ifNotExist)
                {
                    throw new InvalidOperationException();
                }
            }

            if (!this.sett.ContainsKey(tableName))
            {
                this.sett.Add(tableName, new List<Dictionary<string, object>>());
            }

            if (!this.templates.ContainsKey(tableName))
            {
                var template = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
                var constraint = new Dictionary<string, Tuple<bool, RangeConstraint>>();
                foreach (var col in columns)
                {
                    template[col.Item1] = col.Item3 ? this.CreateDefault(col.Item2) : null;
                    constraint[col.Item1] = Tuple.Create(col.Item3, col.Item4);
                }

                this.templates.Add(tableName, template);
                this.constraints.Add(tableName, constraint);
            }
            else if (allowUpdate)
            {
                var template = this.templates[tableName];
                var constraint = this.constraints[tableName];
                foreach (var col in columns)
                {
                    template[col.Item1] = col.Item3 ? this.CreateDefault(col.Item2) : null;
                    constraint[col.Item1] = Tuple.Create(col.Item3, col.Item4);
                }
            }

            if (!this.keys.ContainsKey(tableName))
            {
                this.keys[tableName] = new Dictionary<string, string[]>();
            }

            var tableKeys = this.keys[tableName];
            foreach (var key in keyes)
            {
                tableKeys[key.Key] = key.ToArray();
            }
        }

        private object CreateDefault(DataTypes dt)
        {
            var lookup = new Dictionary<DataTypes, Func<object>>();

//            lookup[DataTypes.Binary] = () => new byte[0];
            lookup[DataTypes.Boolean] = () => false;
            lookup[DataTypes.DateTime] = () => DateTime.MinValue;
            lookup[DataTypes.DateTimeOffset] = () => DateTimeOffset.MinValue;
            lookup[DataTypes.Decimal] = () => 0.0m;
            lookup[DataTypes.Double] = () => 0D;
            lookup[DataTypes.Float] = () => 0f;
            lookup[DataTypes.Guid] = () => Guid.Empty;
            lookup[DataTypes.Int] = () => 0;
            lookup[DataTypes.Long] = () => 0L;
//            lookup[DataTypes.Object] = () => new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            lookup[DataTypes.Short] = () => (short) 0;
            lookup[DataTypes.String] = () => string.Empty;
            lookup[DataTypes.TimeSpan] = () => TimeSpan.Zero;
//            lookup[DataTypes.Xml] = () =>
//            {
//                throw new NotSupportedException(dt.ToString("G"));
//            };
            if (lookup.ContainsKey(dt))
            {
                return lookup[dt]();
            }

            throw new NotSupportedException(dt.ToString("G"));
        }

        private object CorrectValue(object obj, Dictionary<string, Tuple<bool, RangeConstraint>> constraint, string keyName)
        {
            int? maxValue = null;
            int? minValue = null;
            var forceValue = false;
            if (constraint.ContainsKey(keyName))
            {
                var c1 = constraint[keyName];
                if (c1.Item2 != null)
                {
                    minValue = (int?) c1.Item2.Min;
                    maxValue = (int?) c1.Item2.Max;
                }

                forceValue = c1.Item1;
            }

            if (forceValue && obj == null)
            {
                throw new InvalidOperationException("Must specify a value for column: '" + keyName + "'");
            }

            if (minValue.HasValue || maxValue.HasValue)
            {
                var str = obj as string;
                if (str != null)
                {
                    if (maxValue.HasValue)
                    {
                        str = new string(str.Take(maxValue.Value).ToArray());
                    }

                    if (minValue.HasValue)
                    {
                        str = str.PadRight(minValue.Value);
                    }

                    return str;
                }

                return obj;
            }

            return obj;
        }

        private void VerifyKey(string tableName, Dictionary<string, object> dictionary)
        {
            var tableKeys = this.keys[tableName];
            foreach (var tableKey in tableKeys)
            {
                var key = this.CreateKey(tableName, tableKey.Value, dictionary);
                foreach (var row in this.sett[tableName])
                {
                    var existingKey = this.CreateKey(tableName, tableKey.Value, row);
                    if (this.comp.Equals(existingKey, key))
                    {
                        throw new InMemoryDatabaseException();
                    }
                }
            }
        }

        private Dictionary<string, object> CreateKey(string tableName, string[] columnNames, Dictionary<string, object> dictionary)
        {
            var key = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            foreach (var columnName in columnNames)
            {
                if (dictionary.ContainsKey(columnName))
                {
                    key[columnName] = dictionary[columnName];
                }
            }

            return key;
        }

        private class DataManipulatorSupport : IDataManipulatorSupport
        {
            public bool MultipleStatementsPerCommand { get; set; }

            public OverflowHandlingStyle OverflowHandling { get; set; }

            public UnderflowHandlingStyle UnderflowHandling { get; set; }

            public string NativeExceptionTypeName { get; set; }
            public bool InnerJoin { get; set; }
            public bool RightJoin { get; set; }
            public bool LeftJoin { get; set; }
            public bool FullJoin { get; set; }
            public bool CrossJoin { get; set; }
            public bool TextConcatenationOperator { get; set; }
        }

        private class DataDefinerSupport : IDataDefinerSupport
        {
            public bool DroppingTables { get; set; }

            public string NativeExceptionTypeName { get; set; }

            public bool ConstraintEnforcement { get; set; }
        }
    }
}