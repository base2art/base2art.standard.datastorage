﻿namespace Base2art.DataStorage.DataDefinition
{
    public class DataDefinerSupport : IDataDefinerSupport
    {
        public bool DroppingTables { get; set; }

        public string NativeExceptionTypeName { get; set; }

        public bool ConstraintEnforcement { get; set; }
    }
}