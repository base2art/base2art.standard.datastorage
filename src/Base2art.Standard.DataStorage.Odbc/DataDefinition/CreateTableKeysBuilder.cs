﻿namespace Base2art.DataStorage.DataDefinition
{
    using System.Linq;
    using System.Text;
    using Builders;

    public class CreateTableKeysBuilder : ICreateTableKeysBuilder
    {
        public CreateTableKeysBuilder(IStorageTypeMap convertData, IEscapeCharacters escapeCharacters)
        {
            this.EscapeCharacters = escapeCharacters;
            this.ConvertData = convertData;
        }

        public IStorageTypeMap ConvertData { get; }

        protected IEscapeCharacters EscapeCharacters { get; }

        public virtual string BuildSql(
            string schemaName,
            string typeName,
            ILookup<string, string> keys)
        {
            var sb = new StringBuilder();

            foreach (var key in keys)
            {
                this.AppendItem(sb, schemaName, typeName, key.Key, key);
            }

            return sb.ToString();
        }

        public virtual string BuildSql(
            string schemaName,
            string typeName,
            string key,
            IGrouping<string, string> key2)
        {
            var sb = new StringBuilder();

            this.AppendItem(sb, schemaName, typeName, key, key2);

            return sb.ToString();
        }

        private void AppendItem(StringBuilder sb, string schemaName, string typeName, string key, IGrouping<string, string> key2)
        {
            // ALTER TABLE Persons
            // ADD CONSTRAINT uc_PersonID UNIQUE (P_Id,LastName)
            sb.Append("ALTER TABLE ");

            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }

            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append(typeName);
            sb.AppendLine(this.EscapeCharacters.ClosingEscapeSequence);
            sb.AppendLine("ADD CONSTRAINT ");
            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append("uc_");
            sb.Append(key);
            sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            sb.Append(" UNIQUE (");
            sb.AppendJoined(",", key2.Select(x => x).ToArray(), (x, i) =>
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(x);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            });
            sb.Append(")");
            sb.AppendLine(";");
        }
    }
}