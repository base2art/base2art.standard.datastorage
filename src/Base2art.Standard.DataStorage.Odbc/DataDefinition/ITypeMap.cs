﻿namespace Base2art.DataStorage.DataDefinition
{
    public interface ITypeMap
    {
        string StringType(int? min, int? max);

        string ObjectType(int? min, int? max);

        string BinaryType(int? min, int? max);

        string XmlType();

        string ShortType();

        string TimeSpanType();

        string LongType();

        string IntType();

        string GuidType();

        string FloatType();

        string DoubleType();

        string DecimalType();

        string DateTimeOffsetType();

        string DateTimeType();

        string BooleanType();
    }
}