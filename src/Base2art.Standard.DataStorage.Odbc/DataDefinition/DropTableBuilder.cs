﻿namespace Base2art.DataStorage.DataDefinition
{
    using System.Text;
    using Builders;

    public class DropTableBuilder : IDropTableBuilder
    {
        public DropTableBuilder(IEscapeCharacters escapeCharacters) => this.EscapeCharacters = escapeCharacters;

        protected IEscapeCharacters EscapeCharacters { get; }

        public virtual string BuildSql(
            string schemaName,
            string typeName,
            bool ifExists)
        {
            var sb = new StringBuilder();
            sb.Append("DROP TABLE ");
            if (ifExists)
            {
                sb.Append("IF EXISTS ");
            }

            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }

            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append(typeName);
            sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            sb.AppendLine(";");
            return sb.ToString();
        }
    }
}