﻿namespace Base2art.DataStorage.DataDefinition
{
    using System;
    using System.Collections.Generic;

    public class DefaultStorageTypeMap : IStorageTypeMap
    {
        private readonly IDictionary<DataTypes, Func<RangeConstraint, string>> nonPresizedTypeMap =
            new Dictionary<DataTypes, Func<RangeConstraint, string>>();

        private readonly IDictionary<DataTypes, Func<string>> sizedTypeMap =
            new Dictionary<DataTypes, Func<string>>();

        public DefaultStorageTypeMap(ITypeMap typeMap)
        {
            this.sizedTypeMap[DataTypes.Boolean] = () => typeMap.BooleanType();
            this.sizedTypeMap[DataTypes.DateTime] = () => typeMap.DateTimeType();
            this.sizedTypeMap[DataTypes.DateTimeOffset] = () => typeMap.DateTimeOffsetType();
            this.sizedTypeMap[DataTypes.Decimal] = () => typeMap.DecimalType();
            this.sizedTypeMap[DataTypes.Double] = () => typeMap.DoubleType();
            this.sizedTypeMap[DataTypes.Float] = () => typeMap.FloatType();
            this.sizedTypeMap[DataTypes.Guid] = () => typeMap.GuidType();
            this.sizedTypeMap[DataTypes.Int] = () => typeMap.IntType();
            this.sizedTypeMap[DataTypes.Long] = () => typeMap.LongType();
            this.sizedTypeMap[DataTypes.Short] = () => typeMap.ShortType();
            this.sizedTypeMap[DataTypes.TimeSpan] = () => typeMap.TimeSpanType();
            this.sizedTypeMap[DataTypes.Xml] = () => typeMap.XmlType();

            this.nonPresizedTypeMap[DataTypes.Binary] = x => typeMap.BinaryType((int?) x.Min, (int?) x.Max);
            this.nonPresizedTypeMap[DataTypes.Object] = x => typeMap.ObjectType((int?) x.Min, (int?) x.Max);
            this.nonPresizedTypeMap[DataTypes.String] = x => typeMap.StringType((int?) x.Min, (int?) x.Max);
        }

        public string ColumnType(DataTypes dataType, bool forceValue, RangeConstraint check)
        {
            var type = "";
            if (this.sizedTypeMap.ContainsKey(dataType))
            {
                type = this.sizedTypeMap[dataType]();
            }

            if (this.nonPresizedTypeMap.ContainsKey(dataType))
            {
                type = this.nonPresizedTypeMap[dataType](check);
            }

            if (string.IsNullOrWhiteSpace(type))
            {
                throw new InvalidOperationException("Data Type not supported");
            }

            if (forceValue)
            {
                return string.Concat(type, " NOT NULL");
            }

            return type;
        }
    }
}