﻿namespace Base2art.DataStorage
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    public static class FormatterUtils
    {
        public static void AppendJoined<T>(this StringBuilder sb, string joiner, IReadOnlyList<T> records, Action<T, int> prepper)
        {
            if (records.Count > 0)
            {
                prepper(records[0], 0);
            }

            for (var i = 1; i < records.Count; i++)
            {
                sb.AppendLine(joiner);
                prepper(records[i], i);
            }
        }
    }
}