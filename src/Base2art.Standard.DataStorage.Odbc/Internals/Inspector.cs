﻿namespace Base2art.DataStorage.Internals
{
    using System;
    using System.Linq.Expressions;
    using System.Reflection;

    internal static class Inspector
    {
        public static Inspector<T> Inspect<T>() => new Inspector<T>();
    }

    internal class Inspector<T>
    {
        public MethodInfo GetMethod(Expression<Action<T>> expression)
        {
            var body = (MethodCallExpression) expression.Body;
            return body.Method;
        }

        public MethodInfo GetMethod<TResult>(Expression<Func<T, TResult>> expression)
        {
            var body = (MethodCallExpression) expression.Body;
            return body.Method;
        }
    }
}