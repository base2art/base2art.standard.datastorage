#if NETSTANDARD2_0
using Base2art.Serialization.CodeGeneration;

[assembly: IncludeJsonSerializer("Base2art.DataStorage.Serialization", MakePublic = false)]

#endif