﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Builders;
    using Data;

    public class InsertRecordsBuilder : IInsertRecordsBuilder
    {
        private readonly ISharedBuilderMaps convertData;

        public InsertRecordsBuilder(ISharedBuilderMaps convertData, IEscapeCharacters escapeCharacters)
        {
            this.convertData = convertData;
            this.EscapeCharacters = escapeCharacters;
        }

        protected IEscapeCharacters EscapeCharacters { get; }

        public string BuildSql(
            string schemaName,
            string typeName,
            IReadOnlyList<SetsList> sets,
            string suffix,
            IDictionary<string, object> data)
        {
            sets = sets ?? new SetsList[0];
            var names = this.GetNames(sets, x => x.GetNames());

            if (names.Length == 0 || !sets.Any())
            {
                return null;
            }

            var sb = new StringBuilder();
            sb.Append("INSERT INTO ");

            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }

            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append(typeName);
            sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            sb.AppendLine(" (");
            sb.AppendJoined(", ", names, (x, i) =>
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(x);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            });
            sb.AppendLine(" )");

            sb.AppendLine("VALUES");

            sb.AppendJoined(",", sets, (record, i) =>
            {
                sb.Append("(");

                sb.AppendJoined(", ", names, (x, j) =>
                {
                    var name = string.Format("@{0}_{1}", x, i);
                    sb.Append(name);
                    data.Add(name, this.convertData.ConvertDataForSql(record.GetFieldData(x)));
                });

                sb.Append(")");
            });

            sb.AppendLine(";");
            var sql = sb.ToString();
            return sql;
        }

        private string[] GetNames<Ti>(IEnumerable<Ti> records, Func<Ti, IEnumerable<string>> transform)
        {
            var hashSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
            foreach (var record in records)
            {
                hashSet.UnionWith(transform(record));
            }

            return hashSet.ToArray();
        }
    }
}