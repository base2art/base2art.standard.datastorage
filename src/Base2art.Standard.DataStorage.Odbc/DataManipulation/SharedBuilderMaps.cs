﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml;
    using System.Xml.Linq;
    using Data;
    using Serialization;

    public abstract class SharedBuilderMaps : IDataConverter, IEscapeCharacters, ISharedBuilderMaps
    {
        private static readonly IReadOnlyDictionary<OperatorType, string> NonNullMap;
        private static readonly IReadOnlyDictionary<OperatorType, string> NullMap;

        private static readonly Lazy<IJsonSerializer> serializer = new Lazy<IJsonSerializer>(() => new CamelCasingSimpleJsonSerializer());
        private readonly IManipulationCommandBuilderFactory builderFactory;

        static SharedBuilderMaps()
        {
            var nonNullMap = new Dictionary<OperatorType, string>();
            var nullMap = new Dictionary<OperatorType, string>();

            Action<OperatorType, string> setup = (x, y) =>
            {
                nonNullMap[x] = y;
                nullMap[x] = y;
            };

            setup(OperatorType.Add, "+");
            setup(OperatorType.BitwiseAnd, "&");
            setup(OperatorType.BitwiseOr, "|");
            setup(OperatorType.BooleanAnd, "AND");
            setup(OperatorType.BooleanOr, "OR");
            setup(OperatorType.Divide, "/");
            setup(OperatorType.GreaterThan, ">");
            setup(OperatorType.GreaterThanOrEqual, ">=");
            setup(OperatorType.LessThan, "<");
            setup(OperatorType.LessThanOrEqual, "<=");
            setup(OperatorType.Modulus, "%");
            setup(OperatorType.Multiply, "*");
            setup(OperatorType.Subtract, "-");

            setup(OperatorType.In, "IN");
            setup(OperatorType.NotIn, "NOT IN");

            setup(OperatorType.Like, "LIKE");
            setup(OperatorType.NotLike, "NOT LIKE");

            nonNullMap[OperatorType.IdentityEquality] = "=";
            nonNullMap[OperatorType.IdentityInequality] = "<>";

            nullMap[OperatorType.IdentityEquality] = "IS";
            nullMap[OperatorType.IdentityInequality] = "IS NOT";
            NonNullMap = nonNullMap;
            NullMap = nullMap;
        }

        protected SharedBuilderMaps(IManipulationCommandBuilderFactory builderFactory, IDataManipulatorSupport dataManipulatorSupport)
        {
            this.builderFactory = builderFactory;
            this.Supports = dataManipulatorSupport;
        }

        private IJsonSerializer Serializer => serializer.Value;

//        public virtual bool SuportsSqlConcatAsAddition => true;

        public object ConvertDataFromStorage(object input, Type dataType)
        {
            var map = new Dictionary<Type, Func<object, object>>();
            map[typeof(TimeSpan)] = x => this.MapFromTimeSpan(input);
            map[typeof(TimeSpan?)] = x => this.MapFromNullableTimespan(input);
            map[typeof(Guid)] = x => this.MapFromGuid(input);
            map[typeof(Guid?)] = x => this.MapFromNullableGuid(input);
            map[typeof(bool)] = x => this.MapFromBool(input);
            map[typeof(bool?)] = x => this.MapFromNullableBool(input);
            map[typeof(DateTime)] = x => this.MapFromDateTime(input);
            map[typeof(DateTime?)] = x => this.MapFromNullableDateTime(input);

            map[typeof(float)] = x => this.MapFromFloat(input);
            map[typeof(float?)] = x => this.MapFromNullableFloat(input);

            map[typeof(decimal)] = x => this.MapFromDecimal(input);
            map[typeof(decimal?)] = x => this.MapFromNullableDecimal(input);

            map[typeof(short)] = x => this.MapFromInt16(input);
            map[typeof(short?)] = x => this.MapFromNullableInt16(input);

            map[typeof(int)] = x => this.MapFromInt32(input);
            map[typeof(int?)] = x => this.MapFromNullableInt32(input);

            map[typeof(long)] = x => this.MapFromInt64(input);
            map[typeof(long?)] = x => this.MapFromNullableInt64(input);

            map[typeof(Dictionary<string, object>)] = x => this.MapFromObject(x);
            map[typeof(XmlDocument)] = x => this.MapFromXmlDocument(x);
            map[typeof(XElement)] = x => this.MapFromXElement(x);

            if (map.ContainsKey(dataType))
            {
                return map[dataType](input);
            }

            return input; //base.ConvertDataFromSql(input, dataType);
        }

        public abstract string OpeningEscapeSequence { get; }

        public abstract string ClosingEscapeSequence { get; }

        public virtual string Text { get; } = "'";

        public IDataManipulatorSupport Supports { get; }

        public virtual string TextConcatenationOperator { get; } = "||";

//        public abstract SelectBuilder CreateSelectBuilder();

        public object ConvertDataForSql(object input)
        {
            if (input == null)
            {
                return input;
            }

            var map = new Dictionary<Type, Func<object, object>>();
            map[typeof(TimeSpan)] = x => this.MapTimeSpan((TimeSpan) input);
            map[typeof(TimeSpan?)] = x => this.MapNullableTimeSpan((TimeSpan?) input);
            map[typeof(Guid)] = x => this.MapGuid((Guid) input);
            map[typeof(Guid?)] = x => this.MapNullableGuid((Guid?) input);

            map[typeof(DateTimeOffset)] = x => this.MapDateTimeOffset((DateTimeOffset) input);
            map[typeof(DateTimeOffset?)] = x => this.MapNullableDateTimeOffset((DateTimeOffset?) input);
            map[typeof(DateTime)] = x => this.MapDateTime((DateTime) input);
            map[typeof(DateTime?)] = x => this.MapNullableDateTime((DateTime?) input);
            map[typeof(Dictionary<string, object>)] = x => this.MapObjectToSql((Dictionary<string, object>) input);
            map[typeof(XmlDocument)] = x => this.MapObjectToXmlDocument((XmlDocument) input);
            map[typeof(XElement)] = x => this.MapObjectToXElement((XElement) input);

            if (!map.ContainsKey(input.GetType()))
            {
                return input;
            }

            return map[input.GetType()](input);
        }

        public virtual Tuple<bool, string> ConvertColumn(string tableAlias, Tuple<Column, OperatorType?, Column> y)
        {
            if (y.Item2.HasValue)
            {
                var opString = NonNullMap[y.Item2.Value];
                var left = this.ConvertColumnItem(y.Item1);
                var right = this.ConvertColumnItem(y.Item3);

                if (!this.Supports.TextConcatenationOperator && y.Item2.Value == OperatorType.Add
                                                             && (right.Item3 == typeof(string) || left.Item3 == typeof(string)))
                {
                    var contentForString = string.Concat(
                                                         "CONCAT(",
                                                         left.Item1 ? tableAlias : "",
                                                         string.Format(left.Item3 == typeof(string) ? "'{0}'" : "{0}", left.Item2),
                                                         ", ",
                                                         right.Item1 ? tableAlias : "",
                                                         string.Format(right.Item3 == typeof(string) ? "'{0}'" : "{0}", right.Item2),
                                                         ")");
                    return Tuple.Create(false, contentForString);
                }

                // left.Item1 ? this.OpeningEscapeSequence : "",
                var content1 = string.Concat(
                                             left.Item1 ? tableAlias : "",
                                             string.Format(left.Item3 == typeof(string) ? "'{0}'" : "{0}", left.Item2),
                                             " ",
                                             opString,
                                             " ",
                                             right.Item1 ? tableAlias : "",
                                             string.Format(right.Item3 == typeof(string) ? "'{0}'" : "{0}", right.Item2));
                return Tuple.Create(false, content1);
            }

            var x = y.Item1;
            var z = this.ConvertColumnItem(x);
            return Tuple.Create(z.Item1, z.Item2);
        }

        public string OperatorConversion(OperatorType operatorType) => NonNullMap[operatorType];

        public virtual void AddWhereClause(
            StringBuilder sb,
            WhereClause whereClause,
            IDictionary<string, object> data,
            string defaultSchema,
            string table,
            string prefix,
            string suffix)
        {
            var items = whereClause.GetNames();
            sb.AppendJoined(" AND ", items, (item, i) =>
            {
                var parmDuplicateCount = 0;
                var nameIdentifier = string.Format("@{0}{1}_{2}{3}", prefix, item.Item1, parmDuplicateCount, suffix);

                while (data.ContainsKey(nameIdentifier))
                {
                    parmDuplicateCount += 1;
                    nameIdentifier = string.Format("@{0}{1}_{2}{3}", prefix, item.Item1, parmDuplicateCount, suffix);
                }

                var rawValueColumn = item.Item4;
                var rawValue = rawValueColumn.ColumnData;

                var hasValue = rawValue != null;

                var formattedValue = hasValue ? nameIdentifier : "NULL";

                if (item.Item3 == OperatorType.In || item.Item3 == OperatorType.NotIn)
                {
                    if (item.Item4.SelectData != null)
                    {
                        var builder = this.builderFactory.CreateSelectBuilder(this, this);
                        var content = builder.BuildSql(
                                                       item.Item4.SelectData,
                                                       defaultSchema,
                                                       suffix + "_" + i,
                                                       data,
                                                       false,
                                                       false);
                        // this.GetFieldOperation(item.Item3, hasValue, item.Item4.DataType, item.Item4.DataType)
                        var format = "{0} {1} (SELECT * FROM ({2}) as {3}_{4})";
                        sb.AppendFormat(
                                        format,
                                        this.Aggregate(table, item).Item1,
                                        (hasValue ? NonNullMap : NullMap)[item.Item3],
                                        content,
                                        suffix,
                                        i);
                    }
                    else
                    {
                        var innerList = item.Item4.ColumnData == null
                                            ? new List<object>()
                                            : ((IEnumerable) item.Item4.ColumnData).OfType<object>().ToList();

                        var format = "{0} {1} ({2})";

                        var newKeys = new List<string>();
                        for (var j = 0; j < innerList.Count; j++)
                        {
                            var key = "@" + item.Item1 + "_in_" + j + "_" + suffix;
                            data.Add(key, this.ConvertDataForSql(innerList[j]));
                            newKeys.Add(key);
                        }

                        sb.AppendFormat(
                                        format,
                                        this.Aggregate(table, item).Item1,
                                        (hasValue ? NonNullMap : NullMap)[item.Item3],
                                        string.Join(",", newKeys));
                    }
                }
                else
                {
                    var aggregate = this.Aggregate(table, item);
                    this.Append(sb, hasValue, aggregate, item, formattedValue);

                    if (hasValue)
                    {
                        data.Add(nameIdentifier, this.ConvertDataForSql(rawValue));
                    }
                }
            });
        }

        public abstract string NewUUIDSyntax();

        public abstract string RandomSyntax();

        protected virtual Tuple<bool, string, Type> ConvertColumnItem(Column x)
        {
            if (x.SpecialColumn.HasValue)
            {
                if (x.SpecialColumn == SpecialColumns.All)
                {
                    return Tuple.Create<bool, string, Type>(true, "*", null);
                }

                if (x.SpecialColumn == SpecialColumns.Count)
                {
                    return Tuple.Create(
                                        false,
                                        string.Concat("COUNT(*) as ", this.OpeningEscapeSequence, x.ColumnName, this.ClosingEscapeSequence),
                                        typeof(int));
                }

                if (x.SpecialColumn == SpecialColumns.NewUUID)
                {
                    return Tuple.Create(false, this.NewUUIDSyntax(), typeof(Guid));
                }
            }

            if (x.ColumnData != null)
            {
                return Tuple.Create(false, string.Format(CultureInfo.InvariantCulture, "{0}", x.ColumnData), x.DataType);
            }

            return Tuple.Create<bool, string, Type>(true, string.Concat(this.OpeningEscapeSequence, x.ColumnName, this.ClosingEscapeSequence), null);
        }

        protected virtual void Append(
            StringBuilder sb,
            bool hasValue,
            bool groupFirst,
            string part1,
            OperatorType fieldOperation,
            Type t1,
            Type t2,
            string part2)
        {
            var opString = (!hasValue ? NullMap : NonNullMap)[fieldOperation];

            var format = groupFirst ? "({0}) {1} {2}" : "{0} {1} {2}";
            sb.AppendFormat(
                            format,
                            part1,
                            opString,
                            part2);
        }

        private void Append(
            StringBuilder sb,
            bool hasValue,
            Tuple<string, Type[]> aggregate,
            Tuple<Column, Tuple<OperatorType, Column>[], OperatorType, Column> item,
            string formattedValue)
        {
            var groupFirst = !(item.Item2 == null || item.Item2.Length == 0);
            this.Append(sb, hasValue, groupFirst, aggregate.Item1, item.Item3, item.Item4.DataType, item.Item4.DataType, formattedValue);
        }

        protected virtual Tuple<string, Type[]> Aggregate(
            string tableAlias,
            Tuple<Column, Tuple<OperatorType, Column>[], OperatorType, Column> input)
        {
            Func<string, string> alias = x => string.IsNullOrWhiteSpace(tableAlias)
                                                  ? string.Concat(this.OpeningEscapeSequence, x, this.ClosingEscapeSequence)
                                                  : string.Concat(this.OpeningEscapeSequence, tableAlias, this.ClosingEscapeSequence, ".",
                                                                  this.OpeningEscapeSequence, x, this.ClosingEscapeSequence);

            var tups = input.Item2 ?? new Tuple<OperatorType, Column>[0];

            if (tups.Length == 0)
            {
                return Tuple.Create(alias(input.Item1.ColumnName), new[] {input.Item1.DataType});
            }

            if (tups.Length == 1)
            {
                var sbi = new StringBuilder();

                var tup = tups[0];
                this.Append(
                            sbi,
                            true,
                            false,
                            alias(input.Item1.ColumnName),
                            tup.Item1,
                            input.Item1.DataType,
                            tup.Item2.DataType,
                            alias(tup.Item2.ColumnName));
                return Tuple.Create(sbi.ToString(), new[]
                                                    {
                                                        input.Item1.DataType,
                                                        input.Item4.DataType
                                                    });
            }

            throw new InvalidOperationException("More Than 1 Not currently supported...");
        }

        protected virtual object MapObjectToSql(Dictionary<string, object> value)
        {
            if (value == null)
            {
                return null;
            }

            return this.Serializer.Serialize(value);
        }

        protected virtual object MapObjectToXmlDocument(XmlDocument value)
        {
            if (value == null)
            {
                return null;
            }

            using (var stringWriter = new StringWriter())
            {
                using (var xmlTextWriter = XmlWriter.Create(stringWriter))
                {
                    value.WriteTo(xmlTextWriter);
                    xmlTextWriter.Flush();
                    return stringWriter.GetStringBuilder().ToString();
                }
            }
        }

        protected virtual object MapObjectToXElement(XElement value)
        {
            if (value == null)
            {
                return null;
            }

            return value.ToString(SaveOptions.None);
        }

        protected virtual object MapDateTime(DateTime value) => value;

        protected virtual object MapGuid(Guid value) => value;

        protected virtual object MapTimeSpan(TimeSpan value) => value;

        protected virtual object MapDateTimeOffset(DateTimeOffset value) => value;

        protected virtual TimeSpan MapFromNonNullTimeSpan(object value) => (TimeSpan) value;

        protected virtual DateTime MapFromNonNullDateTime(object value) => (DateTime) value;

        protected virtual Guid MapFromNonNullGuid(object value) => (Guid) value;

        protected virtual bool MapFromNonNullBool(object value) => Convert.ToBoolean(value);

        protected virtual float MapFromNonNullFloat(object value) => Convert.ToSingle(value);

        protected virtual short MapFromNonNullInt16(object value) => Convert.ToInt16(value);

        protected virtual int MapFromNonNullInt32(object value) => Convert.ToInt32(value);

        protected virtual long MapFromNonNullInt64(object value) => Convert.ToInt64(value);

        protected virtual decimal MapFromNonNullDecimal(object value) => Convert.ToDecimal(value);

        protected virtual Dictionary<string, object> MapFromNonNullObject(object value) =>
            this.Serializer.Deserialize<Dictionary<string, object>>((string) value ?? "{}");

        protected virtual XmlDocument MapFromNonNullXmlDocument(object value)
        {
            var doc = new XmlDocument();
            doc.LoadXml((string) value);
            return doc;
        }

        protected virtual XElement MapFromNonNullXElement(object value) => XElement.Parse((string) value);

        private object MapNullableTimeSpan(TimeSpan? value) => value.HasValue ? this.MapTimeSpan(value.Value) : null;

        private object MapNullableGuid(Guid? value) => value.HasValue ? this.MapGuid(value.Value) : null;

        private object MapNullableDateTime(DateTime? value) => value.HasValue ? this.MapDateTime(value.Value) : null;

        private object MapNullableDateTimeOffset(DateTimeOffset? value) => value.HasValue ? this.MapDateTimeOffset(value.Value) : null;

        /*--------- FROM ------------*/

        private TimeSpan? MapFromNullableTimespan(object value) => this.MapFromGenericNull(value, this.MapFromNonNullTimeSpan);

        private TimeSpan MapFromTimeSpan(object value) => this.MapFromGeneric(value, this.MapFromNonNullTimeSpan);

        private DateTime? MapFromNullableDateTime(object value) => this.MapFromGenericNull(value, this.MapFromNonNullDateTime);

        private DateTime MapFromDateTime(object value) => this.MapFromGeneric(value, this.MapFromNonNullDateTime);

        private Guid? MapFromNullableGuid(object value) => this.MapFromGenericNull(value, this.MapFromNonNullGuid);

        private bool? MapFromNullableBool(object value) => this.MapFromGenericNull(value, this.MapFromNonNullBool);

        private float? MapFromNullableFloat(object value) => this.MapFromGenericNull(value, this.MapFromNonNullFloat);

        private short? MapFromNullableInt16(object value) => this.MapFromGenericNull(value, this.MapFromNonNullInt16);

        private int? MapFromNullableInt32(object value) => this.MapFromGenericNull(value, this.MapFromNonNullInt32);

        private long? MapFromNullableInt64(object value) => this.MapFromGenericNull(value, this.MapFromNonNullInt64);

        private decimal? MapFromNullableDecimal(object value) => this.MapFromGenericNull(value, this.MapFromNonNullDecimal);

        private Guid MapFromGuid(object value) => this.MapFromGeneric(value, this.MapFromNonNullGuid);

        private bool MapFromBool(object value) => this.MapFromGeneric(value, this.MapFromNonNullBool);

        private float MapFromFloat(object value) => this.MapFromGeneric(value, this.MapFromNonNullFloat);

        private decimal MapFromDecimal(object value) => this.MapFromGeneric(value, this.MapFromNonNullDecimal);

        private short MapFromInt16(object value) => this.MapFromGeneric(value, this.MapFromNonNullInt16);

        private int MapFromInt32(object value) => this.MapFromGeneric(value, this.MapFromNonNullInt32);

        private long MapFromInt64(object value) => this.MapFromGeneric(value, this.MapFromNonNullInt64);

        private Dictionary<string, object> MapFromObject(object value) => this.MapFromGeneric(value, this.MapFromNonNullObject);

        private XmlDocument MapFromXmlDocument(object value) => this.MapFromGeneric(value, this.MapFromNonNullXmlDocument);

        private XElement MapFromXElement(object value) => this.MapFromGeneric(value, this.MapFromNonNullXElement);

        private T MapFromGeneric<T>(object value, Func<object, T> map)
        {
            if (value != null)
            {
                if (value is T)
                {
                    return (T) value;
                }

                return map(value);
            }

            return default;
        }

        private T? MapFromGenericNull<T>(object value, Func<object, T> map)
            where T : struct
        {
            if (value != null)
            {
                if (value is T)
                {
                    return (T) value;
                }

                return map(value);
            }

            return default;
        }
    }
}