﻿namespace Base2art.DataStorage.DataManipulation
{
    public class DataManipulatorSupport : IDataManipulatorSupport
    {
        public bool MultipleStatementsPerCommand { get; set; }
        public OverflowHandlingStyle OverflowHandling { get; set; }
        public UnderflowHandlingStyle UnderflowHandling { get; set; }
        public string NativeExceptionTypeName { get; set; }
        public bool InnerJoin { get; set; } = true;
        public bool RightJoin { get; set; } = true;
        public bool LeftJoin { get; set; } = true;
        public bool FullJoin { get; set; } = false;
        public bool CrossJoin { get; set; } = true;
        public bool TextConcatenationOperator { get; set; } = true;
    }
}