﻿namespace Base2art.DataStorage.DataManipulation
{
    using System.Collections.Generic;
    using System.Text;
    using Builders;
    using Data;

    public class DeleteBuilder : IDeleteBuilder
    {
        private readonly ISharedBuilderMaps convertData;

        public DeleteBuilder(ISharedBuilderMaps convertData, IEscapeCharacters escapeCharacters)
        {
            this.convertData = convertData;
            this.EscapeCharacters = escapeCharacters;
        }

        protected IEscapeCharacters EscapeCharacters { get; }

        public string BuildSql(
            string defaultSchema,
            string schemaName,
            string typeName,
            WhereClause wheres,
            string suffix,
            IDictionary<string, object> data)
        {
            var sb = new StringBuilder();
            sb.Append("DELETE FROM ");

            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }

            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append(typeName);
            sb.AppendLine(this.EscapeCharacters.ClosingEscapeSequence);

            if (wheres != null)
            {
                sb.AppendLine(" WHERE ");

                this.convertData.AddWhereClause(sb, wheres, data, defaultSchema, string.Empty, "", "_Where");
            }

            sb.AppendLine(";");

            var sql = sb.ToString();

            return sql;
        }
    }
}