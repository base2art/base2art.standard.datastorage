﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Builders;
    using Data;

    public class SelectBuilder : ISelectBuilder
    {
        private readonly ISharedBuilderMaps convertData;

        public SelectBuilder(ISharedBuilderMaps convertData, IEscapeCharacters escapeCharacters)
        {
            this.convertData = convertData;
            this.EscapeCharacters = escapeCharacters;
        }

        protected IEscapeCharacters EscapeCharacters { get; }

        protected virtual string TableLevelNoLockHint => "";

        public string BuildSql(
            SelectData selectData,
            string defaultSchema,
            string suffix,
            IDictionary<string, object> data,
            bool end,
            bool includeMarkers)
        {
            var sb = new StringBuilder();

            this.BuildSql(sb, suffix, data, selectData, defaultSchema, selectData.TableName, 1, end, includeMarkers);
            return sb.ToString();
        }

        protected virtual void BuildSql(
            StringBuilder sb,
            string suffix,
            IDictionary<string, object> data,
            SelectData selectData,
            string defaultSchema,
            string tableName,
            int currentTable,
            bool end,
            bool includeMarkers)
        {
            var schemaName = defaultSchema;
            if (!string.IsNullOrWhiteSpace(selectData.SchemaName))
            {
                schemaName = selectData.SchemaName;
            }

            this.Select(sb);
            this.Distinct(sb, selectData.Distinct);
            this.Fields(sb, selectData, currentTable, false, includeMarkers);
            this.From(sb, schemaName, tableName, currentTable, selectData.WithNoLock);
            this.Joins(sb, selectData, defaultSchema, currentTable, selectData.WithNoLock);
            this.Wheres(sb, selectData, defaultSchema, data, suffix, currentTable, false);
            this.Group(sb, selectData, currentTable);
            this.Order(sb, selectData, currentTable);
            this.LimitOffset(sb, selectData);
            this.End(sb, end);
        }

        protected virtual void Select(StringBuilder sb)
        {
            sb.Append("SELECT ");
        }

        protected virtual void Distinct(StringBuilder sb, bool distinct)
        {
            if (distinct)
            {
                sb.Append("DISTINCT ");
            }
        }

        protected virtual void From(
            StringBuilder sb,
            string schemaName,
            string tableName,
            int currentTable,
            bool withNoLock)
        {
            sb.Append("FROM ");

            // -1
            this.AppendColumn(true, sb, currentTable, schemaName, tableName);
            /*if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }

            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append(tableName);
            sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            sb.Append(" ");
            sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
            sb.Append("t");
            sb.Append(currentTable);
            sb.Append(this.EscapeCharacters.ClosingEscapeSequence);*/

            if (withNoLock)
            {
                sb.Append(" ");
                sb.Append(this.TableLevelNoLockHint);
            }

            sb.AppendLine();
        }

        protected virtual void LimitOffset(StringBuilder sb, SelectData selectData)
        {
            var count = selectData.Limit;
            if (count.HasValue)
            {
                this.Limit(sb, count.Value);
            }

            var offset = selectData.Offset;
            if (offset.HasValue)
            {
                this.Offset(sb, offset.Value);
            }
        }

        protected virtual void Offset(StringBuilder sb, int offset)
        {
            sb.Append("OFFSET ");
            sb.Append(offset);
            sb.AppendLine();
        }

        protected virtual void Limit(StringBuilder sb, int count)
        {
            sb.Append("LIMIT ");
            sb.Append(count);
            sb.AppendLine();
        }

        protected void End(StringBuilder sb, bool end)
        {
            if (end)
            {
                sb.AppendLine(";");
            }
        }

        protected virtual void Fields(StringBuilder sb, SelectData selectData, int tableId, bool hasPreviousField, bool includeMarkers)
        {
            var fields = selectData.FieldList;

            if (fields != null && fields.GetNames().Length != 0)
            {
                sb.AppendLine();
                if (hasPreviousField)
                {
                    sb.Append(",");
                }

                sb.AppendJoined(", ", fields.GetNames(), (x, i) =>
                {
                    var tableAlias = string.Concat("  ", this.EscapeCharacters.OpeningEscapeSequence, "t", tableId,
                                                   this.EscapeCharacters.ClosingEscapeSequence, ".");
                    var item = this.convertData.ConvertColumn(tableAlias, x);
                    if (item.Item1)
                    {
                        sb.Append(tableAlias);
                    }

                    sb.AppendLine(item.Item2);
                    hasPreviousField = true;
                });
                sb.AppendLine();
            }

            if (hasPreviousField)
            {
                if (includeMarkers)
                {
                    sb.Append(", 'MARKER' AS __FIELD__SPLIT");
                    sb.Append(tableId);
                    sb.AppendLine();
                }
            }
            else
            {
                if (includeMarkers)
                {
                    sb.Append("'MARKER' AS __FIELD__SPLIT");
                    sb.Append(tableId);
                    sb.AppendLine();

                    hasPreviousField = true;
                }
            }

            if (selectData.JoinData != null && selectData.JoinData.SelectData != null)
            {
                this.Fields(sb, selectData.JoinData.SelectData, tableId + 1, hasPreviousField, includeMarkers);
            }
        }

        protected virtual void Wheres(
            StringBuilder sb,
            SelectData selectData,
            string defaultSchema,
            IDictionary<string, object> data,
            string suffix,
            int tableId_,
            bool hasPreviousField)
        {
            this.AppendComplexClausesRecursive(
                                               sb,
                                               " WHERE ",
                                               " AND ",
                                               tableId_,
                                               selectData,
                                               s => s.WhereClause,
                                               wheres => wheres != null && wheres.GetNames().Length != 0,
                                               (currentTableId, wheres) =>
                                                   this.convertData.AddWhereClause(sb, wheres, data, defaultSchema, "t" + currentTableId,
                                                                                   "t" + currentTableId + "_", suffix),
                                               hasPreviousField);
        }

        protected virtual void Joins(StringBuilder sb, SelectData selectData, string defaultSchema, int currentTable, bool withNoLock)
        {
            if (selectData == null)
            {
                return;
            }

            var data = selectData.JoinData;
            if (data == null)
            {
                return;
            }

            var joinString = this.Map(data.JoinType);
            if (!string.IsNullOrWhiteSpace(joinString))
            {
                sb.Append(joinString);
                sb.Append(" ");

                var schemaName = defaultSchema;
                if (!string.IsNullOrWhiteSpace(data.SelectData.SchemaName))
                {
                    schemaName = data.SelectData.SchemaName;
                }

                this.AppendColumn(true, sb, currentTable + 1, schemaName, data.SelectData.TableName);

                if (withNoLock)
                {
                    sb.Append(" ");
                    sb.Append(this.TableLevelNoLockHint);
                }

                sb.AppendLine();

                if (data.OnData.Length > 0)
                {
                    sb.Append("  ON");
                }

                for (var i = 0; i < data.OnData.Length; i++)
                {
                    var on = data.OnData[i];
                    if (on.Item2 != null && on.Item5 != null)
                    {
                        if (i > 0)
                        {
                            sb.Append("  AND");
                        }

                        sb.Append(" ");

                        this.AppendColumn(false, sb, on.Item1 + 1, on.Item2);

                        var opString = this.convertData.OperatorConversion(on.Item3);

                        sb.Append(" ");
                        sb.Append(opString);
                        sb.Append(" ");

                        this.AppendColumn(false, sb, on.Item4 + 1, on.Item5);
                        sb.AppendLine();
                    }
                }
            }

            this.Joins(sb, data.SelectData, defaultSchema, currentTable + 1, withNoLock);
        }

        protected virtual void Group(
            StringBuilder sb,
            SelectData selectData,
            int tableId)
        {
            Action<int, Column, int> build = (currentTableId, item, idx) =>
            {
                if (item.SpecialColumn.HasValue)
                {
                    throw new InvalidOperationException("Cannot group on Special Columns");
                }

                this.AppendColumn(false, sb, currentTableId, item);

                /*sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append("t");
                sb.Append(currentTableId);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");

                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(item.ColumnName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);*/
            };

            this.AppendClausesRecursive(sb, "GROUP BY ", ",", tableId, selectData, x => x.Grouping, build);
        }

        protected virtual void Order(StringBuilder sb, SelectData selectData, int tableId)
        {
            Action<int, OrderingData, int> build = (currentTableId, item, idx) =>
            {
                if (item.Column.SpecialColumn.HasValue)
                {
                    var colType = item.Column.SpecialColumn;

                    if (colType == SpecialColumns.Count)
                    {
                        sb.Append("COUNT(*)");
                        sb.Append(" ");
                        sb.Append(item.Direction == ListSortDirection.Ascending ? "ASC" : "DESC");
                    }
                    else if (colType == SpecialColumns.NewUUID)
                    {
                        sb.Append(this.convertData.RandomSyntax());
                    }
                }
                else
                {
                    this.AppendColumn(false, sb, currentTableId, item.Column);
                    /*sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                    sb.Append("t");
                    sb.Append(currentTableId);
                    sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                    sb.Append(".");

                    sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                    sb.Append(item.Column.ColumnName);
                    sb.Append(this.EscapeCharacters.ClosingEscapeSequence);*/

                    sb.Append(" ");
                    sb.Append(item.Direction == ListSortDirection.Ascending ? "ASC" : "DESC");
                }
            };

            this.AppendClausesRecursive(sb, "ORDER BY ", ",", tableId, selectData, x => x.Ordering, build);
        }

        private void AppendComplexClausesRecursive<T>(
            StringBuilder sb,
            string keyword,
            string join,
            int tableId,
            SelectData selectData,
            Func<SelectData, T> lookup,
            Func<T, bool> check,
            Action<int, T> build,
            bool hasPreviousField)
        {
            var groups = lookup(selectData);

            if (check(groups))
            {
                sb.AppendLine();

                if (!hasPreviousField)
                {
                    sb.Append(keyword);
                }
                else
                {
                    sb.Append(join);
                }

                build(tableId, groups);

                sb.AppendLine();
                hasPreviousField = true;
            }

            if (selectData.JoinData != null && selectData.JoinData.SelectData != null)
            {
                this.AppendComplexClausesRecursive(sb, keyword, join, tableId + 1, selectData.JoinData.SelectData, lookup, check, build,
                                                   hasPreviousField);
            }
        }

        private void AppendClausesRecursive<T>(
            StringBuilder sb,
            string keyword,
            string join,
            int tableId,
            SelectData selectData,
            Func<SelectData, T[]> lookup,
            Action<int, T, int> build)
        {
            this.AppendComplexClausesRecursive(
                                               sb,
                                               keyword,
                                               join,
                                               tableId,
                                               selectData,
                                               lookup,
                                               groups => groups != null && groups.Length != 0,
                                               (currentTableId, groups) =>
                                                   sb.AppendJoined(join, groups, (item, idx) => build(currentTableId, item, idx)),
                                               false);
        }

        private string Map(JoinType joinType)
        {
            var map = new Dictionary<JoinType, Func<JoinType, string>>();

            string TryGet(JoinType joinType1, bool supports, string text)
            {
                if (!supports)
                {
                    throw new NotSupportedException("Not Supported: " + joinType1);
                }

                return text;
            }

            map[JoinType.Cross] = x => TryGet(x, this.convertData.Supports.CrossJoin, "CROSS JOIN");
            map[JoinType.Right] = x => TryGet(x, this.convertData.Supports.RightJoin, "RIGHT OUTER JOIN");
            map[JoinType.Full] = x => TryGet(x, this.convertData.Supports.FullJoin, "FULL OUTER JOIN");
            map[JoinType.Inner] = x => TryGet(x, this.convertData.Supports.InnerJoin, "INNER JOIN");
            map[JoinType.Left] = x => TryGet(x, this.convertData.Supports.LeftJoin, "LEFT OUTER JOIN");

            map[JoinType.Aggregate] = x => null;
            map[JoinType.AggregateGrouped] = x => null;

            return map[joinType](joinType);
        }

        private void AppendColumn(bool declaration, StringBuilder sb, int currentTable, string schemaName, string tableName)
        {
            if (!string.IsNullOrWhiteSpace(schemaName))
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(schemaName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
                sb.Append(".");
            }

            this.AppendColumn(declaration, sb, currentTable, tableName, null, null);
        }

        private void AppendColumn(
            bool declaration,
            StringBuilder sb,
            int currentTable,
            string tableName,
            string prefix,
            string suffix)
        {
            if (declaration)
            {
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append(tableName);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);

                sb.Append(" ");
                sb.Append(this.EscapeCharacters.OpeningEscapeSequence);
                sb.Append("t");
                sb.Append(currentTable);
                sb.Append(this.EscapeCharacters.ClosingEscapeSequence);
            }
            else
            {
                var prefixFormatted = "";
                var mainFormatted = "";
                var suffixFormatted = "";

                if (!string.IsNullOrWhiteSpace(prefix))
                {
                    prefixFormatted = $"{this.EscapeCharacters.Text}{prefix}{this.EscapeCharacters.Text}";
                }

                if (!string.IsNullOrWhiteSpace(suffix))
                {
                    suffixFormatted = $"{this.EscapeCharacters.Text}{suffix}{this.EscapeCharacters.Text}";
                }

                var mainFormatted1 = $"{this.EscapeCharacters.OpeningEscapeSequence}t{currentTable}{this.EscapeCharacters.ClosingEscapeSequence}";
                var mainFormatted2 = $"{this.EscapeCharacters.OpeningEscapeSequence}{tableName}{this.EscapeCharacters.ClosingEscapeSequence}";
                mainFormatted = $"{mainFormatted1}.{mainFormatted2}";

                if (this.convertData.Supports.TextConcatenationOperator)
                {
                    if (!string.IsNullOrWhiteSpace(prefixFormatted))
                    {
                        sb.Append(prefixFormatted);
                        sb.Append(" ");
                        sb.Append(this.convertData.TextConcatenationOperator);
                        sb.Append(" ");
                    }

                    sb.Append(mainFormatted);
                    if (!string.IsNullOrWhiteSpace(suffixFormatted))
                    {
                        sb.Append(" ");
                        sb.Append(this.convertData.TextConcatenationOperator);
                        sb.Append(" ");
                        sb.Append(suffixFormatted);
                    }
                }
                else
                {
                    var parts = new List<string>();
                    if (!string.IsNullOrWhiteSpace(prefixFormatted))
                    {
                        parts.Add(prefixFormatted);
                    }

                    parts.Add(mainFormatted);
                    if (!string.IsNullOrWhiteSpace(suffixFormatted))
                    {
                        parts.Add(suffixFormatted);
                    }

                    if (parts.Count > 1)
                    {
                        sb.Append("CONCAT(");
                        sb.Append(string.Join(",", parts));
                        sb.Append(")");
                    }
                    else
                    {
                        sb.Append(parts[0]);
                    }
                }
            }
        }

        private void AppendColumn(bool declaration, StringBuilder sb, int onItem1, Column onItem2)
        {
            this.AppendColumn(
                              declaration,
                              sb,
                              onItem1,
                              onItem2.AsColumnText(),
                              onItem2.ColumnMetaData?.Prefix,
                              onItem2.ColumnMetaData?.Suffix);
        }
    }
}