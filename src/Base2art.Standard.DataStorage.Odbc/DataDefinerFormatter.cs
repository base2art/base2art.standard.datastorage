﻿namespace Base2art.DataStorage
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Threading.Tasks;
    using DataDefinition;
    using DataManipulation;
    using DataManipulation.Data;

    public class DataDefinerFormatter : IDataDefiner
    {
        private readonly IDataConverter dataConverter;

        private readonly string defaultSchema;

        private readonly TimeSpan defaultTimeout;
        private readonly IDefinerCommandBuilderFactory definerCommandBuilderFactory;
        private readonly IEscapeCharacters escapeCharacters;

        private readonly IExecutionEngine factory;
        private readonly IManipulationCommandBuilderFactory manipulatorCommandBuilderFactory;
        private readonly IDataManipulatorSupport manipulatorSupport;
        private readonly ISharedBuilderMaps sharedBuilder;

        private readonly IStorageTypeMap typeMap;

        public DataDefinerFormatter(
            IExecutionEngine factory,
            string defaultSchema,
            IDataDefinerSupport dataDefinerSupport,
            IDataManipulatorSupport manipulatorSupport,
            ISharedBuilderMaps sharedBuilder,
            IEscapeCharacters escapeCharacters,
            IDataConverter dataConverter,
            IStorageTypeMap typeMap,
            TimeSpan defaultTimeout,
            IDefinerCommandBuilderFactory definerCommandBuilderFactory,
            IManipulationCommandBuilderFactory manipulatorCommandBuilderFactory)
        {
            this.defaultTimeout = defaultTimeout;
            this.definerCommandBuilderFactory = definerCommandBuilderFactory;
            this.manipulatorCommandBuilderFactory = manipulatorCommandBuilderFactory;
            this.defaultSchema = defaultSchema;
            this.Supports = dataDefinerSupport;
            this.manipulatorSupport = manipulatorSupport;
            this.typeMap = typeMap;
            this.factory = factory;
            this.sharedBuilder = sharedBuilder;
            this.escapeCharacters = escapeCharacters;
            this.dataConverter = dataConverter;
        }

        protected TimeSpan? Timeout => this.defaultTimeout > TimeSpan.Zero ? (TimeSpan?) this.defaultTimeout : null;

        public IDataDefinerSupport Supports { get; }

        public Task CreateTable(
            string schemaName,
            string tableName,
            bool allowUpdate,
            bool ifNotExists,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> records,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes)
        {
            if (allowUpdate)
            {
                return this.UpdateTable(this.SchemaName(schemaName), tableName, ifNotExists, records, indexes, keyes);
            }

            return this.CreateTableInternal(this.SchemaName(schemaName), tableName, ifNotExists, records, indexes, keyes);
        }

        public Task DropTable(string schemaName, string tableName, bool ifExists)
        {
            return this.RunAsyncActionDefiner(
                                              (x, y) => this.definerCommandBuilderFactory.CreateDropTableBuilder(y),
                                              (builder, data) => builder.BuildSql(this.SchemaName(schemaName), tableName, ifExists));
        }

        public Task<IEnumerable<T>> SelectAsync<T>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema,
                                       (sql, data) => this.factory.QueryReaderAsync<T>(sql, data, this.dataConverter, this.Timeout));
        }

        private Task<J> SelectSqlAsync<J>(SelectData selectData, string defaultSchema, Func<string, Dictionary<string, object>, Task<J>> runner)
            where J : class
        {
            return this.Run(() => this.sharedBuilder,
                            this.manipulatorCommandBuilderFactory.CreateSelectBuilder,
                            (x, data) => x.BuildSql(selectData, defaultSchema, "", data, true, true), runner,
                            () => Task.FromResult(default(J)));
        }

        private Task RunAsyncActionDefiner<Builder>(
            Func<IStorageTypeMap, IEscapeCharacters, Builder> createBuilder,
            Func<Builder, Dictionary<string, object>, string> build)
        {
            return this.Run(() => this.typeMap,
                            createBuilder,
                            build,
                            (s, r) => this.factory.Execute(s, r, this.Timeout),
                            () => Task.FromResult(true));
        }

        private J Run<TInput, Builder, J>(
            Func<TInput> inputProvider,
            Func<TInput, IEscapeCharacters, Builder> createBuilder,
            Func<Builder, Dictionary<string, object>, string> build,
            Func<string, Dictionary<string, object>, J> runner,
            Func<J> defaultCreator)
        {
            var input = inputProvider();
            var builder = createBuilder(input, this.escapeCharacters);
            var data = new Dictionary<string, object>();
            var sql = build(builder, data);
            if (string.IsNullOrWhiteSpace(sql))
            {
                return defaultCreator();
            }

            return runner(sql, data);
        }

        private async Task UpdateTable(
            string schemaName,
            string tableName,
            bool ifNotExists,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> records,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes)
        {
            if (await this.TableExists(schemaName, tableName))
            {
                KeyedCollection<string, Tuple<string, DataTypes, bool, RangeConstraint>> columnsToAdd =
                    new MyKeyedCollection<DataTypes, bool, RangeConstraint>();

                foreach (var columnName in records)
                {
                    var colName = columnName.Item1;
                    if (!await this.TableColumnExists(schemaName, tableName, colName, typeof(string)))
                    {
                        columnsToAdd.Add(columnName);
                    }
                }

                if (columnsToAdd.Count > 0)
                {
                    await this.RunAsyncActionDefiner(
                                                     this.definerCommandBuilderFactory.CreateTableAltererBuilder,
                                                     (builder, data) => builder.BuildSql(schemaName, tableName, ifNotExists, columnsToAdd));
                }

                foreach (var item in indexes)
                {
                    try
                    {
                        await this.RunAsyncActionDefiner(this.definerCommandBuilderFactory.CreateTableIndexesBuilder,
                                                         (builder, data) => builder.BuildSql(schemaName, tableName, item.Key, item));
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType().FullName != this.Supports.NativeExceptionTypeName)
                        {
                            throw;
                        }
                    }
                }

                foreach (var item in keyes)
                {
                    try
                    {
                        await this.RunAsyncActionDefiner(
                                                         this.definerCommandBuilderFactory.CreateTableKeysBuilder,
                                                         (builder, data) => builder.BuildSql(schemaName, tableName, item.Key, item));
                    }
                    catch (Exception ex)
                    {
                        if (ex.GetType().FullName != this.Supports.NativeExceptionTypeName)
                        {
                            throw;
                        }
                    }
                }
            }
            else
            {
                await this.CreateTableInternal(schemaName, tableName, ifNotExists, records, indexes, keyes);
            }
        }

        private async Task<bool> TableExists(string schemaName, string tableName)
        {
            try
            {
                var t = Tuple.Create<Column, OperatorType?, Column>(Column.Custom(SpecialColumns.All), null, null);
                var result = await this.SelectAsync<object>(new SelectData
                                                            {
                                                                FieldList = new FieldList(new[] {t}),
                                                                SchemaName = schemaName,
                                                                TableName = tableName,
                                                                Limit = 1,
                                                                Offset = 0
                                                            });
                return true;
            }
            catch (Exception ex)
            {
                if (ex.GetType().FullName != this.Supports.NativeExceptionTypeName)
                {
                    throw;
                }

                return false;
            }
        }

        private async Task<bool> TableColumnExists(string schemaName, string tableName, string columnName, Type columnType)
        {
            try
            {
                var t = Tuple.Create<Column, OperatorType?, Column>(Column.Simple(tableName, columnName, columnType), null, null);
                var result = await this.SelectAsync<object>(new SelectData
                                                            {
                                                                FieldList = new FieldList(new[] {t}),
                                                                SchemaName = schemaName,
                                                                TableName = tableName,
                                                                Limit = 1,
                                                                Offset = 0
                                                            });
                return true;
            }
            catch (Exception ex)
            {
                if (ex.GetType().FullName != this.Supports.NativeExceptionTypeName)
                {
                    throw;
                }

                return false;
            }
        }

        private async Task CreateTableInternal(
            string schemaName,
            string tableName,
            bool ifNotExists,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> records,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes)
        {
            await this.RunAsyncActionDefiner(
                                             this.definerCommandBuilderFactory.CreateTableBuilder,
                                             (builder, data) => builder.BuildSql(schemaName, tableName, ifNotExists, records));

            if (this.manipulatorSupport.MultipleStatementsPerCommand)
            {
                await this.RunAsyncActionDefiner(
                                                 this.definerCommandBuilderFactory.CreateTableIndexesBuilder,
                                                 (builder, data) => builder.BuildSql(schemaName, tableName, indexes));

                await this.RunAsyncActionDefiner(
                                                 this.definerCommandBuilderFactory.CreateTableKeysBuilder,
                                                 (builder, data) => builder.BuildSql(schemaName, tableName, keyes));
            }
            else
            {
                foreach (var idx in indexes)
                {
                    await this.RunAsyncActionDefiner(
                                                     this.definerCommandBuilderFactory.CreateTableIndexesBuilder,
                                                     (builder, data) => builder.BuildSql(schemaName, tableName, idx.Key, idx));
                }

                foreach (var key in keyes)
                {
                    await this.RunAsyncActionDefiner(
                                                     this.definerCommandBuilderFactory.CreateTableKeysBuilder,
                                                     (builder, data) => builder.BuildSql(schemaName, tableName, key.Key, key));
                }
            }
        }

        private string SchemaName(string schemaName)
        {
            if (string.IsNullOrWhiteSpace(schemaName))
            {
                return this.defaultSchema;
            }

            return schemaName;
        }

        private class MyKeyedCollection<T2, T3, T4> : KeyedCollection<string, Tuple<string, T2, T3, T4>>
        {
            protected override string GetKeyForItem(Tuple<string, T2, T3, T4> item) => item.Item1;
        }
    }
}