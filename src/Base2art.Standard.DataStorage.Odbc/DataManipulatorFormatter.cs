﻿namespace Base2art.DataStorage
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DataManipulation;
    using DataManipulation.Data;

    public class DataManipulatorFormatter : IDataManipulator
    {
        private readonly IDataConverter dataConverter;
        private readonly string defaultSchema;

        private readonly TimeSpan defaultTimeout;
        private readonly IEscapeCharacters escapeCharacters;

        private readonly IExecutionEngine factory;
        private readonly IManipulationCommandBuilderFactory manipulatorCommandBuilderFactory;
        private readonly ISharedBuilderMaps sharedBuilderMaps;

        public DataManipulatorFormatter(
            IExecutionEngine factory,
            string defaultSchema,
            IDataManipulatorSupport manipulatorSupport,
            ISharedBuilderMaps sharedBuilderMaps,
            IEscapeCharacters escapeCharacters,
            IDataConverter dataConverter,
            TimeSpan defaultTimeout,
            IManipulationCommandBuilderFactory manipulatorCommandBuilderFactory)
        {
            this.defaultTimeout = defaultTimeout;
            this.manipulatorCommandBuilderFactory = manipulatorCommandBuilderFactory;
            this.defaultSchema = defaultSchema;
            this.factory = factory;

            this.sharedBuilderMaps = sharedBuilderMaps;
            this.escapeCharacters = escapeCharacters;
            this.dataConverter = dataConverter;
            this.Supports = manipulatorSupport;
        }

        protected TimeSpan? Timeout => this.defaultTimeout > TimeSpan.Zero ? (TimeSpan?) this.defaultTimeout : null;

        public IDataManipulatorSupport Supports { get; }

        public Task InsertAsync(string schemaName, string tableName, FieldList list, SelectData records)
        {
            return this.RunAsyncAction(this.manipulatorCommandBuilderFactory.CreateInsertSelectBuilder,
                                       (x, data) => x.BuildSql(this.defaultSchema, this.SchemaName(schemaName), tableName, list, records, "", data));
        }

        public Task InsertAsync(string schemaName, string tableName, IReadOnlyList<SetsList> records)
        {
            return this.RunAsyncAction(this.manipulatorCommandBuilderFactory.CreateInsertRecordsBuilder,
                                       (x, data) => x.BuildSql(this.SchemaName(schemaName), tableName, records, "", data));
        }

        public Task<IEnumerable<T>> SelectAsync<T>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema,
                                       (sql, data) => this.factory.QueryReaderAsync<T>(sql, data, this.dataConverter, this.Timeout));
        }

        public IEnumerable<T> Select<T>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema,
                                  (sql, data) => this.factory.QueryReader<T>(sql, data, this.dataConverter, this.Timeout));
        }

        public Task<IEnumerable<Tuple<T1, T2>>> SelectAsync<T1, T2>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema,
                                       (sql, data) => this.factory.QueryReaderAsync<T1, T2>(sql, data, this.dataConverter, this.Timeout));
        }

        public IEnumerable<Tuple<T1, T2>> Select<T1, T2>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema,
                                  (sql, data) => this.factory.QueryReader<T1, T2>(sql, data, this.dataConverter, this.Timeout));
        }

        public Task<IEnumerable<Tuple<T1, T2, T3>>> SelectAsync<T1, T2, T3>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema,
                                       (sql, data) => this.factory.QueryReaderAsync<T1, T2, T3>(sql, data, this.dataConverter, this.Timeout));
        }

        public IEnumerable<Tuple<T1, T2, T3>> Select<T1, T2, T3>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema,
                                  (sql, data) => this.factory.QueryReader<T1, T2, T3>(sql, data, this.dataConverter, this.Timeout));
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4>>> SelectAsync<T1, T2, T3, T4>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema,
                                       (sql, data) => this.factory.QueryReaderAsync<T1, T2, T3, T4>(sql, data, this.dataConverter, this.Timeout));
        }

        public IEnumerable<Tuple<T1, T2, T3, T4>> Select<T1, T2, T3, T4>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema,
                                  (sql, data) => this.factory.QueryReader<T1, T2, T3, T4>(sql, data, this.dataConverter, this.Timeout));
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5>>> SelectAsync<T1, T2, T3, T4, T5>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema,
                                       (sql, data) => this.factory.QueryReaderAsync<T1, T2, T3, T4, T5>(sql, data, this.dataConverter, this.Timeout));
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5>> Select<T1, T2, T3, T4, T5>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema,
                                  (sql, data) => this.factory.QueryReader<T1, T2, T3, T4, T5>(sql, data, this.dataConverter, this.Timeout));
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>>> SelectAsync<T1, T2, T3, T4, T5, T6>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema,
                                       (sql, data) =>
                                           this.factory.QueryReaderAsync<T1, T2, T3, T4, T5, T6>(sql, data, this.dataConverter, this.Timeout));
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>> Select<T1, T2, T3, T4, T5, T6>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema,
                                  (sql, data) => this.factory.QueryReader<T1, T2, T3, T4, T5, T6>(sql, data, this.dataConverter, this.Timeout));
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>>> SelectAsync<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData)
        {
            return this.SelectSqlAsync(selectData, this.defaultSchema,
                                       (sql, data) =>
                                           this.factory.QueryReaderAsync<T1, T2, T3, T4, T5, T6, T7>(sql, data, this.dataConverter, this.Timeout));
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>> Select<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData)
        {
            return this.SelectSql(selectData, this.defaultSchema,
                                  (sql, data) => this.factory.QueryReader<T1, T2, T3, T4, T5, T6, T7>(sql, data, this.dataConverter, this.Timeout));
        }

        public Task UpdateAsync(string schemaName, string tableName, SetsList sets, WhereClause wheres)
        {
            return this.RunAsyncAction(this.manipulatorCommandBuilderFactory.CreateUpdateBuilder,
                                       (builder, data) =>
                                           builder.BuildSql(this.defaultSchema, this.SchemaName(schemaName), tableName, sets, wheres, "", data));
        }

        public Task DeleteAsync(string schemaName, string tableName, WhereClause wheres)
        {
            return this.RunAsyncAction(this.manipulatorCommandBuilderFactory.CreateDeleteBuilder,
                                       (builder, data) =>
                                           builder.BuildSql(this.defaultSchema, this.SchemaName(schemaName), tableName, wheres, "", data));
        }

        private J SelectSql<J>(SelectData selectData, string defaultSchema, Func<string, Dictionary<string, object>, J> runner)
            where J : class
        {
            return this.Run(() => this.sharedBuilderMaps,
                            this.manipulatorCommandBuilderFactory.CreateSelectBuilder,
                            (x, data) => x.BuildSql(selectData, defaultSchema, "", data, true, true), runner,
                            () => default);
        }

        private Task<J> SelectSqlAsync<J>(SelectData selectData, string defaultSchema, Func<string, Dictionary<string, object>, Task<J>> runner)
            where J : class
        {
            return this.Run(() => this.sharedBuilderMaps,
                            this.manipulatorCommandBuilderFactory.CreateSelectBuilder,
                            (x, data) => x.BuildSql(selectData, defaultSchema, "", data, true, true), runner,
                            () => Task.FromResult(default(J)));
        }

        private Task RunAsyncAction<Builder>(
            Func<ISharedBuilderMaps, IEscapeCharacters, Builder> createBuilder,
            Func<Builder, Dictionary<string, object>, string> build)
        {
            return this.Run(() => this.sharedBuilderMaps,
                            createBuilder,
                            build,
                            (s, r) => this.factory.Execute(s, r, this.Timeout),
                            () => Task.FromResult(true));
        }

        private J Run<TInput, Builder, J>(
            Func<TInput> inputProvider,
            Func<TInput, IEscapeCharacters, Builder> createBuilder,
            Func<Builder, Dictionary<string, object>, string> build,
            Func<string, Dictionary<string, object>, J> runner,
            Func<J> defaultCreator)
        {
            var input = inputProvider();
            var builder = createBuilder(input, this.escapeCharacters);
            var data = new Dictionary<string, object>();
            var sql = build(builder, data);
            if (string.IsNullOrWhiteSpace(sql))
            {
                return defaultCreator();
            }

            return runner(sql, data);
        }

        private string SchemaName(string schemaName)
        {
            if (string.IsNullOrWhiteSpace(schemaName))
            {
                return this.defaultSchema;
            }

            return schemaName;
        }
    }
}