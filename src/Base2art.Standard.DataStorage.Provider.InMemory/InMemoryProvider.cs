﻿namespace Base2art.DataStorage.Provider.InMemory
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DataDefinition;
    using DataManipulation;
    using DataStorage.InMemory;
    using DynamicDataManipulation;
    using Interception.DataDefinition;
    using Interception.DataManipulation;

    public class InMemoryProvider : DataStorageProviderBase
    {
        public InMemoryProvider(
            string[] definerInterceptorNames,
            string[] manipulatorInterceptorNames,
            IDataDefinerInterceptor[] definerInterceptors,
            IDataManipulatorInterceptor[] manipulatorInterceptors)
            : base(definerInterceptorNames, manipulatorInterceptorNames, definerInterceptors, manipulatorInterceptors)
        {
        }

        public virtual bool Isolate { get; } = false;

        public override IDynamicDataStore CreateDynamicDataStoreAccess(NamedConnectionString named)
            => new DynamicDataStore(new DynamicExecutionEngine());

        protected override IDataDefinerAndManipulatorProvider CreateActualStore(NamedConnectionString named) =>
            new Wrapper(new DataDefinerV2(this.Isolate));

        private class Wrapper : IDataDefinerAndManipulatorProvider
        {
            private readonly DataDefinerV2 dataDefiner;

            public Wrapper(DataDefinerV2 dataDefiner) => this.dataDefiner = dataDefiner;

            public IDataDefiner DataDefiner => this.dataDefiner;

            public IDataManipulator DataManipulator => this.dataDefiner;
        }
    }

    public class DynamicExecutionEngine : IDynamicExecutionEngine
    {
        public bool SupportsDynamicQueries { get; } = false;
        public bool SupportsDynamicProcedureQueries { get; } = false;

        public Task<IEnumerable<TOutput>> ExecuteReader<TOutput>(string sql, object data, bool isProcedure)
            => throw new NotImplementedException();

        public Task<int> Execute(string sql, object data, bool isProcedure)
            => throw new NotImplementedException();
    }
}