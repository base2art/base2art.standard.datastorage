namespace Base2art.DataStorage.HolisticPersistence
{
    using System.Collections.Generic;

    public interface IIntermediateMapper
    {
        T DictToObject<T>(IReadOnlyDictionary<string, object> dict);

        // IReadOnlyDictionary<string, object> Cleanup(PropertyData[] props, IReadOnlyDictionary<string, object> argData);

        object Cleanup(DataTypes headerType, object argData);
        string SerializeAsJson<T>(T o);
    }
}