﻿namespace Base2art.DataStorage.HolisticPersistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class DictionaryComparer : IEqualityComparer<Dictionary<string, object>>
    {
        public bool Equals(Dictionary<string, object> x1, Dictionary<string, object> y1)
        {
            if (x1 == y1)
            {
                return true;
            }

            if (x1 == null || y1 == null)
            {
                return false;
            }

            if (x1.Count != y1.Count)
            {
                return false;
            }

            var valueComparer = EqualityComparer<object>.Default;
            foreach (var kvp in x1)
            {
                object value2;
                if (!y1.TryGetValue(kvp.Key, out value2))
                {
                    return false;
                }

                if (!valueComparer.Equals(kvp.Value, value2))
                {
                    return false;
                }
            }

            return true;
        }

        public int GetHashCode(Dictionary<string, object> obj)
        {
            // Overflow is fine, just wrap
            unchecked
            {
                var hash = 17;
                foreach (var key in obj.Keys.OrderBy(x => x, StringComparer.OrdinalIgnoreCase))
                {
                    hash = hash * 23 + key.GetHashCode();
                    hash = hash * 23 + obj[key].GetHashCode();
                }

                return hash;
            }
        }
    }
}