﻿namespace Base2art.DataStorage.HolisticPersistence
{
    using System.Collections.Generic;

    public class RecordRowSliceComparer : IEqualityComparer<RecordRowSlice>
    {
        private readonly ReadonlyDictionaryComparer comp = new ReadonlyDictionaryComparer();

        public bool Equals(RecordRowSlice x, RecordRowSlice y) => this.comp.Equals(x.Viewable, y.Viewable);

        public int GetHashCode(RecordRowSlice record) => this.comp.GetHashCode(record.Viewable);
    }
}