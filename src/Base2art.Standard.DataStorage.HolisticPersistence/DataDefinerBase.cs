﻿namespace Base2art.DataStorage.HolisticPersistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DataDefinition;
    using DataManipulation;
    using DataManipulation.Data;

    public abstract partial class DataDefinerBase : IDataDefinerAndManipulator
    {
        IDataDefinerSupport IDataDefiner.Supports => GetDefinerSupport();

        IDataManipulatorSupport IDataManipulator.Supports => GetManipulatorSupports();
        protected abstract IIntermediateMapper Mapper { get; }

        protected abstract IDataDefinerSupport GetDefinerSupport();
        protected abstract IDataManipulatorSupport GetManipulatorSupports();

        protected abstract Task InsertAbstractAsync(string tableName, IReadOnlyList<SetsList> records);
        protected abstract Task InsertAbstractAsync(string tableName, FieldList list, SelectData records);

        protected abstract Task<IEnumerable<(string objectId, IReadOnlyDictionary<string, object>)>> GetItems(string tableName);

        protected abstract Task DropTableAsync(string tableName, bool ifExists);

        protected abstract Task CreateTableASync(
            string tableName,
            bool allowUpdate,
            bool ifNotExist,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes);

        // Task<IEnumerable<(string objectId, IReadOnlyDictionary<string, object>)>> IItemsProducer.GetItems(string tableName) 
        //     => this.GetItems(tableName);

        public Task CreateTable(
            string schemaName,
            string tableName,
            bool allowUpdate,
            bool ifNotExist,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes)
            => this.CreateTableASync(tableName, allowUpdate, ifNotExist, columns, indexes, keyes);

        protected abstract Task UpdateAbstractAsync(string tableName, SetsList sets, WhereClause wheres);

        protected abstract Task DeleteAbstractAsync(string tableName, WhereClause wheres);

        public Task DropTable(string schemaName, string tableName, bool ifExists)
            => this.DropTableAsync(tableName, ifExists);

        public Task InsertAsync(string schemaName, string tableName, IReadOnlyList<SetsList> records)
            => this.InsertAbstractAsync(tableName, records);

        public Task InsertAsync(string schemaName, string tableName, FieldList list, SelectData records)
            => this.InsertAbstractAsync(tableName, list, records);

        public Task UpdateAsync(string schemaName, string tableName, SetsList sets, WhereClause wheres)
            => this.UpdateAbstractAsync(tableName, sets, wheres);

        public Task DeleteAsync(string schemaName, string tableName, WhereClause wheres)
            => this.DeleteAbstractAsync(tableName, wheres);
    }
}