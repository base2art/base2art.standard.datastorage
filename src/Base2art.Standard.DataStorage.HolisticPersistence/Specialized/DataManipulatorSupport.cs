namespace Base2art.DataStorage.HolisticPersistence.Specialized
{
    using DataManipulation;

    public class DataManipulatorSupport : IDataManipulatorSupport
    {
        public bool MultipleStatementsPerCommand { get; set; }

        public OverflowHandlingStyle OverflowHandling { get; set; }

        public UnderflowHandlingStyle UnderflowHandling { get; set; }

        public string NativeExceptionTypeName { get; set; }
        public bool InnerJoin { get; set; }
        public bool RightJoin { get; set; }
        public bool LeftJoin { get; set; }
        public bool FullJoin { get; set; }
        public bool CrossJoin { get; set; }
        public bool TextConcatenationOperator { get; set; }
    }
}