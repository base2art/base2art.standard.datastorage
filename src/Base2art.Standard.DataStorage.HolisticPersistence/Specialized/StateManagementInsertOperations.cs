namespace Base2art.DataStorage.HolisticPersistence.Specialized
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DataDefinition;
    using DataManipulation.Data;

    public static class StateManagementInsertOperations
    {
        public static async Task Insert(this IStateManager state, string tableName, IReadOnlyList<SetsList> records)
        {
            using (var session = state.Session())
            {
                var items = new List<Dictionary<string, object>>();
                foreach (var record in records)
                {
                    var dictionary = await state.CreateRowFromTemplate(tableName, session.Id);
                    var constraint = (await state.GetConstraint(tableName, session.Id)).CloneReadable(compIc);
                    foreach (var fieldName in record.GetNames())
                    {
                        if (dictionary.ContainsKey(fieldName))
                        {
                            dictionary[fieldName] = CorrectValue(record.GetFieldData(fieldName), constraint, fieldName);
                        }
                    }

                    await VerifyKey(state, tableName, dictionary);
                    items.Add(dictionary);
                }

                await state.AddItemsToTable(tableName, items.ToArray(), session.Id);
            }
        }

        public static async Task Insert(this IStateManager state, string tableName, FieldList insertList, List<RecordRow> data)
        {
            using (var session = state.Session())
            {
                var dictionary = await state.CreateRowFromTemplate(tableName, session.Id);
                var constraint = await state.GetConstraint(tableName, session.Id);
                if (insertList == null)
                {
                    throw new InvalidOperationException("Must specific Insert columns");
                }

                var insertNames = insertList.GetNames();
                var items = new List<Dictionary<string, object>>();
                foreach (var record in data)
                {
                    for (var i = 0; i < insertNames.Length; i++)
                    {
                        var field = insertNames[i];
                        var keyName = field.Item1.ColumnName;
                        if (dictionary.ContainsKey(keyName))
                        {
                            dictionary[keyName] = CorrectValue(record.Items[i], constraint, keyName);
                        }
                    }

                    await VerifyKey(state, tableName, dictionary);
                    items.Add(dictionary);
                }

                await state.AddItemsToTable(tableName, items.ToArray(), session.Id);
            }
        }

        private static object CorrectValue(object obj, IReadOnlyDictionary<string, Tuple<bool, RangeConstraint>> constraint, string keyName)
        {
            int? maxValue = null;
            int? minValue = null;
            var forceValue = false;
            if (constraint.ContainsKey(keyName))
            {
                var c1 = constraint[keyName];
                if (c1.Item2 != null)
                {
                    minValue = (int?) c1.Item2.Min;
                    maxValue = (int?) c1.Item2.Max;
                }

                forceValue = c1.Item1;
            }

            if (forceValue && obj == null)
            {
                throw new InvalidOperationException("Must specify a value for column: '" + keyName + "'");
            }

            if (minValue.HasValue || maxValue.HasValue)
            {
                var str = obj as string;
                if (str != null)
                {
                    if (maxValue.HasValue)
                    {
                        str = new string(str.Take(maxValue.Value).ToArray());
                    }

                    if (minValue.HasValue)
                    {
                        str = str.PadRight(minValue.Value);
                    }

                    return str;
                }

                return obj;
            }

            return obj;
        }

        private static readonly DictionaryComparer comp = new DictionaryComparer();
        private static readonly StringComparer compIc = StringComparer.OrdinalIgnoreCase;

        private static async Task VerifyKey(IStateManager state, string tableName, Dictionary<string, object> dictionary)
        {
            using (var session = state.Session())
            {
                var tableKeys = await state.GetKeys(tableName, session.Id);
                foreach (var tableKey in tableKeys)
                {
                    var key = CreateKey(tableName, tableKey.Value, dictionary);
                    foreach (var row in await state.AllRows(tableName, session.Id))
                    {
                        var existingKey = CreateKey(tableName, tableKey.Value, row.data);
                        if (comp.Equals(existingKey, key))
                        {
                            throw new UniqueKeyViolationException();
                        }
                    }
                }
            }
        }

        private static Dictionary<string, object> CreateKey(string tableName, string[] columnNames, IReadOnlyDictionary<string, object> dictionary)
        {
            var key = new Dictionary<string, object>(compIc);
            foreach (var columnName in columnNames)
            {
                if (dictionary.ContainsKey(columnName))
                {
                    key[columnName] = dictionary[columnName];
                }
            }

            return key;
        }
    }
}