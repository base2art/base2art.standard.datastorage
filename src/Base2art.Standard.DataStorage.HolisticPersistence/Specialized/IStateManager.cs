namespace Base2art.DataStorage.HolisticPersistence.Specialized
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Threading.Tasks;
    using DataDefinition;

    public interface IStateManager
    {
        Task<Dictionary<string, object>> CreateRowFromTemplate(string tableName, Guid sessionId);

        Task AddItemsToTable(string tableName, Dictionary<string, object>[] items, Guid sessionId);

        Task UpdateItems(string tableName, IEnumerable<(string objectId, Dictionary<string, object> newRow)> itemsToUpdate, Guid sessionId);

        Task<IEnumerable<(string objectId, IReadOnlyDictionary<string, object> data)>> AllRows(string tableName, Guid sessionId);

        Task<IReadOnlyDictionary<string, Tuple<bool, RangeConstraint>>> GetConstraint(string tableName, Guid sessionId);
        Task<IReadOnlyDictionary<string, string[]>> GetKeys(string tableName, Guid sessionId);
        Task RemoveTable(string tableName, Guid sessionId);
        Task<bool> ContainsTable(string tableName, Guid sessionId);
        Task AddTable(string tableName, Guid sessionId);
        Task AddTemplateAndConstraints(string tableName, IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns, Guid sessionId);

        Task AddOrUpdateTemplateAndConstraints(string tableName, IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns, Guid sessionId);

        Task AddOrUpdateKey(string tableName, string columnName, string[] keys, Guid sessionId);

        // Task RemoveRecords(string tableName, IReadOnlyDictionary<string, object>[] records);
        Task RemoveRecords(string tableName, string[] objectIds, Guid sessionId);
        IIntermediateMapper Mapper { get; }
        ITransactionalSession Session();
    }
}