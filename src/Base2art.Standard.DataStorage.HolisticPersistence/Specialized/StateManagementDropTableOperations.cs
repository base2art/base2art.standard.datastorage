namespace Base2art.DataStorage.HolisticPersistence.Specialized
{
    using System.Threading.Tasks;

    public static class StateManagementDropTableOperations
    {
        public static async Task DropTable(this IStateManager state, string tableName, bool ifExists)
        {
            using (var session = state.Session())
            {
                if (await state.ContainsTable(tableName, session.Id) || !ifExists)
                {
                    await state.RemoveTable(tableName, session.Id);
                }
            }
        }
    }
}