namespace Base2art.DataStorage.HolisticPersistence.Specialized
{
    using System;

    public class UniqueKeyViolationException : Exception
    {
        public UniqueKeyViolationException()
        {
        }

        public UniqueKeyViolationException(string message) : base(message)
        {
        }

        public UniqueKeyViolationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /*
        // This constructor is needed for serialization.
        protected UniqueKeyViolation(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
        */
    }
}