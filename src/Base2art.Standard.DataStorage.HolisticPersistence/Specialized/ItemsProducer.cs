namespace Base2art.DataStorage.HolisticPersistence.Specialized
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DataManipulation.Data;

    public static class StateItemsProducer
    {
        public static Task<List<RecordRow>> SelectItems(this IStateManager state, SelectData selectData, Guid sessionId)
        {
            return SelectItems(state, selectData, new SimpleSelectHelper(), sessionId);
        }

        public static async Task<List<RecordRow>> SelectItems(this IStateManager state, SelectData selectData, ISelectHelper selectHelper, Guid sessionId)
        {
            var data = await selectHelper.FromData(selectData, tableName => state.AllRows(tableName, sessionId));
            var records = await selectHelper.JoinData(data, tableName => state.AllRows(tableName, sessionId), selectData);
            records = selectHelper.WhereData(records, state, selectData, sessionId);
            records = selectHelper.OrderData(records, selectData);
            records = selectHelper.GroupData(records, selectData);
            records = selectHelper.AggregateData(records, selectData);
            selectHelper.ViewData(records.ToArray(), selectData);
            records = selectHelper.DistinctData(records, selectData);
            records = selectHelper.LimitData(records, selectData);
            return records.ToList();
        }
    }
}

// }
// public static class ItemsProducer
// {
//     public static Task<List<RecordRow>> SelectItems(this IStateManager state, SelectData selectData, Guid sessionId)
//     {
//         return SelectItems(state, selectData, new SimpleSelectHelper());
//     }
//
//     public static async Task<List<RecordRow>> SelectItems(this IStateManager state, SelectData selectData, ISelectHelper selectHelper, Guid sessionId)
//     {
//         
//         var data = await selectHelper.FromData(selectData, state.AllRows());
//         var records = await selectHelper.JoinData(data, producer.GetItems, selectData);
//         records = selectHelper.WhereData(records, producer, selectData);
//         records = selectHelper.OrderData(records, selectData);
//         records = selectHelper.GroupData(records, selectData);
//         records = selectHelper.AggregateData(records, selectData);
//         selectHelper.ViewData(records.ToArray(), selectData);
//         records = selectHelper.DistinctData(records, selectData);
//         records = selectHelper.LimitData(records, selectData);
//         return records.ToList();
//     }
// }
// }