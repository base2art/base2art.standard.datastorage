namespace Base2art.DataStorage.HolisticPersistence.Specialized
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DataManipulation.Data;

    public static class StateManagementUpdateOperations
    {
        public static async Task Update(this IStateManager state, string tableName, SetsList sets, WhereClause wheres)
        {
            using (var session = state.Session())
            {
                var whereClauses = wheres ?? new WhereClause(null);

                var allRows = await state.AllRows(tableName, session.Id);
                var matches = await Comper.Where(state, allRows, whereClauses, session.Id);

                var itemsToUpdate = new List<(string, Dictionary<string, object> newRow)>();

                foreach (var match in matches)
                {
                    Dictionary<string, object> newItem = null;
                    foreach (var fieldName in sets.GetNames())
                    {
                        if (match.Item2.ContainsKey(fieldName))
                        {
                            if (newItem == null)
                            {
                                newItem = match.Item2.CloneReadable(StringComparer.OrdinalIgnoreCase);
                            }

                            newItem[fieldName] = sets.GetFieldData(fieldName);
                        }
                    }

                    if (newItem != null)
                    {
                        itemsToUpdate.Add((match.objectId, newItem));
                    }
                }

                await state.UpdateItems(tableName, itemsToUpdate, session.Id);
            }
        }
    }
}