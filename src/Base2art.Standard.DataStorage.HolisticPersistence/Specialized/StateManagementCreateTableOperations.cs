namespace Base2art.DataStorage.HolisticPersistence.Specialized
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DataDefinition;

    public static class StateManagementCreateTableOperations
    {
        public static async Task CreateTable(
            this IStateManager state,
            string tableName,
            bool allowUpdate,
            bool ifNotExist,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes)
        {
            using (var session = state.Session())
            {
                var containsTable = await state.ContainsTable(tableName, session.Id);
                if (!allowUpdate)
                {
                    if (containsTable && !ifNotExist)
                    {
                        throw new InvalidOperationException();
                    }
                }

                if (!containsTable)
                {
                    await state.AddTable(tableName, session.Id);
                    await state.AddTemplateAndConstraints(tableName, columns, session.Id);
                }

                if (containsTable && allowUpdate)
                {
                    // var template = state.GetTemplate(tableName);
                    // var constraint = state.GetConstraint(tableName);
                    
                    await state.AddOrUpdateTemplateAndConstraints(tableName, columns, session.Id);
                }

                foreach (var key in keyes)
                {
                    await state.AddOrUpdateKey(tableName, key.Key, key.ToArray(), session.Id);
                }
            }
        }
    }
}