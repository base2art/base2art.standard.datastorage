namespace Base2art.DataStorage.HolisticPersistence.Specialized
{
    using DataDefinition;

    public class DataDefinerSupport : IDataDefinerSupport
    {
        public bool DroppingTables { get; set; }

        public string NativeExceptionTypeName { get; set; }

        public bool ConstraintEnforcement { get; set; }
    }
}