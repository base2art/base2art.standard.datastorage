namespace Base2art.DataStorage.HolisticPersistence.Specialized
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DataDefinition;
    using DataManipulation.Data;
    using HolisticPersistence;

    public abstract class StateBasedDataDefiner : DataDefinerBase
    {
        protected IStateManager State { get; }

        protected StateBasedDataDefiner(IStateManager state)
            => this.State = state;

        public abstract object CreateDefault(DataTypes dataType);

        protected sealed override IIntermediateMapper Mapper => this.State.Mapper;

        public override async Task<List<RecordRow>> SelectItems(SelectData selectData)
        {
            using (var session = this.State.Session())
            {
                return await this.State.SelectItems(selectData, session.Id);
            }
        }

        protected override async Task InsertAbstractAsync(string tableName, IReadOnlyList<SetsList> records)
        {
            await this.State.Insert(tableName, records);
        }

        protected override async Task InsertAbstractAsync(string tableName, FieldList insertList, SelectData selectData)
        {
            var data = await this.SelectItems(selectData);
            await this.State.Insert(tableName, insertList, data);
        }

        protected override async Task<IEnumerable<(string objectId, IReadOnlyDictionary<string, object>)>> GetItems(string tableName)
        {
            using (var session = this.State.Session())
            {
                var valueTuples = await this.State.AllRows(tableName, session.Id);
                return valueTuples;
            }
        }

        protected override Task DropTableAsync(string tableName, bool ifExists)
            => this.State.DropTable(tableName, ifExists);

        protected override Task CreateTableASync(
            string tableName,
            bool allowUpdate,
            bool ifNotExist,
            IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> columns,
            ILookup<string, string> indexes,
            ILookup<string, string> keyes)
            => this.State.CreateTable(tableName, allowUpdate, ifNotExist, columns, indexes, keyes);

        protected override Task UpdateAbstractAsync(string tableName, SetsList sets, WhereClause wheres)
            => this.State.Update(tableName, sets, wheres);

        protected override async Task DeleteAbstractAsync(string tableName, WhereClause wheres)
        {
            using (var session = this.State.Session())
            {
                var matches = await Comper.Where(
                                                 this.State,
                                                 await this.State.AllRows(tableName, session.Id),
                                                 wheres ?? new WhereClause(null),
                                                 session.Id);

                await this.State.RemoveRecords(tableName, matches.Select(x => x.objectId).ToArray(), session.Id);
            }
        }
    }
}