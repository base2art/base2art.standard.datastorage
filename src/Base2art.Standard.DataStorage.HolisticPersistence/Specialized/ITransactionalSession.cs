namespace Base2art.DataStorage.HolisticPersistence.Specialized
{
    using System;

    public interface ITransactionalSession : IDisposable
    {
        Guid Id { get; }
    }
}