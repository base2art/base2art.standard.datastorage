﻿namespace Base2art.DataStorage.HolisticPersistence.Specialized
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DataManipulation;
    using DataManipulation.Data;

    public static class Comper
    {
        public static Dictionary<TKey, TValue> CloneReadable<TKey, TValue>(this IReadOnlyDictionary<TKey, TValue> item, IEqualityComparer<TKey> comparer)
        {
            var returnValue = new Dictionary<TKey, TValue>(comparer);

            foreach (var pair in item)
            {
                returnValue[pair.Key] = pair.Value;
            }

            return returnValue;
        }

        public static async Task<IEnumerable<(string objectId, IReadOnlyDictionary<string, object>)>> Where(
            IStateManager state,
            IEnumerable<(string objectId, IReadOnlyDictionary<string, object>)> matches,
            WhereClause whereClauses,
            Guid sessionId)
        {
            foreach (var clause in whereClauses.GetNames())
            {
                var data = clause.Item4.SelectData != null
                               ? HeldValue.Create(await state.SelectItems(clause.Item4.SelectData, sessionId))
                               : HeldValue.Create<List<RecordRow>>();
                matches = matches.Where(record => IsMatch(record.Item2, clause, data));
            }

            return matches;
        }

        public static bool IsMatch(IReadOnlyDictionary<string, object> record,
                                   Tuple<Column, Tuple<OperatorType, Column>[], OperatorType, Column> clause,
                                   IHeldValue<List<RecordRow>> data)
        {
            var op = clause.Item3;
            if (op == OperatorType.In)
            {
                IEnumerable<object> innerList = null;
                if (data.HasValue)
                {
                    innerList = data.Value.Select(x => x.Select(q =>
                                    {
                                        var z = q.Viewable;
                                        var y = z.Keys.FirstOrDefault();
                                        return y != null && z.ContainsKey(y) ? z[y] : null;
                                    }))
                                    .Where(x => x != null)
                                    .Select(x => x.FirstOrDefault());
                }
                else
                {
                    innerList = clause.Item4.ColumnData == null
                                    ? new List<object>()
                                    : ((IEnumerable) clause.Item4.ColumnData).OfType<object>();
                }

                var isIn = innerList.Any(x =>
                {
                    if (!record.ContainsKey(clause.Item1.ColumnName))
                    {
                        return false;
                    }

                    var extra = record[clause.Item1.ColumnName];
                    var y = x as IComparable;

                    if (y != null)
                    {
                        return y.CompareTo(extra) == 0;
                    }

                    return x == extra;
                });
                return isIn;
            }

            if (op == OperatorType.NotIn)
            {
                IEnumerable<object> innerList = null;
                if (data.HasValue)
                {
                    innerList = data.Value.Select(x => x.Select(q =>
                                    {
                                        var z = q.Viewable;
                                        var y = z.Keys.FirstOrDefault();
                                        return y != null && z.ContainsKey(y) ? z[y] : null;
                                    }))
                                    .Where(x => x != null)
                                    .Select(x => x.FirstOrDefault());
                }
                else
                {
                    innerList = clause.Item4.ColumnData == null
                                    ? new List<object>()
                                    : ((IEnumerable) clause.Item4.ColumnData).OfType<object>();
                }

                var isNotIn = !innerList.Any(x =>
                {
                    if (!record.ContainsKey(clause.Item1.ColumnName))
                    {
                        return false;
                    }

                    var extra = record[clause.Item1.ColumnName];
                    var y = x as IComparable;

                    if (y != null)
                    {
                        return y.CompareTo(extra) == 0;
                    }

                    return x == extra;
                });
                return isNotIn;
            }

            var isIn1 = Comp(
                             op,
                             record.ContainsKey(clause.Item1.ColumnName) ? record[clause.Item1.ColumnName] : null,
                             (IComparable) clause.Item4.ColumnData,
                             clause.Item4.ColumnMetaData);
            return isIn1;
        }

        public static bool Comp(OperatorType op, object obj, IComparable columnData, ColumnMetaData columnMetaData)
        {
            if (op == OperatorType.IdentityEquality)
            {
                if (obj == null || columnData == null)
                {
                    return columnData == obj;
                }

                return columnData.CompareTo(Convert.ChangeType(obj, columnData.GetType())) == 0;
            }

            if (op == OperatorType.IdentityInequality)
            {
                if (obj == null || columnData == null)
                {
                    return columnData != obj;
                }

                return columnData.CompareTo(obj) != 0;
            }

            if (columnData == null)
            {
                return false;
            }

            var comp = columnData.CompareTo(Convert.ChangeType(obj, columnData.GetType()));

            if (op == OperatorType.GreaterThan)
            {
                return comp < 0;
            }

            if (op == OperatorType.GreaterThanOrEqual)
            {
                return comp <= 0;
            }

            if (op == OperatorType.LessThan)
            {
                return comp > 0;
            }

            if (op == OperatorType.LessThanOrEqual)
            {
                return comp >= 0;
            }

            if (op == OperatorType.Like)
            {
                var objStr = obj as string;
                if (objStr == null)
                {
                    return false;
                }

                var data = (string) columnData;

                if (!string.IsNullOrWhiteSpace(columnMetaData.Prefix))
                {
                    data = columnMetaData.Prefix + data;
                }

                if (!string.IsNullOrWhiteSpace(columnMetaData.Suffix))
                {
                    data += columnMetaData.Suffix;
                }

                return data.SqlLike(objStr);
            }

            if (op == OperatorType.NotLike)
            {
                var objStr = obj as string;
                if (objStr == null)
                {
                    return false;
                }

                var data = (string) columnData;

                if (!string.IsNullOrWhiteSpace(columnMetaData.Prefix))
                {
                    data = columnMetaData.Prefix + data;
                }

                if (!string.IsNullOrWhiteSpace(columnMetaData.Suffix))
                {
                    data += columnMetaData.Suffix;
                }

                return !data.SqlLike(objStr);
            }

            throw new NotImplementedException();
        }
    }
}