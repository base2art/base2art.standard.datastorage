namespace Base2art.DataStorage.HolisticPersistence.Specialized
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DataManipulation.Data;

    public interface ISelectHelper
    {
        Task<IEnumerable<RecordRowSlice>> FromData(
            SelectData selectData,
            Func<string, Task<IEnumerable<(string objectId, IReadOnlyDictionary<string, object>)>>> sett);

        IEnumerable<RecordRow> LimitData(IEnumerable<RecordRow> matches, SelectData records);

        Task<IEnumerable<RecordRow>> JoinData(
            IEnumerable<RecordRowSlice> matches,
            Func<string, Task<IEnumerable<(string objectId, IReadOnlyDictionary<string, object>)>>> sett,
            SelectData selectData);

        IEnumerable<RecordRow> WhereData(IEnumerable<RecordRow> matches, IStateManager state, SelectData selectData, Guid sessionId);

        IEnumerable<RecordRow> OrderData(IEnumerable<RecordRow> matches, SelectData selectData);

        IEnumerable<RecordRow> GroupData(IEnumerable<RecordRow> matches, SelectData selectData);

        IEnumerable<RecordRow> AggregateData(IEnumerable<RecordRow> matches, SelectData selectData);

        // void ViewData(RecordRow record, int sliceNumber, SelectData selectData);

        void ViewData(IEnumerable<RecordRow> records, SelectData selectData);

        IEnumerable<RecordRow> DistinctData(IEnumerable<RecordRow> matches, SelectData data);
    }
}