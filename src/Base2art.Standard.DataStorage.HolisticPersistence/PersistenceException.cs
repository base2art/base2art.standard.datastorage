﻿namespace Base2art.DataStorage.HolisticPersistence
{
    using System;

    public class PersistenceException : Exception //, ISerializable
    {
        public PersistenceException()
        {
        }

        public PersistenceException(string message) : base(message)
        {
        }

        public PersistenceException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /*
        // This constructor is needed for serialization.
        protected InMemoryDatabaseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
        */
    }
}