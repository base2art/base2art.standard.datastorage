namespace Base2art.DataStorage.HolisticPersistence
{
    public static class HeldValue
    {
        public static IHeldValue<T> Create<T>()
        {
            return new HeldValueHolder<T> {HasValue = false};
        }

        public static IHeldValue<T> Create<T>(T value)
        {
            return new HeldValueHolder<T> {Value = value, HasValue = true};
        }

        private class HeldValueHolder<T> : IHeldValue<T>
        {
            public bool HasValue { get; set; }
            public T Value { get; set; }
        }
    }

    public interface IHeldValue<out T>
    {
        bool HasValue { get; }
        T Value { get; }
    }
}