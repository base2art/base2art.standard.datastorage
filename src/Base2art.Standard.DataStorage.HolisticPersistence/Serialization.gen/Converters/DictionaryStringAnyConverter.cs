// ReSharper disable once CheckNamespace
// THIS CODE IS GENERATED DO NOT EDIT!!!
namespace Base2art.DataStorage.HolisticPersistence.Serialization.Converters
{
    using System;

    /// <summary>
    ///     A Dictionary deserializer.
    /// </summary>
    internal class DictionaryStringAnyConverter : DictionaryAnyAnyConverterBase
    {
        public DictionaryStringAnyConverter(IJsonSerializer serializer): base(serializer)
        {
        }

        protected override bool IsMatch(Type first) => first == typeof(string);
        protected override string SerializeKey(Type objectType, object value) => (string)value;
        protected override object DeserializeKey(Type objectType, string key) => key;
    }
}