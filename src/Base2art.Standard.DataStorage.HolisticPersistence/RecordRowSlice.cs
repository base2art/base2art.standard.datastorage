﻿namespace Base2art.DataStorage.HolisticPersistence
{
    using System.Collections.Generic;

    public class RecordRowSlice
    {
        public RecordRowSlice(
            string tableName,
            IReadOnlyDictionary<string, object> original,
            Dictionary<string, object> viewable)
        {
            this.TableName = tableName;
            this.Viewable = viewable;
            this.Original = original;
        }

        public string TableName { get; }

        public IReadOnlyDictionary<string, object> Original { get; }

        public Dictionary<string, object> Viewable { get; }
    }
}