namespace Base2art.DataStorage.HolisticPersistence
{
    using System.Collections.Generic;
    using System.Reflection;
    using Serialization;

    public class SimpleIntermediateMapper : IIntermediateMapper
    {
        private static readonly IJsonSerializer serializer = new CamelCasingSimpleJsonSerializer();

        T IIntermediateMapper.DictToObject<T>(IReadOnlyDictionary<string, object> dict)
            => this.MapObject<T>(dict ?? new Dictionary<string, object>());

        object IIntermediateMapper.Cleanup(DataTypes headerType, object value) => this.CleanupValue(headerType, value);
        
        public string SerializeAsJson<T>(T o) => serializer.Serialize(o);

        public Dictionary<string, object> DeserializeAsJson(string value) => serializer.Deserialize<Dictionary<string, object>>(value);

        protected virtual object CleanupValue(DataTypes headerType, object value) => value;

        protected virtual T MapObject<T>(IReadOnlyDictionary<string, object> dict)
        {
            var objString = serializer.Serialize(dict);
            if (!typeof(T).GetTypeInfo().IsInterface)
            {
                return serializer.Deserialize<T>(objString);
            }

            return (T) new TypedDictionaryProxy<T>(dict).GetTransparentProxy();
        }
    }
}