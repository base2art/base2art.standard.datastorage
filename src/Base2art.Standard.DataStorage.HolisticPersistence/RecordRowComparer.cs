﻿namespace Base2art.DataStorage.HolisticPersistence
{
    using System.Collections.Generic;

    public class RecordRowComparer : IEqualityComparer<RecordRow>
    {
        private readonly RecordRowSliceComparer comp = new RecordRowSliceComparer();

        public bool Equals(RecordRow x, RecordRow y)
        {
            if (ReferenceEquals(x, y))
            {
                return true;
            }

            if ((x == null) ^ (y == null))
            {
                return false;
            }

            if (x.Slices.Length != y.Slices.Length)
            {
                return false;
            }

            for (var i = 0; i < x.Slices.Length; i++)
            {
                if (!this.comp.Equals(x.Slices[i], y.Slices[i]))
                {
                    return false;
                }
            }

            return true;
        }

        public int GetHashCode(RecordRow record)
        {
            // Overflow is fine, just wrap
            unchecked
            {
                var hash = 17;

                foreach (var slice in record)
                {
                    hash = hash * 23 + this.comp.GetHashCode(slice);
                }

                return hash;
            }
        }
    }
}