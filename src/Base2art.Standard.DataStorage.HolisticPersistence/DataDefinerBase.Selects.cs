namespace Base2art.DataStorage.HolisticPersistence
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using DataManipulation.Data;

    public abstract partial class DataDefinerBase
    {
        public abstract Task<List<RecordRow>> SelectItems(SelectData selectData);

        public async Task<IEnumerable<T>> SelectAsync<T>(SelectData selectData)
        {
            var dicts = await this.SelectItems(selectData);
            return dicts.Select(x => new ComplexRecordRow<T>(this.Mapper, x.FirstOrDefault()).Data.Item1);
        }

        public IEnumerable<T> Select<T>(SelectData selectData)
            => this.SelectAsync<T>(selectData).Result;

        public async Task<IEnumerable<Tuple<T1, T2>>> SelectAsync<T1, T2>(SelectData selectData)
        {
            var result = await this.SelectItems(selectData);
            return result.Select(x => new ComplexRecordRow<T1, T2>(
                                                                   this.Mapper,
                                                                   x.FirstOrDefault(),
                                                                   x.Skip(1).FirstOrDefault()).Data);
        }

        public IEnumerable<Tuple<T1, T2>> Select<T1, T2>(SelectData selectData)
            => this.SelectAsync<T1, T2>(selectData).Result;

        public async Task<IEnumerable<Tuple<T1, T2, T3>>> SelectAsync<T1, T2, T3>(SelectData selectData)
        {
            var result = await this.SelectItems(selectData);
            return result.Select(x => new ComplexRecordRow<T1, T2, T3>(
                                                                       this.Mapper,
                                                                       x.FirstOrDefault(),
                                                                       x.Skip(1).FirstOrDefault(),
                                                                       x.Skip(2).FirstOrDefault()).Data);
        }

        public IEnumerable<Tuple<T1, T2, T3>> Select<T1, T2, T3>(SelectData selectData)
            => this.SelectAsync<T1, T2, T3>(selectData).Result;

        public async Task<IEnumerable<Tuple<T1, T2, T3, T4>>> SelectAsync<T1, T2, T3, T4>(SelectData selectData)
        {
            var result = await this.SelectItems(selectData);
            return result.Select(x => new ComplexRecordRow<T1, T2, T3, T4>(
                                                                           this.Mapper,
                                                                           x.FirstOrDefault(),
                                                                           x.Skip(1).FirstOrDefault(),
                                                                           x.Skip(2).FirstOrDefault(),
                                                                           x.Skip(3).FirstOrDefault()).Data);
        }

        public IEnumerable<Tuple<T1, T2, T3, T4>> Select<T1, T2, T3, T4>(SelectData selectData)
            => this.SelectAsync<T1, T2, T3, T4>(selectData).Result;

        public async Task<IEnumerable<Tuple<T1, T2, T3, T4, T5>>> SelectAsync<T1, T2, T3, T4, T5>(SelectData selectData)
        {
            var result = await this.SelectItems(selectData);
            return result.Select(x => new ComplexRecordRow<T1, T2, T3, T4, T5>(
                                                                               this.Mapper,
                                                                               x.FirstOrDefault(),
                                                                               x.Skip(1).FirstOrDefault(),
                                                                               x.Skip(2).FirstOrDefault(),
                                                                               x.Skip(3).FirstOrDefault(),
                                                                               x.Skip(4).FirstOrDefault()).Data);
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5>> Select<T1, T2, T3, T4, T5>(SelectData selectData)
            => this.SelectAsync<T1, T2, T3, T4, T5>(selectData).Result;

        public async Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>>> SelectAsync<T1, T2, T3, T4, T5, T6>(SelectData selectData)
        {
            var result = await this.SelectItems(selectData);
            return result.Select(x => new ComplexRecordRow<T1, T2, T3, T4, T5, T6>(
                                                                                   this.Mapper,
                                                                                   x.FirstOrDefault(),
                                                                                   x.Skip(1).FirstOrDefault(),
                                                                                   x.Skip(2).FirstOrDefault(),
                                                                                   x.Skip(3).FirstOrDefault(),
                                                                                   x.Skip(4).FirstOrDefault(),
                                                                                   x.Skip(5).FirstOrDefault()).Data);
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>> Select<T1, T2, T3, T4, T5, T6>(SelectData selectData)
            => this.SelectAsync<T1, T2, T3, T4, T5, T6>(selectData).Result;

        public async Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>>> SelectAsync<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData)
        {
            var result = await this.SelectItems(selectData);
            return result.Select(x => new ComplexRecordRow<T1, T2, T3, T4, T5, T6, T7>(
                                                                                       this.Mapper,
                                                                                       x.FirstOrDefault(),
                                                                                       x.Skip(1).FirstOrDefault(),
                                                                                       x.Skip(2).FirstOrDefault(),
                                                                                       x.Skip(3).FirstOrDefault(),
                                                                                       x.Skip(4).FirstOrDefault(),
                                                                                       x.Skip(5).FirstOrDefault(),
                                                                                       x.Skip(6).FirstOrDefault()).Data);
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>> Select<T1, T2, T3, T4, T5, T6, T7>(SelectData selectData)
            => this.SelectAsync<T1, T2, T3, T4, T5, T6, T7>(selectData).Result;
    }
}