﻿namespace Base2art.DataStorage.HolisticPersistence
{
    using System;
    using System.Collections.Generic;

    public class ComplexRecordRow
    {
        // private readonly IIntermediateMapper mapper;
        // public ComplexRecordRow(IIntermediateMapper mapper) => this.mapper = mapper;
        //
        // protected T DictToObject<T>(IReadOnlyDictionary<string, object> dict)
        // {
        //     return this.mapper.DictToObject<T>(dict);
        // }

        // protected async Task<IEnumerable<IReadOnlyDictionary<string, object>>> Where<T>(
        //     T definer,
        //     IEnumerable<Dictionary<string, object>> matches,
        //     WhereClause whereClauses)
        //     where T : IItemsProducer
        // {
        //     foreach (var clause in whereClauses.GetNames())
        //     {
        //         var data = clause.Item4.SelectData != null
        //                        ? HeldValue.Create(await definer.SelectItems(clause.Item4.SelectData))
        //                        : HeldValue.Create<List<RecordRow>>();
        //         
        //         matches = matches.Where(record => Comper.IsMatch(record, clause, data));
        //     }
        //
        //     return matches;
        // }
    }

    public class ComplexRecordRow<T1> : ComplexRecordRow
    {
        private readonly IIntermediateMapper mapper;
        private readonly RecordRowSlice dict0;

        public ComplexRecordRow(IIntermediateMapper mapper, RecordRowSlice dict0)
        {
            this.mapper = mapper;
            this.dict0 = dict0;
        }

        public Tuple<T1> Data => Tuple.Create(this.mapper.DictToObject<T1>(this.dict0.Viewable));
    }

    public class ComplexRecordRow<T1, T2> : ComplexRecordRow
    {
        private readonly RecordRowSlice dict0;

        private readonly IIntermediateMapper mapper;
        private readonly RecordRowSlice dict1;

        public ComplexRecordRow(IIntermediateMapper mapper, RecordRowSlice dict0, RecordRowSlice dict1)
        {
            this.mapper = mapper;
            this.dict1 = dict1;
            this.dict0 = dict0;
        }

        public Tuple<T1, T2> Data => Tuple.Create(
                                                  this.dict0 == null ? default : this.mapper.DictToObject<T1>(this.dict0.Viewable),
                                                  this.dict1 == null ? default : this.mapper.DictToObject<T2>(this.dict1.Viewable));
    }

    public class ComplexRecordRow<T1, T2, T3> : ComplexRecordRow
    {
        private readonly RecordRowSlice dict0;

        private readonly RecordRowSlice dict1;

        private readonly IIntermediateMapper mapper;
        private readonly RecordRowSlice dict2;

        public ComplexRecordRow(IIntermediateMapper mapper, RecordRowSlice dict0, RecordRowSlice dict1, RecordRowSlice dict2)
        {
            this.mapper = mapper;
            this.dict2 = dict2;

            this.dict1 = dict1;
            this.dict0 = dict0;
        }

        public Tuple<T1, T2, T3> Data => Tuple.Create(
                                                      this.dict0 == null ? default : this.mapper.DictToObject<T1>(this.dict0.Viewable),
                                                      this.dict1 == null ? default : this.mapper.DictToObject<T2>(this.dict1.Viewable),
                                                      this.dict2 == null ? default : this.mapper.DictToObject<T3>(this.dict2.Viewable));
    }

    public class ComplexRecordRow<T1, T2, T3, T4> : ComplexRecordRow
    {
        private readonly IIntermediateMapper mapper;
        private readonly RecordRowSlice dict0;

        private readonly RecordRowSlice dict1;

        private readonly RecordRowSlice dict2;

        private readonly RecordRowSlice dict3;

        public ComplexRecordRow(
            IIntermediateMapper mapper,
            RecordRowSlice dict0,
            RecordRowSlice dict1,
            RecordRowSlice dict2,
            RecordRowSlice dict3)
           
        {
            this.mapper = mapper;
            this.dict0 = dict0;
            this.dict1 = dict1;
            this.dict2 = dict2;
            this.dict3 = dict3;
        }

        public Tuple<T1, T2, T3, T4> Data => Tuple.Create(
                                                          this.dict0 == null ? default : this.mapper.DictToObject<T1>(this.dict0.Viewable),
                                                          this.dict1 == null ? default : this.mapper.DictToObject<T2>(this.dict1.Viewable),
                                                          this.dict2 == null ? default : this.mapper.DictToObject<T3>(this.dict2.Viewable),
                                                          this.dict3 == null ? default : this.mapper.DictToObject<T4>(this.dict3.Viewable));
    }

    public class ComplexRecordRow<T1, T2, T3, T4, T5> : ComplexRecordRow
    {
        private readonly IIntermediateMapper mapper;
        private readonly RecordRowSlice dict0;

        private readonly RecordRowSlice dict1;

        private readonly RecordRowSlice dict2;

        private readonly RecordRowSlice dict3;

        private readonly RecordRowSlice dict4;

        public ComplexRecordRow(
            IIntermediateMapper mapper,
            RecordRowSlice dict0,
            RecordRowSlice dict1,
            RecordRowSlice dict2,
            RecordRowSlice dict3,
            RecordRowSlice dict4)
           
        {
            this.mapper = mapper;
            this.dict0 = dict0;
            this.dict1 = dict1;
            this.dict2 = dict2;
            this.dict3 = dict3;
            this.dict4 = dict4;
        }

        public Tuple<T1, T2, T3, T4, T5> Data => Tuple.Create(
                                                              this.dict0 == null ? default : this.mapper.DictToObject<T1>(this.dict0.Viewable),
                                                              this.dict1 == null ? default : this.mapper.DictToObject<T2>(this.dict1.Viewable),
                                                              this.dict2 == null ? default : this.mapper.DictToObject<T3>(this.dict2.Viewable),
                                                              this.dict3 == null ? default : this.mapper.DictToObject<T4>(this.dict3.Viewable),
                                                              this.dict4 == null ? default : this.mapper.DictToObject<T5>(this.dict4.Viewable));
    }

    public class ComplexRecordRow<T1, T2, T3, T4, T5, T6> : ComplexRecordRow
    {
        private readonly IIntermediateMapper mapper;
        private readonly RecordRowSlice dict0;

        private readonly RecordRowSlice dict1;

        private readonly RecordRowSlice dict2;

        private readonly RecordRowSlice dict3;

        private readonly RecordRowSlice dict4;

        private readonly RecordRowSlice dict5;

        public ComplexRecordRow(
            IIntermediateMapper mapper,
            RecordRowSlice dict0,
            RecordRowSlice dict1,
            RecordRowSlice dict2,
            RecordRowSlice dict3,
            RecordRowSlice dict4,
            RecordRowSlice dict5)
        {
            this.mapper = mapper;
            this.dict0 = dict0;
            this.dict1 = dict1;
            this.dict2 = dict2;
            this.dict3 = dict3;
            this.dict4 = dict4;
            this.dict5 = dict5;
        }

        public Tuple<T1, T2, T3, T4, T5, T6> Data => Tuple.Create(
                                                                  this.dict0 == null ? default : this.mapper.DictToObject<T1>(this.dict0.Viewable),
                                                                  this.dict1 == null ? default : this.mapper.DictToObject<T2>(this.dict1.Viewable),
                                                                  this.dict2 == null ? default : this.mapper.DictToObject<T3>(this.dict2.Viewable),
                                                                  this.dict3 == null ? default : this.mapper.DictToObject<T4>(this.dict3.Viewable),
                                                                  this.dict4 == null ? default : this.mapper.DictToObject<T5>(this.dict4.Viewable),
                                                                  this.dict5 == null ? default : this.mapper.DictToObject<T6>(this.dict5.Viewable));
    }

    public class ComplexRecordRow<T1, T2, T3, T4, T5, T6, T7> : ComplexRecordRow
    {
        private readonly IIntermediateMapper mapper;
        private readonly RecordRowSlice dict0;

        private readonly RecordRowSlice dict1;

        private readonly RecordRowSlice dict2;

        private readonly RecordRowSlice dict3;

        private readonly RecordRowSlice dict4;

        private readonly RecordRowSlice dict5;

        private readonly RecordRowSlice dict6;

        public ComplexRecordRow(
            IIntermediateMapper mapper,
            RecordRowSlice dict0,
            RecordRowSlice dict1,
            RecordRowSlice dict2,
            RecordRowSlice dict3,
            RecordRowSlice dict4,
            RecordRowSlice dict5,
            RecordRowSlice dict6)
        {
            this.mapper = mapper;
            this.dict0 = dict0;
            this.dict1 = dict1;
            this.dict2 = dict2;
            this.dict3 = dict3;
            this.dict4 = dict4;
            this.dict5 = dict5;
            this.dict6 = dict6;
        }

        public Tuple<T1, T2, T3, T4, T5, T6, T7> Data => Tuple.Create(
                                                                      this.dict0 == null ? default : this.mapper.DictToObject<T1>(this.dict0.Viewable),
                                                                      this.dict1 == null ? default : this.mapper.DictToObject<T2>(this.dict1.Viewable),
                                                                      this.dict2 == null ? default : this.mapper.DictToObject<T3>(this.dict2.Viewable),
                                                                      this.dict3 == null ? default : this.mapper.DictToObject<T4>(this.dict3.Viewable),
                                                                      this.dict4 == null ? default : this.mapper.DictToObject<T5>(this.dict4.Viewable),
                                                                      this.dict5 == null ? default : this.mapper.DictToObject<T6>(this.dict5.Viewable),
                                                                      this.dict6 == null ? default : this.mapper.DictToObject<T7>(this.dict6.Viewable));
    }
}