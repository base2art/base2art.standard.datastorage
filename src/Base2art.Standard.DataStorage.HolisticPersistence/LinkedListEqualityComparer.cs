﻿namespace Base2art.DataStorage.HolisticPersistence
{
    using System;
    using System.Collections.Generic;

    public class LinkedListEqualityComparer : IEqualityComparer<LinkedList<object>>
    {
        bool IEqualityComparer<LinkedList<object>>.Equals(LinkedList<object> x, LinkedList<object> y)
        {
            if (x.Count != y.Count)
            {
                return false;
            }

            var l_node = x.First;
            var r_node = y.First;

            Func<LinkedListNode<object>, LinkedListNode<object>, bool> compare = null;

            compare = (l, r) =>
            {
                if (l == null && r == null)
                {
                    return true;
                }

                if ((l == null) ^ (r == null))
                {
                    return false;
                }

                var item = EqualityComparer<object>.Default;

                if (!item.Equals(l.Value, r.Value))
                {
                    return false;
                }

                return compare(l.Next, r.Next);
            };

            return compare(l_node, r_node);
        }

        int IEqualityComparer<LinkedList<object>>.GetHashCode(LinkedList<object> obj)
        {
            if (obj.Count == 0)
            {
                return 0;
            }

            unchecked
            {
                var hash = 17;

                // get hash code for all items in array
                foreach (var item in obj)
                {
                    hash = hash * 23 + (item != null ? item.GetHashCode() : 0);
                }

                return hash;
            }
        }
    }
}