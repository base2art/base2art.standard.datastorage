﻿namespace Base2art.DataStorage.HolisticPersistence
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public class RecordRow : IEnumerable<RecordRowSlice>
    {
        private readonly List<object> items = new List<object>();

        public RecordRow(RecordRowSlice[] slices) => this.Slices = slices;

        public RecordRowSlice this[string tableName]
        {
            get { return this.Slices.FirstOrDefault(x => x.TableName == tableName); }
        }

        public object[] Items => this.items.ToArray();

        public RecordRowSlice[] Slices { get; }

        public IEnumerator<RecordRowSlice> GetEnumerator() => (this.Slices ?? new RecordRowSlice[0]).ToList().GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        public void AddObject(object item)
        {
            this.items.Add(item);
        }
    }
}