﻿namespace Base2art.DataStorage.HolisticPersistence
{
    public class MutableStruct<T>
        where T : struct
    {
        public T Value { get; set; }
    }
}