﻿namespace Base2art.DataStorage.Provider.MySql
{
    using System;
    using Base2art.Dapper;
    using Dapper;
    using DataStorage.MySql;
    using Interception.DataDefinition;
    using Interception.DataManipulation;
    using MySqlConnector;

    public class MySqlProvider : DataStorageProviderBase
    {
        private readonly TimeSpan defaultCommandTimeout;

        public MySqlProvider(
            TimeSpan defaultCommandTimeout,
            string[] definerInterceptorNames,
            string[] manipulatorInterceptorNames,
            IDataDefinerInterceptor[] definerInterceptors,
            IDataManipulatorInterceptor[] manipulatorInterceptors) : base(
                                                                          definerInterceptorNames,
                                                                          manipulatorInterceptorNames,
                                                                          definerInterceptors,
                                                                          manipulatorInterceptors) =>
            this.defaultCommandTimeout = defaultCommandTimeout;

        public override IDynamicDataStore CreateDynamicDataStoreAccess(NamedConnectionString named)
            => new DynamicDataStore(new DynamicExecutionEngine(DapperExecutionEngine(named), this.defaultCommandTimeout));

        protected override IDataDefinerAndManipulatorProvider CreateActualStore(NamedConnectionString named)
        {
            var dapperExecutionEngine = DapperExecutionEngine(named);
            return new MySqlFormatterProvider(dapperExecutionEngine, this.defaultCommandTimeout);
        }

        private static DapperExecutionEngine DapperExecutionEngine(NamedConnectionString named)
        {
            var cstr = named.ConnectionString;

            var factory = new OdbcConnectionFactory<MySqlConnection>(cstr);
            var dapperExecutionEngine = new DapperExecutionEngine(factory);
            return dapperExecutionEngine;
        }
    }
}