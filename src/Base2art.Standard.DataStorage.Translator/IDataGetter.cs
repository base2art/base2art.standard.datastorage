﻿namespace Base2art.DataStorage
{
    public interface IDataGetter<T>
    {
        T GetData();
    }
}