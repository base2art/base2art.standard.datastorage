﻿namespace Base2art.DataStorage
{
    using DataDefinition;

    public class Dbms : IDbms
    {
        private readonly IDataDefiner definer;

        public Dbms(IDataDefiner definer) => this.definer = definer;

        public IDbmsSupport Supports => new DbmsSupport(this.definer);

        public ITableCreator<T> CreateTable<T>() => new TableCreator<T>(this.definer, false);

        public ITableCreator<T> CreateOrUpdateTable<T>() => new TableCreator<T>(this.definer, true);

        public ITableDropper<T> DropTable<T>() => new TableDropper<T>(this.definer);

        private class DbmsSupport : IDbmsSupport
        {
            private readonly IDataDefiner dataDefiner;

            public DbmsSupport(IDataDefiner dataDefiner) => this.dataDefiner = dataDefiner;

            public bool DroppingTables => this.dataDefiner.Supports.DroppingTables;

            public bool ConstraintEnforcement => this.dataDefiner.Supports.ConstraintEnforcement;
        }
    }
}