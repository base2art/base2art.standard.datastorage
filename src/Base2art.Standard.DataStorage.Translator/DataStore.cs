﻿namespace Base2art.DataStorage
{
    using System;
    using DataManipulation;
    using Meta;

    public class DataStore : IDataStore
    {
        private readonly IDataManipulator manipulator;

        public DataStore(IDataManipulator manipulator) => this.manipulator = manipulator;

        public IDataStoreSupport Supports => new DataStoreSupport(this.manipulator);

        public IQueryInsert<T> Insert<T>() => new Inserter<T>(this.manipulator);

        public IQuerySingleSelect<T> SelectSingle<T>() => new SingleSelector1<T>(this.manipulator);

        public IQuerySelect<T> Select<T>() => new QuerySelector1<T>(this.manipulator);

        public IQueryDelete<T> Delete<T>() => new Deleter<T>(this.manipulator);

        public IQueryUpdate<T> Update<T>() => new Updater<T>(this.manipulator);

        private class DataStoreSupport : IDataStoreSupport
        {
            private readonly IDataManipulator dataManipulator;

            public DataStoreSupport(IDataManipulator dataManipulator) => this.dataManipulator = dataManipulator;

            public DbmsUnderflowHandlingStyle UnderflowHandling => this.Map(this.dataManipulator.Supports.UnderflowHandling);

            public DbmsOverflowHandlingStyle OverflowHandling => this.Map(this.dataManipulator.Supports.OverflowHandling);

            public bool InnerJoin => this.dataManipulator.Supports.InnerJoin;
            public bool RightJoin => this.dataManipulator.Supports.RightJoin;
            public bool LeftJoin => this.dataManipulator.Supports.LeftJoin;
            public bool FullJoin => this.dataManipulator.Supports.FullJoin;
            public bool CrossJoin => this.dataManipulator.Supports.CrossJoin;

            public string NativeExceptionTypeName => this.dataManipulator.Supports.NativeExceptionTypeName;

            private DbmsUnderflowHandlingStyle Map(UnderflowHandlingStyle value)
            {
                switch (value)
                {
                    case UnderflowHandlingStyle.None:
                        return DbmsUnderflowHandlingStyle.None;
                    case UnderflowHandlingStyle.Pad:
                        return DbmsUnderflowHandlingStyle.Pad;
                    case UnderflowHandlingStyle.ThrowException:
                        return DbmsUnderflowHandlingStyle.ThrowException;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(value), value, null);
                }
            }

            private DbmsOverflowHandlingStyle Map(OverflowHandlingStyle value)
            {
                switch (value)
                {
                    case OverflowHandlingStyle.Truncate:
                        return DbmsOverflowHandlingStyle.Truncate;
                    case OverflowHandlingStyle.Expand:
                        return DbmsOverflowHandlingStyle.Expand;
                    case OverflowHandlingStyle.ThrowException:
                        return DbmsOverflowHandlingStyle.ThrowException;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(value), value, null);
                }
            }
        }
    }
}