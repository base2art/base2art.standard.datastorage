namespace Base2art.DataStorage
{
    using System;
    using DynamicDataManipulation;

    public class DynamicDataStore : IDynamicDataStore
    {
        private readonly IDynamicExecutionEngine executionEngine;

        public DynamicDataStore(IDynamicExecutionEngine executionEngine)
            => this.executionEngine = executionEngine;

        public IDynamicQuery Query(string sql)
        {
            if (!this.executionEngine.SupportsDynamicQueries)
            {
                throw new NotSupportedException();
            }

            return new DynamicQuery(this.executionEngine, sql, false);
        }

        public IDynamicQuery QueryProcedure(string sql)
        {
            if (!this.executionEngine.SupportsDynamicProcedureQueries)
            {
                throw new NotSupportedException();
            }

            return new DynamicQuery(this.executionEngine, sql, true);
        }

        public IDynamicExecution ExecutionOf(string sql)
        {
            if (!this.executionEngine.SupportsDynamicQueries)
            {
                throw new NotSupportedException();
            }

            return new DynamicExecution(this.executionEngine, sql, false);
        }

        public IDynamicExecution ExecuteOfProcedure(string sql)
        {
            if (!this.executionEngine.SupportsDynamicProcedureQueries)
            {
                throw new NotSupportedException();
            }

            return new DynamicExecution(this.executionEngine, sql, true);
        }

        public IDynamicDataStoreSupport Supports => new SupportWrapper(this.executionEngine);

        private class SupportWrapper : IDynamicDataStoreSupport
        {
            private readonly IDynamicExecutionEngine dynamicExecutionEngine;

            public SupportWrapper(IDynamicExecutionEngine dynamicExecutionEngine) => this.dynamicExecutionEngine = dynamicExecutionEngine;

            public bool DynamicQueries => this.dynamicExecutionEngine.SupportsDynamicQueries;
            public bool DynamicProcedureQueries => this.dynamicExecutionEngine.SupportsDynamicProcedureQueries;
        }
    }
}