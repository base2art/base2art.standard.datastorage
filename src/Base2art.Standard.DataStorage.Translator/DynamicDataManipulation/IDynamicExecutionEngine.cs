namespace Base2art.DataStorage.DynamicDataManipulation
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IDynamicExecutionEngine
    {
        bool SupportsDynamicQueries { get; }

        bool SupportsDynamicProcedureQueries { get; }

        Task<IEnumerable<TOutput>> ExecuteReader<TOutput>(string sql, object data, bool isProcedure);

        Task<int> Execute(string sql, object data, bool isProcedure);
    }
}