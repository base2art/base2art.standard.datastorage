namespace Base2art.DataStorage.DynamicDataManipulation
{
    using System.Threading.Tasks;

    public class DynamicExecution : IDynamicExecution
    {
        private readonly IDynamicExecutionEngine executionEngine;
        private readonly bool isProcedure;
        private readonly string sql;
        private object data;

        public DynamicExecution(IDynamicExecutionEngine executionEngine, string sql, bool isProcedure)
        {
            this.executionEngine = executionEngine;
            this.sql = sql;
            this.isProcedure = isProcedure;
        }

        public IDynamicExecution WithParameters<TData>(TData value)
        {
            this.data = value;
            return this;
        }

        public Task<int> Execute() => this.executionEngine.Execute(this.sql, this.data, this.isProcedure);
    }
}