namespace Base2art.DataStorage.DynamicDataManipulation
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class DynamicQuery : IDynamicQuery
    {
        private readonly IDynamicExecutionEngine executionEngine;
        private readonly bool isProcedure;
        private readonly string sql;
        private object data;

        public DynamicQuery(IDynamicExecutionEngine executionEngine, string sql, bool isProcedure)
        {
            this.executionEngine = executionEngine;
            this.sql = sql;
            this.isProcedure = isProcedure;
        }

        public IDynamicQuery WithParameters<T>(T value)
        {
            this.data = value;
            return this;
        }

        public Task<IEnumerable<T>> Execute<T>() => this.executionEngine.ExecuteReader<T>(this.sql, this.data, this.isProcedure);
    }
}