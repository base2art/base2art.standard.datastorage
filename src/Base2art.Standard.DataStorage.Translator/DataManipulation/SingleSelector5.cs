﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Builders;

    internal class SingleSelector5<T1, T2, T3, T4, T5> : IQuerySingleSelect<T1, T2, T3, T4, T5>
    {
        private readonly Selector5Base<T1, T2, T3, T4, T5> proxy;

        public SingleSelector5(Selector5Base<T1, T2, T3, T4, T5> proxy)
        {
            if (proxy == null)
            {
                throw new ArgumentNullException("proxy");
            }

            this.proxy = proxy;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.WithNoLock()
        {
            this.proxy.WithNoLock();
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.Distinct()
        {
            this.proxy.Distinct();
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.Limit(int count)
        {
            this.proxy.Limit(count);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.Offset(int count)
        {
            this.proxy.Offset(count);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.Fields1(Action<IFieldListBuilder<T1>> recordSetup)
        {
            this.proxy.Fields1(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.Fields2(Action<IFieldListBuilder<T2>> recordSetup)
        {
            this.proxy.Fields2(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.Fields3(Action<IFieldListBuilder<T3>> recordSetup)
        {
            this.proxy.Fields3(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.Fields4(Action<IFieldListBuilder<T4>> recordSetup)
        {
            this.proxy.Fields4(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.Fields5(Action<IFieldListBuilder<T5>> recordSetup)
        {
            this.proxy.Fields5(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.Where1(Action<IWhereClauseBuilder<T1>> recordSetup)
        {
            this.proxy.Where1(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.Where2(Action<IWhereClauseBuilder<T2>> recordSetup)
        {
            this.proxy.Where2(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.Where3(Action<IWhereClauseBuilder<T3>> recordSetup)
        {
            this.proxy.Where3(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.Where4(Action<IWhereClauseBuilder<T4>> recordSetup)
        {
            this.proxy.Where4(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.Where5(Action<IWhereClauseBuilder<T5>> recordSetup)
        {
            this.proxy.Where5(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.OrderBy1(Action<IOrderByBuilder<T1>> recordSetup)
        {
            this.proxy.OrderBy1(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.OrderBy2(Action<IOrderByBuilder<T2>> recordSetup)
        {
            this.proxy.OrderBy2(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.OrderBy3(Action<IOrderByBuilder<T3>> recordSetup)
        {
            this.proxy.OrderBy3(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.OrderBy4(Action<IOrderByBuilder<T4>> recordSetup)
        {
            this.proxy.OrderBy4(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5> IQuerySingleSelect<T1, T2, T3, T4, T5>.OrderBy5(Action<IOrderByBuilder<T5>> recordSetup)
        {
            this.proxy.OrderBy5(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, TAggregate> IQuerySingleSelect<T1, T2, T3, T4, T5>.GroupBy1<TAggregate>(
            Action<IGroupByBuilder<T1>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy1<TAggregate>(recordSetup);
            return new SingleSelector6<T1, T2, T3, T4, T5, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, TAggregate> IQuerySingleSelect<T1, T2, T3, T4, T5>.GroupBy2<TAggregate>(
            Action<IGroupByBuilder<T2>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy2<TAggregate>(recordSetup);
            return new SingleSelector6<T1, T2, T3, T4, T5, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, TAggregate> IQuerySingleSelect<T1, T2, T3, T4, T5>.GroupBy3<TAggregate>(
            Action<IGroupByBuilder<T3>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy3<TAggregate>(recordSetup);
            return new SingleSelector6<T1, T2, T3, T4, T5, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, TAggregate> IQuerySingleSelect<T1, T2, T3, T4, T5>.GroupBy4<TAggregate>(
            Action<IGroupByBuilder<T4>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy4<TAggregate>(recordSetup);
            return new SingleSelector6<T1, T2, T3, T4, T5, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, TAggregate> IQuerySingleSelect<T1, T2, T3, T4, T5>.GroupBy5<TAggregate>(
            Action<IGroupByBuilder<T5>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy5<TAggregate>(recordSetup);
            return new SingleSelector6<T1, T2, T3, T4, T5, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, TAggregate> IQuerySingleSelect<T1, T2, T3, T4, T5>.WithCalculated<TAggregate>()
        {
            var newProxy = this.proxy.WithCalculated<TAggregate>();
            return new SingleSelector6<T1, T2, T3, T4, T5, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, TJoin> IQuerySingleSelect<T1, T2, T3, T4, T5>.Join<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.Join(joinSetup);
            return new SingleSelector6<T1, T2, T3, T4, T5, TJoin>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, TJoin> IQuerySingleSelect<T1, T2, T3, T4, T5>.LeftJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.LeftJoin(joinSetup);
            return new SingleSelector6<T1, T2, T3, T4, T5, TJoin>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, TJoin> IQuerySingleSelect<T1, T2, T3, T4, T5>.RightJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.RightJoin(joinSetup);
            return new SingleSelector6<T1, T2, T3, T4, T5, TJoin>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, TJoin> IQuerySingleSelect<T1, T2, T3, T4, T5>.FullJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.FullJoin(joinSetup);
            return new SingleSelector6<T1, T2, T3, T4, T5, TJoin>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, TJoin> IQuerySingleSelect<T1, T2, T3, T4, T5>.CrossJoin<TJoin>()
        {
            var newProxy = this.proxy.CrossJoin<TJoin>();
            return new SingleSelector6<T1, T2, T3, T4, T5, TJoin>(newProxy);
        }

        public async Task<Tuple<T1, T2, T3, T4, T5>> Execute()
        {
            var result = await this.proxy.Manipulator.SelectAsync<T1, T2, T3, T4, T5>(this.proxy.GetData());
            return result?.FirstOrDefault();
        }
    }
}