﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Linq;
    using Builders;
    using Clauses;
    using Data;

    internal class Selector7Base<T1, T2, T3, T4, T5, T6, T7> : IDataGetter<SelectData>
    {
        private readonly Tuple<int, Column, OperatorType, int, Column>[] columnJoins;

        private readonly JoinType jointype;
        private readonly Selector6Base<T1, T2, T3, T4, T5, T6> selector;

        private FieldSelectClause<T7> fields;

        private GroupByBuilder<T7> groupBy;

        private OrderByBuilder<T7> orderBy;

        private WhereClauseWithValue<T7> wheres;

        public Selector7Base(
            Selector6Base<T1, T2, T3, T4, T5, T6> selector,
            JoinType jointype,
            Tuple<int, Column, OperatorType, int, Column>[] columnJoins)
        {
            this.selector = selector;
            this.jointype = jointype;
            this.columnJoins = columnJoins;
        }

        public IDataManipulator Manipulator => this.selector.Manipulator;

        public bool HasFieldsDeclared => this.selector.HasFieldsDeclared;

        /*
        public Selector4Base<T1, T2, T3, TJoin> Join<TJoin>(Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup)
        {
            return new Selector5Base<T1, T2, T3, T4, TJoin>(
                this,
                JoinType.Inner,
                this.GetJoins(joinSetup, joinSetup.Body));
        }
        
        public Selector4Base<T1, T2, T3, TJoin> LeftJoin<TJoin>(Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup)
        {
            return new Selector5Base<T1, T2, T3, TJoin>(
                this,
                JoinType.Left,
                this.GetJoins(joinSetup, joinSetup.Body));
        }
        
        public Selector4Base<T1, T2, T3, TJoin> RightJoin<TJoin>(Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup)
        {
            return new Selector5Base<T1, T2, T3, TJoin>(
                this,
                JoinType.Right,
                this.GetJoins(joinSetup, joinSetup.Body));
        }
        
        public Selector4Base<T1, T2, T3, TJoin> FullJoin<TJoin>(Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup)
        {
            return new Selector3Base<T1, T2, T3, TJoin>(
                this,
                JoinType.Full,
                this.GetJoins(joinSetup, joinSetup.Body));
        }
        
        public Selector4Base<T1, T2, T3, TJoin> CrossJoin<TJoin>()
        {
            return new Selector3Base<T1, T2, T3, TJoin>(
                this,
                JoinType.Cross,
                new Tuple<int, Column, OperatorType, int, Column>[0]);
        }
        */
        public SelectData GetData()
        {
            var data = this.selector.GetData();
            Func<SelectData, SelectData> findNullJoin = null;
            findNullJoin = x =>
            {
                if (x.JoinData == null)
                {
                    return x;
                }

                return findNullJoin(x.JoinData.SelectData);
            };

            var thisData = new SelectData
                           {
                               SchemaName = typeof(T7).Schema(),
                               TableName = typeof(T7).TableName(),
                               Distinct = data.Distinct,
                               FieldList = this.fields.Convert(!this.HasFieldsDeclared),
                               WhereClause = this.wheres.Convert(),
                               Offset = data.Offset,
                               Limit = data.Limit,
                               Ordering = this.orderBy.Convert(),
                               Grouping = this.groupBy.Convert()
                           };

            var dataToUpdate = findNullJoin(data);
            dataToUpdate.JoinData = new JoinData();

            dataToUpdate.JoinData.JoinType = this.jointype;
            dataToUpdate.JoinData.SelectData = thisData;
            dataToUpdate.JoinData.OnData =
                this.columnJoins.Select(x => Tuple.Create(x.Item1, x.Item2, x.Item3, x.Item4, x.Item5)).ToArray();

            return data;
        }

        public void DeclareFieldsInUse()
        {
            this.selector.DeclareFieldsInUse();
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Limit(int limit)
        {
            this.selector.Limit(limit);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Offset(int offset)
        {
            this.selector.Offset(offset);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> WithNoLock()
        {
            this.selector.WithNoLock();
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Distinct()
        {
            this.selector.Distinct();
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Where1(Action<IWhereClauseBuilder<T1>> recordSetup)
        {
            this.selector.Where1Data = this.selector.Where1Data ?? new WhereClauseWithValue<T1>();
            recordSetup(this.selector.Where1Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Where2(Action<IWhereClauseBuilder<T2>> recordSetup)
        {
            this.selector.Where2Data = this.selector.Where2Data ?? new WhereClauseWithValue<T2>();
            recordSetup(this.selector.Where2Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Where3(Action<IWhereClauseBuilder<T3>> recordSetup)
        {
            this.selector.Where3Data = this.selector.Where3Data ?? new WhereClauseWithValue<T3>();
            recordSetup(this.selector.Where3Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Where4(Action<IWhereClauseBuilder<T4>> recordSetup)
        {
            this.selector.Where4Data = this.selector.Where4Data ?? new WhereClauseWithValue<T4>();
            recordSetup(this.selector.Where4Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Where5(Action<IWhereClauseBuilder<T5>> recordSetup)
        {
            this.selector.Where5Data = this.selector.Where5Data ?? new WhereClauseWithValue<T5>();
            recordSetup(this.selector.Where5Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Where6(Action<IWhereClauseBuilder<T6>> recordSetup)
        {
            this.selector.Where6Data = this.selector.Where6Data ?? new WhereClauseWithValue<T6>();
            recordSetup(this.selector.Where6Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Where7(Action<IWhereClauseBuilder<T7>> recordSetup)
        {
            var record = this.wheres ?? new WhereClauseWithValue<T7>();
            recordSetup(record);
            this.wheres = record;
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Fields1(Action<IFieldListBuilder<T1>> fieldSetup)
        {
            this.selector.Field1Data = this.selector.Field1Data ?? new FieldSelectClause<T1>();
            fieldSetup(this.selector.Field1Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Fields2(Action<IFieldListBuilder<T2>> fieldSetup)
        {
            this.selector.Field2Data = this.selector.Field2Data ?? new FieldSelectClause<T2>();
            fieldSetup(this.selector.Field2Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Fields3(Action<IFieldListBuilder<T3>> fieldSetup)
        {
            this.selector.Field3Data = this.selector.Field3Data ?? new FieldSelectClause<T3>();
            fieldSetup(this.selector.Field3Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Fields4(Action<IFieldListBuilder<T4>> fieldSetup)
        {
            this.selector.Field4Data = this.selector.Field4Data ?? new FieldSelectClause<T4>();
            fieldSetup(this.selector.Field4Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Fields5(Action<IFieldListBuilder<T5>> fieldSetup)
        {
            this.selector.Field5Data = this.selector.Field5Data ?? new FieldSelectClause<T5>();
            fieldSetup(this.selector.Field5Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Fields6(Action<IFieldListBuilder<T6>> fieldSetup)
        {
            this.selector.Field6Data = this.selector.Field6Data ?? new FieldSelectClause<T6>();
            fieldSetup(this.selector.Field6Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> Fields7(Action<IFieldListBuilder<T7>> fieldSetup)
        {
            var record = this.fields ?? new FieldSelectClause<T7>();
            fieldSetup(record);
            this.fields = record;
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> OrderBy1(Action<IOrderByBuilder<T1>> recordSetup)
        {
            this.selector.Order1Data =
                this.selector.Order1Data ?? new OrderByBuilder<T1>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order1Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> OrderBy2(Action<IOrderByBuilder<T2>> recordSetup)
        {
            this.selector.Order2Data =
                this.selector.Order2Data ?? new OrderByBuilder<T2>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order2Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> OrderBy3(Action<IOrderByBuilder<T3>> recordSetup)
        {
            this.selector.Order3Data =
                this.selector.Order3Data ?? new OrderByBuilder<T3>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order3Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> OrderBy4(Action<IOrderByBuilder<T4>> recordSetup)
        {
            this.selector.Order4Data =
                this.selector.Order4Data ?? new OrderByBuilder<T4>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order4Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> OrderBy5(Action<IOrderByBuilder<T5>> recordSetup)
        {
            this.selector.Order5Data =
                this.selector.Order5Data ?? new OrderByBuilder<T5>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order5Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> OrderBy6(Action<IOrderByBuilder<T6>> recordSetup)
        {
            this.selector.Order6Data =
                this.selector.Order6Data ?? new OrderByBuilder<T6>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order6Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> OrderBy7(Action<IOrderByBuilder<T7>> recordSetup)
        {
            var record = this.orderBy ?? new OrderByBuilder<T7>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(record);
            this.orderBy = record;
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> GroupBy1(Action<IGroupByBuilder<T1>> recordSetup)
        {
            this.selector.Group1Data = this.selector.Group1Data ?? new GroupByBuilder<T1>();
            recordSetup(this.selector.Group1Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> GroupBy2(Action<IGroupByBuilder<T2>> recordSetup)
        {
            this.selector.Group2Data = this.selector.Group2Data ?? new GroupByBuilder<T2>();
            recordSetup(this.selector.Group2Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> GroupBy3(Action<IGroupByBuilder<T3>> recordSetup)
        {
            this.selector.Group3Data = this.selector.Group3Data ?? new GroupByBuilder<T3>();
            recordSetup(this.selector.Group3Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> GroupBy4(Action<IGroupByBuilder<T4>> recordSetup)
        {
            this.selector.Group4Data = this.selector.Group4Data ?? new GroupByBuilder<T4>();
            recordSetup(this.selector.Group4Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> GroupBy5(Action<IGroupByBuilder<T5>> recordSetup)
        {
            this.selector.Group5Data = this.selector.Group5Data ?? new GroupByBuilder<T5>();
            recordSetup(this.selector.Group5Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> GroupBy6(Action<IGroupByBuilder<T6>> recordSetup)
        {
            this.selector.Group6Data = this.selector.Group6Data ?? new GroupByBuilder<T6>();
            recordSetup(this.selector.Group6Data);
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, T7> GroupBy7(Action<IGroupByBuilder<T7>> recordSetup)
        {
            var record = this.groupBy ?? new GroupByBuilder<T7>();
            recordSetup(record);
            this.groupBy = record;
            return this;
        }
    }
}