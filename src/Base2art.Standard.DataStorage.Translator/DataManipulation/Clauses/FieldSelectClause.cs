﻿namespace Base2art.DataStorage.DataManipulation.Clauses
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Xml;
    using System.Xml.Linq;
    using Builders;
    using Data;
    using Utils;

    internal class FieldSelectClause<T> : IFieldListBuilder<T>
    {
        private readonly List<Tuple<Column, OperatorType?, Column>> valueMap = new List<Tuple<Column, OperatorType?, Column>>();

        IFieldListBuilder<T> IFieldListBuilder<T>.All()
        {
            this.valueMap.Add(Tuple.Create<Column, OperatorType?, Column>(Column.Custom(SpecialColumns.All), null, null));
            return this;
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.Count(Expression<Func<T, long?>> caller)
        {
            var data = caller.GetComplexMemberInfo(typeof(T).TableName(), false);
            this.valueMap.Add(Tuple.Create<Column, OperatorType?, Column>(Column.CustomWithAlias(SpecialColumns.Count, data.Item1.ColumnName), null,
                                                                          null));
            return this;
        }

        IFieldListBuilder<T> IFieldListBuilder<T>.NewUUID(Expression<Func<T, Guid?>> caller)
        {
            var data = caller.GetComplexMemberInfo(typeof(T).TableName(), false);
            this.valueMap.Add(Tuple.Create<Column, OperatorType?, Column>(Column.CustomWithAlias(SpecialColumns.NewUUID, data.Item1.ColumnName), null,
                                                                          null));
            return this;
        }

        // REGION {19df13eb786a015aad0dfe93db403ffc}

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, string>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, byte[]>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, Guid?>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, long?>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, int?>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, DateTime?>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, TimeSpan?>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, short?>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, XmlDocument>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, XElement>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, decimal?>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, float?>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, double?>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, bool?>> caller) => this.Add(caller);

        IFieldListBuilder<T> IFieldListBuilder<T>.Field(Expression<Func<T, Dictionary<string, object>>> caller) => this.Add(caller);
        // END-REGION {19df13eb786a015aad0dfe93db403ffc}

        private IFieldListBuilder<T> Add(Expression caller)
        {
            var data = caller.GetComplexMemberInfo(typeof(T).TableName(), false);
            this.valueMap.Add(data);
            return this;
        }

        public Tuple<Column, OperatorType?, Column>[] GetNames() => this.valueMap.ToArray();
    }
}