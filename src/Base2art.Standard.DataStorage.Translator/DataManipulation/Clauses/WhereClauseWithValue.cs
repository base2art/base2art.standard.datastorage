﻿namespace Base2art.DataStorage.DataManipulation.Clauses
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using Builders;
    using Data;
    using Utils;

    // TYPED
    internal partial class WhereClauseWithValue<T> : IWhereClauseBuilder<T>
    {
        private readonly List<Tuple<Column, IEnumerable>> inMap = new List<Tuple<Column, IEnumerable>>();

        private readonly List<Tuple<Column, IEnumerable>> notInMap = new List<Tuple<Column, IEnumerable>>();

        private readonly List<Tuple<Column, Tuple<OperatorType, Column>[], OperatorType, Column>> valueMap =
            new List<Tuple<Column, Tuple<OperatorType, Column>[], OperatorType, Column>>();

        public Tuple<Column, Tuple<OperatorType, Column>[], OperatorType, Column>[] GetNames()
        {
            return this.valueMap.Select(x => x)
                       .Union(
                              this.inMap.Select(x =>
                              {
                                  var ts = x.Item2 as IDataGetter<SelectData>;

                                  if (ts != null)
                                  {
                                      var data = ts.GetData();
                                      return Tuple.Create(
                                                          x.Item1,
                                                          new Tuple<OperatorType, Column>[0],
                                                          OperatorType.In,
                                                          Column.InnerSelect(data));
                                  }

                                  return Tuple.Create(
                                                      x.Item1,
                                                      new Tuple<OperatorType, Column>[0],
                                                      OperatorType.In,
                                                      Column.List(x.Item2));
                              })
                             )
                       .Union(
                              this.notInMap.Select(x =>
                              {
                                  var ts = x.Item2 as IDataGetter<SelectData>;

                                  if (ts != null)
                                  {
                                      var data = ts.GetData();
                                      return Tuple.Create(
                                                          x.Item1,
                                                          new Tuple<OperatorType, Column>[0],
                                                          OperatorType.NotIn,
                                                          Column.InnerSelect(data));
                                  }

                                  return Tuple.Create(
                                                      x.Item1,
                                                      new Tuple<OperatorType, Column>[0],
                                                      OperatorType.NotIn,
                                                      Column.List(x.Item2));
                              })
                             ).ToArray();
        }

        private Column GetFieldData(string fieldName)
        {
            var tuple = this.valueMap.FirstOrDefault(x => string.Equals(fieldName, x.Item1.FieldNameOrNull(), StringComparison.OrdinalIgnoreCase));
            if (tuple == null)
            {
                return null;
            }

            return tuple.Item4;
        }

        private Tuple<Column, Tuple<OperatorType, Column>[], OperatorType, Column> Create<TI>(Expression body, TI data, Expression op)
        {
            var list = new List<Tuple<OperatorType, Column>>();
            this.GetExpressionName(body, typeof(T).TableName(), OperatorType.NoOp, list);
            return Tuple.Create(
                                list[0].Item2,
                                list.Skip(1).ToArray(),
                                this.GetExpressionOperation(op),
                                Column.Data(data, typeof(TI)));
        }

        private TI GetConstantValue<TI>(Expression body)
        {
            var expression = body as BinaryExpression;
            var typeName = typeof(T).TableName();
            if (expression != null)
            {
                try
                {
                    var lrez = expression.Left.GetMemberName(typeName);
                    if (lrez == null)
                    {
                        return (TI) expression.Left.GetConstantValue();
                    }

                    try
                    {
                        var rrez = expression.Right.GetMemberName(typeName);

                        if (rrez == null)
                        {
                            return (TI) expression.Right.GetConstantValue();
                        }
                    }
                    catch (Exception)
                    {
                        return (TI) expression.Right.GetConstantValue();
                    }
                }
                catch (Exception)
                {
                    return (TI) expression.Left.GetConstantValue();
                }
            }

            return default;
        }

        private void GetExpressionName(Expression body, string tableName, OperatorType parent, List<Tuple<OperatorType, Column>> ops)
        {
            {
                var expression = body as BinaryExpression;
                if (expression != null)
                {
                    this.GetExpressionName(expression.Left, tableName, parent, ops);
                    this.GetExpressionName(expression.Right, tableName, OperatorType.Add, ops);
                    return;
                }
            }

            {
                var expression = body as UnaryExpression;
                if (expression != null)
                {
                    this.GetExpressionName(expression.Operand, tableName, parent, ops);
                    return;
                }
            }

            {
                var expression = body as MemberExpression;
                if (expression != null)
                {
                    var mi = expression.GetMemberInfo(tableName);
                    ops.Add(Tuple.Create(parent, mi));
                    return;
                }
            }

            throw new InvalidOperationException("Unknown ExpressionType: '" + body.NodeType + "' " + body.GetType());
        }

        private OperatorType GetExpressionOperation(Expression body)
        {
            var map = ParmLookupTemp.GetExpressionTypeMap();
            if (map.ContainsKey(body.NodeType))
            {
                return map[body.NodeType];
            }

            if (body.NodeType == ExpressionType.ConvertChecked || body.NodeType == ExpressionType.Convert)
            {
                var expression = body as UnaryExpression;
                return this.GetExpressionOperation(expression.Operand);
            }

            if (body.NodeType == ExpressionType.Call)
            {
                var expression = body as MethodCallExpression;
                if (expression.Method.Name == "LikeOp")
                {
                    return OperatorType.Like;
                }

                if (expression.Method.Name == "NotLikeOp")
                {
                    return OperatorType.NotLike;
                }
            }

            throw new InvalidOperationException("Unknown ExpressionType: '" + body.NodeType + "'");
        }
    }
}