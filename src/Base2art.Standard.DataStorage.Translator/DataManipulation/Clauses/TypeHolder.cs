namespace Base2art.DataStorage.DataManipulation.Clauses
{
    using System;

    public class TypeHolder
    {
        private readonly Func<string> func;

        public TypeHolder(Func<string> func) => this.func = func;

        public string Value => this.func();
    }
}