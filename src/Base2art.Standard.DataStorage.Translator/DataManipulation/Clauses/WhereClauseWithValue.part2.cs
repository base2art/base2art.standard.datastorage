﻿namespace Base2art.DataStorage.DataManipulation.Clauses
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Xml;
    using System.Xml.Linq;
    using Builders;

    internal partial class WhereClauseWithValue<T>
    {
        // REGION {b640a9a917fb3e0dce1bd840bfce7aee}

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, string>> caller,
            string data,
            Expression<Func<string, string, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, byte[]>> caller,
            byte[] data,
            Expression<Func<byte[], byte[], bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, Guid?>> caller,
            Guid? data,
            Expression<Func<Guid?, Guid?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, long?>> caller,
            long? data,
            Expression<Func<long?, long?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, int?>> caller,
            int? data,
            Expression<Func<int?, int?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, DateTime?>> caller,
            DateTime? data,
            Expression<Func<DateTime?, DateTime?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, TimeSpan?>> caller,
            TimeSpan? data,
            Expression<Func<TimeSpan?, TimeSpan?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, short?>> caller,
            short? data,
            Expression<Func<short?, short?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, XmlDocument>> caller,
            XmlDocument data,
            Expression<Func<XmlDocument, XmlDocument, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, XElement>> caller,
            XElement data,
            Expression<Func<XElement, XElement, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, decimal?>> caller,
            decimal? data,
            Expression<Func<decimal?, decimal?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, float?>> caller,
            float? data,
            Expression<Func<float?, float?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, double?>> caller,
            double? data,
            Expression<Func<double?, double?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, bool?>> caller,
            bool? data,
            Expression<Func<bool?, bool?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, Dictionary<string, object>>> caller,
            Dictionary<string, object> data,
            Expression<Func<Dictionary<string, object>, Dictionary<string, object>, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, data, op.Body));
            return this;
        }

        // END-REGION {b640a9a917fb3e0dce1bd840bfce7aee}
    }
}