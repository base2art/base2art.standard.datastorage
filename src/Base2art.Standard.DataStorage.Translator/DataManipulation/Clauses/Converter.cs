﻿namespace Base2art.DataStorage.DataManipulation.Clauses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Data;

    internal static class Converter
    {
        public static FieldList Convert<T>(this FieldSelectClause<T> fields, bool isSingleTableSelect)
        {
            if (fields == null)
            {
                if (isSingleTableSelect)
                {
                    return new FieldList(new[] {Tuple.Create<Column, OperatorType?, Column>(Column.Custom(SpecialColumns.All), null, null)});
                }

                return null;
            }

            return new FieldList(fields.GetNames());
        }

        public static WhereClause Convert<T>(this WhereClauseWithValue<T> wheres)
        {
            if (wheres == null)
            {
                return null;
            }

            return new WhereClause(wheres.GetNames());
        }

        public static SetsList Convert<T>(this RecordClauseWithValue<T> record)
        {
            if (record == null)
            {
                return null;
            }

            return new SetsList(record.GetNames(), record.GetFieldDataMap());
        }

        public static SetsList[] Convert<T>(this IEnumerable<RecordClauseWithValue<T>> records)
        {
            if (records == null)
            {
                return new SetsList[0];
            }

            return records.Select(x => x.Convert()).ToArray();
        }

        public static OrderingData[] Convert<T>(this OrderByBuilder<T> orderBy)
        {
            if (orderBy == null)
            {
                return null;
            }

            return orderBy.GetNames().Select(n =>
                                                 n.Item1.SpecialColumn.HasValue
                                                     ? new OrderingData
                                                       {
                                                           Column = Column.Custom(n.Item1.SpecialColumn.Value),
                                                           Direction = n.Item2
                                                       }
                                                     : new OrderingData
                                                       {
                                                           Column = Column.Simple(n.Item1.ColumnName, n.Item1.DataType),
                                                           Direction = n.Item2
                                                       }).ToArray();
        }

        public static Column[] Convert<T>(this GroupByBuilder<T> grpBy)
        {
            if (grpBy == null)
            {
                return new Column[0];
            }

            return grpBy.GetNames();
        }
    }
}