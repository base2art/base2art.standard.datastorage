﻿namespace Base2art.DataStorage.DataManipulation.Clauses
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Xml;
    using System.Xml.Linq;
    using Builders;

    internal partial class WhereClauseWithValue<T>
    {
        // REGION {002296a3c3d14770c360db4c6984b687}

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, string>> caller,
            Expression<Func<string, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<string>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, byte[]>> caller,
            Expression<Func<byte[], bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<byte[]>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, Guid?>> caller,
            Expression<Func<Guid?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<Guid?>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, long?>> caller,
            Expression<Func<long?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<long?>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, int?>> caller,
            Expression<Func<int?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<int?>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, DateTime?>> caller,
            Expression<Func<DateTime?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<DateTime?>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, TimeSpan?>> caller,
            Expression<Func<TimeSpan?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<TimeSpan?>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, short?>> caller,
            Expression<Func<short?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<short?>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, XmlDocument>> caller,
            Expression<Func<XmlDocument, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<XmlDocument>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, XElement>> caller,
            Expression<Func<XElement, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<XElement>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, decimal?>> caller,
            Expression<Func<decimal?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<decimal?>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, float?>> caller,
            Expression<Func<float?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<float?>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, double?>> caller,
            Expression<Func<double?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<double?>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, bool?>> caller,
            Expression<Func<bool?, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<bool?>(op.Body), op.Body));
            return this;
        }

        IWhereClauseBuilder<T> IWhereClauseBuilder<T>.Field(
            Expression<Func<T, Dictionary<string, object>>> caller,
            Expression<Func<Dictionary<string, object>, bool>> op)
        {
            this.valueMap.Add(this.Create(caller.Body, this.GetConstantValue<Dictionary<string, object>>(op.Body), op.Body));
            return this;
        }

        // END-REGION {002296a3c3d14770c360db4c6984b687}
    }
}