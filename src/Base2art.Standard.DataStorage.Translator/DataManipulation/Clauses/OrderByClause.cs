﻿namespace Base2art.DataStorage.DataManipulation.Clauses
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Xml;
    using System.Xml.Linq;
    using Builders;
    using Data;
    using Utils;
    using ILListSort = Data.ListSortDirection;
    using InterfaceListSort = ListSortDirection;

    public class OrderByBuilder<T> : IOrderByBuilder<T>
    {
        private readonly TypeHolder groupByTypeHolder;
        private readonly List<Tuple<Column, ListSortDirection>> lists = new List<Tuple<Column, ILListSort>>();

        public OrderByBuilder(TypeHolder groupByTypeHolder) => this.groupByTypeHolder = groupByTypeHolder;

//        public OrderByBuilder()
//        {
//            this.groupByTypeHolder = new TypeHolder(() => null);
//        }

        public IOrderByBuilder<T> Random()
        {
            this.lists.Add(Tuple.Create(Column.Custom(SpecialColumns.NewUUID), ILListSort.Ascending));
            return this;
        }

        // REGION {23ca8150eebec9e1ec583888305f96a4}

        public IOrderByBuilder<T> Field(Expression<Func<T, string>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(string)),
                                        this.Map(direction)));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, byte[]>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(byte[])),
                                        this.Map(direction)));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, Guid?>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(Guid?)),
                                        this.Map(direction)));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, long?>> caller, InterfaceListSort direction)
        {
            var groupType = this.groupByTypeHolder.Value;
            var tableName = typeof(T).TableName();
            if (string.Equals(groupType, tableName, StringComparison.OrdinalIgnoreCase))
            {
                this.lists.Add(Tuple.Create(Column.Custom(SpecialColumns.Count), this.Map(direction)));
//                this.lists.Add(Tuple.Create(Column.(typeof(T).TableName(), caller.Body.GetMemberName(), typeof(long?)),
//                                            ));
            }
            else
            {
                this.lists.Add(Tuple.Create(Column.Simple(tableName, caller.Body.GetMemberName(typeof(T).TableName()), typeof(long?)),
                                            this.Map(direction)));
            }

            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, int?>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(int?)),
                                        this.Map(direction)));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, DateTime?>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(DateTime?)),
                                        this.Map(direction)));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(TimeSpan?)),
                                        this.Map(direction)));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, short?>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(short?)),
                                        this.Map(direction)));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, XmlDocument>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(XmlDocument)),
                                        this.Map(direction)));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, XElement>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(XElement)),
                                        this.Map(direction)));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, decimal?>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(decimal?)),
                                        this.Map(direction)));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, float?>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(float?)),
                                        this.Map(direction)));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, double?>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(double?)),
                                        this.Map(direction)));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, bool?>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(bool?)),
                                        this.Map(direction)));
            return this;
        }

        public IOrderByBuilder<T> Field(Expression<Func<T, Dictionary<string, object>>> caller, InterfaceListSort direction)
        {
            this.lists.Add(Tuple.Create(Column.Simple(typeof(T).TableName(), caller.Body.GetMemberName(typeof(T).TableName()), typeof(Dictionary<string, object>)),
                                        this.Map(direction)));
            return this;
        }

        public Tuple<Column, ILListSort>[] GetNames() => this.lists.ToArray();
        // END-REGION {23ca8150eebec9e1ec583888305f96a4}

        private ILListSort Map(InterfaceListSort direction)
        {
            switch (direction)
            {
                case InterfaceListSort.Ascending:
                    return ILListSort.Ascending;
                case InterfaceListSort.Descending:
                    return ILListSort.Descending;
                default:
                    throw new ArgumentOutOfRangeException("Not Found " + direction.ToString("G"));
            }
        }
    }
}