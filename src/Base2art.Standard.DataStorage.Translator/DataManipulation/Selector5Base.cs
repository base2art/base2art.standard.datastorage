﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using Builders;
    using Clauses;
    using Data;

    internal class Selector5Base<T1, T2, T3, T4, T5> : IDataGetter<SelectData>
    {
        private readonly Tuple<int, Column, OperatorType, int, Column>[] columnJoins;

        private readonly JoinType jointype;
        private readonly Selector4Base<T1, T2, T3, T4> selector;

        public Selector5Base(
            Selector4Base<T1, T2, T3, T4> selector,
            JoinType jointype,
            Tuple<int, Column, OperatorType, int, Column>[] columnJoins)
        {
            this.selector = selector;
            this.jointype = jointype;
            this.columnJoins = columnJoins;
        }

        public FieldSelectClause<T1> Field1Data
        {
            get => this.selector.Field1Data;
            set => this.selector.Field1Data = value;
        }

        public FieldSelectClause<T2> Field2Data
        {
            get => this.selector.Field2Data;
            set => this.selector.Field2Data = value;
        }

        public FieldSelectClause<T3> Field3Data
        {
            get => this.selector.Field3Data;
            set => this.selector.Field3Data = value;
        }

        public FieldSelectClause<T4> Field4Data
        {
            get => this.selector.Field4Data;
            set => this.selector.Field4Data = value;
        }

        internal FieldSelectClause<T5> Field5Data { get; set; }

        internal OrderByBuilder<T1> Order1Data
        {
            get => this.selector.Order1Data;
            set => this.selector.Order1Data = value;
        }

        internal OrderByBuilder<T2> Order2Data
        {
            get => this.selector.Order2Data;
            set => this.selector.Order2Data = value;
        }

        internal OrderByBuilder<T3> Order3Data
        {
            get => this.selector.Order3Data;
            set => this.selector.Order3Data = value;
        }

        internal OrderByBuilder<T4> Order4Data
        {
            get => this.selector.Order4Data;
            set => this.selector.Order4Data = value;
        }

        internal OrderByBuilder<T5> Order5Data { get; set; }

        internal GroupByBuilder<T1> Group1Data
        {
            get => this.selector.Group1Data;
            set => this.selector.Group1Data = value;
        }

        internal GroupByBuilder<T2> Group2Data
        {
            get => this.selector.Group2Data;
            set => this.selector.Group2Data = value;
        }

        internal GroupByBuilder<T3> Group3Data
        {
            get => this.selector.Group3Data;
            set => this.selector.Group3Data = value;
        }

        internal GroupByBuilder<T4> Group4Data
        {
            get => this.selector.Group4Data;
            set => this.selector.Group4Data = value;
        }

        internal GroupByBuilder<T5> Group5Data { get; set; }

        public WhereClauseWithValue<T1> Where1Data
        {
            get => this.selector.Where1Data;
            set => this.selector.Where1Data = value;
        }

        public WhereClauseWithValue<T2> Where2Data
        {
            get => this.selector.Where2Data;
            set => this.selector.Where2Data = value;
        }

        public WhereClauseWithValue<T3> Where3Data
        {
            get => this.selector.Where3Data;
            set => this.selector.Where3Data = value;
        }

        public WhereClauseWithValue<T4> Where4Data
        {
            get => this.selector.Where4Data;
            set => this.selector.Where4Data = value;
        }

        public WhereClauseWithValue<T5> Where5Data { get; set; }

        public IDataManipulator Manipulator => this.selector.Manipulator;

        public bool HasFieldsDeclared => this.selector.HasFieldsDeclared;

        public SelectData GetData()
        {
            var data = this.selector.GetData();
            Func<SelectData, SelectData> findNullJoin = null;
            findNullJoin = x =>
            {
                if (x.JoinData == null)
                {
                    return x;
                }

                return findNullJoin(x.JoinData.SelectData);
            };

            var thisData = new SelectData
                           {
                               SchemaName = typeof(T5).Schema(),
                               TableName = typeof(T5).TableName(),
                               Distinct = data.Distinct,
                               FieldList = this.Field5Data.Convert(!this.HasFieldsDeclared),
                               WhereClause = this.Where5Data.Convert(),
                               Offset = data.Offset,
                               Limit = data.Limit,
                               Ordering = this.Order5Data.Convert(),
                               Grouping = this.Group5Data.Convert()
                           };

            var dataToUpdate = findNullJoin(data);
            dataToUpdate.JoinData = new JoinData();

            dataToUpdate.JoinData.JoinType = this.jointype;
            dataToUpdate.JoinData.SelectData = thisData;
            dataToUpdate.JoinData.OnData =
                this.columnJoins.Select(x => Tuple.Create(x.Item1, x.Item2, x.Item3, x.Item4, x.Item5)).ToArray();

            return data;
        }

        public void DeclareFieldsInUse()
        {
            this.selector.DeclareFieldsInUse();
        }

        public Selector5Base<T1, T2, T3, T4, T5> Limit(int limit)
        {
            this.selector.Limit(limit);
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> Offset(int offset)
        {
            this.selector.Offset(offset);
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> WithNoLock()
        {
            this.selector.WithNoLock();
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> Distinct()
        {
            this.selector.Distinct();
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> Where1(Action<IWhereClauseBuilder<T1>> recordSetup)
        {
            this.selector.Where1Data = this.selector.Where1Data ?? new WhereClauseWithValue<T1>();
            recordSetup(this.selector.Where1Data);
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> Where2(Action<IWhereClauseBuilder<T2>> recordSetup)
        {
            this.selector.Where2Data = this.selector.Where2Data ?? new WhereClauseWithValue<T2>();
            recordSetup(this.selector.Where2Data);
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> Where3(Action<IWhereClauseBuilder<T3>> recordSetup)
        {
            this.selector.Where3Data = this.selector.Where3Data ?? new WhereClauseWithValue<T3>();
            recordSetup(this.selector.Where3Data);
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> Where4(Action<IWhereClauseBuilder<T4>> recordSetup)
        {
            this.selector.Where4Data = this.selector.Where4Data ?? new WhereClauseWithValue<T4>();
            recordSetup(this.selector.Where4Data);
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> Where5(Action<IWhereClauseBuilder<T5>> recordSetup)
        {
            var record = this.Where5Data ?? new WhereClauseWithValue<T5>();
            recordSetup(record);
            this.Where5Data = record;
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> Fields1(Action<IFieldListBuilder<T1>> fieldSetup)
        {
            this.selector.Field1Data = this.selector.Field1Data ?? new FieldSelectClause<T1>();
            fieldSetup(this.selector.Field1Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> Fields2(Action<IFieldListBuilder<T2>> fieldSetup)
        {
            this.selector.Field2Data = this.selector.Field2Data ?? new FieldSelectClause<T2>();
            fieldSetup(this.selector.Field2Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> Fields3(Action<IFieldListBuilder<T3>> fieldSetup)
        {
            this.selector.Field3Data = this.selector.Field3Data ?? new FieldSelectClause<T3>();
            fieldSetup(this.selector.Field3Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> Fields4(Action<IFieldListBuilder<T4>> fieldSetup)
        {
            this.selector.Field4Data = this.selector.Field4Data ?? new FieldSelectClause<T4>();
            fieldSetup(this.selector.Field4Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> Fields5(Action<IFieldListBuilder<T5>> fieldSetup)
        {
            var record = this.Field5Data ?? new FieldSelectClause<T5>();
            fieldSetup(record);
            this.Field5Data = record;
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> OrderBy1(Action<IOrderByBuilder<T1>> recordSetup)
        {
            this.selector.Order1Data =
                this.selector.Order1Data ?? new OrderByBuilder<T1>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order1Data);
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> OrderBy2(Action<IOrderByBuilder<T2>> recordSetup)
        {
            this.selector.Order2Data =
                this.selector.Order2Data ?? new OrderByBuilder<T2>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order2Data);
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> OrderBy3(Action<IOrderByBuilder<T3>> recordSetup)
        {
            this.selector.Order3Data =
                this.selector.Order3Data ?? new OrderByBuilder<T3>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order3Data);
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> OrderBy4(Action<IOrderByBuilder<T4>> recordSetup)
        {
            this.selector.Order4Data =
                this.selector.Order4Data ?? new OrderByBuilder<T4>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order4Data);
            return this;
        }

        public Selector5Base<T1, T2, T3, T4, T5> OrderBy5(Action<IOrderByBuilder<T5>> recordSetup)
        {
            var record = this.Order5Data ?? new OrderByBuilder<T5>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(record);
            this.Order5Data = record;
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, TAggregate> GroupBy1<TAggregate>(Action<IGroupByBuilder<T1>> recordSetup)
        {
            this.selector.Group1Data = this.selector.Group1Data ?? new GroupByBuilder<T1>();
            recordSetup(this.selector.Group1Data);
            return new Selector6Base<T1, T2, T3, T4, T5, TAggregate>(
                                                                     this,
                                                                     JoinType.AggregateGrouped,
                                                                     new Tuple<int, Column, OperatorType, int, Column>[0]);
        }

        public Selector6Base<T1, T2, T3, T4, T5, TAggregate> GroupBy2<TAggregate>(Action<IGroupByBuilder<T2>> recordSetup)
        {
            this.selector.Group2Data = this.selector.Group2Data ?? new GroupByBuilder<T2>();
            recordSetup(this.selector.Group2Data);
            return new Selector6Base<T1, T2, T3, T4, T5, TAggregate>(
                                                                     this,
                                                                     JoinType.AggregateGrouped,
                                                                     new Tuple<int, Column, OperatorType, int, Column>[0]);
        }

        public Selector6Base<T1, T2, T3, T4, T5, TAggregate> GroupBy3<TAggregate>(Action<IGroupByBuilder<T3>> recordSetup)
        {
            this.selector.Group3Data = this.selector.Group3Data ?? new GroupByBuilder<T3>();
            recordSetup(this.selector.Group3Data);
            return new Selector6Base<T1, T2, T3, T4, T5, TAggregate>(
                                                                     this,
                                                                     JoinType.AggregateGrouped,
                                                                     new Tuple<int, Column, OperatorType, int, Column>[0]);
        }

        public Selector6Base<T1, T2, T3, T4, T5, TAggregate> GroupBy4<TAggregate>(Action<IGroupByBuilder<T4>> recordSetup)
        {
            this.selector.Group4Data = this.selector.Group4Data ?? new GroupByBuilder<T4>();
            recordSetup(this.selector.Group4Data);
            return new Selector6Base<T1, T2, T3, T4, T5, TAggregate>(
                                                                     this,
                                                                     JoinType.AggregateGrouped,
                                                                     new Tuple<int, Column, OperatorType, int, Column>[0]);
        }

        public Selector6Base<T1, T2, T3, T4, T5, TAggregate> GroupBy5<TAggregate>(Action<IGroupByBuilder<T5>> recordSetup)
        {
            var record = this.Group5Data ?? new GroupByBuilder<T5>();
            recordSetup(record);
            this.Group5Data = record;
            return new Selector6Base<T1, T2, T3, T4, T5, TAggregate>(
                                                                     this,
                                                                     JoinType.AggregateGrouped,
                                                                     new Tuple<int, Column, OperatorType, int, Column>[0]);
        }

        public Selector6Base<T1, T2, T3, T4, T5, TAggregate> WithCalculated<TAggregate>()
            => new Selector6Base<T1, T2, T3, T4, T5, TAggregate>(
                                                                 this,
                                                                 JoinType.Aggregate,
                                                                 new Tuple<int, Column, OperatorType, int, Column>[0]);

        public Selector6Base<T1, T2, T3, T4, T5, TJoin> Join<TJoin>(Expression<Func<T1, T2, T3, T4, T5, TJoin, bool>> joinSetup) =>
            new Selector6Base<T1, T2, T3, T4, T5, TJoin>(
                                                         this,
                                                         JoinType.Inner,
                                                         this.GetJoins(joinSetup, joinSetup.Body));

        public Selector6Base<T1, T2, T3, T4, T5, TJoin> LeftJoin<TJoin>(Expression<Func<T1, T2, T3, T4, T5, TJoin, bool>> joinSetup) =>
            new Selector6Base<T1, T2, T3, T4, T5, TJoin>(
                                                         this,
                                                         JoinType.Left,
                                                         this.GetJoins(joinSetup, joinSetup.Body));

        public Selector6Base<T1, T2, T3, T4, T5, TJoin> RightJoin<TJoin>(Expression<Func<T1, T2, T3, T4, T5, TJoin, bool>> joinSetup) =>
            new Selector6Base<T1, T2, T3, T4, T5, TJoin>(
                                                         this,
                                                         JoinType.Right,
                                                         this.GetJoins(joinSetup, joinSetup.Body));

        public Selector6Base<T1, T2, T3, T4, T5, TJoin> FullJoin<TJoin>(Expression<Func<T1, T2, T3, T4, T5, TJoin, bool>> joinSetup) =>
            new Selector6Base<T1, T2, T3, T4, T5, TJoin>(
                                                         this,
                                                         JoinType.Full,
                                                         this.GetJoins(joinSetup, joinSetup.Body));

        public Selector6Base<T1, T2, T3, T4, T5, TJoin> CrossJoin<TJoin>()
            => new Selector6Base<T1, T2, T3, T4, T5, TJoin>(
                                                            this,
                                                            JoinType.Cross,
                                                            new Tuple<int, Column, OperatorType, int, Column>[0]);
    }
}