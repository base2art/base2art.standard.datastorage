﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Builders;

    internal class SingleSelector4<T1, T2, T3, T4> : IQuerySingleSelect<T1, T2, T3, T4>
    {
        private readonly Selector4Base<T1, T2, T3, T4> proxy;

        public SingleSelector4(Selector4Base<T1, T2, T3, T4> proxy)
        {
            if (proxy == null)
            {
                throw new ArgumentNullException("proxy");
            }

            this.proxy = proxy;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.WithNoLock()
        {
            this.proxy.WithNoLock();
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.Distinct()
        {
            this.proxy.Distinct();
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.Limit(int count)
        {
            this.proxy.Limit(count);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.Offset(int count)
        {
            this.proxy.Offset(count);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.Fields1(Action<IFieldListBuilder<T1>> recordSetup)
        {
            this.proxy.Fields1(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.Fields2(Action<IFieldListBuilder<T2>> recordSetup)
        {
            this.proxy.Fields2(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.Fields3(Action<IFieldListBuilder<T3>> recordSetup)
        {
            this.proxy.Fields3(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.Fields4(Action<IFieldListBuilder<T4>> recordSetup)
        {
            this.proxy.Fields4(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.Where1(Action<IWhereClauseBuilder<T1>> recordSetup)
        {
            this.proxy.Where1(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.Where2(Action<IWhereClauseBuilder<T2>> recordSetup)
        {
            this.proxy.Where2(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.Where3(Action<IWhereClauseBuilder<T3>> recordSetup)
        {
            this.proxy.Where3(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.Where4(Action<IWhereClauseBuilder<T4>> recordSetup)
        {
            this.proxy.Where4(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.OrderBy1(Action<IOrderByBuilder<T1>> recordSetup)
        {
            this.proxy.OrderBy1(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.OrderBy2(Action<IOrderByBuilder<T2>> recordSetup)
        {
            this.proxy.OrderBy2(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.OrderBy3(Action<IOrderByBuilder<T3>> recordSetup)
        {
            this.proxy.OrderBy3(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4> IQuerySingleSelect<T1, T2, T3, T4>.OrderBy4(Action<IOrderByBuilder<T4>> recordSetup)
        {
            this.proxy.OrderBy4(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, TAggregate> IQuerySingleSelect<T1, T2, T3, T4>.GroupBy1<TAggregate>(
            Action<IGroupByBuilder<T1>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy1<TAggregate>(recordSetup);
            return new SingleSelector5<T1, T2, T3, T4, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, TAggregate> IQuerySingleSelect<T1, T2, T3, T4>.GroupBy2<TAggregate>(
            Action<IGroupByBuilder<T2>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy2<TAggregate>(recordSetup);
            return new SingleSelector5<T1, T2, T3, T4, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, TAggregate> IQuerySingleSelect<T1, T2, T3, T4>.GroupBy3<TAggregate>(
            Action<IGroupByBuilder<T3>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy3<TAggregate>(recordSetup);
            return new SingleSelector5<T1, T2, T3, T4, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, TAggregate> IQuerySingleSelect<T1, T2, T3, T4>.GroupBy4<TAggregate>(
            Action<IGroupByBuilder<T4>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy4<TAggregate>(recordSetup);
            return new SingleSelector5<T1, T2, T3, T4, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, TAggregate> IQuerySingleSelect<T1, T2, T3, T4>.WithCalculated<TAggregate>()
        {
            var newProxy = this.proxy.WithCalculated<TAggregate>();
            return new SingleSelector5<T1, T2, T3, T4, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, TJoin> IQuerySingleSelect<T1, T2, T3, T4>.Join<TJoin>(
            Expression<Func<T1, T2, T3, T4, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.Join(joinSetup);
            return new SingleSelector5<T1, T2, T3, T4, TJoin>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, TJoin> IQuerySingleSelect<T1, T2, T3, T4>.LeftJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.LeftJoin(joinSetup);
            return new SingleSelector5<T1, T2, T3, T4, TJoin>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, TJoin> IQuerySingleSelect<T1, T2, T3, T4>.RightJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.RightJoin(joinSetup);
            return new SingleSelector5<T1, T2, T3, T4, TJoin>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, TJoin> IQuerySingleSelect<T1, T2, T3, T4>.FullJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.FullJoin(joinSetup);
            return new SingleSelector5<T1, T2, T3, T4, TJoin>(newProxy);
        }

        IQuerySingleSelect<T1, T2, T3, T4, TJoin> IQuerySingleSelect<T1, T2, T3, T4>.CrossJoin<TJoin>()
        {
            var newProxy = this.proxy.CrossJoin<TJoin>();
            return new SingleSelector5<T1, T2, T3, T4, TJoin>(newProxy);
        }

        public async Task<Tuple<T1, T2, T3, T4>> Execute()
        {
            var result = await this.proxy.Manipulator.SelectAsync<T1, T2, T3, T4>(this.proxy.GetData());
            return result?.FirstOrDefault();
        }
    }
}