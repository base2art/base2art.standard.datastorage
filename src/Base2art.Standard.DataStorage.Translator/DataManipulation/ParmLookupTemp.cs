﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Linq.Expressions;
    using Data;
    using Utils;

    public static class ParmLookupTemp
    {
        private static readonly Dictionary<ExpressionType, OperatorType> map = new Dictionary<ExpressionType, OperatorType>();

        static ParmLookupTemp()
        {
            map[ExpressionType.Add] = OperatorType.Add;
            map[ExpressionType.And] = OperatorType.BitwiseAnd;
            map[ExpressionType.Or] = OperatorType.BitwiseOr;
            map[ExpressionType.AndAlso] = OperatorType.BooleanAnd;
            map[ExpressionType.OrElse] = OperatorType.BooleanOr;
            map[ExpressionType.Divide] = OperatorType.Divide;
            map[ExpressionType.GreaterThan] = OperatorType.GreaterThan;
            map[ExpressionType.GreaterThanOrEqual] = OperatorType.GreaterThanOrEqual;
            map[ExpressionType.Equal] = OperatorType.IdentityEquality;
            map[ExpressionType.NotEqual] = OperatorType.IdentityInequality;
            map[ExpressionType.LessThan] = OperatorType.LessThan;
            map[ExpressionType.LessThanOrEqual] = OperatorType.LessThanOrEqual;
            map[ExpressionType.Modulo] = OperatorType.Modulus;
            map[ExpressionType.Multiply] = OperatorType.Multiply;
            map[ExpressionType.Subtract] = OperatorType.Subtract;
        }

        public static IReadOnlyDictionary<ExpressionType, OperatorType> GetExpressionTypeMap() => map;

        public static Tuple<int, Column, OperatorType, int, Column>[] GetJoins(
            this IDataGetter<SelectData> getter,
            LambdaExpression joinSetup,
            Expression baseExpr)
        {
            if (baseExpr is BinaryExpression expr)
            {
                return GetJoinsBin(getter, joinSetup, expr);
            }

            if (baseExpr is MethodCallExpression expr1)
            {
                return GetJoinsMethod(getter, joinSetup, expr1);
            }

            return new Tuple<int, Column, OperatorType, int, Column>[0];
        }

        public static Tuple<int, Column, OperatorType, int, Column>[] GetJoinsBin(
            this IDataGetter<SelectData> getter,
            LambdaExpression joinSetup,
            BinaryExpression bin)
        {
            var exprs = new List<BinaryExpression>();
            AppendExpressions(bin, exprs);
            return exprs.Select(x =>
                        {
                            var tables = GetTableNames(joinSetup.Parameters, x);

                            var leftMi = x.Left.GetMemberInfo(tables.Item2);
                            var rightMi = x.Right.GetMemberInfo(tables.Item4);

                            var opOf = OperatorType.IdentityEquality;

                            var map = GetExpressionTypeMap();
                            if (map.ContainsKey(x.NodeType))
                            {
                                opOf = map[x.NodeType];
                            }

                            return Tuple.Create(tables.Item1, leftMi, opOf, tables.Item3, rightMi);
                        })
                        .ToArray();
        }

        public static Tuple<int, Column, OperatorType, int, Column>[] GetJoinsMethod(
            this IDataGetter<SelectData> getter,
            LambdaExpression joinSetup,
            MethodCallExpression bin)
        {
            var methodName = bin.Method.Name;

            var item = bin.Object;
            var arg = bin.Arguments[0];

            var setupParms = joinSetup.Parameters;

            switch (methodName)
            {
                case nameof(string.Contains):
                {
                    var col1 = item.GetMemberInfo(setupParms);
                    var col2 = arg.GetMemberInfo(setupParms);
                    var col2_a = col2.Item2;
                    col2_a.ColumnMetaData.Prefix = "%";
                    col2_a.ColumnMetaData.Suffix = "%";
                    return new[] {Tuple.Create(col1.Item1, col1.Item2, OperatorType.Like, col2.Item1, col2_a)};
                }
                case nameof(string.EndsWith):
                {
                    var col1 = item.GetMemberInfo(setupParms);
                    var col2 = arg.GetMemberInfo(setupParms);
                    var col2_a = col2.Item2;
                    col2_a.ColumnMetaData.Suffix = "%";
                    return new[] {Tuple.Create(col1.Item1, col1.Item2, OperatorType.Like, col2.Item1, col2_a)};
                }
                case nameof(string.StartsWith):
                {
                    var col1 = item.GetMemberInfo(setupParms);
                    var col2 = arg.GetMemberInfo(setupParms);
                    var col2_a = col2.Item2;
                    col2_a.ColumnMetaData.Prefix = "%";
                    return new[] {Tuple.Create(col1.Item1, col1.Item2, OperatorType.Like, col2.Item1, col2_a)};
                }
            }

            throw new ArgumentOutOfRangeException();
        }

        private static Tuple<int, string, int, string> GetTableNames(
            ReadOnlyCollection<ParameterExpression> joinSetupParameters,
            BinaryExpression bin)
        {
            var parmNames = joinSetupParameters.Select(x => x.Name).ToArray();
            var parmTypes = joinSetupParameters.Select(x => x.Type.TableName()).ToArray();

            //            var left = joinSetupParameters[0].Name;
            //            var center = joinSetupParameters[1].Name;
            //            var right = joinSetupParameters[2].Name;
            //            var far_right = joinSetupParameters[3].Name;

            var i1 = -1;
            var i2 = -1;
            string l1 = null;
            string l2 = null;

            for (var i = 0; i < parmNames.Length; i++)
            {
                if (bin.Left.IsParameterUsed(parmNames[i]))
                {
                    l1 = parmTypes[i];
                    i1 = i;
                }

                if (bin.Right.IsParameterUsed(parmNames[i]))
                {
                    l2 = parmTypes[i];
                    i2 = i;
                }
            }

            if (string.IsNullOrWhiteSpace(l1) || string.IsNullOrWhiteSpace(l2))
            {
                throw new InvalidOperationException("must use table as parameter id");
            }

            return Tuple.Create(i1, l1, i2, l2);
        }

        private static void AppendExpressions(BinaryExpression bin, List<BinaryExpression> exprs)
        {
            if (bin.NodeType == ExpressionType.AndAlso)
            {
                AppendExpressions(bin.Left as BinaryExpression, exprs);
                AppendExpressions(bin.Right as BinaryExpression, exprs);
            }
            else
            {
                exprs.Add(bin);
            }
        }
    }
}