﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Builders;
    using Data;

    internal class QuerySelector3<T1, T2, T3> : IQuerySelect<T1, T2, T3>, IDataGetter<SelectData>
    {
        private readonly Selector3Base<T1, T2, T3> proxy;

        public QuerySelector3(Selector3Base<T1, T2, T3> proxy)
        {
            if (proxy == null)
            {
                throw new ArgumentNullException("proxy");
            }

            this.proxy = proxy;
        }

        SelectData IDataGetter<SelectData>.GetData() => this.proxy.GetData();

        IQuerySelect<T1, T2, T3> IQuerySelect<T1, T2, T3>.WithNoLock()
        {
            this.proxy.WithNoLock();
            return this;
        }

        IQuerySelect<T1, T2, T3> IQuerySelect<T1, T2, T3>.Distinct()
        {
            this.proxy.Distinct();
            return this;
        }

        IQuerySelect<T1, T2, T3> IQuerySelect<T1, T2, T3>.Limit(int count)
        {
            this.proxy.Limit(count);
            return this;
        }

        IQuerySelect<T1, T2, T3> IQuerySelect<T1, T2, T3>.Offset(int count)
        {
            this.proxy.Offset(count);
            return this;
        }

        IQuerySelect<T1, T2, T3> IQuerySelect<T1, T2, T3>.Fields1(Action<IFieldListBuilder<T1>> recordSetup)
        {
            this.proxy.Fields1(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3> IQuerySelect<T1, T2, T3>.Fields2(Action<IFieldListBuilder<T2>> recordSetup)
        {
            this.proxy.Fields2(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3> IQuerySelect<T1, T2, T3>.Fields3(Action<IFieldListBuilder<T3>> recordSetup)
        {
            this.proxy.Fields3(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3> IQuerySelect<T1, T2, T3>.Where1(Action<IWhereClauseBuilder<T1>> recordSetup)
        {
            this.proxy.Where1(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3> IQuerySelect<T1, T2, T3>.Where2(Action<IWhereClauseBuilder<T2>> recordSetup)
        {
            this.proxy.Where2(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3> IQuerySelect<T1, T2, T3>.Where3(Action<IWhereClauseBuilder<T3>> recordSetup)
        {
            this.proxy.Where3(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3> IQuerySelect<T1, T2, T3>.OrderBy1(Action<IOrderByBuilder<T1>> recordSetup)
        {
            this.proxy.OrderBy1(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3> IQuerySelect<T1, T2, T3>.OrderBy2(Action<IOrderByBuilder<T2>> recordSetup)
        {
            this.proxy.OrderBy2(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3> IQuerySelect<T1, T2, T3>.OrderBy3(Action<IOrderByBuilder<T3>> recordSetup)
        {
            this.proxy.OrderBy3(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, TAggregate> IQuerySelect<T1, T2, T3>.GroupBy1<TAggregate>(Action<IGroupByBuilder<T1>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy1<TAggregate>(recordSetup);
            return new QuerySelector4<T1, T2, T3, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, T3, TAggregate> IQuerySelect<T1, T2, T3>.GroupBy2<TAggregate>(Action<IGroupByBuilder<T2>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy2<TAggregate>(recordSetup);
            return new QuerySelector4<T1, T2, T3, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, T3, TAggregate> IQuerySelect<T1, T2, T3>.GroupBy3<TAggregate>(Action<IGroupByBuilder<T3>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy3<TAggregate>(recordSetup);
            return new QuerySelector4<T1, T2, T3, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, T3, TAggregate> IQuerySelect<T1, T2, T3>.WithCalculated<TAggregate>()
        {
            var newProxy = this.proxy.WithCalculated<TAggregate>();
            return new QuerySelector4<T1, T2, T3, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, T3, TJoin> IQuerySelect<T1, T2, T3>.Join<TJoin>(Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.Join(joinSetup);
            return new QuerySelector4<T1, T2, T3, TJoin>(newProxy);
        }

        IQuerySelect<T1, T2, T3, TJoin> IQuerySelect<T1, T2, T3>.LeftJoin<TJoin>(Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.LeftJoin(joinSetup);
            return new QuerySelector4<T1, T2, T3, TJoin>(newProxy);
        }

        IQuerySelect<T1, T2, T3, TJoin> IQuerySelect<T1, T2, T3>.RightJoin<TJoin>(Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.RightJoin(joinSetup);
            return new QuerySelector4<T1, T2, T3, TJoin>(newProxy);
        }

        IQuerySelect<T1, T2, T3, TJoin> IQuerySelect<T1, T2, T3>.FullJoin<TJoin>(Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.FullJoin(joinSetup);
            return new QuerySelector4<T1, T2, T3, TJoin>(newProxy);
        }

        IQuerySelect<T1, T2, T3, TJoin> IQuerySelect<T1, T2, T3>.CrossJoin<TJoin>()
        {
            var newProxy = this.proxy.CrossJoin<TJoin>();
            return new QuerySelector4<T1, T2, T3, TJoin>(newProxy);
        }

        public Task<IEnumerable<Tuple<T1, T2, T3>>> Execute() => this.proxy.Manipulator.SelectAsync<T1, T2, T3>(this.proxy.GetData());

        public IEnumerator<Tuple<T1, T2, T3>> GetEnumerator() => this.proxy.Manipulator.Select<T1, T2, T3>(this.proxy.GetData()).GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}