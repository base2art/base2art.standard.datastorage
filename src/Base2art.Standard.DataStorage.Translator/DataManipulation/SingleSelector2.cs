﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Builders;

    internal class SingleSelector2<T1, T2> : IQuerySingleSelect<T1, T2>
    {
        private readonly Selector2Base<T1, T2> proxy;

        public SingleSelector2(Selector2Base<T1, T2> selector) => this.proxy = selector;

        IQuerySingleSelect<T1, T2> IQuerySingleSelect<T1, T2>.WithNoLock()
        {
            this.proxy.WithNoLock();
            return this;
        }

        IQuerySingleSelect<T1, T2> IQuerySingleSelect<T1, T2>.Distinct()
        {
            this.proxy.Distinct();
            return this;
        }

        IQuerySingleSelect<T1, T2> IQuerySingleSelect<T1, T2>.Limit(int count)
        {
            this.proxy.Limit(count);
            return this;
        }

        IQuerySingleSelect<T1, T2> IQuerySingleSelect<T1, T2>.Offset(int count)
        {
            this.proxy.Offset(count);
            return this;
        }

        IQuerySingleSelect<T1, T2> IQuerySingleSelect<T1, T2>.Fields1(Action<IFieldListBuilder<T1>> recordSetup)
        {
            this.proxy.Fields1(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2> IQuerySingleSelect<T1, T2>.Fields2(Action<IFieldListBuilder<T2>> recordSetup)
        {
            this.proxy.Fields2(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2> IQuerySingleSelect<T1, T2>.Where1(Action<IWhereClauseBuilder<T1>> recordSetup)
        {
            this.proxy.Where1(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2> IQuerySingleSelect<T1, T2>.Where2(Action<IWhereClauseBuilder<T2>> recordSetup)
        {
            this.proxy.Where2(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2> IQuerySingleSelect<T1, T2>.OrderBy1(Action<IOrderByBuilder<T1>> recordSetup)
        {
            this.proxy.OrderBy1(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2> IQuerySingleSelect<T1, T2>.OrderBy2(Action<IOrderByBuilder<T2>> recordSetup)
        {
            this.proxy.OrderBy2(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, TAggregate> IQuerySingleSelect<T1, T2>.GroupBy1<TAggregate>(Action<IGroupByBuilder<T1>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy1<TAggregate>(recordSetup);
            return new SingleSelector3<T1, T2, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, TAggregate> IQuerySingleSelect<T1, T2>.GroupBy2<TAggregate>(Action<IGroupByBuilder<T2>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy2<TAggregate>(recordSetup);
            return new SingleSelector3<T1, T2, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, TAggregate> IQuerySingleSelect<T1, T2>.WithCalculated<TAggregate>()
        {
            var newProxy = this.proxy.WithCalculated<TAggregate>();
            return new SingleSelector3<T1, T2, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T1, T2, TJoin> IQuerySingleSelect<T1, T2>.Join<TJoin>(Expression<Func<T1, T2, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.Join(joinSetup);
            return new SingleSelector3<T1, T2, TJoin>(newProxy);
        }

        IQuerySingleSelect<T1, T2, TJoin> IQuerySingleSelect<T1, T2>.LeftJoin<TJoin>(Expression<Func<T1, T2, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.LeftJoin(joinSetup);
            return new SingleSelector3<T1, T2, TJoin>(newProxy);
        }

        IQuerySingleSelect<T1, T2, TJoin> IQuerySingleSelect<T1, T2>.RightJoin<TJoin>(Expression<Func<T1, T2, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.RightJoin(joinSetup);
            return new SingleSelector3<T1, T2, TJoin>(newProxy);
        }

        IQuerySingleSelect<T1, T2, TJoin> IQuerySingleSelect<T1, T2>.FullJoin<TJoin>(Expression<Func<T1, T2, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.FullJoin(joinSetup);
            return new SingleSelector3<T1, T2, TJoin>(newProxy);
        }

        IQuerySingleSelect<T1, T2, TJoin> IQuerySingleSelect<T1, T2>.CrossJoin<TJoin>()
        {
            var newProxy = this.proxy.CrossJoin<TJoin>();
            return new SingleSelector3<T1, T2, TJoin>(newProxy);
        }

        public async Task<Tuple<T1, T2>> Execute()
        {
            var result = await this.proxy.Manipulator.SelectAsync<T1, T2>(this.proxy.GetData());
            return result?.FirstOrDefault();
        }
    }
}