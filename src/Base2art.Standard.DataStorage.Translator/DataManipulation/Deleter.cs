﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Threading.Tasks;
    using Builders;
    using Clauses;

    internal class Deleter<T> : IQueryDelete<T>
    {
        private readonly IDataManipulator manipulator;
        private WhereClauseWithValue<T> wheres;

        public Deleter(IDataManipulator manipulator) => this.manipulator = manipulator;

        public IQueryDelete<T> Where(Action<IWhereClauseBuilder<T>> recordSetup)
        {
            var record = new WhereClauseWithValue<T>();
            recordSetup(record);
            this.wheres = record;
            return this;
        }

        public Task Execute()
        {
            var type = typeof(T);
            return this.manipulator.DeleteAsync(type.Schema(), type.TableName(), this.wheres.Convert());
        }
    }
}