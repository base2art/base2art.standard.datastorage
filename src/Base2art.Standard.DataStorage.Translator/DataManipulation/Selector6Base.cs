﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using Builders;
    using Clauses;
    using Data;

    internal class Selector6Base<T1, T2, T3, T4, T5, T6> : IDataGetter<SelectData>
    {
        private readonly Tuple<int, Column, OperatorType, int, Column>[] columnJoins;

        private readonly JoinType jointype;
        private readonly Selector5Base<T1, T2, T3, T4, T5> selector;

        public Selector6Base(
            Selector5Base<T1, T2, T3, T4, T5> selector,
            JoinType jointype,
            Tuple<int, Column, OperatorType, int, Column>[] columnJoins)
        {
            this.selector = selector;
            this.jointype = jointype;
            this.columnJoins = columnJoins;
        }

        public FieldSelectClause<T1> Field1Data
        {
            get => this.selector.Field1Data;
            set => this.selector.Field1Data = value;
        }

        public FieldSelectClause<T2> Field2Data
        {
            get => this.selector.Field2Data;
            set => this.selector.Field2Data = value;
        }

        public FieldSelectClause<T3> Field3Data
        {
            get => this.selector.Field3Data;
            set => this.selector.Field3Data = value;
        }

        public FieldSelectClause<T4> Field4Data
        {
            get => this.selector.Field4Data;
            set => this.selector.Field4Data = value;
        }

        public FieldSelectClause<T5> Field5Data
        {
            get => this.selector.Field5Data;
            set => this.selector.Field5Data = value;
        }

        internal FieldSelectClause<T6> Field6Data { get; set; }

        internal OrderByBuilder<T1> Order1Data
        {
            get => this.selector.Order1Data;
            set => this.selector.Order1Data = value;
        }

        internal OrderByBuilder<T2> Order2Data
        {
            get => this.selector.Order2Data;
            set => this.selector.Order2Data = value;
        }

        internal OrderByBuilder<T3> Order3Data
        {
            get => this.selector.Order3Data;
            set => this.selector.Order3Data = value;
        }

        internal OrderByBuilder<T4> Order4Data
        {
            get => this.selector.Order4Data;
            set => this.selector.Order4Data = value;
        }

        internal OrderByBuilder<T5> Order5Data
        {
            get => this.selector.Order5Data;
            set => this.selector.Order5Data = value;
        }

        internal OrderByBuilder<T6> Order6Data { get; set; }

        public WhereClauseWithValue<T1> Where1Data
        {
            get => this.selector.Where1Data;
            set => this.selector.Where1Data = value;
        }

        public WhereClauseWithValue<T2> Where2Data
        {
            get => this.selector.Where2Data;
            set => this.selector.Where2Data = value;
        }

        public WhereClauseWithValue<T3> Where3Data
        {
            get => this.selector.Where3Data;
            set => this.selector.Where3Data = value;
        }

        public WhereClauseWithValue<T4> Where4Data
        {
            get => this.selector.Where4Data;
            set => this.selector.Where4Data = value;
        }

        public WhereClauseWithValue<T5> Where5Data
        {
            get => this.selector.Where5Data;
            set => this.selector.Where5Data = value;
        }

        public WhereClauseWithValue<T6> Where6Data { get; set; }

        internal GroupByBuilder<T1> Group1Data
        {
            get => this.selector.Group1Data;
            set => this.selector.Group1Data = value;
        }

        internal GroupByBuilder<T2> Group2Data
        {
            get => this.selector.Group2Data;
            set => this.selector.Group2Data = value;
        }

        internal GroupByBuilder<T3> Group3Data
        {
            get => this.selector.Group3Data;
            set => this.selector.Group3Data = value;
        }

        internal GroupByBuilder<T4> Group4Data
        {
            get => this.selector.Group4Data;
            set => this.selector.Group4Data = value;
        }

        internal GroupByBuilder<T5> Group5Data
        {
            get => this.selector.Group5Data;
            set => this.selector.Group5Data = value;
        }

        internal GroupByBuilder<T6> Group6Data { get; set; }

        public IDataManipulator Manipulator => this.selector.Manipulator;

        public bool HasFieldsDeclared => this.selector.HasFieldsDeclared;

        public SelectData GetData()
        {
            var data = this.selector.GetData();
            Func<SelectData, SelectData> findNullJoin = null;
            findNullJoin = x =>
            {
                if (x.JoinData == null)
                {
                    return x;
                }

                return findNullJoin(x.JoinData.SelectData);
            };

            var thisData = new SelectData
                           {
                               SchemaName = typeof(T6).Schema(),
                               TableName = typeof(T6).TableName(),
                               Distinct = data.Distinct,
                               FieldList = this.Field6Data.Convert(!this.HasFieldsDeclared),
                               WhereClause = this.Where6Data.Convert(),
                               Offset = data.Offset,
                               Limit = data.Limit,
                               Ordering = this.Order6Data.Convert(),
                               Grouping = this.Group6Data.Convert()
                           };

            var dataToUpdate = findNullJoin(data);
            dataToUpdate.JoinData = new JoinData();

            dataToUpdate.JoinData.JoinType = this.jointype;
            dataToUpdate.JoinData.SelectData = thisData;
            dataToUpdate.JoinData.OnData =
                this.columnJoins.Select(x => Tuple.Create(x.Item1, x.Item2, x.Item3, x.Item4, x.Item5)).ToArray();

            return data;
        }

        public void DeclareFieldsInUse()
        {
            this.selector.DeclareFieldsInUse();
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Limit(int limit)
        {
            this.selector.Limit(limit);
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Offset(int offset)
        {
            this.selector.Offset(offset);
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> WithNoLock()
        {
            this.selector.WithNoLock();
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Distinct()
        {
            this.selector.Distinct();
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Where1(Action<IWhereClauseBuilder<T1>> recordSetup)
        {
            this.selector.Where1Data = this.selector.Where1Data ?? new WhereClauseWithValue<T1>();
            recordSetup(this.selector.Where1Data);
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Where2(Action<IWhereClauseBuilder<T2>> recordSetup)
        {
            this.selector.Where2Data = this.selector.Where2Data ?? new WhereClauseWithValue<T2>();
            recordSetup(this.selector.Where2Data);
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Where3(Action<IWhereClauseBuilder<T3>> recordSetup)
        {
            this.selector.Where3Data = this.selector.Where3Data ?? new WhereClauseWithValue<T3>();
            recordSetup(this.selector.Where3Data);
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Where4(Action<IWhereClauseBuilder<T4>> recordSetup)
        {
            this.selector.Where4Data = this.selector.Where4Data ?? new WhereClauseWithValue<T4>();
            recordSetup(this.selector.Where4Data);
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Where5(Action<IWhereClauseBuilder<T5>> recordSetup)
        {
            this.selector.Where5Data = this.selector.Where5Data ?? new WhereClauseWithValue<T5>();
            recordSetup(this.selector.Where5Data);
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Where6(Action<IWhereClauseBuilder<T6>> recordSetup)
        {
            var record = this.Where6Data ?? new WhereClauseWithValue<T6>();
            recordSetup(record);
            this.Where6Data = record;
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Fields1(Action<IFieldListBuilder<T1>> fieldSetup)
        {
            this.selector.Field1Data = this.selector.Field1Data ?? new FieldSelectClause<T1>();
            fieldSetup(this.selector.Field1Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Fields2(Action<IFieldListBuilder<T2>> fieldSetup)
        {
            this.selector.Field2Data = this.selector.Field2Data ?? new FieldSelectClause<T2>();
            fieldSetup(this.selector.Field2Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Fields3(Action<IFieldListBuilder<T3>> fieldSetup)
        {
            this.selector.Field3Data = this.selector.Field3Data ?? new FieldSelectClause<T3>();
            fieldSetup(this.selector.Field3Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Fields4(Action<IFieldListBuilder<T4>> fieldSetup)
        {
            this.selector.Field4Data = this.selector.Field4Data ?? new FieldSelectClause<T4>();
            fieldSetup(this.selector.Field4Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Fields5(Action<IFieldListBuilder<T5>> fieldSetup)
        {
            this.selector.Field5Data = this.selector.Field5Data ?? new FieldSelectClause<T5>();
            fieldSetup(this.selector.Field5Data);
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> Fields6(Action<IFieldListBuilder<T6>> fieldSetup)
        {
            var record = this.Field6Data ?? new FieldSelectClause<T6>();
            fieldSetup(record);
            this.Field6Data = record;
            this.DeclareFieldsInUse();
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> OrderBy1(Action<IOrderByBuilder<T1>> recordSetup)
        {
            this.selector.Order1Data =
                this.selector.Order1Data ?? new OrderByBuilder<T1>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order1Data);
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> OrderBy2(Action<IOrderByBuilder<T2>> recordSetup)
        {
            this.selector.Order2Data =
                this.selector.Order2Data ?? new OrderByBuilder<T2>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order2Data);
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> OrderBy3(Action<IOrderByBuilder<T3>> recordSetup)
        {
            this.selector.Order3Data =
                this.selector.Order3Data ?? new OrderByBuilder<T3>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order3Data);
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> OrderBy4(Action<IOrderByBuilder<T4>> recordSetup)
        {
            this.selector.Order4Data =
                this.selector.Order4Data ?? new OrderByBuilder<T4>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order4Data);
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> OrderBy5(Action<IOrderByBuilder<T5>> recordSetup)
        {
            this.selector.Order5Data =
                this.selector.Order5Data ?? new OrderByBuilder<T5>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(this.selector.Order5Data);
            return this;
        }

        public Selector6Base<T1, T2, T3, T4, T5, T6> OrderBy6(Action<IOrderByBuilder<T6>> recordSetup)
        {
            var record = this.Order6Data ?? new OrderByBuilder<T6>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(record);
            this.Order6Data = record;
            return this;
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate> GroupBy1<TAggregate>(Action<IGroupByBuilder<T1>> recordSetup)
        {
            this.selector.Group1Data = this.selector.Group1Data ?? new GroupByBuilder<T1>();
            recordSetup(this.selector.Group1Data);
            return new Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate>(
                                                                         this,
                                                                         JoinType.AggregateGrouped,
                                                                         new Tuple<int, Column, OperatorType, int, Column>[0]);
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate> GroupBy2<TAggregate>(Action<IGroupByBuilder<T2>> recordSetup)
        {
            this.selector.Group2Data = this.selector.Group2Data ?? new GroupByBuilder<T2>();
            recordSetup(this.selector.Group2Data);
            return new Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate>(
                                                                         this,
                                                                         JoinType.AggregateGrouped,
                                                                         new Tuple<int, Column, OperatorType, int, Column>[0]);
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate> GroupBy3<TAggregate>(Action<IGroupByBuilder<T3>> recordSetup)
        {
            this.selector.Group3Data = this.selector.Group3Data ?? new GroupByBuilder<T3>();
            recordSetup(this.selector.Group3Data);
            return new Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate>(
                                                                         this,
                                                                         JoinType.AggregateGrouped,
                                                                         new Tuple<int, Column, OperatorType, int, Column>[0]);
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate> GroupBy4<TAggregate>(Action<IGroupByBuilder<T4>> recordSetup)
        {
            this.selector.Group4Data = this.selector.Group4Data ?? new GroupByBuilder<T4>();
            recordSetup(this.selector.Group4Data);
            return new Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate>(
                                                                         this,
                                                                         JoinType.AggregateGrouped,
                                                                         new Tuple<int, Column, OperatorType, int, Column>[0]);
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate> GroupBy5<TAggregate>(Action<IGroupByBuilder<T5>> recordSetup)
        {
            this.selector.Group5Data = this.selector.Group5Data ?? new GroupByBuilder<T5>();
            recordSetup(this.selector.Group5Data);
            return new Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate>(
                                                                         this,
                                                                         JoinType.AggregateGrouped,
                                                                         new Tuple<int, Column, OperatorType, int, Column>[0]);
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate> GroupBy6<TAggregate>(Action<IGroupByBuilder<T6>> recordSetup)
        {
            var record = this.Group6Data ?? new GroupByBuilder<T6>();
            recordSetup(record);
            this.Group6Data = record;
            return new Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate>(
                                                                         this,
                                                                         JoinType.AggregateGrouped,
                                                                         new Tuple<int, Column, OperatorType, int, Column>[0]);
        }

        public Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate> WithCalculated<TAggregate>() =>
            new Selector7Base<T1, T2, T3, T4, T5, T6, TAggregate>(
                                                                  this,
                                                                  JoinType.Aggregate,
                                                                  new Tuple<int, Column, OperatorType, int, Column>[0]);

        public Selector7Base<T1, T2, T3, T4, T5, T6, TJoin> Join<TJoin>(Expression<Func<T1, T2, T3, T4, T5, T6, TJoin, bool>> joinSetup) =>
            new Selector7Base<T1, T2, T3, T4, T5, T6, TJoin>(
                                                             this,
                                                             JoinType.Inner,
                                                             this.GetJoins(joinSetup, joinSetup.Body));

        public Selector7Base<T1, T2, T3, T4, T5, T6, TJoin> LeftJoin<TJoin>(Expression<Func<T1, T2, T3, T4, T5, T6, TJoin, bool>> joinSetup) =>
            new Selector7Base<T1, T2, T3, T4, T5, T6, TJoin>(
                                                             this,
                                                             JoinType.Left,
                                                             this.GetJoins(joinSetup, joinSetup.Body));

        public Selector7Base<T1, T2, T3, T4, T5, T6, TJoin> RightJoin<TJoin>(Expression<Func<T1, T2, T3, T4, T5, T6, TJoin, bool>> joinSetup) =>
            new Selector7Base<T1, T2, T3, T4, T5, T6, TJoin>(
                                                             this,
                                                             JoinType.Right,
                                                             this.GetJoins(joinSetup, joinSetup.Body));

        public Selector7Base<T1, T2, T3, T4, T5, T6, TJoin> FullJoin<TJoin>(Expression<Func<T1, T2, T3, T4, T5, T6, TJoin, bool>> joinSetup) =>
            new Selector7Base<T1, T2, T3, T4, T5, T6, TJoin>(
                                                             this,
                                                             JoinType.Full,
                                                             this.GetJoins(joinSetup, joinSetup.Body));

        public Selector7Base<T1, T2, T3, T4, T5, T6, TJoin> CrossJoin<TJoin>()
            => new Selector7Base<T1, T2, T3, T4, T5, T6, TJoin>(
                                                                this,
                                                                JoinType.Cross,
                                                                new Tuple<int, Column, OperatorType, int, Column>[0]);
    }
}