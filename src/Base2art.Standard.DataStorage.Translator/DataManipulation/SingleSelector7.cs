﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Builders;

    internal class SingleSelector7<T1, T2, T3, T4, T5, T6, T7> : IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>
    {
        private readonly Selector7Base<T1, T2, T3, T4, T5, T6, T7> proxy;

        public SingleSelector7(Selector7Base<T1, T2, T3, T4, T5, T6, T7> proxy)
        {
            if (proxy == null)
            {
                throw new ArgumentNullException("proxy");
            }

            this.proxy = proxy;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.WithNoLock()
        {
            this.proxy.WithNoLock();
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Distinct()
        {
            this.proxy.Distinct();
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Limit(int count)
        {
            this.proxy.Limit(count);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Offset(int count)
        {
            this.proxy.Offset(count);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Fields1(
            Action<IFieldListBuilder<T1>> recordSetup)
        {
            this.proxy.Fields1(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Fields2(
            Action<IFieldListBuilder<T2>> recordSetup)
        {
            this.proxy.Fields2(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Fields3(
            Action<IFieldListBuilder<T3>> recordSetup)
        {
            this.proxy.Fields3(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Fields4(
            Action<IFieldListBuilder<T4>> recordSetup)
        {
            this.proxy.Fields4(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Fields5(
            Action<IFieldListBuilder<T5>> recordSetup)
        {
            this.proxy.Fields5(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Fields6(
            Action<IFieldListBuilder<T6>> recordSetup)
        {
            this.proxy.Fields6(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Fields7(
            Action<IFieldListBuilder<T7>> recordSetup)
        {
            this.proxy.Fields7(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Where1(
            Action<IWhereClauseBuilder<T1>> recordSetup)
        {
            this.proxy.Where1(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Where2(
            Action<IWhereClauseBuilder<T2>> recordSetup)
        {
            this.proxy.Where2(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Where3(
            Action<IWhereClauseBuilder<T3>> recordSetup)
        {
            this.proxy.Where3(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Where4(
            Action<IWhereClauseBuilder<T4>> recordSetup)
        {
            this.proxy.Where4(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Where5(
            Action<IWhereClauseBuilder<T5>> recordSetup)
        {
            this.proxy.Where5(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Where6(
            Action<IWhereClauseBuilder<T6>> recordSetup)
        {
            this.proxy.Where6(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.Where7(
            Action<IWhereClauseBuilder<T7>> recordSetup)
        {
            this.proxy.Where7(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.OrderBy1(
            Action<IOrderByBuilder<T1>> recordSetup)
        {
            this.proxy.OrderBy1(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.OrderBy2(
            Action<IOrderByBuilder<T2>> recordSetup)
        {
            this.proxy.OrderBy2(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.OrderBy3(
            Action<IOrderByBuilder<T3>> recordSetup)
        {
            this.proxy.OrderBy3(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.OrderBy4(
            Action<IOrderByBuilder<T4>> recordSetup)
        {
            this.proxy.OrderBy4(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.OrderBy5(
            Action<IOrderByBuilder<T5>> recordSetup)
        {
            this.proxy.OrderBy5(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.OrderBy6(
            Action<IOrderByBuilder<T6>> recordSetup)
        {
            this.proxy.OrderBy6(recordSetup);
            return this;
        }

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>.OrderBy7(
            Action<IOrderByBuilder<T7>> recordSetup)
        {
            this.proxy.OrderBy7(recordSetup);
            return this;
        }

        public async Task<Tuple<T1, T2, T3, T4, T5, T6, T7>> Execute()
        {
            var result = await this.proxy.Manipulator.SelectAsync<T1, T2, T3, T4, T5, T6, T7>(this.proxy.GetData());
            return result?.FirstOrDefault();
        }
    }
}