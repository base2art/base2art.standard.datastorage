﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Builders;
    using Data;

    internal class QuerySelector2<T1, T2> : IQuerySelect<T1, T2>, IDataGetter<SelectData>
    {
        private readonly Selector2Base<T1, T2> proxy;

        public QuerySelector2(Selector2Base<T1, T2> selector) => this.proxy = selector;

        SelectData IDataGetter<SelectData>.GetData() => this.proxy.GetData();

        IQuerySelect<T1, T2> IQuerySelect<T1, T2>.WithNoLock()
        {
            this.proxy.WithNoLock();
            return this;
        }

        IQuerySelect<T1, T2> IQuerySelect<T1, T2>.Distinct()
        {
            this.proxy.Distinct();
            return this;
        }

        IQuerySelect<T1, T2> IQuerySelect<T1, T2>.Limit(int count)
        {
            this.proxy.Limit(count);
            return this;
        }

        IQuerySelect<T1, T2> IQuerySelect<T1, T2>.Offset(int count)
        {
            this.proxy.Offset(count);
            return this;
        }

        IQuerySelect<T1, T2> IQuerySelect<T1, T2>.Fields1(Action<IFieldListBuilder<T1>> recordSetup)
        {
            this.proxy.Fields1(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2> IQuerySelect<T1, T2>.Fields2(Action<IFieldListBuilder<T2>> recordSetup)
        {
            this.proxy.Fields2(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2> IQuerySelect<T1, T2>.Where1(Action<IWhereClauseBuilder<T1>> recordSetup)
        {
            this.proxy.Where1(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2> IQuerySelect<T1, T2>.Where2(Action<IWhereClauseBuilder<T2>> recordSetup)
        {
            this.proxy.Where2(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2> IQuerySelect<T1, T2>.OrderBy1(Action<IOrderByBuilder<T1>> recordSetup)
        {
            this.proxy.OrderBy1(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2> IQuerySelect<T1, T2>.OrderBy2(Action<IOrderByBuilder<T2>> recordSetup)
        {
            this.proxy.OrderBy2(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, TAggregate> IQuerySelect<T1, T2>.GroupBy1<TAggregate>(Action<IGroupByBuilder<T1>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy1<TAggregate>(recordSetup);
            return new QuerySelector3<T1, T2, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, TAggregate> IQuerySelect<T1, T2>.GroupBy2<TAggregate>(Action<IGroupByBuilder<T2>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy2<TAggregate>(recordSetup);
            return new QuerySelector3<T1, T2, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, TAggregate> IQuerySelect<T1, T2>.WithCalculated<TAggregate>()
        {
            var newProxy = this.proxy.WithCalculated<TAggregate>();
            return new QuerySelector3<T1, T2, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, TJoin> IQuerySelect<T1, T2>.Join<TJoin>(Expression<Func<T1, T2, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.Join(joinSetup);
            return new QuerySelector3<T1, T2, TJoin>(newProxy);
        }

        IQuerySelect<T1, T2, TJoin> IQuerySelect<T1, T2>.LeftJoin<TJoin>(Expression<Func<T1, T2, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.LeftJoin(joinSetup);
            return new QuerySelector3<T1, T2, TJoin>(newProxy);
        }

        IQuerySelect<T1, T2, TJoin> IQuerySelect<T1, T2>.RightJoin<TJoin>(Expression<Func<T1, T2, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.RightJoin(joinSetup);
            return new QuerySelector3<T1, T2, TJoin>(newProxy);
        }

        IQuerySelect<T1, T2, TJoin> IQuerySelect<T1, T2>.FullJoin<TJoin>(Expression<Func<T1, T2, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.FullJoin(joinSetup);
            return new QuerySelector3<T1, T2, TJoin>(newProxy);
        }

        IQuerySelect<T1, T2, TJoin> IQuerySelect<T1, T2>.CrossJoin<TJoin>()
        {
            var newProxy = this.proxy.CrossJoin<TJoin>();
            return new QuerySelector3<T1, T2, TJoin>(newProxy);
        }

        public Task<IEnumerable<Tuple<T1, T2>>> Execute() => this.proxy.Manipulator.SelectAsync<T1, T2>(this.proxy.GetData());

        public IEnumerator<Tuple<T1, T2>> GetEnumerator() => this.proxy.Manipulator.Select<T1, T2>(this.proxy.GetData()).GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}