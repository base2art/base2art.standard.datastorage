﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Builders;
    using Data;

    internal class QuerySelector1<T> : IQuerySelect<T>, IDataGetter<SelectData>
    {
        private readonly Selector1Base<T> proxy;

        public QuerySelector1(IDataManipulator manipulator) => this.proxy = new Selector1Base<T>(manipulator);

        SelectData IDataGetter<SelectData>.GetData() => this.proxy.GetData();

        IQuerySelect<T> IQuerySelect<T>.WithNoLock()
        {
            this.proxy.WithNoLock();
            return this;
        }

        IQuerySelect<T> IQuerySelect<T>.Distinct()
        {
            this.proxy.Distinct();
            return this;
        }

        IQuerySelect<T> IQuerySelect<T>.Limit(int count)
        {
            this.proxy.Limit(count);
            return this;
        }

        IQuerySelect<T> IQuerySelect<T>.Offset(int count)
        {
            this.proxy.Offset(count);
            return this;
        }

        IQuerySelect<T> IQuerySelect<T>.Fields(Action<IFieldListBuilder<T>> recordSetup)
        {
            this.proxy.Fields(recordSetup);
            return this;
        }

        IQuerySelect<T, TAggregate> IQuerySelect<T>.WithCalculated<TAggregate>()
        {
            var newProxy = this.proxy.WithCalculated<TAggregate>();
            return new QuerySelector2<T, TAggregate>(newProxy);
        }

        IQuerySelect<T> IQuerySelect<T>.Where(Action<IWhereClauseBuilder<T>> recordSetup)
        {
            this.proxy.Where(recordSetup);
            return this;
        }

        IQuerySelect<T> IQuerySelect<T>.OrderBy(Action<IOrderByBuilder<T>> recordSetup)
        {
            this.proxy.OrderBy(recordSetup);
            return this;
        }

        IQuerySelect<T, TAggregate> IQuerySelect<T>.GroupBy<TAggregate>(Action<IGroupByBuilder<T>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy<TAggregate>(recordSetup);
            return new QuerySelector2<T, TAggregate>(newProxy);
        }

        IQuerySelect<T, TJoin> IQuerySelect<T>.Join<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.Join(joinSetup);
            return new QuerySelector2<T, TJoin>(newProxy);
        }

        IQuerySelect<T, TJoin> IQuerySelect<T>.LeftJoin<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.LeftJoin(joinSetup);
            return new QuerySelector2<T, TJoin>(newProxy);
        }

        IQuerySelect<T, TJoin> IQuerySelect<T>.RightJoin<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.RightJoin(joinSetup);
            return new QuerySelector2<T, TJoin>(newProxy);
        }

        IQuerySelect<T, TJoin> IQuerySelect<T>.FullJoin<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.FullJoin(joinSetup);
            return new QuerySelector2<T, TJoin>(newProxy);
        }

        IQuerySelect<T, TJoin> IQuerySelect<T>.CrossJoin<TJoin>()
        {
            var newProxy = this.proxy.CrossJoin<TJoin>();
            return new QuerySelector2<T, TJoin>(newProxy);
        }

        public Task<IEnumerable<T>> Execute() => this.proxy.Manipulator.SelectAsync<T>(this.proxy.GetData());

        public IEnumerator<T> GetEnumerator() => this.proxy.Manipulator.Select<T>(this.proxy.GetData()).GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}