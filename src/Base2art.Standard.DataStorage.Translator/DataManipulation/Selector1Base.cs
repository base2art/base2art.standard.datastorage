﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Linq.Expressions;
    using Builders;
    using Clauses;
    using Data;

    internal class Selector1Base<T> : IDataGetter<SelectData>
    {
        private readonly Type backing = typeof(T);

        private bool distinct;

        private int? limit;

        private bool nolock;

        private int? offset;

        public Selector1Base(IDataManipulator manipulator) => this.Manipulator = manipulator;

        public IDataManipulator Manipulator { get; }

        internal WhereClauseWithValue<T> WhereData { get; set; }

        internal FieldSelectClause<T> FieldData { get; set; }

        internal OrderByBuilder<T> OrderData { get; set; }

        internal GroupByBuilder<T> Group1Data { get; set; }

        public bool HasFieldsDeclared { get; private set; }

        public SelectData GetData() => new SelectData
                                       {
                                           SchemaName = this.backing.Schema(),
                                           TableName = this.backing.TableName(),
                                           Distinct = this.distinct,
                                           FieldList = this.FieldData.Convert(!this.HasFieldsDeclared),
                                           WhereClause = this.WhereData.Convert(),
                                           Offset = this.offset,
                                           Limit = this.limit,
                                           Ordering = this.OrderData.Convert(),
                                           Grouping = this.Group1Data.Convert(),
                                           WithNoLock = this.nolock
                                       };

        public void DeclareFieldsInUse()
        {
            this.HasFieldsDeclared = true;
        }

        public Selector1Base<T> Limit(int limit)
        {
            this.limit = limit;
            return this;
        }

        public Selector1Base<T> Offset(int offset)
        {
            this.offset = offset;
            return this;
        }

        public Selector1Base<T> Distinct()
        {
            this.distinct = true;
            return this;
        }

        public Selector1Base<T> WithNoLock()
        {
            this.nolock = true;
            return this;
        }

        public Selector1Base<T> Where(Action<IWhereClauseBuilder<T>> recordSetup)
        {
            var record = this.WhereData ?? new WhereClauseWithValue<T>();
            recordSetup(record);
            this.WhereData = record;
            return this;
        }

        public Selector1Base<T> Fields(Action<IFieldListBuilder<T>> fieldSetup)
        {
            var record = this.FieldData ?? new FieldSelectClause<T>();
            fieldSetup(record);
            this.FieldData = record;
            this.HasFieldsDeclared = true;
            return this;
        }

        public Selector1Base<T> OrderBy(Action<IOrderByBuilder<T>> recordSetup)
        {
            var record = this.OrderData ?? new OrderByBuilder<T>(new TypeHolder(() => this.GetData().GetGroupByAggregateType()));
            recordSetup(record);
            this.OrderData = record;
            return this;
        }

        public Selector2Base<T, TAggregate> GroupBy<TAggregate>(Action<IGroupByBuilder<T>> recordSetup)
        {
            var record = this.Group1Data ?? new GroupByBuilder<T>();
            recordSetup(record);
            this.Group1Data = record;
            return new Selector2Base<T, TAggregate>(
                                                    this,
                                                    JoinType.AggregateGrouped,
                                                    new Tuple<int, Column, OperatorType, int, Column>[0]);
        }

        public Selector2Base<T, TAggregate> WithCalculated<TAggregate>()
            => new Selector2Base<T, TAggregate>(
                                                this,
                                                JoinType.Aggregate,
                                                new Tuple<int, Column, OperatorType, int, Column>[0]);

        public Selector2Base<T, TJoin> Join<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
            => new Selector2Base<T, TJoin>(
                                           this,
                                           JoinType.Inner,
                                           this.GetJoins(joinSetup,
                                                         joinSetup.Body));

        public Selector2Base<T, TJoin> LeftJoin<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
            => new Selector2Base<T, TJoin>(
                                           this,
                                           JoinType.Left,
                                           this.GetJoins(joinSetup,
                                                         joinSetup.Body));

        public Selector2Base<T, TJoin> RightJoin<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
            => new Selector2Base<T, TJoin>(
                                           this,
                                           JoinType.Right,
                                           this.GetJoins(joinSetup,
                                                         joinSetup.Body));

        public Selector2Base<T, TJoin> FullJoin<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
            => new Selector2Base<T, TJoin>(
                                           this,
                                           JoinType.Full,
                                           this.GetJoins(joinSetup,
                                                         joinSetup.Body));

        public Selector2Base<T, TJoin> CrossJoin<TJoin>()
            => new Selector2Base<T, TJoin>(
                                           this,
                                           JoinType.Cross,
                                           new Tuple<int, Column, OperatorType, int, Column>[0]);
    }
}