﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Builders;
    using Data;

    internal class QuerySelector6<T1, T2, T3, T4, T5, T6> : IQuerySelect<T1, T2, T3, T4, T5, T6>, IDataGetter<SelectData>
    {
        private readonly Selector6Base<T1, T2, T3, T4, T5, T6> proxy;

        public QuerySelector6(Selector6Base<T1, T2, T3, T4, T5, T6> proxy)
        {
            if (proxy == null)
            {
                throw new ArgumentNullException("proxy");
            }

            this.proxy = proxy;
        }

        SelectData IDataGetter<SelectData>.GetData() => this.proxy.GetData();

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.WithNoLock()
        {
            this.proxy.WithNoLock();
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Distinct()
        {
            this.proxy.Distinct();
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Limit(int count)
        {
            this.proxy.Limit(count);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Offset(int count)
        {
            this.proxy.Offset(count);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Fields1(Action<IFieldListBuilder<T1>> recordSetup)
        {
            this.proxy.Fields1(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Fields2(Action<IFieldListBuilder<T2>> recordSetup)
        {
            this.proxy.Fields2(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Fields3(Action<IFieldListBuilder<T3>> recordSetup)
        {
            this.proxy.Fields3(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Fields4(Action<IFieldListBuilder<T4>> recordSetup)
        {
            this.proxy.Fields4(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Fields5(Action<IFieldListBuilder<T5>> recordSetup)
        {
            this.proxy.Fields5(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Fields6(Action<IFieldListBuilder<T6>> recordSetup)
        {
            this.proxy.Fields6(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Where1(Action<IWhereClauseBuilder<T1>> recordSetup)
        {
            this.proxy.Where1(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Where2(Action<IWhereClauseBuilder<T2>> recordSetup)
        {
            this.proxy.Where2(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Where3(Action<IWhereClauseBuilder<T3>> recordSetup)
        {
            this.proxy.Where3(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Where4(Action<IWhereClauseBuilder<T4>> recordSetup)
        {
            this.proxy.Where4(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Where5(Action<IWhereClauseBuilder<T5>> recordSetup)
        {
            this.proxy.Where5(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.Where6(Action<IWhereClauseBuilder<T6>> recordSetup)
        {
            this.proxy.Where6(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.OrderBy1(Action<IOrderByBuilder<T1>> recordSetup)
        {
            this.proxy.OrderBy1(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.OrderBy2(Action<IOrderByBuilder<T2>> recordSetup)
        {
            this.proxy.OrderBy2(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.OrderBy3(Action<IOrderByBuilder<T3>> recordSetup)
        {
            this.proxy.OrderBy3(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.OrderBy4(Action<IOrderByBuilder<T4>> recordSetup)
        {
            this.proxy.OrderBy4(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.OrderBy5(Action<IOrderByBuilder<T5>> recordSetup)
        {
            this.proxy.OrderBy5(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6> IQuerySelect<T1, T2, T3, T4, T5, T6>.OrderBy6(Action<IOrderByBuilder<T6>> recordSetup)
        {
            this.proxy.OrderBy6(recordSetup);
            return this;
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6, TAggregate> IQuerySelect<T1, T2, T3, T4, T5, T6>.GroupBy1<TAggregate>(
            Action<IGroupByBuilder<T1>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy1<TAggregate>(recordSetup);
            return new QuerySelector7<T1, T2, T3, T4, T5, T6, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6, TAggregate> IQuerySelect<T1, T2, T3, T4, T5, T6>.GroupBy2<TAggregate>(
            Action<IGroupByBuilder<T2>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy2<TAggregate>(recordSetup);
            return new QuerySelector7<T1, T2, T3, T4, T5, T6, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6, TAggregate> IQuerySelect<T1, T2, T3, T4, T5, T6>.GroupBy3<TAggregate>(
            Action<IGroupByBuilder<T3>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy3<TAggregate>(recordSetup);
            return new QuerySelector7<T1, T2, T3, T4, T5, T6, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6, TAggregate> IQuerySelect<T1, T2, T3, T4, T5, T6>.GroupBy4<TAggregate>(
            Action<IGroupByBuilder<T4>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy4<TAggregate>(recordSetup);
            return new QuerySelector7<T1, T2, T3, T4, T5, T6, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6, TAggregate> IQuerySelect<T1, T2, T3, T4, T5, T6>.GroupBy5<TAggregate>(
            Action<IGroupByBuilder<T5>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy5<TAggregate>(recordSetup);
            return new QuerySelector7<T1, T2, T3, T4, T5, T6, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6, TAggregate> IQuerySelect<T1, T2, T3, T4, T5, T6>.GroupBy6<TAggregate>(
            Action<IGroupByBuilder<T6>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy6<TAggregate>(recordSetup);
            return new QuerySelector7<T1, T2, T3, T4, T5, T6, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6, TAggregate> IQuerySelect<T1, T2, T3, T4, T5, T6>.WithCalculated<TAggregate>()
        {
            var newProxy = this.proxy.WithCalculated<TAggregate>();
            return new QuerySelector7<T1, T2, T3, T4, T5, T6, TAggregate>(newProxy);
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6, TJoin> IQuerySelect<T1, T2, T3, T4, T5, T6>.Join<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, T6, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.Join(joinSetup);
            return new QuerySelector7<T1, T2, T3, T4, T5, T6, TJoin>(newProxy);
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6, TJoin> IQuerySelect<T1, T2, T3, T4, T5, T6>.LeftJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, T6, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.LeftJoin(joinSetup);
            return new QuerySelector7<T1, T2, T3, T4, T5, T6, TJoin>(newProxy);
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6, TJoin> IQuerySelect<T1, T2, T3, T4, T5, T6>.RightJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, T6, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.RightJoin(joinSetup);
            return new QuerySelector7<T1, T2, T3, T4, T5, T6, TJoin>(newProxy);
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6, TJoin> IQuerySelect<T1, T2, T3, T4, T5, T6>.FullJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, T6, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.FullJoin(joinSetup);
            return new QuerySelector7<T1, T2, T3, T4, T5, T6, TJoin>(newProxy);
        }

        IQuerySelect<T1, T2, T3, T4, T5, T6, TJoin> IQuerySelect<T1, T2, T3, T4, T5, T6>.CrossJoin<TJoin>()
        {
            var newProxy = this.proxy.CrossJoin<TJoin>();
            return new QuerySelector7<T1, T2, T3, T4, T5, T6, TJoin>(newProxy);
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>>> Execute() =>
            this.proxy.Manipulator.SelectAsync<T1, T2, T3, T4, T5, T6>(this.proxy.GetData());

        public IEnumerator<Tuple<T1, T2, T3, T4, T5, T6>> GetEnumerator() =>
            this.proxy.Manipulator.Select<T1, T2, T3, T4, T5, T6>(this.proxy.GetData()).GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();
    }
}