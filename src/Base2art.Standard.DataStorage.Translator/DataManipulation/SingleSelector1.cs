﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Builders;

    internal class SingleSelector1<T> : IQuerySingleSelect<T>
    {
        private readonly Selector1Base<T> proxy;

        public SingleSelector1(IDataManipulator manipulator)
        {
            this.proxy = new Selector1Base<T>(manipulator);
            this.proxy.Limit(1);
        }

        IQuerySingleSelect<T> IQuerySingleSelect<T>.WithNoLock()
        {
            this.proxy.WithNoLock();
            return this;
        }

        IQuerySingleSelect<T> IQuerySingleSelect<T>.Distinct()
        {
            this.proxy.Distinct();
            return this;
        }

        IQuerySingleSelect<T> IQuerySingleSelect<T>.Limit(int count)
        {
            this.proxy.Limit(count);
            return this;
        }

        IQuerySingleSelect<T> IQuerySingleSelect<T>.Offset(int count)
        {
            this.proxy.Offset(count);
            return this;
        }

        IQuerySingleSelect<T> IQuerySingleSelect<T>.Fields(Action<IFieldListBuilder<T>> recordSetup)
        {
            this.proxy.Fields(recordSetup);
            return this;
        }

        IQuerySingleSelect<T> IQuerySingleSelect<T>.Where(Action<IWhereClauseBuilder<T>> recordSetup)
        {
            this.proxy.Where(recordSetup);
            return this;
        }

        IQuerySingleSelect<T> IQuerySingleSelect<T>.OrderBy(Action<IOrderByBuilder<T>> recordSetup)
        {
            this.proxy.OrderBy(recordSetup);
            return this;
        }

        IQuerySingleSelect<T, TAggregate> IQuerySingleSelect<T>.GroupBy<TAggregate>(Action<IGroupByBuilder<T>> recordSetup)
        {
            var newProxy = this.proxy.GroupBy<TAggregate>(recordSetup);
            return new SingleSelector2<T, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T, TAggregate> IQuerySingleSelect<T>.WithCalculated<TAggregate>()
        {
            var newProxy = this.proxy.WithCalculated<TAggregate>();
            return new SingleSelector2<T, TAggregate>(newProxy);
        }

        IQuerySingleSelect<T, TJoin> IQuerySingleSelect<T>.Join<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.Join(joinSetup);
            return new SingleSelector2<T, TJoin>(newProxy);
        }

        IQuerySingleSelect<T, TJoin> IQuerySingleSelect<T>.LeftJoin<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.LeftJoin(joinSetup);
            return new SingleSelector2<T, TJoin>(newProxy);
        }

        IQuerySingleSelect<T, TJoin> IQuerySingleSelect<T>.RightJoin<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.RightJoin(joinSetup);
            return new SingleSelector2<T, TJoin>(newProxy);
        }

        IQuerySingleSelect<T, TJoin> IQuerySingleSelect<T>.FullJoin<TJoin>(Expression<Func<T, TJoin, bool>> joinSetup)
        {
            var newProxy = this.proxy.FullJoin(joinSetup);
            return new SingleSelector2<T, TJoin>(newProxy);
        }

        IQuerySingleSelect<T, TJoin> IQuerySingleSelect<T>.CrossJoin<TJoin>()
        {
            var newProxy = this.proxy.CrossJoin<TJoin>();
            return new SingleSelector2<T, TJoin>(newProxy);
        }

        public async Task<T> Execute()
        {
            var result = await this.proxy.Manipulator.SelectAsync<T>(this.proxy.GetData());
            return result == null ? default : result.FirstOrDefault();
        }
    }
}