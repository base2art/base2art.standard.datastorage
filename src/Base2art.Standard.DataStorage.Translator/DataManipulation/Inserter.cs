﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Builders;
    using Clauses;
    using Data;

    internal class Inserter<T> : IQueryInsert<T>, IQueryInsertRecords<T>, IQueryInsertSelect<T>
    {
        private readonly IDataManipulator manipulator;
        private readonly List<RecordClauseWithValue<T>> records = new List<RecordClauseWithValue<T>>();

        private FieldSelectClause<T> fields;

        private IEnumerable select;

        public Inserter(IDataManipulator manipulator) => this.manipulator = manipulator;

        public IQueryInsertRecords<T> Record(Action<ISetListBuilder<T>> recordSetup)
        {
            var record = new RecordClauseWithValue<T>();
            recordSetup(record);
            this.records.Add(record);
            return this;
        }

        public IQueryInsertSelect<T> Records<TOther>(IEnumerable<TOther> items)
        {
            this.select = items;
            return this;
        }

        public IQueryInsertSelect<T> Fields(Action<IFieldListBuilder<T>> fieldsSetup)
        {
            var record = new FieldSelectClause<T>();
            fieldsSetup(record);
            this.fields = record;
            return this;
        }

        public async Task Execute()
        {
            var tableType = typeof(T);
            if (this.select == null && this.fields == null)
            {
                var currentRecords = this.records.Convert().ToArray();
                if (!this.manipulator.Supports.MultipleStatementsPerCommand)
                {
                    foreach (var item in currentRecords)
                    {
                        await this.manipulator.InsertAsync(tableType.Schema(), tableType.TableName(), new[] {item});
                    }
                }
                else
                {
                    await this.manipulator.InsertAsync(tableType.Schema(), tableType.TableName(), currentRecords);
                }

                return;
            }

            var sdg = this.select as IDataGetter<SelectData>;
            SelectData sd = null;
            if (sdg == null)
            {
                return;
            }

            sd = sdg.GetData();

            await this.manipulator.InsertAsync(tableType.Schema(), tableType.TableName(), this.fields.Convert(false), sd);
        }
    }
}