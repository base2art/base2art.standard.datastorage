﻿namespace Base2art.DataStorage
{
    using System;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;

    public static class TableType
    {
        public static string Schema(this Type type)
        {
            var attr = type.GetTypeInfo().GetCustomAttributes<CategoryAttribute>()
                           .FirstOrDefault();

            if (attr == null)
            {
                return "";
            }

            return attr.Category;
        }

        public static string TableName(this Type type)
        {
            var attr = type.GetTypeInfo().GetCustomAttributes<DefaultValueAttribute>()
                           .FirstOrDefault();

            if (attr == null)
            {
                return type.Name;
            }

            return attr.Value as string;
        }
    }
}