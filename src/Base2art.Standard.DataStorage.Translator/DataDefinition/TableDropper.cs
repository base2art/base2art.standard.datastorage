﻿namespace Base2art.DataStorage.DataDefinition
{
    using System.Threading.Tasks;

    public class TableDropper<T> : ITableDropper<T>
    {
        private readonly IDataDefiner definer;

        private bool ifExists;

        public TableDropper(IDataDefiner definer) => this.definer = definer;

        public ITableDropper<T> IfExists()
        {
            this.ifExists = true;
            return this;
        }

        public Task Execute()
        {
            var tableType = typeof(T);
            return this.definer.DropTable(tableType.Schema(), tableType.TableName(), this.ifExists);
        }
    }
}