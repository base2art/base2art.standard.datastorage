﻿namespace Base2art.DataStorage.DataDefinition.Clauses
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Xml;
    using System.Xml.Linq;
    using Builders;

    public partial class TableFieldsClause<T>
    {
        public IFieldSetupBuilder<T> Field(Expression<Func<T, bool>> caller) => this.FieldOf(caller, DataTypes.Boolean);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, bool?>> caller) => this.FieldOf(caller, DataTypes.Boolean);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, decimal>> caller) => this.FieldOf(caller, DataTypes.Decimal);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, decimal?>> caller) => this.FieldOf(caller, DataTypes.Decimal);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, double>> caller) => this.FieldOf(caller, DataTypes.Double);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, double?>> caller) => this.FieldOf(caller, DataTypes.Double);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, float>> caller) => this.FieldOf(caller, DataTypes.Float);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, float?>> caller) => this.FieldOf(caller, DataTypes.Float);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, int>> caller) => this.FieldOf(caller, DataTypes.Int);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, int?>> caller) => this.FieldOf(caller, DataTypes.Int);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, long>> caller) => this.FieldOf(caller, DataTypes.Long);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, long?>> caller) => this.FieldOf(caller, DataTypes.Long);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, short>> caller) => this.FieldOf(caller, DataTypes.Short);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, short?>> caller) => this.FieldOf(caller, DataTypes.Short);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, DateTime>> caller) => this.FieldOf(caller, DataTypes.DateTime);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, DateTime?>> caller) => this.FieldOf(caller, DataTypes.DateTime);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, DateTimeOffset>> caller) => this.FieldOf(caller, DataTypes.DateTimeOffset);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, DateTimeOffset?>> caller) => this.FieldOf(caller, DataTypes.DateTimeOffset);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, TimeSpan>> caller) => this.FieldOf(caller, DataTypes.TimeSpan);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller) => this.FieldOf(caller, DataTypes.TimeSpan);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, Guid>> caller) => this.FieldOf(caller, DataTypes.Guid);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, Guid?>> caller) => this.FieldOf(caller, DataTypes.Guid);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, string>> caller) => this.FieldOf(caller, DataTypes.String, false, null);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, string>> caller, bool forceValue, Range<int> lengthRanges) =>
            this.FieldOf(caller, DataTypes.String, forceValue, lengthRanges);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, byte[]>> caller) => this.FieldOf(caller, DataTypes.Binary, false, null);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, byte[]>> caller, bool forceValue, Range<int> lengthRanges) =>
            this.FieldOf(caller, DataTypes.Binary, forceValue, lengthRanges);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, XmlDocument>> caller) => this.FieldOf(caller, DataTypes.Xml, false, null);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, XmlDocument>> caller, bool forceValue) =>
            this.FieldOf(caller, DataTypes.Xml, forceValue, null);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, XElement>> caller) => this.FieldOf(caller, DataTypes.Xml, false, null);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, XElement>> caller, bool forceValue) =>
            this.FieldOf(caller, DataTypes.Xml, forceValue, null);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, Dictionary<string, object>>> caller) =>
            this.FieldOf(caller, DataTypes.Object, false, null);

        public IFieldSetupBuilder<T> Field(Expression<Func<T, Dictionary<string, object>>> caller, bool forceValue) =>
            this.FieldOf(caller, DataTypes.Object, forceValue, null);
    }
}