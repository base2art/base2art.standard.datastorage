﻿namespace Base2art.DataStorage.DataDefinition.Clauses
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Builders;
    using Utils;

    public partial class TableFieldsClause<T> : IFieldSetupBuilder<T>
    {
        private readonly List<Tuple<string, DataTypes, bool, RangeConstraint>> fields = new List<Tuple<string, DataTypes, bool, RangeConstraint>>();

        public IReadOnlyList<Tuple<string, DataTypes, bool, RangeConstraint>> GetColumns() => this.fields;

        private IFieldSetupBuilder<T> FieldOf<TOther>(
            Expression<Func<T, TOther>> caller,
            DataTypes types)
            where TOther : struct
        {
            this.fields.Add(Tuple.Create(caller.Body.GetMemberName(typeof(T).TableName()), types, true, RangeConstraint.Create(null, null)));
            return this;
        }

        private IFieldSetupBuilder<T> FieldOf<TOther>(
            Expression<Func<T, TOther?>> caller,
            DataTypes types)
            where TOther : struct
        {
            this.fields.Add(Tuple.Create(caller.Body.GetMemberName(typeof(T).TableName()), types, false, RangeConstraint.Create(null, null)));
            return this;
        }

        private IFieldSetupBuilder<T> FieldOf<TOther>(
            Expression<Func<T, TOther>> caller,
            DataTypes types,
            bool forceValue,
            Range range)
        {
            RangeConstraint constraint = new RangeConstraint<int>(null, null);
            if (range != null)
            {
                constraint = RangeConstraint.Create(range.Min, range.Max);
            }

            this.fields.Add(Tuple.Create(caller.Body.GetMemberName(typeof(T).TableName()), types, forceValue, constraint));

            return this;
        }
    }
}