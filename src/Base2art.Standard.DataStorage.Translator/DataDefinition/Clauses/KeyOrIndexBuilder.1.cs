﻿namespace Base2art.DataStorage.DataDefinition.Clauses
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Xml;
    using System.Xml.Linq;
    using Builders;

    public partial class KeyOrIndexBuilder<T>
    {
        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, bool?>> caller) => this.FieldOf(caller, DataTypes.Boolean);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, decimal?>> caller) => this.FieldOf(caller, DataTypes.Decimal);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, double?>> caller) => this.FieldOf(caller, DataTypes.Double);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, float?>> caller) => this.FieldOf(caller, DataTypes.Float);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, int?>> caller) => this.FieldOf(caller, DataTypes.Int);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, long?>> caller) => this.FieldOf(caller, DataTypes.Long);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, short?>> caller) => this.FieldOf(caller, DataTypes.Short);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, DateTime?>> caller) => this.FieldOf(caller, DataTypes.DateTime);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller) => this.FieldOf(caller, DataTypes.TimeSpan);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, Guid?>> caller) => this.FieldOf(caller, DataTypes.Guid);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, string>> caller) => this.FieldOfClass(caller, DataTypes.String);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, byte[]>> caller) => this.FieldOfClass(caller, DataTypes.Binary);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, XmlDocument>> caller) => this.FieldOfClass(caller, DataTypes.Xml);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, XElement>> caller) => this.FieldOfClass(caller, DataTypes.Xml);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, Dictionary<string, object>>> caller) => this.FieldOfClass(caller, DataTypes.Object);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, bool>> caller) => this.FieldOf(caller, DataTypes.Boolean);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, decimal>> caller) => this.FieldOf(caller, DataTypes.Decimal);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, double>> caller) => this.FieldOf(caller, DataTypes.Double);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, float>> caller) => this.FieldOf(caller, DataTypes.Float);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, int>> caller) => this.FieldOf(caller, DataTypes.Int);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, long>> caller) => this.FieldOf(caller, DataTypes.Long);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, short>> caller) => this.FieldOf(caller, DataTypes.Short);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, DateTime>> caller) => this.FieldOf(caller, DataTypes.DateTime);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, DateTimeOffset>> caller) => this.FieldOf(caller, DataTypes.DateTimeOffset);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, DateTimeOffset?>> caller) => this.FieldOf(caller, DataTypes.DateTimeOffset);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, TimeSpan>> caller) => this.FieldOf(caller, DataTypes.TimeSpan);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, Guid>> caller) => this.FieldOf(caller, DataTypes.Guid);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, XmlDocument>> caller, bool forceValue) => this.FieldOfClass(caller, DataTypes.Xml);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, XElement>> caller, bool forceValue) => this.FieldOfClass(caller, DataTypes.Xml);

        public IKeyOrIndexBuilder<T> Field(Expression<Func<T, Dictionary<string, object>>> caller, bool forceValue) =>
            this.FieldOfClass(caller, DataTypes.Object);
    }
}