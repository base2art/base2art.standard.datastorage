﻿namespace Base2art.DataStorage.DataDefinition.Clauses
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using Builders;
    using Utils;

    public partial class KeyOrIndexBuilder<T> : IKeyOrIndexBuilder<T>
    {
        private readonly List<string> fields = new List<string>();

        public IReadOnlyList<string> GetColumns() => this.fields;

        private IKeyOrIndexBuilder<T> FieldOf<TOther>(
            Expression<Func<T, TOther>> caller,
            DataTypes types)
            where TOther : struct
        {
            this.fields.Add(caller.Body.GetMemberName(typeof(T).TableName()));
            return this;
        }

        private IKeyOrIndexBuilder<T> FieldOf<TOther>(
            Expression<Func<T, TOther?>> caller,
            DataTypes types)
            where TOther : struct
        {
            this.fields.Add(caller.Body.GetMemberName(typeof(T).TableName()));
            return this;
        }

        private IKeyOrIndexBuilder<T> FieldOfClass<TOther>(
            Expression<Func<T, TOther>> caller,
            DataTypes types)
        {
            this.fields.Add(caller.Body.GetMemberName(typeof(T).TableName()));
            return this;
        }
    }
}