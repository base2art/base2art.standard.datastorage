﻿namespace Base2art.DataStorage.Utils
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using DataManipulation;
    using DataManipulation.Data;

    public static class ExpressionParser
    {
        public static string GetMemberName(this Expression expr, string tableName) => expr.GetMemberInfo(tableName).ColumnName;

        public static Column GetMemberInfo(this Expression expr, string tableName) => GetComplexMemberInfo(expr, tableName, false).Item1;

        public static Tuple<int, Column> GetMemberInfo(this Expression expr, ReadOnlyCollection<ParameterExpression> parms)
        {
            var expression = expr as MemberExpression;
            if (expression != null)
            {
                var parmExpr = expression.Expression as ParameterExpression;
                if (parmExpr != null)
                {
                    var item = parms.Select((x, i) => Tuple.Create(x, i))
                                    .First(y => y.Item1.Name == parmExpr.Name);

                    return Tuple.Create(item.Item2, GetComplexMemberInfo(expression, item.Item1.Type.TableName(), false).Item1);
                }
            }

            throw new ArgumentOutOfRangeException(nameof(parms));
        }

        public static bool IsParameterUsed(this Expression expr, string parmName)
        {
            if (expr.NodeType == ExpressionType.MemberAccess)
            {
                var expression = expr as MemberExpression;

                if (expression != null)
                {
                    var innerExpression = expression.Expression;
                    if (innerExpression.NodeType == ExpressionType.Convert || innerExpression.NodeType == ExpressionType.ConvertChecked)
                    {
                        var ie = innerExpression as UnaryExpression;
                        if (ie != null)
                        {
                            return ie.Operand.ToString() == parmName;
                        }
                    }

                    return expression.Expression.ToString() == parmName;
                }
            }

            if (expr.NodeType == ExpressionType.ConvertChecked || expr.NodeType == ExpressionType.Convert)
            {
                var expression = expr as UnaryExpression;
                return IsParameterUsed(expression.Operand, parmName);
            }

            if (expr.NodeType == ExpressionType.Lambda)
            {
                var expression = expr as LambdaExpression;
                return IsParameterUsed(expression.Body, parmName);
            }

            if (expr.NodeType == ExpressionType.Parameter)
            {
                var expression = expr as ParameterExpression;
                return expression.Name == parmName;
            }

            if (expr.NodeType == ExpressionType.Call)
            {
                var expression = expr as MethodCallExpression;
                if (expression.Method.DeclaringType == typeof(Guid) && expression.Method.Name == "NewGuid")
                {
                    return false;
                }
            }

            if (expr.NodeType == ExpressionType.Constant)
            {
                var expression = expr as ConstantExpression;
                return false;
            }

            throw new InvalidOperationException("Unknown ExpressionType: '" + expr.NodeType + "'");
        }

        public static Tuple<Column, OperatorType?, Column> GetComplexMemberInfo(this Expression expr, string tableName, bool returnNullOnUnsupported)
        {
            if (expr.NodeType == ExpressionType.MemberAccess)
            {
                var expression = expr as MemberExpression;

                var prop = expression.Member as PropertyInfo;
                if (prop != null)
                {
                    return Tuple.Create<Column, OperatorType?, Column>(Column.Simple(tableName, prop.Name, prop.PropertyType), null, null);
                }
            }

            if (expr.NodeType == ExpressionType.ConvertChecked || expr.NodeType == ExpressionType.Convert)
            {
                var expression = expr as UnaryExpression;
                return GetComplexMemberInfo(expression.Operand, tableName, returnNullOnUnsupported);
            }

            if (expr.NodeType == ExpressionType.Lambda)
            {
                var expression = expr as LambdaExpression;
                return GetComplexMemberInfo(expression.Body, tableName, returnNullOnUnsupported);
            }

            if (expr.NodeType == ExpressionType.Parameter)
            {
                var expression = expr as ParameterExpression;

                return Tuple.Create<Column, OperatorType?, Column>(Column.Simple(tableName, expression.Name, expression.Type), null, null);
            }

            if (expr.NodeType == ExpressionType.Call)
            {
                var expression = expr as MethodCallExpression;
                if (expression.Method.DeclaringType == typeof(Guid) && expression.Method.Name == "NewGuid")
                {
                    return Tuple.Create<Column, OperatorType?, Column>(Column.Custom(SpecialColumns.NewUUID), null, null);
                }
            }

            if (expr.NodeType == ExpressionType.Constant)
            {
                var expression = expr as ConstantExpression;
                return Tuple.Create<Column, OperatorType?, Column>(Column.Data(expression.Value, expression.Type), null, null);
            }

            var map = new Dictionary<ExpressionType, OperatorType>();

            map[ExpressionType.Add] = OperatorType.Add;
            map[ExpressionType.AddChecked] = OperatorType.Add;

            map[ExpressionType.Subtract] = OperatorType.Subtract;
            map[ExpressionType.SubtractChecked] = OperatorType.Subtract;

            map[ExpressionType.Multiply] = OperatorType.Multiply;
            map[ExpressionType.MultiplyChecked] = OperatorType.Multiply;

            map[ExpressionType.Divide] = OperatorType.Divide;

            map[ExpressionType.Modulo] = OperatorType.Modulus;

            foreach (var exprType in map.Keys)
            {
                if (expr.NodeType == exprType)
                {
                    var expression = expr as BinaryExpression;
                    var left = GetComplexMemberInfo(expression.Left, tableName, true);
                    var right = GetComplexMemberInfo(expression.Right, tableName, true);

                    return Tuple.Create<Column, OperatorType?, Column>(
                                                                       left.Item1,
                                                                       map[exprType],
                                                                       right.Item1);
                }
            }

            throw new InvalidOperationException("Unknown ExpressionType: '" + expr.NodeType + "'");
        }

        public static T GetConstantValue<T>(this Expression expr) => (T) GetConstantValue(expr);

        public static object GetConstantValue(this Expression expr)
        {
            if (expr.NodeType == ExpressionType.Constant)
            {
                var expression = expr as ConstantExpression;
                return expression.Value;
            }

            if (expr.NodeType == ExpressionType.ConvertChecked || expr.NodeType == ExpressionType.Convert)
            {
                var expression = expr as UnaryExpression;
                return GetConstantValue(expression.Operand);
            }

            if (expr.NodeType == ExpressionType.Lambda)
            {
                var expression = expr as LambdaExpression;
                return GetConstantValue(expression.Body);
            }

            throw new InvalidOperationException("Unknown ExpressionType: '" + expr.NodeType + "'");
        }
    }
}