﻿namespace Base2art.DataStorage.DataDefinition
{
    using System;
    using System.Threading.Tasks;
    using Builders;

    public interface ITableCreator<T>
    {
        ITableCreator<T> WithKey(string keyName, Action<IKeyOrIndexBuilder<T>> fieldsSetup);

        ITableCreator<T> WithIndex(string indexName, Action<IKeyOrIndexBuilder<T>> fieldsSetup);

        ITableCreator<T> Fields(Action<IFieldSetupBuilder<T>> fieldsSetup);

        ITableCreator<T> IfNotExists();

        Task Execute();
    }
}