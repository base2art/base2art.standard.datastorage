﻿namespace Base2art.DataStorage.DataDefinition.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Xml;
    using System.Xml.Linq;

    public interface IFieldSetupBuilder<T>
    {
        // structs
        IFieldSetupBuilder<T> Field(Expression<Func<T, bool>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, bool?>> caller);

        IFieldSetupBuilder<T> Field(Expression<Func<T, decimal>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, decimal?>> caller);

        IFieldSetupBuilder<T> Field(Expression<Func<T, double>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, double?>> caller);

        IFieldSetupBuilder<T> Field(Expression<Func<T, float>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, float?>> caller);

        IFieldSetupBuilder<T> Field(Expression<Func<T, int>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, int?>> caller);

        IFieldSetupBuilder<T> Field(Expression<Func<T, long>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, long?>> caller);

        IFieldSetupBuilder<T> Field(Expression<Func<T, short>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, short?>> caller);

        IFieldSetupBuilder<T> Field(Expression<Func<T, DateTime>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, DateTime?>> caller);

        IFieldSetupBuilder<T> Field(Expression<Func<T, DateTimeOffset>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, DateTimeOffset?>> caller);

        IFieldSetupBuilder<T> Field(Expression<Func<T, TimeSpan>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller);

        IFieldSetupBuilder<T> Field(Expression<Func<T, Guid>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, Guid?>> caller);

        // class
        IFieldSetupBuilder<T> Field(Expression<Func<T, string>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, string>> caller, bool forceValue, Range<int> lengthRanges);

        IFieldSetupBuilder<T> Field(Expression<Func<T, byte[]>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, byte[]>> caller, bool forceValue, Range<int> lengthRanges);

        IFieldSetupBuilder<T> Field(Expression<Func<T, XmlDocument>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, XmlDocument>> caller, bool forceValue);

        IFieldSetupBuilder<T> Field(Expression<Func<T, XElement>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, XElement>> caller, bool forceValue);

        IFieldSetupBuilder<T> Field(Expression<Func<T, Dictionary<string, object>>> caller);
        IFieldSetupBuilder<T> Field(Expression<Func<T, Dictionary<string, object>>> caller, bool forceValue);
    }
}