﻿namespace Base2art.DataStorage.DataDefinition.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Xml;
    using System.Xml.Linq;

    public interface IKeyOrIndexBuilder<T>
    {
        IKeyOrIndexBuilder<T> Field(Expression<Func<T, bool?>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, decimal?>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, string>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, byte[]>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, Guid?>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, float?>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, double?>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, short?>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, int?>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, long?>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, DateTime?>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, XmlDocument>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, XElement>> caller);

        IKeyOrIndexBuilder<T> Field(Expression<Func<T, Dictionary<string, object>>> caller);
    }
}