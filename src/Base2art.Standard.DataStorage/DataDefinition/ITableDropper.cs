﻿namespace Base2art.DataStorage.DataDefinition
{
    using System.Threading.Tasks;

    public interface ITableDropper<T>
    {
        ITableDropper<T> IfExists();

        Task Execute();
    }
}