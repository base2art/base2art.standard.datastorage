﻿namespace Base2art.DataStorage.DataDefinition
{
    using System;

    public class Range
    {
        public IComparable Min { get; set; }

        public IComparable Max { get; set; }

        public static Range Create(IComparable min, IComparable max) => new Range
                                                                        {
                                                                            Min = min,
                                                                            Max = max
                                                                        };

        public static Range<T> Create<T>(T min, T max) where T : struct, IComparable<T>, IComparable => new Range<T>(min, max);
    }

    public class Range<T> : Range
        where T : struct, IComparable<T>, IComparable
    {
        private T? max;

        private T? min;

        public Range(T? min, T? max)
        {
            this.Min = min;
            this.Max = max;
        }

        public new T? Min
        {
            get => this.min;

            set
            {
                base.Min = value;
                this.min = value;
            }
        }

        public new T? Max
        {
            get => this.max;

            set
            {
                base.Max = value;
                this.max = value;
            }
        }
    }
}