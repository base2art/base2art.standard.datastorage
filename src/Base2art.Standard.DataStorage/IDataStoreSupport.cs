﻿namespace Base2art.DataStorage
{
    using Meta;

    public interface IDataStoreSupport
    {
        DbmsUnderflowHandlingStyle UnderflowHandling { get; }

        DbmsOverflowHandlingStyle OverflowHandling { get; }

        bool InnerJoin { get; }
        bool RightJoin { get; }
        bool LeftJoin { get; }
        bool FullJoin { get; }
        bool CrossJoin { get; }

        string NativeExceptionTypeName { get; }
    }
}