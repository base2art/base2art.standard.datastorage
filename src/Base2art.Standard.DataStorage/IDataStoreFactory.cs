﻿namespace Base2art.DataStorage
{
    public interface IDataStoreFactory
    {
        IDataStore Create(string name);
    }
}