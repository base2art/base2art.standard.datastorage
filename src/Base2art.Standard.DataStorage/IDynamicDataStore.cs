namespace Base2art.DataStorage
{
    using DynamicDataManipulation;

    public interface IDynamicDataStore
    {
        IDynamicDataStoreSupport Supports { get; }

        IDynamicQuery Query(string sql);

        IDynamicQuery QueryProcedure(string sql);

        IDynamicExecution ExecutionOf(string sql);

        IDynamicExecution ExecuteOfProcedure(string sql);
    }
}