﻿namespace Base2art.DataStorage.Meta
{
    public enum DbmsUnderflowHandlingStyle
    {
        None = 0,
        Pad = 1,
        ThrowException = 2
    }
}