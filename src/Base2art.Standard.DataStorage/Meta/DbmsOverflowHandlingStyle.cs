﻿namespace Base2art.DataStorage.Meta
{
    public enum DbmsOverflowHandlingStyle
    {
        Truncate = 0,
        Expand = 1,
        ThrowException = 2
    }
}