﻿namespace Base2art.DataStorage
{
    public interface IDbmsFactory
    {
        IDbms Create(string name);
    }
}