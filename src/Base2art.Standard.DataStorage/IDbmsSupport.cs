﻿namespace Base2art.DataStorage
{
    public interface IDbmsSupport
    {
        bool DroppingTables { get; }

        bool ConstraintEnforcement { get; }
    }
}