﻿namespace Base2art.DataStorage
{
    public enum ListSortDirection
    {
        Ascending,

        Descending
    }
}