namespace Base2art.DataStorage
{
    public interface IDynamicDataStoreFactory
    {
        IDynamicDataStore Create(string name);
    }
}