namespace Base2art.DataStorage
{
    public interface IDynamicDataStoreSupport
    {
        bool DynamicQueries { get; }

        bool DynamicProcedureQueries { get; }
    }
}