﻿namespace Base2art.DataStorage
{
    using DataDefinition;

    public interface IDbms
    {
        IDbmsSupport Supports { get; }

        ITableCreator<T> CreateTable<T>();

        ITableCreator<T> CreateOrUpdateTable<T>();

        ITableDropper<T> DropTable<T>();
    }
}