﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Linq.Expressions;
    using System.Threading.Tasks;
    using Builders;

    public interface IQuerySingleSelect<T>
    {
        IQuerySingleSelect<T> WithNoLock();

        IQuerySingleSelect<T> Distinct();

        IQuerySingleSelect<T> Limit(int count);

        IQuerySingleSelect<T> Offset(int count);

        IQuerySingleSelect<T> Fields(Action<IFieldListBuilder<T>> recordSetup);

        IQuerySingleSelect<T> Where(Action<IWhereClauseBuilder<T>> recordSetup);

        IQuerySingleSelect<T> OrderBy(Action<IOrderByBuilder<T>> recordSetup);

        IQuerySingleSelect<T, TAggregate> GroupBy<TAggregate>(Action<IGroupByBuilder<T>> recordSetup);

        IQuerySingleSelect<T, TAggregate> WithCalculated<TAggregate>();

        IQuerySingleSelect<T, TJoin> Join<TJoin>(
            Expression<Func<T, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T, TJoin> LeftJoin<TJoin>(
            Expression<Func<T, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T, TJoin> RightJoin<TJoin>(
            Expression<Func<T, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T, TJoin> FullJoin<TJoin>(
            Expression<Func<T, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T, TJoin> CrossJoin<TJoin>();

        Task<T> Execute();
    }

    public interface IQuerySingleSelect<T1, T2>
    {
        IQuerySingleSelect<T1, T2> WithNoLock();

        IQuerySingleSelect<T1, T2> Distinct();

        IQuerySingleSelect<T1, T2> Limit(int count);

        IQuerySingleSelect<T1, T2> Offset(int count);

        IQuerySingleSelect<T1, T2> Fields1(Action<IFieldListBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2> Fields2(Action<IFieldListBuilder<T2>> recordSetup);

        IQuerySingleSelect<T1, T2> Where1(Action<IWhereClauseBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2> Where2(Action<IWhereClauseBuilder<T2>> recordSetup);

        IQuerySingleSelect<T1, T2> OrderBy1(Action<IOrderByBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2> OrderBy2(Action<IOrderByBuilder<T2>> recordSetup);

        IQuerySingleSelect<T1, T2, TAggregate> GroupBy1<TAggregate>(Action<IGroupByBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, TAggregate> GroupBy2<TAggregate>(Action<IGroupByBuilder<T2>> recordSetup);

        IQuerySingleSelect<T1, T2, TAggregate> WithCalculated<TAggregate>();

        IQuerySingleSelect<T1, T2, TJoin> Join<TJoin>(
            Expression<Func<T1, T2, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, TJoin> LeftJoin<TJoin>(
            Expression<Func<T1, T2, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, TJoin> RightJoin<TJoin>(
            Expression<Func<T1, T2, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, TJoin> FullJoin<TJoin>(
            Expression<Func<T1, T2, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, TJoin> CrossJoin<TJoin>();

        Task<Tuple<T1, T2>> Execute();
    }

    public interface IQuerySingleSelect<T1, T2, T3>
    {
        IQuerySingleSelect<T1, T2, T3> WithNoLock();

        IQuerySingleSelect<T1, T2, T3> Distinct();

        IQuerySingleSelect<T1, T2, T3> Limit(int count);

        IQuerySingleSelect<T1, T2, T3> Offset(int count);

        IQuerySingleSelect<T1, T2, T3> Fields1(Action<IFieldListBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3> Fields2(Action<IFieldListBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3> Fields3(Action<IFieldListBuilder<T3>> recordSetup);

        IQuerySingleSelect<T1, T2, T3> Where1(Action<IWhereClauseBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3> Where2(Action<IWhereClauseBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3> Where3(Action<IWhereClauseBuilder<T3>> recordSetup);

        IQuerySingleSelect<T1, T2, T3> OrderBy1(Action<IOrderByBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3> OrderBy2(Action<IOrderByBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3> OrderBy3(Action<IOrderByBuilder<T3>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, TAggregate> GroupBy1<TAggregate>(Action<IGroupByBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, TAggregate> GroupBy2<TAggregate>(Action<IGroupByBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, TAggregate> GroupBy3<TAggregate>(Action<IGroupByBuilder<T3>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, TAggregate> WithCalculated<TAggregate>();

        IQuerySingleSelect<T1, T2, T3, TJoin> Join<TJoin>(
            Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, TJoin> LeftJoin<TJoin>(
            Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, TJoin> RightJoin<TJoin>(
            Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, TJoin> FullJoin<TJoin>(
            Expression<Func<T1, T2, T3, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, TJoin> CrossJoin<TJoin>();

        Task<Tuple<T1, T2, T3>> Execute();
    }

    public interface IQuerySingleSelect<T1, T2, T3, T4>
    {
        IQuerySingleSelect<T1, T2, T3, T4> WithNoLock();

        IQuerySingleSelect<T1, T2, T3, T4> Distinct();

        IQuerySingleSelect<T1, T2, T3, T4> Limit(int count);

        IQuerySingleSelect<T1, T2, T3, T4> Offset(int count);

        IQuerySingleSelect<T1, T2, T3, T4> Fields1(Action<IFieldListBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4> Fields2(Action<IFieldListBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4> Fields3(Action<IFieldListBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4> Fields4(Action<IFieldListBuilder<T4>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4> Where1(Action<IWhereClauseBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4> Where2(Action<IWhereClauseBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4> Where3(Action<IWhereClauseBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4> Where4(Action<IWhereClauseBuilder<T4>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4> OrderBy1(Action<IOrderByBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4> OrderBy2(Action<IOrderByBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4> OrderBy3(Action<IOrderByBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4> OrderBy4(Action<IOrderByBuilder<T4>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4, TAggregate> GroupBy1<TAggregate>(Action<IGroupByBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, TAggregate> GroupBy2<TAggregate>(Action<IGroupByBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, TAggregate> GroupBy3<TAggregate>(Action<IGroupByBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, TAggregate> GroupBy4<TAggregate>(Action<IGroupByBuilder<T4>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4, TAggregate> WithCalculated<TAggregate>();

        IQuerySingleSelect<T1, T2, T3, T4, TJoin> Join<TJoin>(
            Expression<Func<T1, T2, T3, T4, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, T4, TJoin> LeftJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, T4, TJoin> RightJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, T4, TJoin> FullJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, T4, TJoin> CrossJoin<TJoin>();

        Task<Tuple<T1, T2, T3, T4>> Execute();
    }

    public interface IQuerySingleSelect<T1, T2, T3, T4, T5>
    {
        IQuerySingleSelect<T1, T2, T3, T4, T5> WithNoLock();

        IQuerySingleSelect<T1, T2, T3, T4, T5> Distinct();

        IQuerySingleSelect<T1, T2, T3, T4, T5> Limit(int count);

        IQuerySingleSelect<T1, T2, T3, T4, T5> Offset(int count);

        IQuerySingleSelect<T1, T2, T3, T4, T5> Fields1(Action<IFieldListBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5> Fields2(Action<IFieldListBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5> Fields3(Action<IFieldListBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5> Fields4(Action<IFieldListBuilder<T4>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5> Fields5(Action<IFieldListBuilder<T5>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5> Where1(Action<IWhereClauseBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5> Where2(Action<IWhereClauseBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5> Where3(Action<IWhereClauseBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5> Where4(Action<IWhereClauseBuilder<T4>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5> Where5(Action<IWhereClauseBuilder<T5>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5> OrderBy1(Action<IOrderByBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5> OrderBy2(Action<IOrderByBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5> OrderBy3(Action<IOrderByBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5> OrderBy4(Action<IOrderByBuilder<T4>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5> OrderBy5(Action<IOrderByBuilder<T5>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, TAggregate> GroupBy1<TAggregate>(Action<IGroupByBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, TAggregate> GroupBy2<TAggregate>(Action<IGroupByBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, TAggregate> GroupBy3<TAggregate>(Action<IGroupByBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, TAggregate> GroupBy4<TAggregate>(Action<IGroupByBuilder<T4>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, TAggregate> GroupBy5<TAggregate>(Action<IGroupByBuilder<T5>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, TAggregate> WithCalculated<TAggregate>();

        IQuerySingleSelect<T1, T2, T3, T4, T5, TJoin> Join<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, TJoin> LeftJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, TJoin> RightJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, TJoin> FullJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, TJoin> CrossJoin<TJoin>();

        Task<Tuple<T1, T2, T3, T4, T5>> Execute();
    }

    public interface IQuerySingleSelect<T1, T2, T3, T4, T5, T6>
    {
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> WithNoLock();

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Distinct();

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Limit(int count);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Offset(int count);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Fields1(Action<IFieldListBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Fields2(Action<IFieldListBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Fields3(Action<IFieldListBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Fields4(Action<IFieldListBuilder<T4>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Fields5(Action<IFieldListBuilder<T5>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Fields6(Action<IFieldListBuilder<T6>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Where1(Action<IWhereClauseBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Where2(Action<IWhereClauseBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Where3(Action<IWhereClauseBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Where4(Action<IWhereClauseBuilder<T4>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Where5(Action<IWhereClauseBuilder<T5>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> Where6(Action<IWhereClauseBuilder<T6>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> OrderBy1(Action<IOrderByBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> OrderBy2(Action<IOrderByBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> OrderBy3(Action<IOrderByBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> OrderBy4(Action<IOrderByBuilder<T4>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> OrderBy5(Action<IOrderByBuilder<T5>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6> OrderBy6(Action<IOrderByBuilder<T6>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, TAggregate> GroupBy1<TAggregate>(Action<IGroupByBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, TAggregate> GroupBy2<TAggregate>(Action<IGroupByBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, TAggregate> GroupBy3<TAggregate>(Action<IGroupByBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, TAggregate> GroupBy4<TAggregate>(Action<IGroupByBuilder<T4>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, TAggregate> GroupBy5<TAggregate>(Action<IGroupByBuilder<T5>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, TAggregate> GroupBy6<TAggregate>(Action<IGroupByBuilder<T6>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, TAggregate> WithCalculated<TAggregate>();

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, TJoin> Join<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, T6, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, TJoin> LeftJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, T6, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, TJoin> RightJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, T6, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, TJoin> FullJoin<TJoin>(
            Expression<Func<T1, T2, T3, T4, T5, T6, TJoin, bool>> joinSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, TJoin> CrossJoin<TJoin>();

        Task<Tuple<T1, T2, T3, T4, T5, T6>> Execute();
    }

    public interface IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7>
    {
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> WithNoLock();

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Distinct();

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Limit(int count);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Offset(int count);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Fields1(Action<IFieldListBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Fields2(Action<IFieldListBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Fields3(Action<IFieldListBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Fields4(Action<IFieldListBuilder<T4>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Fields5(Action<IFieldListBuilder<T5>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Fields6(Action<IFieldListBuilder<T6>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Fields7(Action<IFieldListBuilder<T7>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Where1(Action<IWhereClauseBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Where2(Action<IWhereClauseBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Where3(Action<IWhereClauseBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Where4(Action<IWhereClauseBuilder<T4>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Where5(Action<IWhereClauseBuilder<T5>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Where6(Action<IWhereClauseBuilder<T6>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> Where7(Action<IWhereClauseBuilder<T7>> recordSetup);

        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> OrderBy1(Action<IOrderByBuilder<T1>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> OrderBy2(Action<IOrderByBuilder<T2>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> OrderBy3(Action<IOrderByBuilder<T3>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> OrderBy4(Action<IOrderByBuilder<T4>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> OrderBy5(Action<IOrderByBuilder<T5>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> OrderBy6(Action<IOrderByBuilder<T6>> recordSetup);
        IQuerySingleSelect<T1, T2, T3, T4, T5, T6, T7> OrderBy7(Action<IOrderByBuilder<T7>> recordSetup);

        Task<Tuple<T1, T2, T3, T4, T5, T6, T7>> Execute();
    }
}