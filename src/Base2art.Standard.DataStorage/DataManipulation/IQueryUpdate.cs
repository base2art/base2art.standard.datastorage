﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Threading.Tasks;
    using Builders;

    public interface IQueryUpdate<T>
    {
        IQueryUpdate<T> Where(Action<IWhereClauseBuilder<T>> recordSetup);

        IQueryUpdate<T> Set(Action<ISetListBuilder<T>> recordSetup);

        Task Execute();
    }
}