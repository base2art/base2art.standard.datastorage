﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Linq.Expressions;

    public static class Operations
    {
        public static Expression<Func<string, string, bool>> EqualTo
        {
            get { return Expr((x, y) => EqualOp(x, y)); }
        }

        public static Expression<Func<string, string, bool>> NotEqualTo
        {
            get { return Expr((x, y) => NotEqualOp(x, y)); }
        }

        public static Expression<Func<string, string, bool>> Like
        {
            get { return Expr((x, y) => LikeOp(x, y)); }
        }

        public static Expression<Func<string, string, bool>> NotLike
        {
            get { return Expr((x, y) => NotLikeOp(x, y)); }
        }

        /// [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
        private static bool NotLikeOp(string x, string y) => true;

        /// [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
        private static bool LikeOp(string x, string y) => true;

        /// [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
        private static bool EqualOp(string x, string y) => true;

        /// [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
        private static bool NotEqualOp(string x, string y) => true;

        private static Expression<Func<string, string, bool>> Expr(Expression<Func<string, string, bool>> expr) => expr;
    }
}