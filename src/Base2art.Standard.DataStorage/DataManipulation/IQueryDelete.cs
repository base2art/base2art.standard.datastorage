﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Threading.Tasks;
    using Builders;

    public interface IQueryDelete<T>
    {
        IQueryDelete<T> Where(Action<IWhereClauseBuilder<T>> recordSetup);

        Task Execute();
    }
}