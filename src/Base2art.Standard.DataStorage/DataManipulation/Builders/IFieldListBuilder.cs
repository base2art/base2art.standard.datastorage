﻿namespace Base2art.DataStorage.DataManipulation.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Xml;
    using System.Xml.Linq;

    // TYPED
    public interface IFieldListBuilder<T>
    {
        IFieldListBuilder<T> All();

        IFieldListBuilder<T> Count(Expression<Func<T, long?>> caller);

        IFieldListBuilder<T> NewUUID(Expression<Func<T, Guid?>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, bool?>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, decimal?>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, string>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, byte[]>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, Guid?>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, float?>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, double?>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, short?>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, int?>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, long?>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, DateTime?>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, XmlDocument>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, XElement>> caller);

        IFieldListBuilder<T> Field(Expression<Func<T, Dictionary<string, object>>> caller);
    }
}