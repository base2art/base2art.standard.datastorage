﻿namespace Base2art.DataStorage.DataManipulation.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Xml;
    using System.Xml.Linq;

    public interface IOrderByBuilder<T>
    {
        IOrderByBuilder<T> Random();

        IOrderByBuilder<T> Field(Expression<Func<T, bool?>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, decimal?>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, string>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, byte[]>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, Guid?>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, float?>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, double?>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, short?>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, int?>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, long?>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, DateTime?>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, XmlDocument>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, XElement>> caller, ListSortDirection direction);

        IOrderByBuilder<T> Field(Expression<Func<T, Dictionary<string, object>>> caller, ListSortDirection direction);
    }
}