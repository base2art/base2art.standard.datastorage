﻿namespace Base2art.DataStorage.DataManipulation.Builders
{
    using System;
    using System.Collections.Generic;
    using System.Linq.Expressions;
    using System.Xml;
    using System.Xml.Linq;

    public interface IGroupByBuilder<T>
    {
        IGroupByBuilder<T> Field(Expression<Func<T, bool?>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, decimal?>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, string>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, byte[]>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, Guid?>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, float?>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, double?>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, short?>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, int?>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, long?>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, DateTime?>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, TimeSpan?>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, XmlDocument>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, XElement>> caller);

        IGroupByBuilder<T> Field(Expression<Func<T, Dictionary<string, object>>> caller);
    }
}