﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Builders;

    public interface IQueryInsert<T>
        //: IQueryInsertSelect<T>, IQueryInsertRecords<T>
    {
        IQueryInsertRecords<T> Record(Action<ISetListBuilder<T>> recordSetup);

        IQueryInsertSelect<T> Fields(Action<IFieldListBuilder<T>> fieldsSetup);

        IQueryInsertSelect<T> Records<TOther>(IEnumerable<TOther> items);

        Task Execute();
    }
}