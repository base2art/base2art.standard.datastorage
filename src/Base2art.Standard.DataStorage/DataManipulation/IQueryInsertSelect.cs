﻿namespace Base2art.DataStorage.DataManipulation
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Builders;

    public interface IQueryInsertSelect<T>
    {
        IQueryInsertSelect<T> Fields(Action<IFieldListBuilder<T>> fieldsSetup);

        IQueryInsertSelect<T> Records<TOther>(IEnumerable<TOther> items);

        Task Execute();
    }
}