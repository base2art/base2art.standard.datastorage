﻿namespace Base2art.DataStorage
{
    using DataManipulation;

    public interface IDataStore
    {
        IDataStoreSupport Supports { get; }

        IQueryInsert<T> Insert<T>();

        IQuerySingleSelect<T> SelectSingle<T>();

        IQuerySelect<T> Select<T>();

        IQueryDelete<T> Delete<T>();

        IQueryUpdate<T> Update<T>();
    }
}