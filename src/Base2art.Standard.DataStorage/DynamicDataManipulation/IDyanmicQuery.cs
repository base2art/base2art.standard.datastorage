namespace Base2art.DataStorage.DynamicDataManipulation
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IDynamicQuery
    {
        IDynamicQuery WithParameters<T>(T data);
        Task<IEnumerable<T>> Execute<T>();
    }
}