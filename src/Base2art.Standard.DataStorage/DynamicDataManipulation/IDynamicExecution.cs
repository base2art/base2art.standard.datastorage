namespace Base2art.DataStorage.DynamicDataManipulation
{
    using System.Threading.Tasks;

    public interface IDynamicExecution
    {
        IDynamicExecution WithParameters<T>(T data);
        Task<int> Execute();
    }
}