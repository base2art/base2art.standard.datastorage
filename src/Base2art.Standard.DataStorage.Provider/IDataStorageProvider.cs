﻿namespace Base2art.DataStorage
{
    public interface IDataStorageProvider
    {
        IDataStore CreateDataStoreAccess(NamedConnectionString connectionString);

        IDbms CreateDbmsAccess(NamedConnectionString connectionString);

        IDynamicDataStore CreateDynamicDataStoreAccess(NamedConnectionString connectionString);
    }
}