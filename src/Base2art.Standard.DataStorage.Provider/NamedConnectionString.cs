﻿namespace Base2art.DataStorage
{
    using System.Collections.Generic;

    public class NamedConnectionString
    {
        public string Name { get; set; }

        public string ConnectionString { get; set; }

        public Dictionary<string, string> AdditionalParameters { get; set; }
    }
}