﻿namespace Base2art.DataStorage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class DbmsFactory : IDbmsFactory
    {
        private readonly List<TypedNamedConnectionString> cstrs = new List<TypedNamedConnectionString>();

        private readonly Dictionary<string, IDataStorageProvider> providers =
            new Dictionary<string, IDataStorageProvider>(StringComparer.OrdinalIgnoreCase);

        public DbmsFactory(List<TypedNamedConnectionString> items, IDataStorageProvider[] providers)
        {
            this.cstrs.AddRange((IEnumerable<TypedNamedConnectionString>) items ?? new TypedNamedConnectionString[0]);
            foreach (var prov in providers ?? new IDataStorageProvider[0])
            {
                this.providers[prov.GetType().FullName] = prov;
                this.providers[prov.GetType().Name] = prov;
            }
        }

        public IDbms Create(string name)
        {
            var cstr = this.cstrs.FirstOrDefault(x => string.Equals(name, x.Name, StringComparison.OrdinalIgnoreCase));

            if (cstr == null)
            {
                throw new InvalidOperationException("Could Not Find Connection String: '" + name + "'");
            }

            if (!this.providers.ContainsKey(cstr.ProviderClassName))
            {
                throw new InvalidOperationException("Could Not Find Provider Class Name: '" + cstr.ProviderClassName + "'");
            }

            return this.providers[cstr.ProviderClassName]
                       .CreateDbmsAccess(new NamedConnectionString
                                         {
                                             ConnectionString = cstr.ConnectionString,
                                             Name = cstr.Name,
                                             AdditionalParameters = cstr.AdditionalParameters
                                         });
        }
    }
}