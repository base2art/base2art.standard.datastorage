﻿namespace Base2art.DataStorage
{
    using System.Collections.Generic;

    public class TypedNamedConnectionString
    {
        public string Name { get; set; }

        public string ConnectionString { get; set; }

        public string ProviderClassName { get; set; }

        public Dictionary<string, string> AdditionalParameters { get; set; }
    }
}