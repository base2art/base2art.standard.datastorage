﻿namespace Base2art.DataStorage.Dapper
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Base2art.Dapper;

    public class DapperExecutionEngine : IExecutionEngine
    {
        private readonly IConnectionFactory factory;

        public DapperExecutionEngine(IConnectionFactory factory) => this.factory = factory;

        public Task<IEnumerable<TOutput>> QueryReaderAsync<TOutput>(string sql, object data, IDataConverter dataConverter, TimeSpan? timeout)
            => this.factory.QueryReaderAsync(sql, data, x => Parser.Parse<TOutput>(x, dataConverter.ConvertDataFromStorage), timeout);

        public Task<IEnumerable<TOutput>> QueryProcedureReaderAsync<TOutput>(string sql, object data, IDataConverter dataConverter, TimeSpan? timeout)
            => this.factory.QueryStoredProcReaderAsync(sql, data, x => Parser.Parse<TOutput>(x, dataConverter.ConvertDataFromStorage), timeout);

        public Task<IEnumerable<T>> QueryReaderAsync<T>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return this.factory.QueryReaderAsync(sql, data, x => Parser.Parse<T>(x, dataConverter.ConvertDataFromStorage), timeout);
        }

        public IEnumerable<T> QueryReader<T>(string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return this.factory.QueryReader(sql, data, x => Parser.Parse<T>(x, dataConverter.ConvertDataFromStorage), timeout);
        }

        public Task<IEnumerable<Tuple<T1, T2>>> QueryReaderAsync<T1, T2>(string sql, Dictionary<string, object> data, IDataConverter dataConverter,
                                                                         TimeSpan? timeout)
        {
            return this.factory.QueryReaderAsync(sql, data,
                                                 x => Parser.ParseTuple<T1, T2>(x, dataConverter.ConvertDataFromStorage),
                                                 timeout);
        }

        public IEnumerable<Tuple<T1, T2>> QueryReader<T1, T2>(string sql, Dictionary<string, object> data, IDataConverter dataConverter,
                                                              TimeSpan? timeout)
        {
            return this.factory.QueryReader(sql, data,
                                            x => Parser.ParseTuple<T1, T2>(x, dataConverter.ConvertDataFromStorage),
                                            timeout);
        }

        public Task<IEnumerable<Tuple<T1, T2, T3>>> QueryReaderAsync<T1, T2, T3>(string sql, Dictionary<string, object> data,
                                                                                 IDataConverter dataConverter, TimeSpan? timeout)
        {
            return this.factory.QueryReaderAsync(sql, data,
                                                 x => Parser.ParseTuple<T1, T2, T3>(x, dataConverter.ConvertDataFromStorage), timeout);
        }

        public IEnumerable<Tuple<T1, T2, T3>> QueryReader<T1, T2, T3>(string sql, Dictionary<string, object> data, IDataConverter dataConverter,
                                                                      TimeSpan? timeout)
        {
            return this.factory.QueryReader(sql, data,
                                            x => Parser.ParseTuple<T1, T2, T3>(x, dataConverter.ConvertDataFromStorage),
                                            timeout);
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4>>> QueryReaderAsync<T1, T2, T3, T4>(string sql, Dictionary<string, object> data,
                                                                                         IDataConverter dataConverter, TimeSpan? timeout)
        {
            return this.factory.QueryReaderAsync(sql, data,
                                                 x => Parser.ParseTuple<T1, T2, T3, T4>(x, dataConverter.ConvertDataFromStorage), timeout);
        }

        public IEnumerable<Tuple<T1, T2, T3, T4>> QueryReader<T1, T2, T3, T4>(string sql, Dictionary<string, object> data,
                                                                              IDataConverter dataConverter, TimeSpan? timeout)
        {
            return this.factory.QueryReader(sql, data,
                                            x =>
                                                Parser.ParseTuple<T1, T2, T3, T4>(x, dataConverter.ConvertDataFromStorage), timeout);
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5>>> QueryReaderAsync<T1, T2, T3, T4, T5>(
            string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return this.factory.QueryReaderAsync(sql, data,
                                                 x =>
                                                     Parser.ParseTuple<T1, T2, T3, T4, T5>(x, dataConverter.ConvertDataFromStorage), timeout);
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5>> QueryReader<T1, T2, T3, T4, T5>(string sql, Dictionary<string, object> data,
                                                                                      IDataConverter dataConverter, TimeSpan? timeout)
        {
            return this.factory.QueryReader(sql, data,
                                            x =>
                                                Parser.ParseTuple<T1, T2, T3, T4, T5>(x, dataConverter.ConvertDataFromStorage), timeout);
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>>> QueryReaderAsync<T1, T2, T3, T4, T5, T6>(
            string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return this.factory.QueryReaderAsync(sql, data,
                                                 x =>
                                                     Parser
                                                         .ParseTuple<T1, T2, T3, T4, T5, T6
                                                         >(x, dataConverter.ConvertDataFromStorage),
                                                 timeout);
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6>> QueryReader<T1, T2, T3, T4, T5, T6>(
            string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return this.factory.QueryReader(sql, data,
                                            x =>
                                                Parser
                                                    .ParseTuple<T1, T2, T3, T4, T5, T6
                                                    >(x, dataConverter.ConvertDataFromStorage), timeout);
        }

        public Task<IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>>> QueryReaderAsync<T1, T2, T3, T4, T5, T6, T7>(
            string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return this.factory.QueryReaderAsync(sql, data,
                                                 x =>
                                                     Parser
                                                         .ParseTuple<T1, T2, T3, T4, T5, T6, T7
                                                         >(x, dataConverter.ConvertDataFromStorage),
                                                 timeout);
        }

        public IEnumerable<Tuple<T1, T2, T3, T4, T5, T6, T7>> QueryReader<T1, T2, T3, T4, T5, T6, T7>(
            string sql, Dictionary<string, object> data, IDataConverter dataConverter, TimeSpan? timeout)
        {
            return this.factory.QueryReader(sql, data,
                                            x =>
                                                Parser
                                                    .ParseTuple<T1, T2, T3, T4, T5, T6, T7
                                                    >(x, dataConverter.ConvertDataFromStorage),
                                            timeout);
        }

        public Task Execute(string sql, Dictionary<string, object> data, TimeSpan? timeout) => this.factory.ExecuteAsync(sql, data, timeout);

        public Task<int> Execute(string sql, object data, TimeSpan? timeout) => this.factory.ExecuteAsync(sql, data, timeout);
        public Task<int> ExecuteProcedure(string sql, object data, TimeSpan? timeout) => this.factory.ExecuteStoredProcAsync(sql, data, timeout);
    }
}