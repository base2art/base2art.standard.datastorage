﻿namespace Base2art.DataStorage.Provider.GoogleDocs
{
    using System.IO;
    using DataStorage;
    using DataDefinition;
    using DataManipulation;
    using Base2art.DataStorage.GoogleDocs;
    using Base2art.DataStorage.Interception.DataDefinition;
    using Base2art.DataStorage.Interception.DataManipulation;
    using Provider;

    public class GoogleDocsProvider : DataStorageProviderBase
    {
        public GoogleDocsProvider(
            string[] definerInterceptorNames,
            string[] manipulatorInterceptorNames,
            IDataDefinerInterceptor[] definerInterceptors,
            IDataManipulatorInterceptor[] manipulatorInterceptors)
            : base(definerInterceptorNames, manipulatorInterceptorNames, definerInterceptors, manipulatorInterceptors)
        {
        }

        public override IDynamicDataStore CreateDynamicDataStoreAccess(NamedConnectionString named)
            => new DynamicDataStore(new DynamicExecutionEngine());

        protected override IDataDefinerAndManipulatorProvider CreateActualStore(NamedConnectionString named)
        {
            var connectionString = new GoogleDocsConnectionStringBuilder(named.ConnectionString);
            return new Wrapper(new DataDefiner(new DirectoryInfo(connectionString.CredentialsPath), connectionString.InitialCatalog));
        }

        private class Wrapper : IDataDefinerAndManipulatorProvider
        {
            private readonly DataDefiner dataDefiner;

            public Wrapper(DataDefiner dataDefiner) => this.dataDefiner = dataDefiner;

            public IDataDefiner DataDefiner => this.dataDefiner;

            public IDataManipulator DataManipulator => this.dataDefiner;
        }
    }
}