namespace Base2art.DataStorage.Provider.GoogleDocs
{
    using System;
    using System.Collections.Generic;

    public class GoogleDocsConnectionStringBuilder : ConnectionStringBuilderBase<GoogleDocsConnectionStringBuilder.Keywords>
    {
        public enum Keywords
        {
            CredentialsPath,
            InitialCatalog
        }

        private string credentialsPath;
        private string initialCatalog;

        public GoogleDocsConnectionStringBuilder(string connectionString) : base(CreateKeyWordMap)
            => this.ConnectionString = connectionString;

        public GoogleDocsConnectionStringBuilder() : base(CreateKeyWordMap)
        {
        }

        public string CredentialsPath
        {
            get => this.credentialsPath;
            set => this.credentialsPath = this.SetValue(nameof(this.CredentialsPath), value, x => x);
        }

        public string InitialCatalog
        {
            get => this.initialCatalog;
            set => this.initialCatalog = this.SetValue(nameof(this.InitialCatalog), value, x => x);
        }

        private static IReadOnlyDictionary<string, Keywords> CreateKeyWordMap()
            => new Dictionary<string, Keywords>(StringComparer.OrdinalIgnoreCase)
               {
                   [nameof(Keywords.CredentialsPath)] = Keywords.CredentialsPath,
                   ["Credentials Path"] = Keywords.CredentialsPath,
                   [nameof(Keywords.InitialCatalog)] = Keywords.InitialCatalog,
                   ["Initial Catalog"] = Keywords.InitialCatalog
               };
    }
}