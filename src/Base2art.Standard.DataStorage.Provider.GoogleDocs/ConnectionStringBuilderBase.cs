namespace Base2art.DataStorage.Provider.GoogleDocs
{
    using System;
    using System.Collections.Generic;
    using System.Data.Common;
    using System.Globalization;

    public class ConnectionStringBuilderBase<TKey> : DbConnectionStringBuilder
        where TKey : Enum
    {
        private readonly IReadOnlyDictionary<string, TKey> keywords;

        public ConnectionStringBuilderBase(Func<IReadOnlyDictionary<string, TKey>> provider)
        {
            this.keywords = provider();
        }

        public override object this[string keyword]
        {
            get
            {
                TKey index = this.GetIndex(keyword);
                return this.GetAt(index);
            }
            set
            {
                if (value != null)
                {
                    var keywordType = this.GetIndex(keyword);

                    var currentType = this.GetType();
                    var prop = currentType.GetProperty(keywordType.ToString("G"));
                    if (prop?.SetMethod == null)
                    {
                        throw new ArgumentOutOfRangeException(nameof(keywordType));
                    }

                    prop.SetMethod.Invoke(this, new object[] {this.ConvertTo(prop.PropertyType, value)});
                }
                else
                {
                    this.Remove(keyword);
                }
            }
        }

        private object ConvertTo(Type propType, object value)
        {
            if (propType == typeof(string))
            {
                return ConvertToString(value);
            }

            if (propType == typeof(Guid))
            {
                return ConvertToGuid(value);
            }

            throw new ArgumentOutOfRangeException($"{nameof(propType)}: {propType.Name}");
        }

        protected static string ConvertToString(object value)
        {
            try
            {
                return ((IConvertible) value).ToString(CultureInfo.InvariantCulture);
            }
            catch (InvalidCastException)
            {
                return "";
            }
        }

        protected static Guid ConvertToGuid(object value)
        {
            Guid.TryParse(ConvertToString(value), out Guid val);
            return val;
        }

        protected T SetValue<T>(string keyword, T value, Func<T, string> convert)
        {
            base[keyword] = convert(value);
            return value;
        }

        protected TKey GetIndex(string keyword)
        {
            if (string.IsNullOrWhiteSpace(keyword))
            {
                throw new ArgumentNullException(nameof(keyword));
            }

            if (this.keywords.TryGetValue(keyword, out var value))
            {
                return value;
            }

            throw new NotSupportedException($"{nameof(keyword)}:{keyword}");
        }

        protected object GetAt(TKey index)
        {
            var currentType = this.GetType();
            var prop = currentType.GetProperty(index.ToString("G"));
            if (prop?.GetMethod == null)
            {
                throw new ArgumentOutOfRangeException(nameof(index));
            }

            return prop.GetMethod.Invoke(this, new object[0]);
        }
    }
}