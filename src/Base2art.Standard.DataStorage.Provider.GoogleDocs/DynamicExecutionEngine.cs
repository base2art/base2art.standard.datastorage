namespace Base2art.DataStorage.Provider.GoogleDocs
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using DynamicDataManipulation;

    public class DynamicExecutionEngine : IDynamicExecutionEngine
    {
        public bool SupportsDynamicQueries { get; } = false;
        public bool SupportsDynamicProcedureQueries { get; } = false;

        public Task<IEnumerable<TOutput>> ExecuteReader<TOutput>(string sql, object data, bool isProcedure)
            => throw new NotImplementedException();

        public Task<int> Execute(string sql, object data, bool isProcedure)
            => throw new NotImplementedException();
    }
}