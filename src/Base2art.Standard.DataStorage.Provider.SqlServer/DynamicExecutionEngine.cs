namespace Base2art.DataStorage.Provider.SqlServer
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Dapper;
    using DataManipulation;
    using DataStorage.SqlServer;
    using DynamicDataManipulation;

    public class DynamicExecutionEngine : IDynamicExecutionEngine
    {
        private readonly DapperExecutionEngine dapperExecutionEngine;
        private readonly TimeSpan defaultCommandTimeout;

        public DynamicExecutionEngine(DapperExecutionEngine dapperExecutionEngine, TimeSpan defaultCommandTimeout)
        {
            this.dapperExecutionEngine = dapperExecutionEngine;
            this.defaultCommandTimeout = defaultCommandTimeout;
        }

        public bool SupportsDynamicQueries { get; } = true;

        public bool SupportsDynamicProcedureQueries { get; } = true;

        public Task<IEnumerable<TOutput>> ExecuteReader<TOutput>(string sql, object data, bool isProcedure)
        {
            var builderMap = new BuilderMaps(new SqlServerManipulationCommandBuilderFactory(), new DataManipulatorSupport());
            return isProcedure
                       ? this.dapperExecutionEngine.QueryProcedureReaderAsync<TOutput>(sql, data, builderMap, this.defaultCommandTimeout)
                       : this.dapperExecutionEngine.QueryReaderAsync<TOutput>(sql, data, builderMap, this.defaultCommandTimeout);
        }

        public Task<int> Execute(string sql, object data, bool isProcedure)
            => isProcedure
                   ? this.dapperExecutionEngine.ExecuteProcedure(sql, data, this.defaultCommandTimeout)
                   : this.dapperExecutionEngine.Execute(sql, data, this.defaultCommandTimeout);
    }
}