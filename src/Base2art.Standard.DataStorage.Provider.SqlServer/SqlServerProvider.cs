﻿namespace Base2art.DataStorage.Provider.SqlServer
{
    using System;
    using System.Data.SqlClient;
    using Base2art.Dapper;
    using Dapper;
    using DataStorage.SqlServer;
    using Interception.DataDefinition;
    using Interception.DataManipulation;

    public class SqlServerProvider : DataStorageProviderBase
    {
        private readonly TimeSpan defaultCommandTimeout;

        public SqlServerProvider(
            TimeSpan defaultCommandTimeout,
            string[] definerInterceptorNames,
            string[] manipulatorInterceptorNames,
            IDataDefinerInterceptor[] definerInterceptors,
            IDataManipulatorInterceptor[] manipulatorInterceptors) : base(
                                                                          definerInterceptorNames,
                                                                          manipulatorInterceptorNames,
                                                                          definerInterceptors,
                                                                          manipulatorInterceptors) =>
            this.defaultCommandTimeout = defaultCommandTimeout;

        public override IDynamicDataStore CreateDynamicDataStoreAccess(NamedConnectionString named)
            => new DynamicDataStore(new DynamicExecutionEngine(DapperExecutionEngine(named), this.defaultCommandTimeout));

        protected override IDataDefinerAndManipulatorProvider CreateActualStore(NamedConnectionString named)
        {
            var dapperExecutionEngine = DapperExecutionEngine(named);
            var schema = string.Empty;
            if (named.AdditionalParameters != null && named.AdditionalParameters.ContainsKey("schema"))
            {
                schema = named.AdditionalParameters["schema"];
            }

            return new SqlServerFormatterProvider(dapperExecutionEngine, schema, this.defaultCommandTimeout);
        }

        private static DapperExecutionEngine DapperExecutionEngine(NamedConnectionString named)
        {
            var cstr = named.ConnectionString;

            var factory = new OdbcConnectionFactory<SqlConnection>(cstr);

            var dapperExecutionEngine = new DapperExecutionEngine(factory);
            return dapperExecutionEngine;
        }
    }
}