﻿namespace Base2art.DataStorage.SQLite.DataManipulation
{
    using DataStorage.DataManipulation;

    public class SelectBuilder : DataStorage.DataManipulation.SelectBuilder
    {
        public SelectBuilder(ISharedBuilderMaps commonBuilder, IEscapeCharacters escapeCharacters) : base(commonBuilder, escapeCharacters)
        {
        }

        protected override string TableLevelNoLockHint => "";
    }
}