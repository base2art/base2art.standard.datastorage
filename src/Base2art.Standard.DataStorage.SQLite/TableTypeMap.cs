﻿namespace Base2art.DataStorage.SQLite
{
    using DataStorage.DataDefinition;

    public class TableTypeMap : ITypeMap
    {
        public string StringType(int? min, int? max)
        {
            if (max.HasValue)
            {
                if (min.GetValueOrDefault(-1) == max.Value)
                {
                    return "NCHAR(" + max + ")";
                }

                return "NVARCHAR(" + max.Value + ")";
            }

            return "NTEXT";
        }

        public string ObjectType(int? min, int? max) => this.StringType(min, max);

        public string BinaryType(int? min, int? max)
        {
            if (max.HasValue)
            {
                return "VARBINARY(" + max.Value + ")";
            }

            //            return this.BinaryType(RangeConstraint.Create(0, 4000));
            return "IMAGE";
        }

        public string XmlType() => this.StringType(null, null);

        public string ShortType() => "SMALLINT";

        public string TimeSpanType() => "BIGINT";

        public string LongType() => "BIGINT";

        public string IntType() => "INT";

        public string GuidType() => "UNIQUEIDENTIFIER";

        public string FloatType() => "REAL";

        public string DoubleType() => "FLOAT";

        public string DecimalType() => "DECIMAL";

        public string DateTimeOffsetType() => "VARCHAR(30)";

        public string DateTimeType() => "VARCHAR(30)";

        public string BooleanType() => "BIT";
    }
}