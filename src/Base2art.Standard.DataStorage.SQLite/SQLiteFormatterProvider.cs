﻿namespace Base2art.DataStorage.SQLite
{
    using System;
    using DataStorage.DataDefinition;
    using DataStorage.DataManipulation;

    public class SQLiteFormatterProvider : IDataDefinerAndManipulatorProvider
    {
        private const string ExceptionName = "Microsoft.Data.Sqlite.SqliteException";
        private static readonly IManipulationCommandBuilderFactory manipulatorBuilderFactory;
        private static readonly DefaultStorageTypeMap typeMap;
        private static readonly SharedBuilderMaps sharedBuilderMaps;
        private readonly TimeSpan defaultTimeout = TimeSpan.Zero;
        private readonly IExecutionEngine engine;

        static SQLiteFormatterProvider()
        {
            manipulatorBuilderFactory = new SQLiteManipulationCommandBuilderFactory();

            ITypeMap tableTypeMap = new TableTypeMap();
            typeMap = new DefaultStorageTypeMap(tableTypeMap);
            sharedBuilderMaps = new BuilderMaps(manipulatorBuilderFactory, DataManipulatorSupport);
        }

        public SQLiteFormatterProvider(IExecutionEngine engine) => this.engine = engine;

        public static IDataManipulatorSupport DataManipulatorSupport { get; } = new DataManipulatorSupport
                                                                                {
                                                                                    MultipleStatementsPerCommand = false,
                                                                                    OverflowHandling = OverflowHandlingStyle.Expand,
                                                                                    UnderflowHandling = UnderflowHandlingStyle.None,
                                                                                    NativeExceptionTypeName = ExceptionName,
                                                                                    RightJoin = false
                                                                                };

        public IDataDefiner DataDefiner => new DataDefinerFormatter(
                                                                    this.engine,
                                                                    null,
                                                                    new DataDefinerSupport
                                                                    {
                                                                        DroppingTables = true,
                                                                        NativeExceptionTypeName = ExceptionName,
                                                                        ConstraintEnforcement = false
                                                                    },
                                                                    DataManipulatorSupport,
                                                                    sharedBuilderMaps,
                                                                    sharedBuilderMaps,
                                                                    sharedBuilderMaps,
                                                                    typeMap,
                                                                    this.defaultTimeout,
                                                                    new SQLiteDefinerCommandBuilderFactory(),
                                                                    manipulatorBuilderFactory);

        public IDataManipulator DataManipulator => new DataManipulatorFormatter(
                                                                                this.engine,
                                                                                null,
                                                                                DataManipulatorSupport,
                                                                                sharedBuilderMaps,
                                                                                sharedBuilderMaps,
                                                                                sharedBuilderMaps,
                                                                                this.defaultTimeout,
                                                                                manipulatorBuilderFactory);
    }
}