﻿namespace Base2art.DataStorage.SQLite.DataDefinition
{
    using System.Linq;
    using DataStorage.DataDefinition;

    public class CreateTableKeysBuilder : DataStorage.DataDefinition.CreateTableKeysBuilder
    {
        public CreateTableKeysBuilder(IStorageTypeMap convertData, IEscapeCharacters escapeCharacters)
            : base(convertData, escapeCharacters)
        {
        }

        public override string BuildSql(string schemaName, string typeName, string key, IGrouping<string, string> key2) => string.Empty;

        public override string BuildSql(string schemaName, string typeName, ILookup<string, string> keys) => string.Empty;
    }
}