﻿namespace Base2art.DataStorage.SQLite
{
    using System;
    using System.Globalization;
    using System.Text;
    using DataStorage.DataManipulation;

    public class BuilderMaps : SharedBuilderMaps
    {
        public BuilderMaps(IManipulationCommandBuilderFactory builderFactory, IDataManipulatorSupport dataManipulatorSupport)
            : base(builderFactory, dataManipulatorSupport)
        {
        }

        public override string OpeningEscapeSequence => "[";

        public override string ClosingEscapeSequence => "]";

        protected override object MapDateTime(DateTime value) => value.ToString("O");

        protected override DateTime MapFromNonNullDateTime(object value)
        {
            var text = value.ToString();
            var result = DateTime.ParseExact(text, "O", CultureInfo.InvariantCulture, DateTimeStyles.AdjustToUniversal);
            return result;
        }

        protected override TimeSpan MapFromNonNullTimeSpan(object value) => TimeSpan.FromMilliseconds(Convert.ToInt64(value));

        protected override object MapTimeSpan(TimeSpan value) => value.TotalMilliseconds;

        protected override void Append(
            StringBuilder sb,
            bool hasValue,
            bool groupFirst,
            string part1,
            OperatorType fieldOperation,
            Type t1,
            Type t2,
            string part2)
        {
            if ((t1 == typeof(DateTime) || t1 == typeof(DateTime?)) && (t2 == typeof(TimeSpan) || t2 == typeof(TimeSpan?)))
            {
                var format = groupFirst ? " {1}(MILLISECOND, ({2}), {0} )" : " {1}(MILLISECOND, {2}, {0})";
                if (fieldOperation == OperatorType.Add)
                {
                    sb.AppendFormat(
                                    format,
                                    part1,
                                    " DATEADD",
                                    part2);
                    return;
                }

                if (fieldOperation == OperatorType.Subtract)
                {
                    sb.AppendFormat(
                                    format,
                                    part1,
                                    " DATESUB",
                                    part2);
                    return;
                }
            }

            base.Append(sb, hasValue, groupFirst, part1, fieldOperation, t1, t2, part2);
        }

        public static Guid FromMySql(byte[] bytearray)
        {
            var ba = new byte[16];
            ba[3] = bytearray[0];
            ba[2] = bytearray[1];
            ba[1] = bytearray[2];
            ba[0] = bytearray[3];

            ba[5] = bytearray[4];
            ba[4] = bytearray[5];

            ba[7] = bytearray[6];
            ba[6] = bytearray[7];

            ba[8] = bytearray[8];
            ba[9] = bytearray[9];

            ba[10] = bytearray[10];
            ba[11] = bytearray[11];
            ba[12] = bytearray[12];
            ba[13] = bytearray[13];
            ba[14] = bytearray[14];
            ba[15] = bytearray[15];

            return new Guid(ba);
        }

        public static byte[] ToMySql(Guid value)
        {
            var bytearray = value.ToByteArray();
            var ba = new byte[16];
            ba[3] = bytearray[0];
            ba[2] = bytearray[1];
            ba[1] = bytearray[2];
            ba[0] = bytearray[3];

            ba[5] = bytearray[4];
            ba[4] = bytearray[5];

            ba[7] = bytearray[6];
            ba[6] = bytearray[7];

            ba[8] = bytearray[8];
            ba[9] = bytearray[9];

            ba[10] = bytearray[10];
            ba[11] = bytearray[11];
            ba[12] = bytearray[12];
            ba[13] = bytearray[13];
            ba[14] = bytearray[14];
            ba[15] = bytearray[15];

            return ba;
        }

        protected override object MapGuid(Guid value) => ToMySql(value);

        protected override Guid MapFromNonNullGuid(object value) => FromMySql((byte[]) value);

        public override string NewUUIDSyntax() => "randomblob(16)";
        public override string RandomSyntax() => "randomblob(16)";
    }
}