

$srcs = Get-ChildItem -Path "src"
foreach ($src in $srcs)
{
    $obj = Join-Path $src.FullName "obj"
    if (Test-Path $obj)
    {
        Remove-Item -Recurse -Force $obj
    }
    
    $bin = Join-Path $src.FullName "bin"
    if (Test-Path $bin)
    {
        Remove-Item -Recurse -Force $bin
    }
}
