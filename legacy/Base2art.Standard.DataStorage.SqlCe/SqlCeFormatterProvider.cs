﻿namespace Base2art.DataStorage.SqlCe
{
    using System;
    using DataStorage.DataDefinition;
    using DataStorage.DataManipulation;

    public class SqlCeFormatterProvider : IDataDefinerAndManipulatorProvider
    {
        private const string ExceptionName = "System.Data.SqlServerCe.SqlCeException";
        private static readonly IManipulationCommandBuilderFactory manipulatorBuilderFactory;
        private static readonly DefaultStorageTypeMap typeMap;
        private static readonly SharedBuilderMaps sharedBuilderMaps;
        private readonly TimeSpan defaultTimeout = TimeSpan.Zero;
        private readonly IExecutionEngine engine;

        static SqlCeFormatterProvider()
        {
            manipulatorBuilderFactory = new SqlCeManipulationCommandBuilderFactory();

            ITypeMap tableTypeMap = new TableTypeMap();
            typeMap = new DefaultStorageTypeMap(tableTypeMap);
            sharedBuilderMaps = new BuilderMaps(manipulatorBuilderFactory, DataManipulatorSupport);
        }

        public SqlCeFormatterProvider(IExecutionEngine engine) => this.engine = engine;

        public static IDataManipulatorSupport DataManipulatorSupport { get; } = new DataManipulatorSupport
                                                                                {
                                                                                    MultipleStatementsPerCommand = false,
                                                                                    OverflowHandling = OverflowHandlingStyle.Truncate,
                                                                                    UnderflowHandling = UnderflowHandlingStyle.Pad,
                                                                                    NativeExceptionTypeName = ExceptionName
                                                                                };

        public IDataDefiner DataDefiner => new DataDefinerFormatter(
                                                                    this.engine,
                                                                    null,
                                                                    new DataDefinerSupport
                                                                    {
                                                                        DroppingTables = false,
                                                                        NativeExceptionTypeName = ExceptionName,
                                                                        ConstraintEnforcement = true
                                                                    },
                                                                    DataManipulatorSupport,
                                                                    sharedBuilderMaps,
                                                                    sharedBuilderMaps,
                                                                    sharedBuilderMaps,
                                                                    typeMap,
                                                                    this.defaultTimeout,
                                                                    new SqlCeDefinerCommandBuilderFactory(),
                                                                    manipulatorBuilderFactory);

        public IDataManipulator DataManipulator => new DataManipulatorFormatter(
                                                                                this.engine,
                                                                                null,
                                                                                DataManipulatorSupport,
                                                                                sharedBuilderMaps,
                                                                                sharedBuilderMaps,
                                                                                sharedBuilderMaps,
                                                                                this.defaultTimeout,
                                                                                manipulatorBuilderFactory);
    }
}