﻿namespace Base2art.DataStorage.Provider.SqlCe
{
    using System.Data.SqlServerCe;
    using Base2art.Dapper;
    using Dapper;
    using DataStorage.SqlCe;
    using Interception.DataDefinition;
    using Interception.DataManipulation;

    public class SqlCeProvider : DataStorageProviderBase
    {
        public SqlCeProvider(
            string[] definerInterceptorNames,
            string[] manipulatorInterceptorNames,
            IDataDefinerInterceptor[] definerInterceptors,
            IDataManipulatorInterceptor[] manipulatorInterceptors) : base(
                                                                          definerInterceptorNames,
                                                                          manipulatorInterceptorNames,
                                                                          definerInterceptors,
                                                                          manipulatorInterceptors)
        {
        }

        protected override IDataDefinerAndManipulatorProvider CreateActualStore(NamedConnectionString named)
        {
            var cstr = named.ConnectionString;

            var factory = new OdbcConnectionFactory<SqlCeConnection>(cstr);

            return new SqlCeFormatterProvider(new DapperExecutionEngine(factory));
        }
    }
}