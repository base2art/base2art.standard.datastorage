﻿namespace Base2art.DataStorage.SqlCe.Features
{
    using Specs;
    using Xunit;

    public class SimpleDbInteraction
    {
        private SimpleDbInteraction_Selecting selecting;

        private SimpleDbInteraction_Inserting inserting;

        private IDataDefinerAndManipulator definer;

        private void BeforeEach()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new SimpleDbInteraction_Selecting();
            this.inserting = new SimpleDbInteraction_Inserting();
        }

        [Fact]
        public async void Should_CountResults()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_CountResults(dbms, db);
        }

        [Fact]
        public async void Should_EscapeTableNames()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_EscapeTableNames(dbms, db);
        }

        [Fact]
        public async void Should_FetchDistinct()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_FetchDistinct(dbms, db);
        }

        [Fact]
        public async void Should_GroupResults()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_GroupResults(dbms, db);
        }

        [Fact]
        public async void Should_InsertIntoFromSelect()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.inserting.Should_InsertIntoFromSelect(dbms, db);
        }

        [Fact]
        public async void Should_InsertMultiple()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.inserting.Should_InsertMultiple(dbms, db);
        }

        [Fact]
        public async void Should_Join_2Ons()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_2Ons(dbms, db);
        }

        [Fact]
        public async void Should_Join_2Times()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_2Times(dbms, db);
        }

        [Fact]
        public async void Should_Join_CrossJoin()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_CrossJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_InnerJoin()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_InnerJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin_NonNullableWhere()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin_NonNullableWhere(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin2Times()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin2Times(dbms, db);
        }

        [Fact]
        public async void Should_Join_RightJoin()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_RightJoin(dbms, db);
        }

        [Fact]
        public async void Should_LimitAndOffsetResults()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_LimitAndOffsetResults(dbms, db);
        }

        [Fact]
        public async void Should_OrderRandomly()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_OrderRandomly(dbms, db);
        }

        [Fact]
        public async void Should_OrderResults()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_OrderResults(dbms, db);
        }

        [Fact]
        public async void Should_ScopeToRequestedFields()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_ScopeToRequestedFields(dbms, db);
        }

        [Fact]
        public async void Should_Select_WithFiltering()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Select_WithFiltering(dbms, db);
        }

        [Fact]
        public async void Should_Select_WithFilteringMultipleWheres()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Select_WithFilteringMultipleWheres(dbms, db);
        }

        [Fact]
        public async void Should_SelectFieldIn()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectFieldIn(dbms, db);
        }

        [Fact]
        public async void Should_SelectFieldInQuery()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectFieldInQuery(dbms, db);
        }

        [Fact]
        public async void Should_SelectMultpleTables()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectMultpleTables(dbms, db);
        }

        [Fact]
        public async void Should_SelectNullTernaryFilter()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectNullTernaryFilter(dbms, db);
        }

        [Fact]
        public async void Should_SelectSingleField()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectSingleField(dbms, db);
        }

        [Fact]
        public async void Should_SimpleSelect()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SimpleSelect(dbms, db);
        }

        [Fact]
        public async void Should_UpdateData()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_UpdateData(dbms, db);
        }
    }
}