﻿namespace Base2art.DataStorage.SqlCe.Features
{
    using System.Data.SqlServerCe;
    using Specs;
    using Xunit;

    public class CreateTableFeature
    {
        private IDataDefinerAndManipulator definer;

        private CreateTableSpec selecting;

        private void BeforeEach()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new CreateTableSpec(typeof(SqlCeException));
            // canDropTables: false
        }

        [Fact]
        public async void ShouldBeAbleToAddIndexes()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToAddIndexes(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToAddKeyes()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToAddKeyes(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToDeclareFields()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToDeclareFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToInferFields()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToInferFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToInferRequiredFields()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToInferRequiredFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToUpgradeTables()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToUpgradeTables(dbms, db);
        }

        [Fact]
        public async void ShouldCreateTable()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldCreateTable(dbms, db);
        }
    }
}