﻿namespace Base2art.DataStorage.SqlCe.Features
{
    using Specs;
    using Xunit;

    public class AllDataTypeInteractionInterfaceFeature
    {
        private IDataDefinerAndManipulator definer;

        private AllDataTypeInteractionInterfaceSpec selecting;

        private void BeforeEach()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new AllDataTypeInteractionInterfaceSpec();
        }

        [Fact]
        public async void CreateAllGenerically()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.CreateAllGenerically(dbms, db);
        }

        [Fact]
        public async void CreateInsertSelectDelete()
        {
            this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.CreateInsertSelectDelete(dbms, db);
        }
    }
}