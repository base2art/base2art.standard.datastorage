﻿namespace Base2art.DataStorage.SqlCe.Features
{
    using System;
    using System.Data.SqlServerCe;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using Base2art.Dapper;
    using Dapper;
    using DataStorage.DataDefinition;
    using Interception.DataDefinition;
    using Interception.DataDefinition.Specific;
    using Interception.DataManipulation;
    using Interception.DataManipulation.Specific;

    public static class FeatureSetup
    {
        private static readonly Lazy<string> ipAddress;

        static FeatureSetup() => ipAddress = new Lazy<string>(GetLocalIPAddress);

        public static IDataDefinerAndManipulator Definer()
        {
            var cstr = ConnectionString();
            var engine = new SqlCeEngine(cstr);
            engine.CreateDatabase();
            var factory = new OdbcConnectionFactory<SqlCeConnection>(cstr);

            var formatter = new SqlCeFormatterProvider(new DapperExecutionEngine(factory));

            var commandBuilder = new SqlCeManipulationCommandBuilderFactory();
            var supports = formatter.DataManipulator.Supports;

            return new WrappingDataStorage(
                                           new IDataDefinerInterceptor[]
                                           {
                                               new DefinerCommandDebuggerInterceptor(
                                                                                     "/home/tyoung/temp/sql-logs/",
                                                                                     ".sqlce-logs.ddl",
                                                                                     new SqlCeDefinerCommandBuilderFactory(),
                                                                                     new DefaultStorageTypeMap(new TableTypeMap()),
                                                                                     new BuilderMaps(commandBuilder, supports))
                                           },
                                           new IDataManipulatorInterceptor[]
                                           {
                                               new ManipulatorCommandDebuggerInterceptor(
                                                                                         "/home/tyoung/temp/sql-logs/",
                                                                                         ".sqlce-logs.sql",
                                                                                         new SqlCeManipulationCommandBuilderFactory(),
                                                                                         new BuilderMaps(commandBuilder, supports),
                                                                                         new BuilderMaps(commandBuilder, supports))
                                           },
                                           formatter.DataDefiner,
                                           formatter.DataManipulator);
        }

        public static string ConnectionString()
        {
            var builder = new SqlCeConnectionStringBuilder();
            builder.Password = "nunitUser!";
            builder.DataSource = "Nunit.sdf";

            if (File.Exists(builder.DataSource))
            {
                File.Delete(builder.DataSource);
            }

            return builder.ToString();
        }

        public static string IPAddress() => ipAddress.Value;

        private static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }

            throw new Exception("Local IP Address Not Found!");
        }
    }
}