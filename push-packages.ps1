
cd nuget-repository

$pkgs = Get-ChildItem -Path "." -filter "*.nupkg"

$key = Get-Content -Raw "~/.config/base2art.nuget-config"
$key = $key.Trim()

ForEach ($pkg in $pkgs) 
{
    if ($pkg.Name -eq "Base2art.Standard.DataStorage.0.0.0.2.nupkg")
    {
       echo "Skipping: $($pkg.Name)..."
    }
    else
    {
        echo "Uploading: $($pkg.Name)..."
        dotnet nuget push $pkg.Name -k "$($key)" --source https://api.nuget.org/v3/index.json
    }
    
}

cd ..
