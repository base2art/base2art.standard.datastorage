﻿namespace Base2art.DataStorage.SQLite.Features
{
    using System.Threading.Tasks;
    using Specs;
    using Xunit;

    public class SimpleDbInteraction
    {
        private SimpleDbInteraction_Selecting selecting;

        private SimpleDbInteraction_Inserting inserting;

        private IDataDefinerAndManipulator definer;

        private DbJoinAndAggregate_Selects joining;

        private async Task BeforeEach()
        {
            this.definer = await FeatureSetup.Definer();
            this.selecting = new SimpleDbInteraction_Selecting();
            this.inserting = new SimpleDbInteraction_Inserting();
            this.joining = new DbJoinAndAggregate_Selects();
        }

        [Fact]
        public async void Should_CountResults()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_CountResults(dbms, db);
        }

        [Fact]
        public async void Should_EscapeTableNames()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_EscapeTableNames(dbms, db);
        }

        [Fact]
        public async void Should_FetchDistinct()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_FetchDistinct(dbms, db);
        }

        [Fact]
        public async void Should_GroupByContent()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.joining.Should_GroupByContent(dbms, db);
        }

        [Fact]
        public async void Should_GroupResults()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_GroupResults(dbms, db);
        }

        [Fact]
        public async void Should_InsertIntoFromSelect()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.inserting.Should_InsertIntoFromSelect(dbms, db);
        }

        [Fact]
        public async void Should_InsertMultiple()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.inserting.Should_InsertMultiple(dbms, db);
        }

        [Fact]
        public async void Should_InsertWithNewIdIntoFromSelect()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.inserting.Should_InsertWithNewIdIntoFromSelect(dbms, db);
        }

        [Fact]
        public async void Should_Join_2Ons()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_2Ons(dbms, db);
        }

        [Fact]
        public async void Should_Join_2Times()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_2Times(dbms, db);
        }

        [Fact]
        public async void Should_Join_CrossJoin()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_CrossJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_InnerJoin()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_InnerJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin_NonNullableWhere()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin_NonNullableWhere(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin2Times()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin2Times(dbms, db);
        }

        [Fact]
        public async void Should_Join_RightJoin()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);

            await this.selecting.Should_Join_RightJoin(dbms, db);
        }

        [Fact]
        public async void Should_JoinOnLike()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.joining.Should_JoinOnLike(dbms, db);
        }

        [Fact]
        public async void Should_LimitAndOffsetResults()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_LimitAndOffsetResults(dbms, db);
        }

        [Fact]
        public async void Should_OrderRandomly()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_OrderRandomly(dbms, db);
        }

        [Fact]
        public async void Should_OrderResults()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_OrderResults(dbms, db);
        }

        [Fact]
        public async void Should_ScopeToRequestedFields()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_ScopeToRequestedFields(dbms, db);
        }

        [Fact]
        public async void Should_Select_WithFiltering()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Select_WithFiltering(dbms, db);
        }

        [Fact]
        public async void Should_Select_WithFilteringMultipleWheres()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Select_WithFilteringMultipleWheres(dbms, db);
        }

        [Fact]
        public async void Should_SelectFieldIn()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectFieldIn(dbms, db);
        }

        [Fact]
        public async void Should_SelectFieldInQuery()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectFieldInQuery(dbms, db);
        }

        [Fact]
        public async void Should_SelectMultpleTables()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectMultpleTables(dbms, db);
        }

        [Fact]
        public async void Should_SelectNullTernaryFilter()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectNullTernaryFilter(dbms, db);
        }

        [Fact]
        public async void Should_SelectSingleField()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectSingleField(dbms, db);
        }

        [Fact]
        public async void Should_SimpleSelect()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SimpleSelect(dbms, db);
        }

        [Fact]
        public async void Should_UpdateData()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_UpdateData(dbms, db);
        }
    }
}