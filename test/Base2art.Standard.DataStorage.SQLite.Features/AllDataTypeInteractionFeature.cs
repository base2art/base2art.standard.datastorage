﻿namespace Base2art.DataStorage.SQLite.Features
{
    using System.Threading.Tasks;
    using Specs;
    using Xunit;

    public class AllDataTypeInteractionFeature
    {
        private IDataDefinerAndManipulator definer;

        private AllDataTypeInteractionSpec selecting;

        private async Task BeforeEach()
        {
            this.definer = await FeatureSetup.Definer();
            this.selecting = new AllDataTypeInteractionSpec();
        }

        [Fact]
        public async void CreateAllGenerically()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.CreateAllGenerically(dbms, db);
        }

        [Fact]
        public async void CreateInsertSelectDelete()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.CreateInsertSelectDelete(dbms, db);
        }
    }
}