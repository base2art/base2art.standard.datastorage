﻿namespace Base2art.DataStorage.SQLite.Features
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading.Tasks;
    using Base2art.Dapper;
    using Base2art.Dapper.OdbcClient;
    using Dapper;
    using DataStorage.DataDefinition;
    using Interception.DataDefinition;
    using Interception.DataDefinition.Specific;
    using Interception.DataManipulation;
    using Interception.DataManipulation.Specific;
    using Microsoft.Data.Sqlite;

    public static class FeatureSetup
    {
        private static readonly Lazy<string> ipAddress;

        static FeatureSetup() => ipAddress = new Lazy<string>(GetLocalIPAddress);

        public static async Task<IDataDefinerAndManipulator> Definer()
        {
            var cstr = await ConnectionString();
            var factory = new OdbcConnectionFactory<DelegatedConnection<SqliteConnection>>(cstr);

            var formatter = new SQLiteFormatterProvider(new DapperExecutionEngine(factory));

            var commandBuilder = new SQLiteManipulationCommandBuilderFactory();
            var supports = formatter.DataManipulator.Supports;
            return new WrappingDataStorage(
                                           new IDataDefinerInterceptor[]
                                           {
                                               new DefinerCommandDebuggerInterceptor(
                                                                                     "/home/tyoung/temp/sql-logs/",
                                                                                     ".SQLite-logs.ddl",
                                                                                     new SQLiteDefinerCommandBuilderFactory(),
                                                                                     new DefaultStorageTypeMap(new TableTypeMap()),
                                                                                     new BuilderMaps(commandBuilder, supports))
                                           },
                                           new IDataManipulatorInterceptor[]
                                           {
                                               new ManipulatorCommandDebuggerInterceptor(
                                                                                         "/home/tyoung/temp/sql-logs/",
                                                                                         ".SQLite-logs.sql",
                                                                                         commandBuilder,
                                                                                         new BuilderMaps(commandBuilder, supports),
                                                                                         new BuilderMaps(commandBuilder, supports))
                                           },
                                           formatter.DataDefiner,
                                           formatter.DataManipulator);
        }

        public static async Task<string> ConnectionString(int counter = 0)
        {
            GC.Collect();
            if (counter > 0)
            {
                await Task.Delay(TimeSpan.FromSeconds(2));
            }

            if (counter == 20)
            {
                throw new TimeoutException();
            }

            var builder = new SqliteConnectionStringBuilder();
//            builder.Password = "nunitUser!";
            builder.DataSource = "nunit.sqlite";
//            builder.Pooling = true;

            if (File.Exists(builder.DataSource))
            {
                try
                {
                    File.Delete(builder.DataSource);
                }
                catch (Exception)
                {
                    return await ConnectionString(counter + 1);
                }
            }

            return builder.ToString();
        }

        public static string IPAddress() => ipAddress.Value;

        private static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }

            throw new Exception("Local IP Address Not Found!");
        }
    }
}