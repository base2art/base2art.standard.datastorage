﻿namespace Base2art.DataStorage.SQLite.Features
{
    using System.Threading.Tasks;
    using Microsoft.Data.Sqlite;
    using Specs;
    using Xunit;

    public class CreateTableFeature
    {
        private IDataDefinerAndManipulator definer;

        private CreateTableSpec selecting;

        private async Task BeforeEach()
        {
            this.definer = await FeatureSetup.Definer();
            this.selecting = new CreateTableSpec(typeof(SqliteException));
            //  canDropTables: false,
            // truncatesChar: true
            //,truncatesChar: true
        }

        [Fact]
        public async void ShouldBeAbleToAddIndexes()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToAddIndexes(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToAddKeyes()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToAddKeyes(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToDeclareFields()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToDeclareFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToInferFields()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToInferFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToInferRequiredFields()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToInferRequiredFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToUpgradeTables()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);

            await this.selecting.ShouldBeAbleToUpgradeTables(dbms, db);
        }

        [Fact]
        public async void ShouldCreateTable()
        {
            await this.BeforeEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldCreateTable(dbms, db);
        }
    }
}