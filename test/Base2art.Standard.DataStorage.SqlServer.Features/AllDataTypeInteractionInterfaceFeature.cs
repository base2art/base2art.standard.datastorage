﻿namespace Base2art.DataStorage.SqlServer.Features
{
    using Specs;
    using Xunit;

    public class AllDataTypeInteractionInterfaceFeature
    {
        private IDataDefinerAndManipulator definer;

        private AllDataTypeInteractionInterfaceSpec selecting;

        private void BeforEach()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new AllDataTypeInteractionInterfaceSpec();
        }

        [Fact]
        public async void CreateAllGenerically()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.CreateAllGenerically(dbms, db);
        }

        [Fact]
        public async void CreateInsertSelectDelete()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.CreateInsertSelectDelete(dbms, db);
        }
    }
}