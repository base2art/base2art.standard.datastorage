﻿namespace Base2art.DataStorage.SqlServer.Features
{
    using System.Data.SqlClient;
    using Specs;
    using Xunit;

    public class CreateTableFeature
    {
        private IDataDefinerAndManipulator definer;

        private CreateTableSpec selecting;

        private void BeforEach()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new CreateTableSpec(typeof(SqlException));
            // canDropTables: true, throwsExceptionOnOverflow: true
        }

        [Fact]
        public async void ShouldBeAbleToAddIndexes()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToAddIndexes(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToAddKeyes()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToAddKeyes(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToDeclareFields()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToDeclareFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToInferFields()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToInferFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToInferRequiredFields()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToInferRequiredFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToUpgradeTables()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToUpgradeTables(dbms, db);
        }

        [Fact]
        public async void ShouldCreateTable()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldCreateTable(dbms, db);
        }
    }
}