﻿namespace Base2art.DataStorage.SqlServer.Features
{
    using System;
    using System.Data.SqlClient;
    using System.IO;
    using System.Net;
    using System.Net.Sockets;
    using Base2art.Dapper;
    using Dapper;
    using DataStorage.DataDefinition;
    using Interception.DataDefinition;
    using Interception.DataDefinition.Specific;
    using Interception.DataManipulation;
    using Interception.DataManipulation.Specific;

    public static class FeatureSetup
    {
        private static readonly Lazy<string> ipAddress;

        static FeatureSetup() => ipAddress = new Lazy<string>(GetLocalIPAddress);

        public static IDataDefinerAndManipulator Definer()
        {
            var cstr = ConnectionString();
            var factory = new OdbcConnectionFactory<SqlConnection>(cstr);

            var formatter = new SqlServerFormatterProvider(new DapperExecutionEngine(factory), "nunit1", TimeSpan.FromSeconds(10));

            var commandBuilder = new SqlServerManipulationCommandBuilderFactory();
            var supports = formatter.DataManipulator.Supports;
            return new WrappingDataStorage(
                                           new IDataDefinerInterceptor[]
                                           {
                                               new DefinerCommandDebuggerInterceptor(
                                                                                     "/home/tyoung/temp/sql-logs/",
                                                                                     ".sql-logs.ddl",
                                                                                     new SqlServerDefinerCommandBuilderFactory(),
                                                                                     new DefaultStorageTypeMap(new TableTypeMap()),
                                                                                     new BuilderMaps(commandBuilder, supports))
                                           },
                                           new IDataManipulatorInterceptor[]
                                           {
                                               new ManipulatorCommandDebuggerInterceptor(
                                                                                         "/home/tyoung/temp/sql-logs/",
                                                                                         ".sql-logs.sql",
                                                                                         commandBuilder,
                                                                                         new BuilderMaps(commandBuilder, supports),
                                                                                         new BuilderMaps(commandBuilder, supports))
                                           },
                                           formatter.DataDefiner,
                                           formatter.DataManipulator);
        }

        public static string ConnectionString()
        {
            var path = Path.Combine(
                                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                    "base2art",
                                    "data-storage",
                                    "connectionString-sqlserver.config");

            if (File.Exists(path))
            {
                return File.ReadAllText(path).Replace("\r", "\n").Replace("\n", "");
            }

            var builder = new SqlConnectionStringBuilder();
            builder.InitialCatalog = "nunit";
            builder.DataSource = ".";
            builder.Pooling = true;
            builder.IntegratedSecurity = true;

            return builder.ToString();
        }

        public static string IPAddress() => ipAddress.Value;

        private static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }

            throw new Exception("Local IP Address Not Found!");
        }
    }
}