﻿namespace Base2art.DataStorage.InMemory.Features
{
    using System;
    using System.Collections.Generic;
    using Data;
    using FluentAssertions;
    using Xunit;

    public class AllTypes
    {
        private IDataDefinerAndManipulator definer;

        private void BeforEach()
        {
            this.definer = FeatureSetup.CreateDataDefiner();
        }

        private void Compare<T>(T[] x, T[] y)
            where T : IComparable<T>
        {
            x.Length.Should().Be(y.Length);

            for (var i = 0; i < x.Length; i++)
            {
                x[i].CompareTo(y[i]).Should().Be(0);
            }
        }

        [Fact]
        public async void ShouldCreateInsertUpdate()
        {
            this.BeforEach();
            IDbms dbms = new Dbms(this.definer);
            IDataStore db = new DataStore(this.definer);

            await dbms.CreateTable<all_field_types_v1>()
                      .Fields(t => t.Field(x => x.Boolean_value)
                                    .Field(x => x.Decimal_value)
                                    .Field(x => x.Double_value)
                                    .Field(x => x.Float_value)
                                    .Field(x => x.int_value)
                                    .Field(x => x.long_value)
                                    .Field(x => x.short_value)
                                    .Field(x => x.string_value)
                                    .Field(x => x.binary_value)
                                    .Field(x => x.date_value)
                                    .Field(x => x.dateTimeOffset_value)
                                    .Field(x => x.datetime_value)
                                    .Field(x => x.interval_value)
                                    .Field(x => x.guid_value)
                                    .Field(x => x.xml1_value)
                                    .Field(x => x.xml2_value)
                                    .Field(x => x.json_value)
                                    .Field(x => x.Boolean_nullable_value)
                                    .Field(x => x.Decimal_nullable_value)
                                    .Field(x => x.Double_nullable_value)
                                    .Field(x => x.Float_nullable_value)
                                    .Field(x => x.int_nullable_value)
                                    .Field(x => x.long_nullable_value)
                                    .Field(x => x.short_nullable_value)
                                    .Field(x => x.date_nullable_value)
                                    .Field(x => x.dateTimeOffset_nullable_value)
                                    .Field(x => x.datetime_nullable_value)
                                    .Field(x => x.interval_nullable_value)
                                    .Field(x => x.guid_nullable_value)
                             )
                      .Execute();

            var g = Guid.NewGuid();

            await db.Insert<all_field_types_v1>()
                    .Record(t => t.Field(x => x.Boolean_value, true)
                                  .Field(x => x.Decimal_value, 10.0m)
                                  .Field(x => x.Double_value, double.MaxValue / 2)
                                  .Field(x => x.Float_value, float.MaxValue / 2)
                                  .Field(x => x.int_value, int.MaxValue / 2)
                                  .Field(x => x.long_value, int.MaxValue)
                                  .Field(x => x.short_value, short.MaxValue / 2)
                                  .Field(x => x.string_value, g.ToString("N"))
                                  .Field(x => x.binary_value, g.ToByteArray())
                                  .Field(x => x.date_value, DateTime.UtcNow.Date)
                                  //                        .Field(x => x.dateTimeOffset_value)
                                  .Field(x => x.datetime_value, DateTime.UtcNow)
                                  .Field(x => x.interval_value, DateTime.UtcNow.TimeOfDay)
                                  .Field(x => x.guid_value, g)
                                  .Field(x => x.xml1_value, null)
                                  .Field(x => x.xml2_value, null)
                                  .Field(x => x.json_value, new Dictionary<string, object> {{"a", 1}})
                                  .Field(x => x.Boolean_nullable_value, true)
                                  .Field(x => x.Decimal_nullable_value, 10.0m)
                                  .Field(x => x.Double_nullable_value, double.MaxValue / 2)
                                  .Field(x => x.Float_nullable_value, float.MaxValue / 2)
                                  .Field(x => x.int_nullable_value, int.MaxValue / 2)
                                  .Field(x => x.long_nullable_value, int.MaxValue)
                                  .Field(x => x.short_nullable_value, short.MaxValue / 2)
                                  .Field(x => x.date_nullable_value, DateTime.UtcNow.Date)
                                  .Field(x => x.datetime_nullable_value, DateTime.UtcNow)
                                  .Field(x => x.interval_nullable_value, DateTime.UtcNow.TimeOfDay)
                                  .Field(x => x.guid_nullable_value, g))
                    .Execute();

            var data = await db.SelectSingle<all_field_types_v1>().Execute();
            this.Compare(data.binary_value, g.ToByteArray());
            data.Boolean_nullable_value.Should().BeTrue();
            data.Boolean_value.Should().BeTrue();
            data.date_nullable_value.Should().BeCloseTo(DateTime.UtcNow.Date);
            data.date_value.Should().BeCloseTo(DateTime.UtcNow.Date);

            data.datetime_nullable_value.Should().BeCloseTo(DateTime.UtcNow, 1099);
            data.datetime_value.Should().BeCloseTo(DateTime.UtcNow, 1099);

            data.Decimal_nullable_value.Should().Be(10.0m);
            data.Decimal_value.Should().Be(10m);

            data.Double_nullable_value.Should().Be(double.MaxValue / 2);
            data.Double_value.Should().Be(double.MaxValue / 2);

            data.Float_nullable_value.Should().BeApproximately(float.MaxValue / 2, 2.0f * 10000000000000000000000000000000000f);
            data.Float_value.Should().BeApproximately(float.MaxValue / 2, 2.0f * 10000000000000000000000000000000000f);

            data.guid_nullable_value.Should().Be(g);
            data.guid_value.Should().Be(g);

            data.int_nullable_value.Should().Be(int.MaxValue / 2);
            data.int_value.Should().Be(int.MaxValue / 2);

            data.long_nullable_value.Should().Be(int.MaxValue);
            data.long_value.Should().Be(int.MaxValue);

            data.short_nullable_value.Should().Be(short.MaxValue / 2);
            data.short_value.Should().Be(short.MaxValue / 2);

            data.interval_nullable_value.Should().BeCloseTo(DateTime.UtcNow.TimeOfDay, 1099);
            data.interval_value.Should().BeCloseTo(DateTime.UtcNow.TimeOfDay, 1099);

//            data.object_value.Should().Be(null);
            data.json_value.Count.Should().Be(1);
            data.string_value.Should().Be(g.ToString("N"));
            data.xml1_value.Should().BeNull();
            data.xml2_value.Should().Be(null);
        }
    }
}