﻿namespace Base2art.DataStorage.InMemory.Features
{
    using System;
    using Specs;
    using Xunit;
    using Xunit.Abstractions;

    public class CreateTableFeature
    {
        private readonly ITestOutputHelper testOutputHelper;
        private IDataDefinerAndManipulator definer;

        private CreateTableSpec selecting;

        public CreateTableFeature(ITestOutputHelper testOutputHelper)
        {
            this.testOutputHelper = testOutputHelper;
        }

        private void BeforEach()
        {
            this.definer = FeatureSetup.CreateDataDefiner();
            this.selecting = new CreateTableSpec(typeof(InvalidOperationException));
            // throwsExceptionOnOverflow: false
        }

        [Fact]
        public async void ShouldBeAbleToAddIndexes()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToAddIndexes(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToAddKeyes()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToAddKeyes(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToDeclareFields()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToDeclareFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToInferFields()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToInferFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToInferRequiredFields()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToInferRequiredFields(dbms, db, this.testOutputHelper);
        }

        [Fact]
        public async void ShouldBeAbleToUpgradeTables()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToUpgradeTables(dbms, db);
        }

        [Fact]
        public async void ShouldCreateTable()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldCreateTable(dbms, db);
        }
    }
}