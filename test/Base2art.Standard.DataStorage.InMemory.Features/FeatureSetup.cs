﻿namespace Base2art.DataStorage.InMemory.Features
{
    using DataDefinition;
    using DataManipulation;
    using Interception.DataDefinition;
    using Interception.DataDefinition.Specific;
    using Interception.DataManipulation;
    using Interception.DataManipulation.Specific;
    using SqlServer;

    public class FeatureSetup
    {
        public static IDataDefinerAndManipulator CreateDataDefiner()
        {
            var definer = new DataDefinerV2(true);
            IDataDefiner d1 = definer;
            IDataManipulator d2 = definer;

            var commandBuilder = new SqlServerManipulationCommandBuilderFactory();
            return new WrappingDataStorage(
                                           new IDataDefinerInterceptor[]
                                           {
                                               new DefinerCommandDebuggerInterceptor(
                                                                                     "/home/tyoung/temp/sql-logs/",
                                                                                     ".sql-logs.mem.ddl",
                                                                                     new SqlServerDefinerCommandBuilderFactory(),
                                                                                     new DefaultStorageTypeMap(new TableTypeMap()),
                                                                                     new BuilderMaps(commandBuilder, d2.Supports))
                                           },
                                           new IDataManipulatorInterceptor[]
                                           {
                                               new ManipulatorCommandDebuggerInterceptor(
                                                                                         "/home/tyoung/temp/sql-logs/",
                                                                                         ".sql-logs.mem.sql",
                                                                                         commandBuilder,
                                                                                         new BuilderMaps(commandBuilder, d2.Supports),
                                                                                         new BuilderMaps(commandBuilder, d2.Supports))
                                           },
                                           definer,
                                           definer);
        }
    }
}