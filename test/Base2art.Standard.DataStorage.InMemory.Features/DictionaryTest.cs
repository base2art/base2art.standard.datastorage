﻿namespace Base2art.DataStorage.InMemory.Features
{
    using System.Collections.Generic;
    using FluentAssertions;
    using HolisticPersistence;
    using Xunit;

    public class ComparerFeatures
    {
        [Theory]
        [InlineData("a", 1, "a", 1, true)]
        [InlineData("a", 1, "b", 1, false)]
        [InlineData("a", 1, "b", "1", false)]
        [InlineData("a", 1, "a", 2, false)]
        [InlineData("a", 1, "a", "1", false)]
        public void ShouldCompare_DifferentData(string key1, object data1, string key2, object data2, bool areEqual)
        {
            var dict1 = new LinkedList<object>();
            var dict2 = new LinkedList<object>();

            dict1.AddLast(key1);
            dict1.AddLast(data1);
            dict2.AddLast(key2);
            dict2.AddLast(data2);

            IEqualityComparer<LinkedList<object>> comparer = new LinkedListEqualityComparer();
            var result = comparer.Equals(dict1, dict2);
            result.Should().Be(areEqual);
        }

        [Fact]
        public void ShouldCompare_Empty()
        {
            var dict1 = new LinkedList<object>();
            var dict2 = new LinkedList<object>();

            IEqualityComparer<LinkedList<object>> comparer = new LinkedListEqualityComparer();
            comparer.Equals(dict1, dict2).Should().Be(true);
        }
    }
}