﻿namespace Base2art.DataStorage.InMemory.Features
{
    using System;
    using System.Collections.Generic;
    using FluentAssertions;
    using HolisticPersistence;
    using Xunit;

    public class RecordRowSliceComparerFeatures
    {
        [Theory]
        [InlineData("a", 1, "a", 1, true)]
        [InlineData("a", 1, "b", 1, false)]
        [InlineData("a", 1, "b", "1", false)]
        [InlineData("a", 1, "a", 2, false)]
        [InlineData("a", 1, "a", "1", false)]
        public void ShouldCompare_DifferentData(string key1, object data1, string key2, object data2, bool areEqual)
        {
            var dict1 = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var dict2 = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            dict1[key1] = data1;
            dict2[key2] = data2;
            var comparer = new RecordRowSliceComparer();
            var result = comparer.Equals(new RecordRowSlice("table1", dict1, dict1), new RecordRowSlice("table2", dict2, dict2));
            result.Should().Be(areEqual);
        }

        [Fact]
        public void ShouldCompare_Empty()
        {
            var dict1 = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var dict2 = new Dictionary<string, object>(StringComparer.OrdinalIgnoreCase);
            var comparer = new RecordRowSliceComparer();
            var result = comparer.Equals(new RecordRowSlice("table1", dict1, dict1), new RecordRowSlice("table2", dict2, dict2));
            result.Should().Be(true);
        }
    }
}