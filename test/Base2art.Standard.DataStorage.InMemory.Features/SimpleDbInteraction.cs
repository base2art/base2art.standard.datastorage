﻿namespace Base2art.DataStorage.InMemory.Features
{
    using Specs;
    using Xunit;

    public class SimpleDbInteraction
    {
        private IDataDefinerAndManipulator definer;

        private SimpleDbInteraction_Selecting selecting;

        private SimpleDbInteraction_Inserting inserting;
        private DbJoinAndAggregate_Selects joining;

        private void BeforEach()
        {
            this.definer = FeatureSetup.CreateDataDefiner();
            this.selecting = new SimpleDbInteraction_Selecting();
            this.inserting = new SimpleDbInteraction_Inserting();
            this.joining = new DbJoinAndAggregate_Selects();
        }

        [Fact]
        public async void Should_CountResults()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_CountResults(dbms, db);
        }

        [Fact]
        public async void Should_EscapeTableNames()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_EscapeTableNames(dbms, db);
        }

        [Fact]
        public async void Should_FetchDistinct()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_FetchDistinct(dbms, db);
        }

        [Fact]
        public async void Should_GroupByContent()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.joining.Should_GroupByContent(dbms, db);
        }

        [Fact]
        public async void Should_GroupResults()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_GroupResults(dbms, db);
        }

        [Fact]
        public async void Should_InsertIntoFromSelect()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.inserting.Should_InsertIntoFromSelect(dbms, db);
        }

        [Fact]
        public async void Should_InsertMultiple()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.inserting.Should_InsertMultiple(dbms, db);
        }

        [Fact]
        public async void Should_InsertWithNewIdIntoFromSelect()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.inserting.Should_InsertWithNewIdIntoFromSelect(dbms, db);
        }

        [Fact]
        public async void Should_Join_2Ons()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_2Ons(dbms, db);
        }

        [Fact]
        public async void Should_Join_2Times()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_2Times(dbms, db);
        }

        [Fact]
        public async void Should_Join_CrossJoin()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_CrossJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_InnerJoin()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_InnerJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin_NonNullableWhere()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin_NonNullableWhere(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin2Times()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin2Times(dbms, db);
        }

        [Fact]
        public async void Should_Join_RightJoin()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_RightJoin(dbms, db);
        }

        [Fact]
        public async void Should_JoinOnLike()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.joining.Should_JoinOnLike(dbms, db);
        }

        [Fact]
        public async void Should_LimitAndOffsetResults()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_LimitAndOffsetResults(dbms, db);
        }

        [Fact]
        public async void Should_OrderRandomly()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_OrderRandomly(dbms, db);
        }

        [Fact]
        public async void Should_OrderResults()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_OrderResults(dbms, db);
        }

        [Fact]
        public async void Should_ScopeToRequestedFields()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_ScopeToRequestedFields(dbms, db);
        }

        [Fact]
        public async void Should_Select_WithFiltering()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Select_WithFiltering(dbms, db);
        }

        [Fact]
        public async void Should_Select_WithFilteringMultipleWheres()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Select_WithFilteringMultipleWheres(dbms, db);
        }

        [Fact]
        public async void Should_SelectFieldIn()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectFieldIn(dbms, db);
        }

        [Fact]
        public async void Should_SelectFieldInQuery()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectFieldInQuery(dbms, db);
        }

        [Fact]
        public async void Should_SelectMultpleTables()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectMultpleTables(dbms, db);
        }

        [Fact]
        public async void Should_SelectNullTernaryFilter()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectNullTernaryFilter(dbms, db);
        }

        [Fact]
        public async void Should_SelectSingleField()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectSingleField(dbms, db);
        }

        [Fact]
        public async void Should_SimpleSelect()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SimpleSelect(dbms, db);
        }

        [Fact]
        public async void Should_UpdateData()
        {
            this.BeforEach();
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_UpdateData(dbms, db);
        }

//        [Fact]
//        public async void Should_IsNullReturn()
//        {
//            IDataStore db = new DataStore(definer);
//            IDbms dbms = new Dbms(this.definer);
//            await this.selecting.Should_IsNullReturn(dbms, db);
//        }
    }
}