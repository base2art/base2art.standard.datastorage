﻿namespace Base2art.Standard.DataStorage.Provider.Features
{
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.DataStorage.Specs;
    using Xunit;

    public class SimpleDbInteraction
    {
        private readonly SimpleDbInteraction_Inserting inserting = new SimpleDbInteraction_Inserting();
        private readonly DbJoinAndAggregate_Selects joining = new DbJoinAndAggregate_Selects();
        private readonly SimpleDbInteraction_Selecting selecting = new SimpleDbInteraction_Selecting();

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_FetchDistinct(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_FetchDistinct(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_JoinOnLike(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.joining.Should_JoinOnLike(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_GroupByContent(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.joining.Should_GroupByContent(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_FetchFieldInByGuid(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_FetchFieldInByGuid(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_FetchLatestByJoin(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_FetchLatestByJoin(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_FetchLatestByJoinWithInterface(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_FetchLatestByJoinInterface(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_Join_CrossJoin(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_Join_CrossJoin(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_Join_InnerJoin(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_Join_InnerJoin(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_Join_LeftJoin(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_Join_LeftJoin(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_Join_LeftJoin_NonNullableWhere(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_Join_LeftJoin_NonNullableWhere(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_Join_LeftJoin2Times(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_Join_LeftJoin2Times(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_Join_RightJoin(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_Join_RightJoin(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_LimitAndOffsetResults(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_LimitAndOffsetResults(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_OrderResults(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_OrderResults(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_GroupResults(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_GroupResults(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_CountResults(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_CountResults(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_ScopeToRequestedFields(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_ScopeToRequestedFields(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_Select_WithFiltering(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_Select_WithFiltering(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_Select_WithFilteringMultipleWheres(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_Select_WithFilteringMultipleWheres(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_SimpleSelect(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_SimpleSelect(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_SelectSingleField(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_SelectSingleField(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_SelectNullTernaryFilter(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_SelectNullTernaryFilter(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_InsertIntoFromSelect(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.inserting.Should_InsertIntoFromSelect(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_InsertWithNewIdIntoFromSelect(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            
            int? maxValue = dbmsName == "GoogleDocs" ? 1000 : (int?)null;
            await this.inserting.Should_InsertWithNewIdIntoFromSelect(dbms, db, maxValue);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_InsertMultiple(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.inserting.Should_InsertMultiple(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_InsertDynamic(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            var dynamicData = definer.CreateDynamicDataStoreAccess(connectionString);
            await this.inserting.Should_InsertDynamic(dbmsName, dbms, db, dynamicData);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_SelectFieldInQuery(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_SelectFieldInQuery(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_SelectFieldIn(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_SelectFieldIn(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_SelectMultpleTables(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_SelectMultpleTables(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_Join_2Times(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_Join_2Times(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_Join_2Ons(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_Join_2Ons(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_OrderRandomly(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_OrderRandomly(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_EscapeTableNames(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_EscapeTableNames(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void Should_UpdateData(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.Should_UpdateData(dbms, db);
        }
    }
}