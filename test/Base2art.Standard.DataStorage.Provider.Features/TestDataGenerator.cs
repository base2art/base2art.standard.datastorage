﻿namespace Base2art.Standard.DataStorage.Provider.Features
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.SqlClient;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.DataStorage.Interception.DataDefinition;
    using Base2art.DataStorage.Interception.DataManipulation;
    using Base2art.DataStorage.Provider.GoogleDocs;
    using Base2art.DataStorage.Provider.InMemory;
    using Base2art.DataStorage.Provider.MySql;
    using Base2art.DataStorage.Provider.SQLite;
    using Base2art.DataStorage.Provider.SqlServer;
    using Base2art.DataStorage.Provider.GoogleDocs;
    using Microsoft.Data.Sqlite;
    using MySqlConnector;

    public class TestDataGenerator : IEnumerable<object[]>
    {
        private static readonly Lazy<string[]> ipAddress;

        private readonly List<object[]> data = new List<object[]>
                                               {
                                                   new object[]
                                                   {
                                                       "InMemory",
                                                       Task.FromResult<IDataStorageProvider>(new CustomInMemoryProvider(
                                                                                              new string[0],
                                                                                              new string[0],
                                                                                              DefinerInterceptors,
                                                                                              ManipulatorInterceptors)),
                                                       Task.FromResult(new NamedConnectionString())
                                                   },
                                                   new object[]
                                                   {
                                                       "SqlServer",
                                                       Task.FromResult<IDataStorageProvider>(new SqlServerProvider(
                                                                                              TimeSpan.FromSeconds(60),
                                                                                              new string[0],
                                                                                              new string[0],
                                                                                              DefinerInterceptors,
                                                                                              ManipulatorInterceptors)),
                                                       Task.FromResult(new NamedConnectionString
                                                                       {
                                                                           Name = "test",
                                                                           ConnectionString = SqlConnectionString(),
                                                                           AdditionalParameters =
                                                                               new Dictionary<string, string> {{"schema", "nunit1"}}
                                                                       })
                                                   },
                                                   new object[]
                                                   {
                                                       "Sqlite",
                                                       Task.FromResult<IDataStorageProvider>(new SQLiteProvider(
                                                                                              new string[0],
                                                                                              new string[0],
                                                                                              DefinerInterceptors,
                                                                                              ManipulatorInterceptors)),
                                                       new Func<Task<NamedConnectionString>>(async () => new NamedConnectionString
                                                                                                         {
                                                                                                             Name = "test",
                                                                                                             ConnectionString = await SqliteConnectionString()
                                                                                                         })()
                                                   },
                                                   new object[]
                                                   {
                                                       "GoogleDocs",
                                                       Task.FromResult<IDataStorageProvider>(new GoogleDocsProvider(
                                                                                              new string[0],
                                                                                              new string[0],
                                                                                              DefinerInterceptors,
                                                                                              ManipulatorInterceptors)),
                                                       new Func<Task<NamedConnectionString>>(async () =>
                                                       {
                                                           var delay = counter switch
                                                           {
                                                               0 => 0,
                                                               1 => 15,
                                                               _ => 30
                                                           };

                                                           counter += 1;

                                                           await Task.Delay(TimeSpan.FromSeconds(delay));

                                                           return new NamedConnectionString
                                                                  {
                                                                      Name = "test",
                                                                      ConnectionString = GoogleDocsConnectionString()
                                                                  };
                                                       })()
                                                   },
                                                   new object[]
                                                   {
                                                       "MySql",
                                                       Task.FromResult<IDataStorageProvider>(new MySqlProvider(
                                                                                              TimeSpan.FromSeconds(60),
                                                                                              new string[0],
                                                                                              new string[0],
                                                                                              DefinerInterceptors,
                                                                                              ManipulatorInterceptors)),
                                                       new Func<Task<NamedConnectionString>>(async () => new NamedConnectionString
                                                                                                         {
                                                                                                             Name = "test",
                                                                                                             ConnectionString =
                                                                                                                 MySqlConnectionString()
                                                                                                         })()
                                                   }
                                               };

        private static int counter;

        private static string GoogleDocsConnectionString()
        {
            var cstr = new GoogleDocsConnectionStringBuilder();
            cstr.CredentialsPath = "/home/tyoung/.config/Base2art/superContest/";
            cstr.InitialCatalog = "1UN-GTDCsT7JP-PqmecXKqo3fdOrqg77wzhs3vXCISxo";
            return cstr.ConnectionString;
        }

        static TestDataGenerator() => ipAddress = new Lazy<string[]>(GetLocalIPAddress);

        private static IDataManipulatorInterceptor[] ManipulatorInterceptors { get; } = new IDataManipulatorInterceptor[0];

        private static IDataDefinerInterceptor[] DefinerInterceptors { get; } = new IDataDefinerInterceptor[0];

        public IEnumerator<object[]> GetEnumerator() => this.data.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => this.GetEnumerator();

        private static string SqlConnectionString()
        {
            var path = Path.Combine(
                                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                    "base2art",
                                    "data-storage",
                                    "connectionString-sqlserver.config");

            if (File.Exists(path))
            {
                return File.ReadAllText(path).Replace("\r", "\n").Replace("\n", "");
            }

            var builder = new SqlConnectionStringBuilder();
            builder.InitialCatalog = "nunit";
            builder.DataSource = ".";
            builder.Pooling = true;
            builder.IntegratedSecurity = true;

            return builder.ToString();
        }

        public static async Task<string> SqliteConnectionString(int counter = 0)
        {
            GC.Collect();
            if (counter > 0)
            {
                await Task.Delay(TimeSpan.FromSeconds(2));
            }

            if (counter == 20)
            {
                throw new TimeoutException();
            }

            var builder = new SqliteConnectionStringBuilder();
            builder.DataSource = "nunit.sqlite";

            if (!File.Exists(builder.DataSource))
            {
                return builder.ToString();
            }

            try
            {
                File.Delete(builder.DataSource);
                return builder.ToString();
            }
            catch (Exception)
            {
                return await SqliteConnectionString(counter + 1);
            }
        }

        public static string MySqlConnectionStringFromConfig()
        {
            var path = Path.Combine(
                                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                    "base2art",
                                    "data-storage",
                                    "connectionString-mysql.config");

            if (!File.Exists(path))
            {
                throw new InvalidOperationException();
            }

            return File.ReadAllText(path).Replace("\r", "\n").Replace("\n", "");
        }

        public static string MySqlConnectionStringHardCoded()
        {
            var builder = new MySqlConnectionStringBuilder();
            builder.UserID = "nunit";
            builder.Database = "nunit";
            builder.Password = "nunitUser!";
            builder.Server = "10.17.4.57";
            builder.Pooling = true;
            builder.SslMode = MySqlSslMode.None;

            return builder.ToString();
        }

        public static string MySqlConnectionString()
        {
            var ipAddresses = IPAddress();

            var remoteIps = new[] {"10.0.2.15", "10.17.168.68", "10.17.168.30", "172.20.24.59", "172.20.25.40", "10.17.168.43"};

            return ipAddresses.Any(ipAddr => remoteIps.Contains(ipAddr))
                       ? MySqlConnectionStringHardCoded()
                       : MySqlConnectionStringFromConfig();
        }

        public static string[] IPAddress() => ipAddress.Value;

        private static string[] GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());

            var rez = host.AddressList
                          .Where(ip => ip.AddressFamily == AddressFamily.InterNetwork)
                          .Select(x => x.ToString())
                          .ToArray();

            if (rez == null || rez.Length == 0)
            {
                throw new Exception("Local IP Address Not Found!");
            }

            return rez;
        }

        private class CustomInMemoryProvider : InMemoryProvider
        {
            private IDataDefinerAndManipulatorProvider store;

            public CustomInMemoryProvider(
                string[] definerInterceptorNames,
                string[] manipulatorInterceptorNames,
                IDataDefinerInterceptor[] definerInterceptors,
                IDataManipulatorInterceptor[] manipulatorInterceptors) :
                base(definerInterceptorNames, manipulatorInterceptorNames, definerInterceptors, manipulatorInterceptors)
            {
            }

            public override bool Isolate { get; } = true;

            protected override IDataDefinerAndManipulatorProvider CreateActualStore(NamedConnectionString named) =>
                this.store ?? (this.store = base.CreateActualStore(named));
        }
    }
}