﻿namespace Base2art.Standard.DataStorage.Provider.Features
{
    using System;
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.DataStorage.Specs;
    using Xunit;

    public class CreateTableFeature
    {
        private readonly CreateTableSpec selecting = new CreateTableSpec(typeof(Exception));

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void ShouldCreateTable(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.ShouldCreateTable(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void ShouldBeAbleToDeclareFields(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.ShouldBeAbleToDeclareFields(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void ShouldBeAbleToInferFields(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.ShouldBeAbleToInferFields(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void ShouldBeAbleToInferRequiredFields(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.ShouldBeAbleToInferRequiredFields(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void ShouldBeAbleToUpgradeTables(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.ShouldBeAbleToUpgradeTables(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void ShouldBeAbleToAddKeyes(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.ShouldBeAbleToAddKeyes(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void ShouldBeAbleToAddIndexes(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.ShouldBeAbleToAddIndexes(dbms, db);
        }
    }
}