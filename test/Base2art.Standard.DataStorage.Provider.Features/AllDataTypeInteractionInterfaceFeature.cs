﻿namespace Base2art.Standard.DataStorage.Provider.Features
{
    using System.Threading.Tasks;
    using Base2art.DataStorage;
    using Base2art.DataStorage.Specs;
    using Xunit;

    public class AllDataTypeInteractionInterfaceFeature
    {
        private readonly AllDataTypeInteractionInterfaceSpec selecting = new AllDataTypeInteractionInterfaceSpec();

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void CreateInsertSelectDelete(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.CreateInsertSelectDelete(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void CreateInsertSelectById(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.CreateInsertSelectById(dbms, db);
        }

        [Theory]
        [ClassData(typeof(TestDataGenerator))]
        public async void CreateAllGenerically(
            string dbmsName,
            Task<IDataStorageProvider> definerProvider,
            Task<NamedConnectionString> connectionStringProvider)
        {
            var definer = await definerProvider;
            var connectionString = await connectionStringProvider;
            var db = definer.CreateDataStoreAccess(connectionString);
            var dbms = definer.CreateDbmsAccess(connectionString);
            await this.selecting.CreateAllGenerically(dbms, db);
        }
    }
}