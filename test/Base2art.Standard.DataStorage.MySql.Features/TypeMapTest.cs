﻿namespace Base2art.DataStorage.MySql.Features
{
    using System;
    using FluentAssertions;
    using Xunit;

    public class TypeMapTest
    {
        [Fact]
        public void Load()
        {
            var g = new Guid("bb427715-b167-4abc-86aa-d8c2bbd84aa0");
            var data = BuilderMaps.ToMySql(g);

            new Guid(data).Should().Be(new Guid("157742BB67B1BC4A86AAD8C2BBD84AA0"));

            new Guid(data).Equals(new Guid("157742BB67B1BC4A86AAD8C2BBD84AA0"))
                          .Should().BeTrue();
        }
    }
}