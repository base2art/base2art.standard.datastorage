﻿namespace Base2art.DataStorage.MySql.Features
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Sockets;
    using Base2art.Dapper;
    using Dapper;
    using DataDefinition;
    using Interception.DataDefinition;
    using Interception.DataDefinition.Specific;
    using Interception.DataManipulation;
    using Interception.DataManipulation.Specific;
    using MySqlConnector;

    public static class FeatureSetup
    {
        private static readonly Lazy<string[]> ipAddress;

        static FeatureSetup() => ipAddress = new Lazy<string[]>(GetLocalIPAddress);

        public static IDataDefinerAndManipulator Definer()
        {
            var factory = new OdbcConnectionFactory<MySqlConnection>(ConnectionString());
            var formatter = new MySqlFormatterProvider(new DapperExecutionEngine(factory), TimeSpan.FromSeconds(10));
            var commandBuilder = new MySqlManipulationCommandBuilderFactory();
            var supports = formatter.DataManipulator.Supports;
            return new WrappingDataStorage(
                                           new IDataDefinerInterceptor[]
                                           {
                                               new DefinerCommandDebuggerInterceptor(
                                                                                     "/home/tyoung/temp/sql-logs/",
                                                                                     ".mysql-logs.ddl",
                                                                                     new MySqlDefinerCommandBuilderFactory(),
                                                                                     new DefaultStorageTypeMap(new TableTypeMap()),
                                                                                     new BuilderMaps(commandBuilder, supports))
                                           },
                                           new IDataManipulatorInterceptor[]
                                           {
                                               new ManipulatorCommandDebuggerInterceptor(
                                                                                         "/home/tyoung/temp/sql-logs/",
                                                                                         ".mysql-logs.sql",
                                                                                         new MySqlManipulationCommandBuilderFactory(),
                                                                                         new BuilderMaps(commandBuilder, supports),
                                                                                         new BuilderMaps(commandBuilder, supports))
                                           },
                                           formatter.DataDefiner,
                                           formatter.DataManipulator);
        }

        public static string ConnectionStringFromConfig()
        {
            var path = Path.Combine(
                                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                                    "base2art",
                                    "data-storage",
                                    "connectionString-mysql.config");

            if (!File.Exists(path))
            {
                throw new InvalidOperationException();
            }

            return File.ReadAllText(path).Replace("\r", "\n").Replace("\n", "");
        }

        public static string ConnectionStringHardCoded()
        {
            var builder = new MySqlConnectionStringBuilder();
            builder.UserID = "nunit";
            builder.Database = "nunit";
            builder.Password = "nunitUser!";
            builder.Server = "10.17.4.57";
            builder.Pooling = true;
            builder.SslMode = MySqlSslMode.None;

            return builder.ToString();
        }

        public static string ConnectionString()
        {
            var ipAddresses = IPAddress();

            var remoteIps = new[] {"10.0.2.15", "10.17.168.68", "10.17.168.30", "172.20.24.59", "172.20.25.40", "10.17.168.43"};

            return ipAddresses.Any(ipAddr => remoteIps.Contains(ipAddr))
                       ? ConnectionStringHardCoded()
                       : ConnectionStringFromConfig();
        }

        public static string[] IPAddress() => ipAddress.Value;

        private static string[] GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());

            var rez = host.AddressList
                          .Where(ip => ip.AddressFamily == AddressFamily.InterNetwork)
                          .Select(x => x.ToString())
                          .ToArray();

            if (rez == null || rez.Length == 0)
            {
                throw new Exception("Local IP Address Not Found!");
            }

            return rez;
        }
    }
}