﻿namespace Base2art.DataStorage.MySql.Features
{
    using Specs;
    using Xunit;

    public class AllDataTypeInteractionFeature
    {
        public AllDataTypeInteractionFeature()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new AllDataTypeInteractionSpec();
        }

        private readonly IDataDefinerAndManipulator definer;

        private readonly AllDataTypeInteractionSpec selecting;

        [Fact]
        public async void CreateAllGenerically()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.CreateAllGenerically(dbms, db);
        }

        [Fact]
        public async void CreateInsertSelectDelete()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.CreateInsertSelectDelete(dbms, db);
        }
    }
}