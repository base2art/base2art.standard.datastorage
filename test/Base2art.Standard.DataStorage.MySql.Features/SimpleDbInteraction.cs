﻿namespace Base2art.DataStorage.MySql.Features
{
    using Specs;
    using Xunit;

    public class SimpleDbInteraction
    {
        public SimpleDbInteraction()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new SimpleDbInteraction_Selecting();
            this.inserting = new SimpleDbInteraction_Inserting();
            this.joining = new DbJoinAndAggregate_Selects();
        }

        private readonly SimpleDbInteraction_Selecting selecting;

        private readonly SimpleDbInteraction_Inserting inserting;

        private readonly IDataDefinerAndManipulator definer;
        private readonly DbJoinAndAggregate_Selects joining;

        [Fact]
        public async void Should_CountResults()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_CountResults(dbms, db);
        }

        [Fact]
        public async void Should_EscapeTableNames()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_EscapeTableNames(dbms, db);
        }

        [Fact]
        public async void Should_FetchDistinct()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_FetchDistinct(dbms, db);
        }

        [Fact]
        public async void Should_GroupByContent()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.joining.Should_GroupByContent(dbms, db);
        }

        [Fact]
        public async void Should_GroupResults()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_GroupResults(dbms, db);
        }

        [Fact]
        public async void Should_InsertIntoFromSelect()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.inserting.Should_InsertIntoFromSelect(dbms, db);
        }

        [Fact]
        public async void Should_InsertMultiple()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.inserting.Should_InsertMultiple(dbms, db);
        }

        [Fact]
        public async void Should_InsertWithNewIdIntoFromSelect()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.inserting.Should_InsertWithNewIdIntoFromSelect(dbms, db);
        }

        [Fact]
        public async void Should_Join_2Ons()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_2Ons(dbms, db);
        }

        [Fact]
        public async void Should_Join_2Times()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_2Times(dbms, db);
        }

        [Fact]
        public async void Should_Join_CrossJoin()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_CrossJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_InnerJoin()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_InnerJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin_NonNullableWhere()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin_NonNullableWhere(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin2Times()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_LeftJoin2Times(dbms, db);
        }

        [Fact]
        public async void Should_Join_RightJoin()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Join_RightJoin(dbms, db);
        }

        [Fact]
        public async void Should_JoinOnLike()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.joining.Should_JoinOnLike(dbms, db);
        }

        [Fact]
        public async void Should_LimitAndOffsetResults()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_LimitAndOffsetResults(dbms, db);
        }

        [Fact]
        public async void Should_OrderRandomly()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_OrderRandomly(dbms, db);
        }

        [Fact]
        public async void Should_OrderResults()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_OrderResults(dbms, db);
        }

        [Fact]
        public async void Should_ScopeToRequestedFields()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_ScopeToRequestedFields(dbms, db);
        }

        [Fact]
        public async void Should_Select_WithFiltering()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Select_WithFiltering(dbms, db);
        }

        [Fact]
        public async void Should_Select_WithFilteringMultipleWheres()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_Select_WithFilteringMultipleWheres(dbms, db);
        }

        [Fact]
        public async void Should_SelectFieldIn()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectFieldIn(dbms, db);
        }

        [Fact]
        public async void Should_SelectFieldInQuery()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectFieldInQuery(dbms, db);
        }

        [Fact]
        public async void Should_SelectMultpleTables()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectMultpleTables(dbms, db);
        }

        [Fact]
        public async void Should_SelectNullTernaryFilter()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectNullTernaryFilter(dbms, db);
        }

        [Fact]
        public async void Should_SelectSingleField()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SelectSingleField(dbms, db);
        }

        [Fact]
        public async void Should_SimpleSelect()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_SimpleSelect(dbms, db);
        }

        [Fact]
        public async void Should_UpdateData()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.Should_UpdateData(dbms, db);
        }
    }
}