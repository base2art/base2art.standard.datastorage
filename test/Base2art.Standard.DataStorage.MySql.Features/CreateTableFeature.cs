﻿namespace Base2art.DataStorage.MySql.Features
{
    using MySqlConnector;
    using Specs;
    using Xunit;

    public class CreateTableFeature
    {
        public CreateTableFeature()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new CreateTableSpec(typeof(MySqlException));
            // canDropTables: true, throwsExceptionOnOverflow: true, truncatesChar: true
            // truncatesChar: true, throwsExceptionOnOverflow: true
        }

        private readonly IDataDefinerAndManipulator definer;

        private readonly CreateTableSpec selecting;

        [Fact]
        public async void ShouldBeAbleToAddIndexes()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToAddIndexes(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToAddKeyes()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToAddKeyes(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToDeclareFields()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToDeclareFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToInferFields()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToInferFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToInferRequiredFields()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToInferRequiredFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToUpgradeTables()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldBeAbleToUpgradeTables(dbms, db);
        }

        [Fact]
        public async void ShouldCreateTable()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.ShouldCreateTable(dbms, db);
        }
    }
}