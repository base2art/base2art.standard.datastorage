﻿namespace Base2art.DataStorage.MySql.Features
{
    using Specs;
    using Xunit;

    public class AllDataTypeInteractionInterfaceFeature
    {
        public AllDataTypeInteractionInterfaceFeature()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new AllDataTypeInteractionInterfaceSpec();
        }

        private readonly IDataDefinerAndManipulator definer;

        private readonly AllDataTypeInteractionInterfaceSpec selecting;

        [Fact]
        public async void CreateAllGenerically()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.CreateAllGenerically(dbms, db);
        }

        [Fact]
        public async void CreateInsertSelectDelete()
        {
            IDataStore db = new DataStore(this.definer);
            IDbms dbms = new Dbms(this.definer);
            await this.selecting.CreateInsertSelectDelete(dbms, db);
        }
    }
}