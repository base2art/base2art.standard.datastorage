﻿namespace Base2art.DataStorage.GoogleDocs.Features
{
    using System;
    using System.Threading.Tasks;
    using Specs;
    using Xunit;
    using Xunit.Abstractions;

    public class CreateTableFeature
    {
        private readonly ITestOutputHelper testOutputHelper;

        public CreateTableFeature(ITestOutputHelper testOutputHelper)
        {
            this.testOutputHelper = testOutputHelper;
            this.selecting = new CreateTableSpec(typeof(InvalidOperationException));
            // canDropTables: true, throwsExceptionOnOverflow: true, truncatesChar: true
            // truncatesChar: true, throwsExceptionOnOverflow: true
        }

        private readonly CreateTableSpec selecting;

        [Fact]
        public async void ShouldBeAbleToAddIndexes()
        {
            var definer1 = await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.ShouldBeAbleToAddIndexes(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToAddKeyes()
        {
            var definer1 = await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.ShouldBeAbleToAddKeyes(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToDeclareFields()
        {
            var definer1 = await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.ShouldBeAbleToDeclareFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToInferFields()
        {
            var definer1 = await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.ShouldBeAbleToInferFields(dbms, db);
        }

        [Fact]
        public async void ShouldBeAbleToInferRequiredFields()
        {
            var definer1 = await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.ShouldBeAbleToInferRequiredFields(dbms, db, this.testOutputHelper);
        }

        [Fact]
        public async void ShouldBeAbleToUpgradeTables()
        {
            var definer1 = await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.ShouldBeAbleToUpgradeTables(dbms, db);
        }

        [Fact]
        public async void ShouldCreateTable()
        {
            var definer1 = await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.ShouldCreateTable(dbms, db);
        }
    }
}