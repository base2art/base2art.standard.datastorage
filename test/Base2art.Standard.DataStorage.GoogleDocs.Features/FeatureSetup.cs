namespace Base2art.DataStorage.GoogleDocs.Features
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Provider.GoogleDocs;

    public static class FeatureSetup
    {
        private static int counter;
        private static GoogleDocsConnectionStringBuilder GoogleDocsConnectionString()
        {
            var cstr = new GoogleDocsConnectionStringBuilder();
            cstr.CredentialsPath = "/home/tyoung/.config/Base2art/superContest/";
            cstr.InitialCatalog = "1UN-GTDCsT7JP-PqmecXKqo3fdOrqg77wzhs3vXCISxo";
            return cstr;
        }

        public static async Task<IDataDefinerAndManipulator> Definer()
        {
            var delay = counter switch
            {
                0 => 0,
                1 => 15,
                _ => 30
            };
            
            counter += 1;
            
            await Task.Delay(TimeSpan.FromSeconds(delay));
            var cstr = GoogleDocsConnectionString();
            return new DataDefiner(new DirectoryInfo(cstr.CredentialsPath), cstr.InitialCatalog);
        }
    }
}