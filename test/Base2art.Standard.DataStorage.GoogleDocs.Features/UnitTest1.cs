namespace Base2art.DataStorage.GoogleDocs.Features
{
    using Base2art.DataStorage.GoogleDocs;
    using FluentAssertions;
    using Xunit;

    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            StateManager.RangeCreator(0).Should().Be("");

            StateManager.RangeCreator(1).Should().Be("A");
            StateManager.RangeCreator(2).Should().Be("B");
            StateManager.RangeCreator(25).Should().Be("Y");
            StateManager.RangeCreator(26).Should().Be("Z");
            StateManager.RangeCreator(27).Should().Be("AA");
            StateManager.RangeCreator(28).Should().Be("AB");
            StateManager.RangeCreator(26 + 26).Should().Be("AZ");
            StateManager.RangeCreator(26 + 26 + 1).Should().Be("BA");
        }
    }
}