﻿namespace Base2art.DataStorage.GoogleDocs.Features
{
    using Specs;
    using Xunit;

    public class SimpleDbInteraction
    {
        public SimpleDbInteraction()
        {
            this.selecting = new SimpleDbInteraction_Selecting();
            this.inserting = new SimpleDbInteraction_Inserting();
            this.joining = new DbJoinAndAggregate_Selects();
        }

        private readonly SimpleDbInteraction_Selecting selecting;

        private readonly SimpleDbInteraction_Inserting inserting;

        private readonly DbJoinAndAggregate_Selects joining;

        [Fact]
        public async void Should_CountResults()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_CountResults(dbms, db);
        }

        [Fact]
        public async void Should_EscapeTableNames()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_EscapeTableNames(dbms, db);
        }

        [Fact]
        public async void Should_FetchDistinct()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_FetchDistinct(dbms, db);
        }

        [Fact]
        public async void Should_GroupByContent()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.joining.Should_GroupByContent(dbms, db);
        }

        [Fact]
        public async void Should_GroupResults()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_GroupResults(dbms, db);
        }

        [Fact]
        public async void Should_InsertIntoFromSelect()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.inserting.Should_InsertIntoFromSelect(dbms, db);
        }

        [Fact]
        public async void Should_InsertMultiple()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.inserting.Should_InsertMultiple(dbms, db);
        }

        [Fact]
        public async void Should_InsertWithNewIdIntoFromSelect()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.inserting.Should_InsertWithNewIdIntoFromSelect(dbms, db, 1000);
        }

        [Fact]
        public async void Should_Join_2Ons()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_Join_2Ons(dbms, db);
        }

        [Fact]
        public async void Should_Join_2Times()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_Join_2Times(dbms, db);
        }

        [Fact]
        public async void Should_Join_CrossJoin()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_Join_CrossJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_InnerJoin()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_Join_InnerJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_Join_LeftJoin(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin_NonNullableWhere()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_Join_LeftJoin_NonNullableWhere(dbms, db);
        }

        [Fact]
        public async void Should_Join_LeftJoin2Times()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_Join_LeftJoin2Times(dbms, db);
        }

        [Fact]
        public async void Should_Join_RightJoin()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_Join_RightJoin(dbms, db);
        }

        [Fact]
        public async void Should_JoinOnLike()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.joining.Should_JoinOnLike(dbms, db);
        }

        [Fact]
        public async void Should_LimitAndOffsetResults()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_LimitAndOffsetResults(dbms, db);
        }

        [Fact]
        public async void Should_OrderRandomly()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_OrderRandomly(dbms, db);
        }

        [Fact]
        public async void Should_OrderResults()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_OrderResults(dbms, db);
        }

        [Fact]
        public async void Should_ScopeToRequestedFields()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_ScopeToRequestedFields(dbms, db);
        }

        [Fact]
        public async void Should_Select_WithFiltering()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_Select_WithFiltering(dbms, db);
        }

        [Fact]
        public async void Should_Select_WithFilteringMultipleWheres()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_Select_WithFilteringMultipleWheres(dbms, db);
        }

        [Fact]
        public async void Should_SelectFieldIn()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_SelectFieldIn(dbms, db);
        }

        [Fact]
        public async void Should_SelectFieldInQuery()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_SelectFieldInQuery(dbms, db);
        }

        [Fact]
        public async void Should_SelectMultpleTables()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_SelectMultpleTables(dbms, db);
        }

        [Fact]
        public async void Should_SelectNullTernaryFilter()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_SelectNullTernaryFilter(dbms, db);
        }

        [Fact]
        public async void Should_SelectSingleField()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_SelectSingleField(dbms, db);
        }

        [Fact]
        public async void Should_SimpleSelect()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_SimpleSelect(dbms, db);
        }

        [Fact]
        public async void Should_UpdateData()
        {
            IDataDefinerAndManipulator definer1 =  await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.Should_UpdateData(dbms, db);
        }
    }
}