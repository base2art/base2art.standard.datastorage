﻿namespace Base2art.DataStorage.GoogleDocs.Features
{
    using Specs;
    using Xunit;

    public class AllDataTypeInteractionInterfaceFeature
    {
        public AllDataTypeInteractionInterfaceFeature()
        {
            this.selecting = new AllDataTypeInteractionInterfaceSpec();
        }

        private readonly AllDataTypeInteractionInterfaceSpec selecting;

        [Fact]
        public async void CreateAllGenerically()
        {
            IDataDefinerAndManipulator definer1 = await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.CreateAllGenerically(dbms, db);
        }

        [Fact]
        public async void CreateInsertSelectDelete()
        {
            IDataDefinerAndManipulator definer1 = await FeatureSetup.Definer();
            IDataStore db = new DataStore(definer1);
            IDbms dbms = new Dbms(definer1);
            await this.selecting.CreateInsertSelectDelete(dbms, db);
        }
    }
}