﻿
namespace Base2art.DataStorage.Provided.Features
{
    using System;
    using Base2art.DataStorage.Specs;
    using NUnit.Framework;

    [TestFixture]
    public class SimpleDbInteraction
    {
        private SimpleDbInteraction_Selecting selecting;

        private SimpleDbInteraction_Inserting inserting;

        private IDataStorageProvider definer;
        
        [SetUp]
        public void BeforEach()
        {
            this.definer = FeatureSetup.Definer();
            this.selecting = new SimpleDbInteraction_Selecting();
            this.inserting = new SimpleDbInteraction_Inserting();
        }
        
        [Test]
        public async void Should_FetchDistinct(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_FetchDistinct(dbms, db);
        }
        
        [Test]
        public async void Should_Join_CrossJoin(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_Join_CrossJoin(dbms, db);
        }
        
        [Test]
        public async void Should_Join_InnerJoin(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_Join_InnerJoin(dbms, db);
        }
        
        [Test]
        public async void Should_Join_LeftJoin(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_Join_LeftJoin(dbms, db);
        }
        
        [Test]
        public async void Should_Join_LeftJoin_NonNullableWhere(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_Join_LeftJoin_NonNullableWhere(dbms, db);
        }
        
        [Test]
        public async void Should_Join_LeftJoin2Times(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_Join_LeftJoin2Times(dbms, db);
        }
        
        [Test]
        public async void Should_Join_RightJoin(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_Join_RightJoin(dbms, db);
        }
        
        [Test]
        public async void Should_LimitAndOffsetResults(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_LimitAndOffsetResults(dbms, db);
        }
        
        [Test]
        public async void Should_OrderResults(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_OrderResults(dbms, db);
        }
        
        [Test]
        public async void Should_GroupResults(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_GroupResults(dbms, db);
        }
        
        [Test]
        public async void Should_CountResults(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_CountResults(dbms, db);
        }
        
        [Test]
        public async void Should_ScopeToRequestedFields(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_ScopeToRequestedFields(dbms, db);
        }
        
        [Test]
        public async void Should_Select_WithFiltering(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_Select_WithFiltering(dbms, db);
        }
        
        [Test]
        public async void Should_Select_WithFilteringMultipleWheres(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_Select_WithFilteringMultipleWheres(dbms, db);
        }
        
        [Test]
        public async void Should_SimpleSelect(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_SimpleSelect(dbms, db);
        }
        
        [Test]
        public async void Should_SelectSingleField(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_SelectSingleField(dbms, db);
        }
        
        [Test]
        public async void Should_SelectNullTernaryFilter(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_SelectNullTernaryFilter(dbms, db);
        }
        
        [Test]
        public async void Should_InsertIntoFromSelect(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.inserting.Should_InsertIntoFromSelect(dbms, db);
        }
        
        [Test]
        public async void Should_InsertMultiple(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.inserting.Should_InsertMultiple(dbms, db);
        }
        
        [Test]
        public async void Should_SelectFieldInQuery(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_SelectFieldInQuery(dbms, db);
        }
        
        [Test]
        public async void Should_SelectFieldIn(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_SelectFieldIn(dbms, db);
        }
        
        [Test]
        public async void Should_SelectMultpleTables(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_SelectMultpleTables(dbms, db);
        }
        
        [Test]
        public async void Should_Join_2Times(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_Join_2Times(dbms, db);
        }
        
        [Test]
        public async void Should_Join_2Ons(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_Join_2Ons(dbms, db);
        }
        
        [Test]
        public async void Should_OrderRandomly(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_OrderRandomly(dbms, db);
        }
        
        [Test]
        public async void Should_EscapeTableNames(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_EscapeTableNames(dbms, db);
        }
        
        [Test]
        public async void Should_UpdateData(NamedConnectionString cstr)
        {
            IDataStore db = this.definer.CreateDataStoreAccess(cstr);
            IDbms dbms = this.definer.CreateDbmsAccess(cstr);
            await this.selecting.Should_UpdateData(dbms, db);
        }
    }
}
