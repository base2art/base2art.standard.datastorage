﻿namespace Base2art.DataStorage.Specs
{
    using System;
    using System.Threading.Tasks;
    using Data;
    using DataDefinition;
    using DataManipulation;
    using FluentAssertions;
    using Threading.Tasks;

    public class SimpleDbInteraction_Inserting
    {
        private readonly TimeSpan? delay;

        public SimpleDbInteraction_Inserting(TimeSpan? delay = null) => this.delay = delay;

        public async Task Should_InsertIntoFromSelect(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.living_parent_count)
                                   .Field(x => x.when))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.social_security_number, "123-14-2135")
                                 .Field(x => x.living_parent_count, 2)
                                 .Field(x => x.when, new DateTime(2017, 02, 03)))
                    .Execute();

            await this.Delay();

            await db.Insert<person>()
                    .Fields(r =>
                                r.Field(x => x.name)
                                 .Field(x => x.social_security_number)
                                 .Field(x => x.when)
                                 .Field(x => x.living_parent_count))
                    .Records(
                             db.Select<person>()
                               .Fields(r =>
                                           r.Field(x => x.social_security_number)
                                            .Field(x => x.social_security_number)
                                            .Field(x => x.when)
                                            .Field(x => x.living_parent_count - 1)))
                    .Execute();

            await this.Delay();

            var derived = await db.Select<person>()
                                  .Where(r => r.Field(x => x.name, "%-2135", Operations.Like))
                                  .Execute()
                                  .Then()
                                  .ToArray();

            derived.Length.Should().Be(1);
            derived[0].name.Should().Be("123-14-2135");
            derived[0].social_security_number.Should().Be("123-14-2135");
            derived[0].living_parent_count.Should().Be(1);
            derived[0].when.Should().Be(new DateTime(2017, 02, 03));
        }

        public async Task Should_InsertWithNewIdIntoFromSelect(IDbms dbms, IDataStore db, int? maxLength = null)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person_with_id>().IfExists().Execute();
            }

            await dbms.CreateTable<person_with_id>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.living_parent_count)
                                   .Field(x => x.notes, false, Range.Create(0, maxLength.GetValueOrDefault(ushort.MaxValue) + 1))
                                   .Field(x => x.when))
                      .Execute();

            await db.Insert<person_with_id>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.name, "Scott")
                                 .Field(x => x.social_security_number, "123-14-2135")
                                 .Field(x => x.living_parent_count, 2)
                                 .Field(x => x.notes, new string('A', maxLength.GetValueOrDefault(short.MaxValue) * 4))
                                 .Field(x => x.when, new DateTime(2017, 02, 03)))
                    .Execute();

            await this.Delay();

            await db.Insert<person_with_id>()
                    .Fields(r =>
                                r.Field(x => x.id)
                                 .Field(x => x.name)
                                 .Field(x => x.social_security_number)
                                 .Field(x => x.when)
                                 .Field(x => x.living_parent_count))
                    .Records(
                             db.Select<person_with_id>()
                               .Fields(r =>
                                           r.NewUUID(x => x.id)
                                            .Field(x => x.social_security_number)
                                            .Field(x => x.social_security_number)
                                            .Field(x => x.when)
                                            .Field(x => x.living_parent_count - 1)))
                    .Execute();

            await this.Delay();

            var derived = await db.Select<person_with_id>()
//                                  .Where(r => r.Field(x => x.name, "%-2135", Operations.Like))
                                  .Execute()
                                  .Then()
                                  .ToArray();

            derived.Length.Should().Be(2);
            derived[0].id.Should().NotBe(Guid.Empty);
            derived[1].id.Should().NotBe(Guid.Empty);
            derived[1].id.Should().NotBe(derived[0].id);
        }

        public async Task Should_InsertMultiple(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.living_parent_count))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.social_security_number, "123-14-2135")
                                 .Field(x => x.living_parent_count, 2))
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.social_security_number, "456-14-7892")
                                 .Field(x => x.living_parent_count, 3))
                    .Record(r =>
                                r.Field(x => x.name, "Chyna")
                                 .Field(x => x.social_security_number, "938-28-2672")
                                 .Field(x => x.living_parent_count, 1))
                    .Execute();

            await this.Delay();

            var derived = await db.Select<person>()
                                  .Where(r => r.Field(x => x.social_security_number, "%-14-%", Operations.Like))
                                  .Execute()
                                  .Then()
                                  .ToArray();

            derived.Length.Should().Be(2);
            derived[0].name.Should().Be("Scott");
            derived[0].social_security_number.Should().Be("123-14-2135");
            derived[0].living_parent_count.Should().Be(2);

            derived[1].name.Should().Be("Matt");
            derived[1].social_security_number.Should().Be("456-14-7892");
            derived[1].living_parent_count.Should().Be(3);
        }

        public async Task Should_InsertDynamic(string name, IDbms dbms, IDataStore db, IDynamicDataStore dynamicDb)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.living_parent_count))
                      .Execute();

            if (!dynamicDb.Supports.DynamicQueries)
            {
                return;
            }

            var schema = name == "SqlServer" ? "nunit1." : "";
            var sql =
                $"INSERT INTO {schema}person_v2 (name, social_security_number, living_parent_count) VALUES (@name, @social_security_number, @living_parent_count)";
            await dynamicDb.ExecutionOf(sql)
                           .WithParameters(new {name = "Scott", social_security_number = "123-14-2135", living_parent_count = 2})
                           .Execute();

            await dynamicDb.ExecutionOf(sql)
                           .WithParameters(new {name = "Matt", social_security_number = "456-14-7892", living_parent_count = 3})
                           .Execute();

            await dynamicDb.ExecutionOf(sql)
                           .WithParameters(new {name = "Chyna", social_security_number = "938-28-2672", living_parent_count = 1})
                           .Execute();

            await this.Delay();

            var derived = await db.Select<person>()
                                  .Where(r => r.Field(x => x.social_security_number, "%-14-%", Operations.Like))
                                  .Execute()
                                  .Then()
                                  .ToArray();

            derived.Length.Should().Be(2);
            derived[0].name.Should().Be("Scott");
            derived[0].social_security_number.Should().Be("123-14-2135");
            derived[0].living_parent_count.Should().Be(2);

            derived[1].name.Should().Be("Matt");
            derived[1].social_security_number.Should().Be("456-14-7892");
            derived[1].living_parent_count.Should().Be(3);

            var selectSql = $"Select * FROM {schema}person_v2 WHERE social_security_number LIKE @value";
            derived = await dynamicDb.Query(selectSql)
                                     .WithParameters(new {value = "%-14-%"})
                                     .Execute<person>()
                                     .Then()
                                     .ToArray();

            derived.Length.Should().Be(2);
            derived[0].name.Should().Be("Scott");
            derived[0].social_security_number.Should().Be("123-14-2135");
            derived[0].living_parent_count.Should().Be(2);

            derived[1].name.Should().Be("Matt");
            derived[1].social_security_number.Should().Be("456-14-7892");
            derived[1].living_parent_count.Should().Be(3);

            selectSql = $"Select * FROM {schema}person_v2 WHERE social_security_number LIKE '%-14-%'";
            derived = await dynamicDb.Query(selectSql)
                                     .Execute<person>()
                                     .Then()
                                     .ToArray();

            derived.Length.Should().Be(2);
            derived[0].name.Should().Be("Scott");
            derived[0].social_security_number.Should().Be("123-14-2135");
            derived[0].living_parent_count.Should().Be(2);

            derived[1].name.Should().Be("Matt");
            derived[1].social_security_number.Should().Be("456-14-7892");
            derived[1].living_parent_count.Should().Be(3);

            if (dynamicDb.Supports.DynamicProcedureQueries)
            {
                try
                {
                    var badSprocCall = $"Select * FROM {schema}person_v2 WHERE social_security_number LIKE @value";

                    derived = await dynamicDb.QueryProcedure(badSprocCall)
                                             .WithParameters(new {value = "%-14-%"})
                                             .Execute<person>()
                                             .Then()
                                             .ToArray();
                }
                catch (Exception e)
                {
                    if (e.GetType().FullName == db.Supports.NativeExceptionTypeName)
                    {
                        return;
                    }
                }

                throw new ArgumentOutOfRangeException();
            }
        }

        private Task Delay() => this.delay.HasValue ? Task.Delay(this.delay.Value) : Task.FromResult(true);
    }
}