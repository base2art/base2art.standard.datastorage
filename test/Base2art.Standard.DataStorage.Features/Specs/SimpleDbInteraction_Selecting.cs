﻿namespace Base2art.DataStorage.Specs
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using Data;
    using DataDefinition;
    using DataManipulation;
    using FluentAssertions;
    using Serialization;
    using Threading.Tasks;
    using Xunit;

    public class SimpleDbInteraction_Selecting
    {
        private readonly TimeSpan? delay;

        public SimpleDbInteraction_Selecting(TimeSpan? delay = null) => this.delay = delay;

        public async Task Should_SimpleSelect(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11)))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.social_security_number, "123-14-2135"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.social_security_number, "123-34-2135"))
                    .Execute();

            await this.Delay();

            var matts = await db.Select<person>()
                                .Where(r => r.Field(x => x.name, "Matt", (x, y) => x == y))
                                .Execute()
                                .Then()
                                .ToArray();

            matts.Length.Should().Be(1);
            matts[0].name.Should().Be("Matt");

            var scott = await db.Select<person>()
                                .Where(r => r.Field(x => x.name, "Scott", (x, y) => x == y))
                                .Execute()
                                .Then()
                                .ToArray();

            scott.Length.Should().Be(1);
            scott[0].name.Should().Be("Scott");

            var notMatts = await db.Select<person>()
                                   .Where(r => r.Field(x => x.name, "Matt", (x, y) => x != y))
                                   .Execute()
                                   .Then()
                                   .ToArray();

            notMatts.Length.Should().Be(1);
            notMatts[0].name.Should().Be("Scott");

            var ssnLike = await db.Select<person>()
                                  .Where(r => r.Field(x => x.social_security_number, "%-14-%", Operations.Like))
                                  .Execute()
                                  .Then()
                                  .ToArray();

            ssnLike.Length.Should().Be(1);
            ssnLike[0].name.Should().Be("Scott");

            var ssnNotLike = await db.Select<person>()
                                     .Where(r => r.Field(x => x.social_security_number, "%-14-%", Operations.NotLike))
                                     .Execute()
                                     .Then()
                                     .ToArray();

            ssnNotLike.Length.Should().Be(1);
            ssnNotLike[0].name.Should().Be("Matt");
        }

        public async Task Should_SelectSingleField(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11)))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.social_security_number, "123-14-2135"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.social_security_number, "123-34-2135"))
                    .Execute();

            await this.Delay();

            var matts = await db.Select<person>()
                                .Fields(x => x.Field(y => y.name))
                                .Where(r => r.Field(x => x.name, "Matt", (x, y) => x == y))
                                .Execute()
                                .Then()
                                .ToArray();

            matts.Length.Should().Be(1);
            matts[0].name.Should().Be("Matt");
            matts[0].social_security_number.Should().BeNull();

            var scott = await db.Select<person>()
                                .Fields(x => x.Field(y => y.name))
                                .Where(r => r.Field(x => x.name, "Scott", (x, y) => x == y))
                                .Execute()
                                .Then()
                                .ToArray();

            scott.Length.Should().Be(1);
            scott[0].name.Should().Be("Scott");
            matts[0].social_security_number.Should().BeNull();
        }

        public async Task Should_FetchFieldInByGuid(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<category>().IfExists().Execute();
            }

            await dbms.CreateTable<category>().Execute();

            var id1 = Guid.NewGuid();
            var id2 = Guid.NewGuid();
            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.id, id1))
                    .Execute();
            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.id, id2))
                    .Execute();

            await this.Delay();

            var matts = await db.Select<category>()
                                .Fields(x => x.Field(y => y.name))
                                .Where(r => r.Field(x => x.id, id2, (x, y) => x == y))
                                .Execute()
                                .Then()
                                .ToArray();

            matts.Length.Should().Be(1);
            matts[0].name.Should().Be("Matt");
            matts[0].id.Should().BeEmpty();

            var scotts = db.Select<category>()
                           .Fields(x => x.Field(y => y.id))
                           .Where(r => r.Field(x => x.name, "Scott", (x, y) => x == y));

            var scott = await db.Select<category>()
                                .Fields(x => x.Field(y => y.name))
                                .Where(r => r.FieldIn(x => x.id, scotts))
                                .Execute()
                                .Then()
                                .ToArray();

            scott.Length.Should().Be(1);
            scott[0].name.Should().Be("Scott");
        }

        public async Task Should_FetchLatestByJoin(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<sample_row>().IfExists().Execute();
            }

            await dbms.CreateTable<sample_row>().Execute();

            var id1 = Guid.NewGuid();
            await db.Insert<sample_row>()
                    .Record(r =>
                                r.Field(x => x.content, "Scott")
                                 .Field(x => x.id, id1)
                                 .Field(x => x.audit_when, DateTime.Now))
                    .Execute();

            await Task.Delay(TimeSpan.FromSeconds(1.5));

            await db.Insert<sample_row>()
                    .Record(r =>
                                r.Field(x => x.content, "Matt")
                                 .Field(x => x.id, id1)
                                 .Field(x => x.audit_when, DateTime.Now))
                    .Execute();

            await this.Delay();

            /*
SELECT t1.* from myTable t1
LEFT OUTER JOIN myTable t2 on t2.ID=t1.ID AND t2.`Date` > t1.`Date`
WHERE t2.`Date` IS NULL;
*/
            {
                var matts = await db.Select<sample_row>()
                                    .LeftJoin<sample_row>((t1, t2) => t1.id == t2.id && t1.audit_when < t2.audit_when)
                                    .Where2(r => r.Field(x => x.audit_when, null, (x, y) => x == y))
                                    .Execute()
                                    .Then()
                                    .ToArray();

                matts.Length.Should().Be(1);
                matts[0].Item1.content.Should().Be("Matt");
                matts[0].Item2.id.Should().Be(Guid.Empty);
            }

            {
                var matts = await db.Select<sample_row>()
                                    .LeftJoin<sample_row>((t1, t2) => t2.id == t1.id && t2.audit_when > t1.audit_when)
                                    .Where2(r => r.Field(x => x.audit_when, null, (x, y) => x == y))
                                    .Execute()
                                    .Then()
                                    .ToArray();

                matts.Length.Should().Be(1);
                matts[0].Item1.content.Should().Be("Matt");
                matts[0].Item2.id.Should().Be(Guid.Empty);
            }
        }

        public async Task Should_FetchLatestByJoinInterface(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<sample_row_implementing_interface>().IfExists().Execute();
            }

            await dbms.CreateTable<sample_row_implementing_interface>().Execute();

            var id1 = Guid.NewGuid();
            await db.Insert<sample_row_implementing_interface>()
                    .Record(r =>
                                r.Field(x => x.content, "Scott")
                                 .Field(x => x.id, id1)
                                 .Field(x => x.audit_when, DateTime.Now))
                    .Execute();

            await Task.Delay(TimeSpan.FromSeconds(1.5));

            await db.Insert<sample_row_implementing_interface>()
                    .Record(r =>
                                r.Field(x => x.content, "Matt")
                                 .Field(x => x.id, id1)
                                 .Field(x => x.audit_when, DateTime.Now))
                    .Execute();

            await this.Delay();

            /*
SELECT t1.* from myTable t1
LEFT OUTER JOIN myTable t2 on t2.ID=t1.ID AND t2.`Date` > t1.`Date`
WHERE t2.`Date` IS NULL;
*/

            var t = new Getter<sample_row_implementing_interface>(db);

            {
                var matts = await t.Get();

                matts.Length.Should().Be(1);
                matts[0].Item1.content.Should().Be("Matt");
                matts[0].Item2.id.Should().Be(Guid.Empty);
            }

            {
                var matts = await db.Select<sample_row_implementing_interface>()
                                    .LeftJoin<sample_row_implementing_interface>((t1, t2) => t2.id == t1.id && t2.audit_when > t1.audit_when)
                                    .Where2(r => r.Field(x => x.audit_when, null, (x, y) => x == y))
                                    .Execute()
                                    .Then()
                                    .ToArray();

                matts.Length.Should().Be(1);
                matts[0].Item1.content.Should().Be("Matt");
                matts[0].Item2.id.Should().Be(Guid.Empty);
            }
        }

        public async Task Should_SelectNullTernaryFilter(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.has_bool))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.social_security_number, "123-14-2135")
                                 .Field(x => x.has_bool, true))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.social_security_number, "123-34-2135")
                                 .Field(x => x.has_bool, false))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Glen")
                                 .Field(x => x.social_security_number, "123-34-2135")
                                 .Field(x => x.has_bool, null))
                    .Execute();

            await this.Delay();

            var glens = await db.Select<person>()
                                .Where(r => r.Field(x => x.has_bool, x => x == null))
                                .Execute()
                                .Then()
                                .ToArray();

            glens.Length.Should().Be(1);
            glens[0].name.Should().Be("Glen");
            glens[0].has_bool.HasValue.Should().Be(false);

            var matts = await db.Select<person>()
                                .Where(r => r.Field(x => x.has_bool, x => x == false))
                                .Execute()
                                .Then()
                                .ToArray();

            matts.Length.Should().Be(1);
            matts[0].name.Should().Be("Matt");
            matts[0].has_bool.Should().Be(false);

            matts = await db.Select<person>()
                            .Where(r => r.Field(x => x.has_bool, false, (x, y) => x == y))
                            .Execute()
                            .Then()
                            .ToArray();

            matts.Length.Should().Be(1);
            matts[0].name.Should().Be("Matt");
            matts[0].has_bool.Should().Be(false);

            /*
            var scott = await db.Select<person>()
                .Where(r => r.Field(x => x.name, "Scott", (x, y) => x == y))
                .Execute()
                .Then()
                .ToArray();
            
            scott.Length.Should().Be(1);
            scott[0].name.Should().Be("Scott");
            
            
            var notMatts = await db.Select<person>()
                .Where(r => r.Field(x => x.name, "Matt", (x, y) => x != y))
                .Execute()
                .Then()
                .ToArray();
            
            notMatts.Length.Should().Be(1);
            notMatts[0].name.Should().Be("Scott");
            
            
            var ssnLike = await db.Select<person>()
                .Where(r => r.Field(x => x.social_security_number, "%-14-%", Operations.Like))
                .Execute()
                .Then()
                .ToArray();
            
            ssnLike.Length.Should().Be(1);
            ssnLike[0].name.Should().Be("Scott");
            
            
            var ssnNotLike = await db.Select<person>()
                .Where(r => r.Field(x => x.social_security_number, "%-14-%", Operations.NotLike))
                .Execute()
                .Then()
                .ToArray();
            
            ssnNotLike.Length.Should().Be(1);
            ssnNotLike[0].name.Should().Be("Matt");
             */
        }

        public async Task Should_Select_WithFiltering(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.living_parent_count))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Todd")
                                 .Field(x => x.living_parent_count, 0)
                                 .Field(x => x.social_security_number, "123-14-2135"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.living_parent_count, 1)
                                 .Field(x => x.social_security_number, "123-14-2135"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 2)
                                 .Field(x => x.social_security_number, "123-34-2135"))
                    .Execute();

            var greaterThan = await db.Select<person>()
                                      .Where(r => r.Field(x => x.living_parent_count, x => x > 1))
                                      .Execute()
                                      .Then()
                                      .ToArray();

            greaterThan.Length.Should().Be(1);
            greaterThan[0].name.Should().Be("Matt");

            var greaterThanEqual = await db.Select<person>()
                                           .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                                           .Execute()
                                           .Then()
                                           .ToArray();

            greaterThanEqual.Length.Should().Be(2);
            greaterThanEqual[1].name.Should().Be("Matt");
            greaterThanEqual[0].name.Should().Be("Scott");

            var lessThanEqual = await db.Select<person>()
                                        .Where(r => r.Field(x => x.living_parent_count, x => x <= 1))
                                        .Execute()
                                        .Then()
                                        .ToArray();

            lessThanEqual.Length.Should().Be(2);
            lessThanEqual[0].name.Should().Be("Todd");
            lessThanEqual[1].name.Should().Be("Scott");

            var lessThan = await db.Select<person>()
                                   .Where(r => r.Field(x => x.living_parent_count, x => x < 1))
                                   .Execute()
                                   .Then()
                                   .ToArray();

            lessThan.Length.Should().Be(1);
            lessThan[0].name.Should().Be("Todd");
        }

        public async Task Should_Select_WithFilteringMultipleWheres(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.living_parent_count))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Todd")
                                 .Field(x => x.living_parent_count, 0)
                                 .Field(x => x.social_security_number, "123-14-2135"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.living_parent_count, 1)
                                 .Field(x => x.social_security_number, "123-14-2135"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 2)
                                 .Field(x => x.social_security_number, "123-34-2135"))
                    .Execute();

            var greaterThan = await db.Select<person>()
                                      .Where(r => r.Field(x => x.living_parent_count, x => x != 0).Field(x => x.living_parent_count, x => x != 2))
                                      .Execute()
                                      .Then()
                                      .ToArray();

            greaterThan.Length.Should().Be(1);
            greaterThan[0].name.Should().Be("Scott");
        }

        public async Task Should_ScopeToRequestedFields(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.living_parent_count))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Todd")
                                 .Field(x => x.living_parent_count, 0)
                                 .Field(x => x.social_security_number, "123-14-2112"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.living_parent_count, 1)
                                 .Field(x => x.social_security_number, "123-14-1235"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 2)
                                 .Field(x => x.social_security_number, "123-34-2313"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 3)
                                 .Field(x => x.social_security_number, "123-34-2135"))
                    .Execute();

            var data = await db.Select<person>()
                               .Fields(r => r.Field(x => x.name))
                               .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                               .OrderBy(r =>
                                            r.Field(x => x.name, ListSortDirection.Descending)
                                             .Field(x => x.social_security_number, ListSortDirection.Descending))
                               .Execute()
                               .Then()
                               .ToArray();

            data.Length.Should().Be(3);
            data[0].name.Should().Be("Scott");
            data[0].social_security_number.Should().Be(null);
            data[1].name.Should().Be("Matt");
            data[1].social_security_number.Should().Be(null);
            data[2].name.Should().Be("Matt");
            data[2].social_security_number.Should().Be(null);
        }

        public async Task Should_OrderResults(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.living_parent_count))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Todd")
                                 .Field(x => x.living_parent_count, 0)
                                 .Field(x => x.social_security_number, "123-14-2112"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.living_parent_count, 1)
                                 .Field(x => x.social_security_number, "123-14-1235"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 2)
                                 .Field(x => x.social_security_number, "123-34-2313"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 3)
                                 .Field(x => x.social_security_number, "123-34-2135"))
                    .Execute();

            var greaterThanEqual_Asc1 = await db.Select<person>()
                                                .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                                                .OrderBy(r =>
                                                             r.Field(x => x.name, ListSortDirection.Ascending)
                                                              .Field(x => x.social_security_number, ListSortDirection.Ascending))
                                                .Execute()
                                                .Then()
                                                .ToArray();

            greaterThanEqual_Asc1.Length.Should().Be(3);
            greaterThanEqual_Asc1[0].name.Should().Be("Matt");
            greaterThanEqual_Asc1[0].social_security_number.Should().Be("123-34-2135");
            greaterThanEqual_Asc1[1].name.Should().Be("Matt");
            greaterThanEqual_Asc1[1].social_security_number.Should().Be("123-34-2313");
            greaterThanEqual_Asc1[2].name.Should().Be("Scott");
            greaterThanEqual_Asc1[2].social_security_number.Should().Be("123-14-1235");

            var greaterThanEqual_Asc2 = await db.Select<person>()
                                                .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                                                .OrderBy(r =>
                                                             r.Field(x => x.name, ListSortDirection.Ascending)
                                                              .Field(x => x.social_security_number, ListSortDirection.Descending))
                                                .Execute()
                                                .Then()
                                                .ToArray();

            greaterThanEqual_Asc2.Length.Should().Be(3);
            greaterThanEqual_Asc2[0].name.Should().Be("Matt");
            greaterThanEqual_Asc2[0].social_security_number.Should().Be("123-34-2313");
            greaterThanEqual_Asc2[1].name.Should().Be("Matt");
            greaterThanEqual_Asc2[1].social_security_number.Should().Be("123-34-2135");
            greaterThanEqual_Asc2[2].name.Should().Be("Scott");
            greaterThanEqual_Asc2[2].social_security_number.Should().Be("123-14-1235");

            var greaterThanEqual_Desc1 = await db.Select<person>()
                                                 .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                                                 .OrderBy(r =>
                                                              r.Field(x => x.name, ListSortDirection.Descending)
                                                               .Field(x => x.social_security_number, ListSortDirection.Ascending))
                                                 .Execute()
                                                 .Then()
                                                 .ToArray();

            greaterThanEqual_Desc1.Length.Should().Be(3);
            greaterThanEqual_Desc1[0].name.Should().Be("Scott");
            greaterThanEqual_Desc1[0].social_security_number.Should().Be("123-14-1235");
            greaterThanEqual_Desc1[1].name.Should().Be("Matt");
            greaterThanEqual_Desc1[1].social_security_number.Should().Be("123-34-2135");
            greaterThanEqual_Desc1[2].name.Should().Be("Matt");
            greaterThanEqual_Desc1[2].social_security_number.Should().Be("123-34-2313");

            var greaterThanEqual_Desc2 = await db.Select<person>()
                                                 .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                                                 .OrderBy(r =>
                                                              r.Field(x => x.name, ListSortDirection.Descending)
                                                               .Field(x => x.social_security_number, ListSortDirection.Descending))
                                                 .Execute()
                                                 .Then()
                                                 .ToArray();

            greaterThanEqual_Desc2.Length.Should().Be(3);
            greaterThanEqual_Desc2[0].name.Should().Be("Scott");
            greaterThanEqual_Desc2[0].social_security_number.Should().Be("123-14-1235");
            greaterThanEqual_Desc2[1].name.Should().Be("Matt");
            greaterThanEqual_Desc2[1].social_security_number.Should().Be("123-34-2313");
            greaterThanEqual_Desc2[2].name.Should().Be("Matt");
            greaterThanEqual_Desc2[2].social_security_number.Should().Be("123-34-2135");
        }

        public async Task Should_GroupResults(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.living_parent_count))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Todd")
                                 .Field(x => x.living_parent_count, 0)
                                 .Field(x => x.social_security_number, "123-14-2112"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.living_parent_count, 1)
                                 .Field(x => x.social_security_number, "123-14-1235"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 2)
                                 .Field(x => x.social_security_number, "123-34-2313"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 3)
                                 .Field(x => x.social_security_number, "123-34-2135"))
                    .Execute();

            var greaterThanEqual_Asc1 = await db.Select<person>()
                                                .Fields(r => r.Field(x => x.name))
                                                .GroupBy<person_aggregate>(r => r.Field(x => x.name))
                                                .Fields2(r => r.Count(x => x.count))
                                                .OrderBy1(r => r.Field(x => x.name, ListSortDirection.Ascending))
                                                .Execute()
                                                .Then()
                                                .ToArray();

            greaterThanEqual_Asc1.Length.Should().Be(3);
            greaterThanEqual_Asc1[0].Item1.name.Should().Be("Matt");
            greaterThanEqual_Asc1[0].Item2.count.Should().Be(2);
            greaterThanEqual_Asc1[0].Item1.social_security_number.Should().BeNull();
            greaterThanEqual_Asc1[1].Item1.name.Should().Be("Scott");
            greaterThanEqual_Asc1[1].Item2.count.Should().Be(1);
            greaterThanEqual_Asc1[1].Item1.social_security_number.Should().BeNull();
            greaterThanEqual_Asc1[2].Item1.name.Should().Be("Todd");
            greaterThanEqual_Asc1[2].Item2.count.Should().Be(1);
            greaterThanEqual_Asc1[2].Item1.social_security_number.Should().BeNull();
        }

        public async Task Should_CountResults(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.living_parent_count))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Todd")
                                 .Field(x => x.living_parent_count, 0)
                                 .Field(x => x.social_security_number, "123-14-2112"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.living_parent_count, 1)
                                 .Field(x => x.social_security_number, "123-14-1235"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 2)
                                 .Field(x => x.social_security_number, "123-34-2313"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 1)
                                 .Field(x => x.social_security_number, "123-34-2135"))
                    .Execute();

            var count = await db.SelectSingle<person>()
                                .WithCalculated<person_aggregate>()
                                .Fields2(r => r.Count(x => x.count))
                                .Execute();

            count.Item2.count.Should().Be(4);

            var countWhere = await db.SelectSingle<person>()
                                     .WithCalculated<person_aggregate>()
                                     .Fields2(r => r.Count(x => x.count))
                                     .Where1(r => r.Field(x => x.living_parent_count, x => x >= 1))
                                     .Execute();

            countWhere.Item2.count.Should().Be(3);

            var countWhereWithJoin = await db.SelectSingle<person>()
                                             .WithCalculated<person_aggregate>()
                                             .LeftJoin<person>((x, y, z) => x.social_security_number == z.social_security_number)
                                             .Fields2(r => r.Count(x => x.count))
                                             .Where1(r => r.Field(x => x.living_parent_count, x => x >= 1))
                                             .Execute();

            countWhereWithJoin.Item2.count.Should().Be(3);

            var countWhereWithJoin1 = await db.SelectSingle<person>()
                                              .LeftJoin<person>((x, y) => x.social_security_number == y.social_security_number)
                                              .WithCalculated<person_aggregate>()
                                              .Fields3(r => r.Count(x => x.count))
                                              .Where1(r => r.Field(x => x.living_parent_count, x => x >= 1))
                                              .Execute();

            countWhereWithJoin1.Item3.count.Should().Be(3);

            var countWhereWithJoinAndGroupBy1 = await db.Select<person>()
                                                        .LeftJoin<person>((x, y) => x.social_security_number == y.social_security_number)
                                                        .GroupBy2<person_aggregate>(x => x.Field(y => y.living_parent_count))
                                                        .Fields3(r => r.Count(x => x.count))
                                                        .Fields2(r => r.Field(x => x.living_parent_count))
                                                        .Execute();

            //            countWhereWithJoinAndGroupBy1.Item3.count.Should().Be(3);
            var coll = countWhereWithJoinAndGroupBy1.ToDictionary(z => z.Item2.living_parent_count, x => x.Item3.count);
            coll.Count.Should().Be(3);
            coll[0].Should().Be(1);
            coll[1].Should().Be(2);
            coll[2].Should().Be(1);
        }

        public async Task Should_LimitAndOffsetResults(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.living_parent_count))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Todd")
                                 .Field(x => x.living_parent_count, 0)
                                 .Field(x => x.social_security_number, "123-14-2112"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.living_parent_count, 1)
                                 .Field(x => x.social_security_number, "123-14-1235"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 2)
                                 .Field(x => x.social_security_number, "123-34-2313"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 3)
                                 .Field(x => x.social_security_number, "123-34-2135"))
                    .Execute();

            var greaterThanEqual_Asc1 = await db.Select<person>()
                                                .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                                                .Offset(1)
                                                .Limit(1)
                                                .OrderBy(r =>
                                                             r.Field(x => x.name, ListSortDirection.Ascending)
                                                              .Field(x => x.social_security_number, ListSortDirection.Ascending))
                                                .Execute()
                                                .Then()
                                                .ToArray();

            greaterThanEqual_Asc1.Length.Should().Be(1);
            greaterThanEqual_Asc1[0].name.Should().Be("Matt");
            greaterThanEqual_Asc1[0].social_security_number.Should().Be("123-34-2313");
        }

        public async Task Should_FetchDistinct(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
                await dbms.DropTable<person>()
                          .IfExists()
                          .Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.living_parent_count))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Todd")
                                 .Field(x => x.living_parent_count, 0)
                                 .Field(x => x.social_security_number, "123-14-2112"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 1)
                                 .Field(x => x.social_security_number, "123-14-1235"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 2)
                                 .Field(x => x.social_security_number, "123-34-2313"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 3)
                                 .Field(x => x.social_security_number, "123-34-2135"))
                    .Execute();

            var greaterThanEqual_Asc1 = await db.Select<person>()
                                                .Distinct()
                                                .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                                                .OrderBy(r =>
                                                             r.Field(x => x.name, ListSortDirection.Ascending))
                                                .Execute()
                                                .Then()
                                                .ToArray();

            greaterThanEqual_Asc1.Length.Should().Be(3);

            var greaterThanEqual = await db.Select<person>()
                                           .Distinct()
                                           .Fields(r => r.Field(x => x.name))
                                           .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                                           .OrderBy(r =>
                                                        r.Field(x => x.name, ListSortDirection.Ascending))
                                           .Execute()
                                           .Then()
                                           .ToArray();

            greaterThanEqual.Length.Should().Be(1);
            greaterThanEqual[0].name.Should().Be("Matt");
            greaterThanEqual[0].social_security_number.Should().BeNull();
        }

        public async Task Should_Join_InnerJoin(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<category>().IfExists().Execute();
                await dbms.DropTable<article>().IfExists().Execute();
            }

            await dbms.CreateTable<category>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.name))
                      .Execute();

            await dbms.CreateTable<article>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.category_id)
                                   .Field(x => x.name))
                      .Execute();

            var healthId = Guid.NewGuid();
            var sportsId = Guid.NewGuid();
            var politicsId = Guid.NewGuid();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, healthId)
                                 .Field(x => x.name, "Health"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, sportsId)
                                 .Field(x => x.name, "Sports"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, politicsId)
                                 .Field(x => x.name, "Politics"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Cubs Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Penguins Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, politicsId)
                                 .Field(x => x.name, "Trump Win!!"))
                    .Execute();

            var greaterThanEqual = await db.Select<category>()
                                           .Join<article>((x, y) => x.id == y.category_id)
                                           .Execute()
                                           .Then()
                                           .ToArray();

            greaterThanEqual.Length.Should().Be(3);
        }

        public async Task Should_Join_CrossJoin(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<category>().IfExists().Execute();
                await dbms.DropTable<article>().IfExists().Execute();
            }

            await dbms.CreateTable<category>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.name))
                      .Execute();

            await dbms.CreateTable<article>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.category_id)
                                   .Field(x => x.name))
                      .Execute();

            var healthId = Guid.NewGuid();
            var sportsId = Guid.NewGuid();
            var politicsId = Guid.NewGuid();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, healthId)
                                 .Field(x => x.name, "Health"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, sportsId)
                                 .Field(x => x.name, "Sports"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, politicsId)
                                 .Field(x => x.name, "Politics"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Cubs Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Penguins Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, politicsId)
                                 .Field(x => x.name, "Trump Win!!"))
                    .Execute();

            if (db.Supports.CrossJoin)
            {
                var greaterThanEqual = await db.Select<category>()
                                               .CrossJoin<article>()
                                               .Execute()
                                               .Then()
                                               .ToArray();

                greaterThanEqual.Length.Should().Be(9);
            }
            else
            {
                new Func<Task>(async () => await db.Select<category>()
                                                   .CrossJoin<article>()
                                                   .Execute()
                                                   .Then()
                                                   .ToArray()).Should().Throw<NotSupportedException>();
            }
        }

        public async Task Should_Join_LeftJoin(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<category>().IfExists().Execute();
                await dbms.DropTable<article>().IfExists().Execute();
            }

            await dbms.CreateTable<category>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.name))
                      .Execute();

            await dbms.CreateTable<article>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.category_id)
                                   .Field(x => x.name))
                      .Execute();

            var healthId = Guid.NewGuid();
            var sportsId = Guid.NewGuid();
            var politicsId = Guid.NewGuid();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, healthId)
                                 .Field(x => x.name, "Health"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, sportsId)
                                 .Field(x => x.name, "Sports"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, politicsId)
                                 .Field(x => x.name, "Politics"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Cubs Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Penguins Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, politicsId)
                                 .Field(x => x.name, "Trump Win!!"))
                    .Execute();

            var greaterThanEqual = await db.Select<category>()
                                           .LeftJoin<article>((x, y) => x.id == y.category_id)
                                           .Execute()
                                           .Then()
                                           .ToArray();

            greaterThanEqual.Length.Should().Be(4);
        }

        public async Task Should_Join_LeftJoin_NonNullableWhere(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<category>().IfExists().Execute();
                await dbms.DropTable<article>().IfExists().Execute();
            }

            await dbms.CreateTable<category>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.name))
                      .Execute();

            await dbms.CreateTable<article>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.category_id)
                                   .Field(x => x.name))
                      .Execute();

            var healthId = Guid.NewGuid();
            var sportsId = Guid.NewGuid();
            var politicsId = Guid.NewGuid();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, healthId)
                                 .Field(x => x.name, "Health"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, sportsId)
                                 .Field(x => x.name, "Sports"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, politicsId)
                                 .Field(x => x.name, "Politics"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Cubs Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Penguins Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, politicsId)
                                 .Field(x => x.name, "Trump Win!!"))
                    .Execute();

            var greaterThanEqual = await db.Select<category>()
                                           .LeftJoin<article>((x, y) => x.id == y.category_id)
                                           .Where2(rs => rs.Field(x => x.id, x => x == null))
                                           .Execute()
                                           .Then()
                                           .ToArray();

            greaterThanEqual.Length.Should().Be(1);
        }

        public async Task Should_Join_LeftJoin2Times(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<sample_row>().IfExists().Execute();
                await dbms.DropTable<sample_row_state>().IfExists().Execute();
                await dbms.DropTable<sample_row_version>().IfExists().Execute();
            }

            await dbms.CreateTable<sample_row>().Execute();
            await dbms.CreateTable<sample_row_state>().Execute();
            await dbms.CreateTable<sample_row_version>().Execute();

            var primaryId1 = Guid.NewGuid();
            var primaryId2 = Guid.NewGuid();
            var primaryId3 = Guid.NewGuid();

            await db.Insert<sample_row>()
                    .Record(r => r.Field(x => x.id, primaryId1).Field(x => x.content, "1"))
                    .Execute();
            await db.Insert<sample_row_state>()
                    .Record(r => r.Field(x => x.id, 412).Field(x => x.sample_id, primaryId1))
                    .Execute();
            await db.Insert<sample_row_version>()
                    .Record(r => r.Field(x => x.id, 423).Field(x => x.sample_id, primaryId1))
                    .Execute();

            var q1 = await db.Select<sample_row>()
                             .LeftJoin<sample_row_state>((x, y) => x.id == y.sample_id)
                             .LeftJoin<sample_row_version>((x, y, z) => x.id == z.sample_id)
                             .Execute()
                             .Then()
                             .ToArray();

            q1.Length.Should().Be(1);
            q1[0].Item1.Should().NotBeNull();
            q1[0].Item2.Should().NotBeNull();
            q1[0].Item2.id.Should().Be(412);
            q1[0].Item3.Should().NotBeNull();
            q1[0].Item3.id.Should().Be(423);

            await db.Insert<sample_row>()
                    .Record(r => r.Field(x => x.id, primaryId2).Field(x => x.content, "2"))
                    .Execute();
            await db.Insert<sample_row_version>()
                    .Record(r => r.Field(x => x.id, 523).Field(x => x.sample_id, primaryId2))
                    .Execute();

            await db.Insert<sample_row>()
                    .Record(r => r.Field(x => x.id, primaryId3).Field(x => x.content, "3"))
                    .Execute();
            await db.Insert<sample_row_state>()
                    .Record(r => r.Field(x => x.id, 612).Field(x => x.sample_id, primaryId3))
                    .Execute();

            var q1a = await db.Select<sample_row>()
                              .LeftJoin<sample_row_state>((x, y) => x.id == y.sample_id)
                              .LeftJoin<sample_row_version>((x, y, z) => x.id == z.sample_id)
                              .Where1(rs => rs.Field(x => x.id, primaryId1, (x, y) => x == y))
                              .Execute()
                              .Then()
                              .ToArray();

            q1a.Length.Should().Be(1);
            q1a[0].Item1.Should().NotBeNull();
            q1a[0].Item2.Should().NotBeNull();
            q1a[0].Item2.id.Should().Be(412);
            q1a[0].Item3.Should().NotBeNull();
            q1a[0].Item3.id.Should().Be(423);

            var q2 = await db.Select<sample_row>()
                             .LeftJoin<sample_row_state>((x, y) => x.id == y.sample_id)
                             .LeftJoin<sample_row_version>((x, y, z) => x.id == z.sample_id)
                             .Where1(rs => rs.Field(x => x.id, primaryId2, (x, y) => x == y))
                             .Execute()
                             .Then()
                             .ToArray();

            q2.Length.Should().Be(1);
            q2[0].Item1.Should().NotBeNull();
            q2[0].Item2.Should().NotBeNull();
            q2[0].Item2.id.Should().Be(0);
            q2[0].Item3.Should().NotBeNull();
            q2[0].Item3.id.Should().Be(523);

            var q3 = await db.Select<sample_row>()
                             .LeftJoin<sample_row_state>((x, y) => x.id == y.sample_id)
                             .LeftJoin<sample_row_version>((x, y, z) => x.id == z.sample_id)
                             .Where1(rs => rs.Field(x => x.id, primaryId3, (x, y) => x == y))
                             .Execute()
                             .Then()
                             .ToArray();

            q3.Length.Should().Be(1);
            q3[0].Item1.id.Should().NotBeEmpty();
            q3[0].Item1.Should().NotBeNull();
            q3[0].Item2.Should().NotBeNull();
            q3[0].Item2.id.Should().Be(612);
            q3[0].Item3.Should().NotBeNull();
            q3[0].Item3.id.Should().Be(0);

            var q3_ = await db.Select<sample_row_OtherCased>()
                              .LeftJoin<sample_row_state>((x, y) => x.ID == y.sample_id)
                              .LeftJoin<sample_row_version>((x, y, z) => x.ID == z.sample_id)
                              .Where1(rs => rs.Field(x => x.ID, primaryId3, (x, y) => x == y))
                              .Execute()
                              .Then()
                              .ToArray();

            q3_.Length.Should().Be(1);
            q3_[0].Item1.Should().NotBeNull();
            q3_[0].Item1.ID.Should().NotBeEmpty();
            q3_[0].Item2.Should().NotBeNull();
            q3_[0].Item2.id.Should().Be(612);
            q3_[0].Item3.Should().NotBeNull();
            q3_[0].Item3.id.Should().Be(0);
        }

        public async Task Should_Join_RightJoin(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<category>().IfExists().Execute();
                await dbms.DropTable<article>().IfExists().Execute();
            }

            await dbms.CreateTable<category>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.name))
                      .Execute();

            await dbms.CreateTable<article>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.category_id)
                                   .Field(x => x.name))
                      .Execute();

            var healthId = Guid.NewGuid();
            var sportsId = Guid.NewGuid();
            var politicsId = Guid.NewGuid();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, healthId)
                                 .Field(x => x.name, "Health"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, sportsId)
                                 .Field(x => x.name, "Sports"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, politicsId)
                                 .Field(x => x.name, "Politics"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Cubs Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Penguins Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, politicsId)
                                 .Field(x => x.name, "Trump Win!!"))
                    .Execute();

            if (db.Supports.RightJoin)
            {
                var greaterThanEqual = await db.Select<article>()
                                               .RightJoin<category>((x, y) => x.category_id == y.id)
                                               .Execute()
                                               .Then()
                                               .ToArray();

                greaterThanEqual.Length.Should().Be(4);
            }
            else
            {
                new Func<Task>(async () => await db.Select<article>()
                                                   .RightJoin<category>((x, y) => x.category_id == y.id)
                                                   .Execute()
                                                   .Then()
                                                   .ToArray()).Should().Throw<NotSupportedException>();
            }
        }

        public async Task Should_SelectFieldInQuery(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<category>().IfExists().Execute();
                await dbms.DropTable<article>().IfExists().Execute();
            }

            await dbms.CreateTable<category>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.name))
                      .Execute();

            await dbms.CreateTable<article>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.category_id)
                                   .Field(x => x.name))
                      .Execute();

            var healthId = Guid.NewGuid();
            var sportsId = Guid.NewGuid();
            var politicsId = Guid.NewGuid();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, healthId)
                                 .Field(x => x.name, "Health"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, sportsId)
                                 .Field(x => x.name, "Sports"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, politicsId)
                                 .Field(x => x.name, "Politics"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Cubs Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Penguins Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, politicsId)
                                 .Field(x => x.name, "Trump Win!!"))
                    .Execute();

            var inner = db.Select<category>()
                          .Fields(r => r.Field(x => x.id))
                          .Where(r => r.Field(x => x.name, "S%", Operations.Like));

            var greaterThanEqual = await db.Select<article>()
                                           .Where(r => r.FieldIn(x => x.category_id, inner))
                                           .Execute()
                                           .Then()
                                           .ToArray();

            greaterThanEqual.Length.Should().Be(2);

            var notIn = await db.Select<article>()
                                .Where(r => r.FieldNotIn(x => x.category_id, inner))
                                .Execute()
                                .Then()
                                .ToArray();

            notIn.Length.Should().Be(1);
            notIn.First().name.Should().Be("Trump Win!!");
        }

        public async Task Should_OrderRandomly(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<article>().IfExists().Execute();
            }

            await dbms.CreateTable<article>().Execute();

            var sportsId = Guid.NewGuid();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, new Guid("69C696FD-EAED-490F-BF89-C850FE338970"))
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Cubs Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, new Guid("9A1B4FEE-75B7-4539-A73C-F546366AA4AB"))
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Penguins Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, new Guid("36B3B0EB-F050-4C5F-A24D-EC0D40361157"))
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Sox Win!!"))
                    .Execute();

            var greaterThanEqual1 = await db.Select<article>()
                                            .OrderBy(r => r.Random())
                                            .Execute()
                                            .Then()
                                            .ToArray();

            greaterThanEqual1.Length.Should().Be(3);

            var greaterThanEqual2 = await db.Select<article>()
                                            .OrderBy(r => r.Random())
                                            .Execute()
                                            .Then()
                                            .ToArray();

            var greaterThanEqual3 = await db.Select<article>()
                                            .OrderBy(r => r.Random())
                                            .Execute()
                                            .Then()
                                            .ToArray();

            var ids1 = greaterThanEqual1.Select(x => x.id).ToArray();
            var ids2 = greaterThanEqual2.Select(x => x.id).ToArray();
            var ids3 = greaterThanEqual3.Select(x => x.id).ToArray();

            bool AreEqual(Guid[] a, Guid[] b)
            {
                if (a?.Length != b?.Length)
                {
                    return false;
                }

                for (int i = 0; i < a?.Length; i++)
                {
                    if (a[i] != b[i])
                    {
                        return false;
                    }
                }

                return true;
            }

            Assert.False(AreEqual(ids1, ids2) && AreEqual(ids1, ids3) && AreEqual(ids2, ids3));
        }

        public async Task Should_EscapeTableNames(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<user>().IfExists().Execute();
            }

            await dbms.CreateTable<user>().Execute();

            var sportsId = Guid.NewGuid();

            await db.Insert<user>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.name, "James Doe"))
                    .Execute();

            await db.Insert<user>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.name, "Jim Smith"))
                    .Execute();

            var greaterThanEqual = await db.Select<user>()
                                           .Execute()
                                           .Then()
                                           .ToArray();

            greaterThanEqual.Length.Should().Be(2);
        }

        public async Task Should_SelectFieldIn(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<category>().IfExists().Execute();
                await dbms.DropTable<article>().IfExists().Execute();
            }

            await dbms.CreateTable<category>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.name))
                      .Execute();

            var healthId = Guid.NewGuid();
            var sportsId = Guid.NewGuid();
            var politicsId = Guid.NewGuid();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, healthId)
                                 .Field(x => x.name, "Health"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, sportsId)
                                 .Field(x => x.name, "Sports"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, politicsId)
                                 .Field(x => x.name, "Politics"))
                    .Execute();

            var data = await db.Select<category>()
                               .Where(r => r.FieldIn(x => x.id, new[] {healthId}))
                               .Execute()
                               .Then()
                               .ToArray();

            data.Length.Should().Be(1);
        }

        public async Task Should_SelectMultpleTables(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<category>().IfExists().Execute();
                await dbms.DropTable<article>().IfExists().Execute();
            }

            await dbms.CreateTable<category>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.name))
                      .Execute();

            await dbms.CreateTable<article>()
                      .Fields(r =>
                                  r.Field(x => x.id)
                                   .Field(x => x.category_id)
                                   .Field(x => x.name))
                      .Execute();

            var healthId = Guid.NewGuid();
            var sportsId = Guid.NewGuid();
            var politicsId = Guid.NewGuid();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, healthId)
                                 .Field(x => x.name, "Health"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, sportsId)
                                 .Field(x => x.name, "Sports"))
                    .Execute();

            await db.Insert<category>()
                    .Record(r =>
                                r.Field(x => x.id, politicsId)
                                 .Field(x => x.name, "Politics"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Cubs Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Penguins Win!!"))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, Guid.NewGuid())
                                 .Field(x => x.category_id, politicsId)
                                 .Field(x => x.name, "Trump Win!!"))
                    .Execute();

            var greaterThanEqual = await db.Select<article>()
                                           .Join<category>((x, y) => x.category_id == y.id)
                                           .Execute()
                                           .Then()
                                           .ToArray();

            greaterThanEqual.Length.Should().Be(3);

            greaterThanEqual[0].Item1.id.Should().NotBe(Guid.Empty);
            greaterThanEqual[0].Item1.category_id.Should().Be(sportsId);
            greaterThanEqual[0].Item1.name.Should().Be("Cubs Win!!");

            greaterThanEqual[0].Item2.name.Should().Be("Sports");
            greaterThanEqual[0].Item2.id.Should().Be(sportsId);

            greaterThanEqual[1].Item1.id.Should().NotBe(Guid.Empty);
            greaterThanEqual[1].Item1.category_id.Should().Be(sportsId);
            greaterThanEqual[1].Item1.name.Should().Be("Penguins Win!!");
            greaterThanEqual[1].Item2.id.Should().Be(sportsId);
            greaterThanEqual[1].Item2.name.Should().Be("Sports");

            greaterThanEqual[2].Item1.id.Should().NotBe(Guid.Empty);
            greaterThanEqual[2].Item1.category_id.Should().Be(politicsId);
            greaterThanEqual[2].Item1.name.Should().Be("Trump Win!!");
            greaterThanEqual[2].Item2.id.Should().Be(politicsId);
            greaterThanEqual[2].Item2.name.Should().Be("Politics");
        }

        public async Task Should_Join_2Times(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<publishers>().IfExists().Execute();
                await dbms.DropTable<article>().IfExists().Execute();
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<publishers>()
                      .Fields(r => r.Field(x => x.article_id)
                                    .Field(x => x.published_on)
                                    .Field(x => x.person_id, false, Range.Create(11, 11)))
                      .Execute();

            await dbms.CreateTable<article>().Execute();
            await dbms.CreateTable<person>()
                      .Fields(r => r.Field(x => x.living_parent_count)
                                    .Field(x => x.name)
                                    .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                    .Field(x => x.has_bool))
                      .Execute();

            //            var personId = Guid.NewGuid();
            var articleId = Guid.NewGuid();
            var sportsId = Guid.NewGuid();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "scott")
                                 .Field(x => x.social_security_number, "123-34-4544")
                                 .Field(x => x.living_parent_count, 1))
                    .Execute();

            await db.Insert<article>()
                    .Record(r =>
                                r.Field(x => x.id, articleId)
                                 .Field(x => x.category_id, sportsId)
                                 .Field(x => x.name, "Cubs Win!!"))
                    .Execute();

            await db.Insert<publishers>()
                    .Record(r =>
                                r.Field(x => x.article_id, articleId)
                                 .Field(x => x.person_id, "123-34-4544")
                                 .Field(x => x.published_on, DateTime.UtcNow))
                    .Execute();

            var greaterThanEqual = await db.Select<publishers>()
                                           .WithNoLock()
                                           .Join<article>((x, y) => x.article_id == y.id)
                                           .Join<person>((x, y, z) => x.person_id == z.social_security_number)
                                           .Execute()
                                           .Then()
                                           .ToArray();

            greaterThanEqual.Length.Should().Be(1);
        }

        public async Task Should_Join_2Ons(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<employee>().IfExists().Execute();
            }

            await dbms.CreateTable<employee>()
                      .Fields(rs => rs
                                    .Field(x => x.last_name, false, Range.Create(0, 255))
                                    .Field(x => x.first_name, false, Range.Create(0, 255))
                                    .Field(x => x.manager_last_name, false, Range.Create(0, 255))
                                    .Field(x => x.manager_first_name, false, Range.Create(0, 255))
                                    .Field(x => x.birth_date))
                      .Execute();

            await db.Insert<employee>()
                    .Record(rs => rs.Field(x => x.first_name, "Scott").Field(x => x.last_name, "Young")
                                    .Field(x => x.birth_date, new DateTime(1992, 03, 01)))
                    .Execute();

            await db.Insert<employee>()
                    .Record(rs => rs.Field(x => x.manager_first_name, "Scott")
                                    .Field(x => x.manager_last_name, "Young")
                                    .Field(x => x.first_name, "John")
                                    .Field(x => x.last_name, "Doe")
                                    .Field(x => x.birth_date, new DateTime(1973, 01, 04)))
                    .Execute();

            var result = await db.Select<employee>()
                                 .LeftJoin<employee>((x, y) => x.manager_first_name == y.first_name && x.manager_last_name == y.last_name)
                                 .Where2(x => x.Field(y => y.first_name, y => y != null))
                                 .Execute();
            result.Count().Should().Be(1);
            result.Should().NotBeNull();
            result.First().Item1.birth_date.Should().Be(new DateTime(1973, 01, 04));

            var ser = new CamelCasingIndentedSimpleJsonSerializer();
            ser.Serialize(result.Select(x => x.Item1)).Should().Be(@"[
  {
    ""manager_first_name"": ""Scott"",
    ""manager_last_name"": ""Young"",
    ""first_name"": ""John"",
    ""last_name"": ""Doe"",
    ""birth_date"": ""1973-01-04T00:00:00""
  }
]");

            ser.Serialize(result).Should().Be(@"[
  {
    ""item1"": {
      ""manager_first_name"": ""Scott"",
      ""manager_last_name"": ""Young"",
      ""first_name"": ""John"",
      ""last_name"": ""Doe"",
      ""birth_date"": ""1973-01-04T00:00:00""
    },
    ""item2"": {
      ""manager_first_name"": null,
      ""manager_last_name"": null,
      ""first_name"": ""Scott"",
      ""last_name"": ""Young"",
      ""birth_date"": ""1992-03-01T00:00:00""
    }
  }
]");

            ser.Serialize(result.Select(x => x.Item1).First()).Should().Be(@"{
  ""manager_first_name"": ""Scott"",
  ""manager_last_name"": ""Young"",
  ""first_name"": ""John"",
  ""last_name"": ""Doe"",
  ""birth_date"": ""1973-01-04T00:00:00""
}");
        }

        public async Task Should_UpdateData(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person_with_attributes>().IfExists().Execute();
            }

            await dbms.CreateTable<person_with_attributes>().Execute();

            await db.Insert<person_with_attributes>()
                    .Record(rs => rs.Field(x => x.name, "Scott")
                                    .Field(x => x.social_security_number, "123-45-9860")
                                    .Field(x => x.living_parent_count, 3))
                    .Execute();

            await db.Insert<person_with_attributes>()
                    .Record(rs => rs.Field(x => x.name, "Matt")
                                    .Field(x => x.social_security_number, "345-95-9860")
                                    .Field(x => x.living_parent_count, 1))
                    .Execute();

            var result = await db.Select<person_with_attributes>()
                                 .Where(rs => rs.Field(x => x.social_security_number, "345-95-9860", (x, y) => x == y))
                                 .Execute()
                                 .Then()
                                 .ToArray();

            result.Should().NotBeNull();
            result.Length.Should().Be(1);
            result[0].living_parent_count.Should().Be(1);
            result[0].name.Should().Be("Matt");

            await db.Update<person_with_attributes>()
                    .Set(rs => rs.Field(x => x.name, "James"))
                    .Where(rs => rs.Field(x => x.social_security_number, "345-95-9860", (x, y) => x == y))
                    .Execute();

            result = await db.Select<person_with_attributes>()
                             .Where(rs => rs.Field(x => x.social_security_number, "345-95-9860", (x, y) => x == y))
                             .Execute()
                             .Then()
                             .ToArray();

            result.Should().NotBeNull();
            result.Length.Should().Be(1);
            result[0].living_parent_count.Should().Be(1);
            result[0].name.Should().Be("James");
        }

        private Task Delay() => this.delay.HasValue ? Task.Delay(this.delay.Value) : Task.FromResult(true);
    }

    public class Getter<T>
        where T : ITableRow
    {
        private readonly IDataStore db;

        public Getter(IDataStore db) => this.db = db;

        public async Task<Tuple<T, T>[]> Get()
        {
            return await this.db.Select<T>()
                             .LeftJoin<T>((t1, t2) => t1.id == t2.id && t1.audit_when < t2.audit_when)
                             .Where2(r => r.Field(x => x.audit_when, null, (x, y) => x == y))
                             .Execute()
                             .Then()
                             .ToArray();
        }
    }
}