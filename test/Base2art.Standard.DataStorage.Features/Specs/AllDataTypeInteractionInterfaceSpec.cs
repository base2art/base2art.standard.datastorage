﻿namespace Base2art.DataStorage.Specs
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Data;
    using FluentAssertions;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class AllDataTypeInteractionInterfaceSpec
    {
//        private readonly TimeSpan? delay;

        public AllDataTypeInteractionInterfaceSpec(TimeSpan? delay = null)
        {
//            this.delay = delay;
        }

//        public bool CanDropTables { get; }

        public async Task CreateAllGenerically(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<all_field_types_interface_v1>().IfExists().Execute();
            }

            await dbms.CreateTable<all_field_types_interface_v1>().Execute();
        }

        public async Task CreateInsertSelectById(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<all_field_types_interface_v1>().IfExists().Execute();
            }

            await dbms.CreateTable<all_field_types_interface_v1>().Fields(t => t.Field(x => x.Boolean_value).Field(x => x.Decimal_value)
                                                                                .Field(x => x.Double_value).Field(x => x.Float_value)
                                                                                .Field(x => x.int_value).Field(x => x.long_value)
                                                                                .Field(x => x.short_value).Field(x => x.string_value)
                                                                                .Field(x => x.binary_value)
                                                                                .Field(x => x
                                                                                           .date_value) //                        .Field(x => x.dateTimeOffset_value)
                                                                                .Field(x => x.datetime_value).Field(x => x.interval_value)
                                                                                .Field(x => x.guid_value).Field(x => x.xml1_value)
                                                                                .Field(x => x.xml2_value).Field(x => x.json_value)
                                                                                .Field(x => x.Boolean_nullable_value)
                                                                                .Field(x => x.Decimal_nullable_value)
                                                                                .Field(x => x.Double_nullable_value)
                                                                                .Field(x => x.Float_nullable_value).Field(x => x.int_nullable_value)
                                                                                .Field(x => x.long_nullable_value).Field(x => x.short_nullable_value)
                                                                                .Field(x => x.date_nullable_value)
                                                                                .Field(x => x.dateTimeOffset_nullable_value)
                                                                                .Field(x => x.datetime_nullable_value)
                                                                                .Field(x => x.interval_nullable_value)
                                                                                .Field(x => x.guid_nullable_value)).Execute();
            var g = Guid.NewGuid();
            await db.Insert<all_field_types_interface_v1>().Record(t => t.Field(x => x.Boolean_value, true).Field(x => x.Decimal_value, 10.0m)
                                                                         .Field(x => x.Double_value, double.MaxValue / 2)
                                                                         .Field(x => x.Float_value, float.MaxValue / 2)
                                                                         .Field(x => x.int_value, int.MaxValue / 2)
                                                                         .Field(x => x.long_value, int.MaxValue)
                                                                         .Field(x => x.short_value, short.MaxValue / 2)
                                                                         .Field(x => x.string_value, g.ToString("N"))
                                                                         .Field(x => x.binary_value, g.ToByteArray())
                                                                         .Field(x => x.date_value,
                                                                                DateTime
                                                                                    .UtcNow
                                                                                    .Date) //                        .Field(x => x.dateTimeOffset_value, DateTime.UtcNow)
                                                                         .Field(x => x.datetime_value, DateTime.UtcNow)
                                                                         .Field(x => x.interval_value, DateTime.UtcNow.TimeOfDay)
                                                                         .Field(x => x.guid_value, g).Field(x => x.xml1_value, null)
                                                                         .Field(x => x.xml2_value, null)
                                                                         .Field(x => x.json_value, new Dictionary<string, object>
                                                                                                   {
                                                                                                       {
                                                                                                           "a",
                                                                                                           1
                                                                                                       }
                                                                                                   })
                                                                         .Field(x => x.Boolean_nullable_value, true)
                                                                         .Field(x => x.Decimal_nullable_value, 10.0m)
                                                                         .Field(x => x.Double_nullable_value, double.MaxValue / 2)
                                                                         .Field(x => x.Float_nullable_value, float.MaxValue / 2)
                                                                         .Field(x => x.int_nullable_value, int.MaxValue / 2)
                                                                         .Field(x => x.long_nullable_value, int.MaxValue)
                                                                         .Field(x => x.short_nullable_value, short.MaxValue / 2)
                                                                         .Field(x => x.date_nullable_value, DateTime.UtcNow.Date)
                                                                         .Field(x => x.datetime_nullable_value,
                                                                                DateTime
                                                                                    .UtcNow) //                        .Field(x => x.dateTimeOffset_nullable_value, DateTime.UtcNow)
                                                                         .Field(x => x.interval_nullable_value, DateTime.UtcNow.TimeOfDay)
                                                                         .Field(x => x.guid_nullable_value, g)).Execute();

            var dataValues = await db.Select<all_field_types_interface_v1>()
                                     .Where(rs => rs.Field(x => x.guid_value, g, (x, y) => x == y))
                                     .Execute();

            (await db.Select<all_field_types_interface_v1>()
                     .Where(rs => rs.Field(x => x.guid_value, Guid.NewGuid(), (x, y) => x == y))
                     .Execute()).Count().Should().Be(0);

            dataValues.Count().Should().Be(1);
            var data = dataValues.FirstOrDefault();

            this.Compare(data.binary_value, g.ToByteArray());
            data.Boolean_nullable_value.Should().BeTrue();
            data.Boolean_value.Should().BeTrue();
            data.date_nullable_value.Should().BeCloseTo(DateTime.UtcNow.Date);
            data.date_value.Should().BeCloseTo(DateTime.UtcNow.Date);
            data.datetime_nullable_value.Should().BeCloseTo(DateTime.UtcNow, 5499);
            data.datetime_value.Should().BeCloseTo(DateTime.UtcNow, 5499);
            data.Decimal_nullable_value.Should().Be(10.0m);
            data.Decimal_value.Should().Be(10m);
            data.Double_nullable_value.Should().BeApproximately(double.MaxValue / 2,double.MaxValue / 4);
            data.Double_value.Should().BeApproximately(double.MaxValue / 2,double.MaxValue / 4);
            data.Float_nullable_value.Should().BeApproximately(float.MaxValue / 2, float.MaxValue / 4);
            data.Float_value.Should().BeApproximately(float.MaxValue / 2, float.MaxValue / 4);
            data.guid_nullable_value.Should().Be(g);
            data.guid_value.Should().Be(g);
            data.int_nullable_value.Should().Be(int.MaxValue / 2);
            data.int_value.Should().Be(int.MaxValue / 2);
            data.long_nullable_value.Should().Be(int.MaxValue);
            data.long_value.Should().Be(int.MaxValue);
            data.short_nullable_value.Should().Be(short.MaxValue / 2);
            data.short_value.Should().Be(short.MaxValue / 2);
            data.interval_nullable_value.Should().BeCloseTo(DateTime.UtcNow.TimeOfDay, 4099);
            data.interval_value.Should().BeCloseTo(DateTime.UtcNow.TimeOfDay, 4099);
            //            data.object_value.Should().Be(null);
            data.json_value.Count.Should().Be(1);
            data.string_value.Should().Be(g.ToString("N"));
            data.xml1_value.Should().BeNull();
            data.xml2_value.Should().Be(null);
            //            data.interval_value.Should().Be(int.MaxValue);

            var output = JsonConvert.SerializeObject(data,
                                                     new JsonSerializerSettings
                                                     {
                                                         ContractResolver = new CamelCasePropertyNamesContractResolver(),
                                                         ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                                                     });
            // Console.WriteLine(output);
            /*
             *{"boolean_value":true,"decimal_value":10.0,"double_value":8.9884656743115785E+307,"float_value":1.70141173E+38,"int_value":1073741823,"long_value":2147483647,"short_value":16383,"string_value":"0c13f0e46d7049cbb525300463358ba0","date_value":"2018-06-08T00:00:00","dateTimeOffset_value":"0001-01-01T00:00:00+00:00","datetime_value":"2018-06-08T08:30:14","interval_value":"08:30:14","guid_value":"0c13f0e4-6d70-49cb-b525-300463358ba0","binary_value":"5PATDHBty0m1JTAEYzWLoA==","xml1_value":null,"xml2_value":null,"json_value":{"a":1},"boolean_nullable_value":true,"decimal_nullable_value":10.0,"double_nullable_value":8.9884656743115785E+307,"float_nullable_value":1.70141173E+38,"int_nullable_value":1073741823,"long_nullable_value":2147483647,"short_nullable_value":16383,"date_nullable_value":"2018-06-08T00:00:00","dateTimeOffset_nullable_value":null,"datetime_nullable_value":"2018-06-08T08:30:14","interval_nullable_value":"08:30:14","guid_nullable_value":"0c13f0e4-6d70-49cb-b525-300463358ba0","target":{"data":{"boolean_value":true,"decimal_value":10.0,"double_value":8.9884656743115785E+307,"float_value":1.70141173E+38,"int_value":1073741823,"long_value":2147483647,"short_value":16383,"string_value":"0c13f0e46d7049cbb525300463358ba0","date_value":"2018-06-08T00:00:00","dateTimeOffset_value":"0001-01-01T00:00:00+00:00","datetime_value":"2018-06-08T08:30:14","interval_value":"08:30:14","guid_value":"0c13f0e4-6d70-49cb-b525-300463358ba0","binary_value":"5PATDHBty0m1JTAEYzWLoA==","xml1_value":null,"xml2_value":null,"json_value":{"a":1},"boolean_nullable_value":true,"decimal_nullable_value":10.0,"double_nullable_value":8.9884656743115785E+307,"float_nullable_value":1.70141173E+38,"int_nullable_value":1073741823,"long_nullable_value":2147483647,"short_nullable_value":16383,"date_nullable_value":"2018-06-08T00:00:00","dateTimeOffset_nullable_value":null,"datetime_nullable_value":"2018-06-08T08:30:14","interval_nullable_value":"08:30:14","guid_nullable_value":"0c13f0e4-6d70-49cb-b525-300463358ba0"}}}
             * 
             */
            output.Should().StartWith("{\"boolean_value\":true,\"decimal_value\":10.0,");
            output.Should().Contain(",\"decimal_nullable_value\":10.0,");

            var dataValues1 = await db.Select<all_field_types_interface_v1>()
                                      .Where(rs => rs.Field(x => x.guid_nullable_value, g, (x, y) => x == y))
                                      .Execute();

            dataValues1.Count().Should().Be(1);
        }

        public async Task CreateInsertSelectDelete(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<all_field_types_interface_v1>().IfExists().Execute();
            }

            await dbms.CreateTable<all_field_types_interface_v1>().Fields(t => t.Field(x => x.Boolean_value).Field(x => x.Decimal_value)
                                                                                .Field(x => x.Double_value).Field(x => x.Float_value)
                                                                                .Field(x => x.int_value).Field(x => x.long_value)
                                                                                .Field(x => x.short_value).Field(x => x.string_value)
                                                                                .Field(x => x.binary_value)
                                                                                .Field(x => x
                                                                                           .date_value) //                        .Field(x => x.dateTimeOffset_value)
                                                                                .Field(x => x.datetime_value).Field(x => x.interval_value)
                                                                                .Field(x => x.guid_value).Field(x => x.xml1_value)
                                                                                .Field(x => x.xml2_value).Field(x => x.json_value)
                                                                                .Field(x => x.Boolean_nullable_value)
                                                                                .Field(x => x.Decimal_nullable_value)
                                                                                .Field(x => x.Double_nullable_value)
                                                                                .Field(x => x.Float_nullable_value).Field(x => x.int_nullable_value)
                                                                                .Field(x => x.long_nullable_value).Field(x => x.short_nullable_value)
                                                                                .Field(x => x.date_nullable_value)
                                                                                .Field(x => x.dateTimeOffset_nullable_value)
                                                                                .Field(x => x.datetime_nullable_value)
                                                                                .Field(x => x.interval_nullable_value)
                                                                                .Field(x => x.guid_nullable_value)).Execute();
            var g = Guid.NewGuid();
            await db.Insert<all_field_types_interface_v1>().Record(t => t.Field(x => x.Boolean_value, true).Field(x => x.Decimal_value, 10.0m)
                                                                         .Field(x => x.Double_value, double.MaxValue / 2)
                                                                         .Field(x => x.Float_value, float.MaxValue / 2)
                                                                         .Field(x => x.int_value, int.MaxValue / 2)
                                                                         .Field(x => x.long_value, int.MaxValue)
                                                                         .Field(x => x.short_value, short.MaxValue / 2)
                                                                         .Field(x => x.string_value, g.ToString("N"))
                                                                         .Field(x => x.binary_value, g.ToByteArray())
                                                                         .Field(x => x.date_value,
                                                                                DateTime
                                                                                    .UtcNow
                                                                                    .Date) //                        .Field(x => x.dateTimeOffset_value, DateTime.UtcNow)
                                                                         .Field(x => x.datetime_value, DateTime.UtcNow)
                                                                         .Field(x => x.interval_value, DateTime.UtcNow.TimeOfDay)
                                                                         .Field(x => x.guid_value, g).Field(x => x.xml1_value, null)
                                                                         .Field(x => x.xml2_value, null)
                                                                         .Field(x => x.json_value, new Dictionary<string, object>
                                                                                                   {
                                                                                                       {
                                                                                                           "a",
                                                                                                           1
                                                                                                       }
                                                                                                   })
                                                                         .Field(x => x.Boolean_nullable_value, true)
                                                                         .Field(x => x.Decimal_nullable_value, 10.0m)
                                                                         .Field(x => x.Double_nullable_value, double.MaxValue / 2)
                                                                         .Field(x => x.Float_nullable_value, float.MaxValue / 2)
                                                                         .Field(x => x.int_nullable_value, int.MaxValue / 2)
                                                                         .Field(x => x.long_nullable_value, int.MaxValue)
                                                                         .Field(x => x.short_nullable_value, short.MaxValue / 2)
                                                                         .Field(x => x.date_nullable_value, DateTime.UtcNow.Date)
                                                                         .Field(x => x.datetime_nullable_value,
                                                                                DateTime
                                                                                    .UtcNow) //                        .Field(x => x.dateTimeOffset_nullable_value, DateTime.UtcNow)
                                                                         .Field(x => x.interval_nullable_value, DateTime.UtcNow.TimeOfDay)
                                                                         .Field(x => x.guid_nullable_value, g)).Execute();
            var data = await db.SelectSingle<all_field_types_interface_v1>().Execute();
            this.Compare(data.binary_value, g.ToByteArray());
            data.Boolean_nullable_value.Should().BeTrue();
            data.Boolean_value.Should().BeTrue();
            data.date_nullable_value.Should().BeCloseTo(DateTime.UtcNow.Date);
            data.date_value.Should().BeCloseTo(DateTime.UtcNow.Date);
            data.datetime_nullable_value.Should().BeCloseTo(DateTime.UtcNow, 2499);
            data.datetime_value.Should().BeCloseTo(DateTime.UtcNow, 2499);
            data.Decimal_nullable_value.Should().Be(10.0m);
            data.Decimal_value.Should().Be(10m);
            data.Double_nullable_value.Should().BeApproximately(double.MaxValue / 2, double.MaxValue / 4);
            data.Double_value.Should().BeApproximately(double.MaxValue / 2, double.MaxValue / 4);
            data.Float_nullable_value.Should().BeApproximately(float.MaxValue / 2, float.MaxValue / 4);
            data.Float_value.Should().BeApproximately(float.MaxValue / 2, float.MaxValue / 4);
            data.guid_nullable_value.Should().Be(g);
            data.guid_value.Should().Be(g);
            data.int_nullable_value.Should().Be(int.MaxValue / 2);
            data.int_value.Should().Be(int.MaxValue / 2);
            data.long_nullable_value.Should().Be(int.MaxValue);
            data.long_value.Should().Be(int.MaxValue);
            data.short_nullable_value.Should().Be(short.MaxValue / 2);
            data.short_value.Should().Be(short.MaxValue / 2);
            data.interval_nullable_value.Should().BeCloseTo(DateTime.UtcNow.TimeOfDay, 4099);
            data.interval_value.Should().BeCloseTo(DateTime.UtcNow.TimeOfDay, 4099);
            //            data.object_value.Should().Be(null);
            data.json_value.Count.Should().Be(1);
            data.string_value.Should().Be(g.ToString("N"));
            data.xml1_value.Should().BeNull();
            data.xml2_value.Should().Be(null);
            //            data.interval_value.Should().Be(int.MaxValue);

            var output = JsonConvert.SerializeObject(data,
                                                     new JsonSerializerSettings
                                                     {
                                                         ContractResolver = new CamelCasePropertyNamesContractResolver(),
                                                         ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                                                     });
            // Console.WriteLine(output);
            /*
             *{"boolean_value":true,"decimal_value":10.0,"double_value":8.9884656743115785E+307,"float_value":1.70141173E+38,"int_value":1073741823,"long_value":2147483647,"short_value":16383,"string_value":"0c13f0e46d7049cbb525300463358ba0","date_value":"2018-06-08T00:00:00","dateTimeOffset_value":"0001-01-01T00:00:00+00:00","datetime_value":"2018-06-08T08:30:14","interval_value":"08:30:14","guid_value":"0c13f0e4-6d70-49cb-b525-300463358ba0","binary_value":"5PATDHBty0m1JTAEYzWLoA==","xml1_value":null,"xml2_value":null,"json_value":{"a":1},"boolean_nullable_value":true,"decimal_nullable_value":10.0,"double_nullable_value":8.9884656743115785E+307,"float_nullable_value":1.70141173E+38,"int_nullable_value":1073741823,"long_nullable_value":2147483647,"short_nullable_value":16383,"date_nullable_value":"2018-06-08T00:00:00","dateTimeOffset_nullable_value":null,"datetime_nullable_value":"2018-06-08T08:30:14","interval_nullable_value":"08:30:14","guid_nullable_value":"0c13f0e4-6d70-49cb-b525-300463358ba0","target":{"data":{"boolean_value":true,"decimal_value":10.0,"double_value":8.9884656743115785E+307,"float_value":1.70141173E+38,"int_value":1073741823,"long_value":2147483647,"short_value":16383,"string_value":"0c13f0e46d7049cbb525300463358ba0","date_value":"2018-06-08T00:00:00","dateTimeOffset_value":"0001-01-01T00:00:00+00:00","datetime_value":"2018-06-08T08:30:14","interval_value":"08:30:14","guid_value":"0c13f0e4-6d70-49cb-b525-300463358ba0","binary_value":"5PATDHBty0m1JTAEYzWLoA==","xml1_value":null,"xml2_value":null,"json_value":{"a":1},"boolean_nullable_value":true,"decimal_nullable_value":10.0,"double_nullable_value":8.9884656743115785E+307,"float_nullable_value":1.70141173E+38,"int_nullable_value":1073741823,"long_nullable_value":2147483647,"short_nullable_value":16383,"date_nullable_value":"2018-06-08T00:00:00","dateTimeOffset_nullable_value":null,"datetime_nullable_value":"2018-06-08T08:30:14","interval_nullable_value":"08:30:14","guid_nullable_value":"0c13f0e4-6d70-49cb-b525-300463358ba0"}}}
             * 
             */
            output.Should().StartWith("{\"boolean_value\":true,\"decimal_value\":10.0,");
            output.Should().Contain(",\"decimal_nullable_value\":10.0,");
        }

        private void Compare<T>(T[] x, T[] y) where T : IComparable<T>
        {
            x.Length.Should().Be(y.Length);
            for (var i = 0; i < x.Length; i++)
            {
                x[i].Should().Be(y[i]);
            }
        }
    }
}