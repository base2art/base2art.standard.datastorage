namespace Base2art.DataStorage.Specs
{
    using System;
    using System.Threading.Tasks;
    using Data;
    using DataDefinition;
    using FluentAssertions;
    using Threading.Tasks;

    public class DbJoinAndAggregate_Selects
    {
        private readonly TimeSpan? delay;

        public DbJoinAndAggregate_Selects(TimeSpan? delay = null) => this.delay = delay;

        public async Task Should_JoinOnLike(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
                await dbms.DropTable<feed_item>().IfExists().Execute();
                await dbms.DropTable<podcaster>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11)))
                      .Execute();
            await dbms.CreateTable<feed_item>().Execute();
            await dbms.CreateTable<podcaster>().Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.social_security_number, "123-14-2135"))
                    .Execute();

            await db.Insert<feed_item>()
                    .Record(r =>
                                r.Field(x => x.title, "This is a story about Scott and Matt")
                                 .Field(x => x.id, "feed-00001"))
                    .Execute();

            await db.Insert<feed_item>()
                    .Record(r =>
                                r.Field(x => x.title, "This is a story about Just Matt")
                                 .Field(x => x.id, "feed-00002"))
                    .Execute();

            await this.Delay();

            /*
INSERT INTO podcaster (id, person_id, feed_item_id) 
  SELECT randomblob(16), p.id, fi.id
    FROM `feed_item` `fi`
    JOIN `person` `p`
      ON `fi`.`Title` LIKE ('%' || (`p`.`name`) || '%')
    LEFT JOIN `podcaster` `pc`
      ON `pc`.`person_id` = `p`.`id` AND
         `fi`.`id` = `pc`.`feed_item_id`
    WHERE pc.id IS NULL
*/
            var matts = await db.Select<feed_item>()
                                .Join<person>((fi, p) => fi.title.Contains(p.name))
                                .LeftJoin<podcaster>((fi, p, pc) => pc.social_security_number == p.social_security_number)
                                .Where3(r => r.Field(x => x.social_security_number, null, (x, y) => x == y))
                                .Execute()
                                .Then()
                                .ToArray();

            matts.Length.Should().Be(1);

            var matts1 = await db.Select<person>()
                                 .Join<feed_item>((p, fi) => fi.title.Contains(p.name))
                                 .LeftJoin<podcaster>((p, fi, pc) => pc.social_security_number == p.social_security_number)
                                 .Where3(r => r.Field(x => x.social_security_number, null, (x, y) => x == y))
                                 .Execute()
                                 .Then()
                                 .ToArray();

            matts1.Length.Should().Be(1);
//            matts[0].name.Should().Be("Matt");
        }

        public async Task Should_GroupByContent(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
                await dbms.DropTable<feed_item>().IfExists().Execute();
                await dbms.DropTable<podcaster>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11)))
                      .Execute();
            await dbms.CreateTable<feed_item>().Execute();
            await dbms.CreateTable<podcaster>().Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.social_security_number, "123-14-2135"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Scott")
                                 .Field(x => x.social_security_number, "123-14-2136"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.social_security_number, "123-14-2137"))
                    .Execute();

            await this.Delay();

            /*
INSERT INTO podcaster (id, person_id, feed_item_id) 
  SELECT randomblob(16), p.id, fi.id
    FROM `feed_item` `fi`
    JOIN `person` `p`
      ON `fi`.`Title` LIKE ('%' || (`p`.`name`) || '%')
    LEFT JOIN `podcaster` `pc`
      ON `pc`.`person_id` = `p`.`id` AND
         `fi`.`id` = `pc`.`feed_item_id`
    WHERE pc.id IS NULL
*/

            var greaterThanEqual_Asc1 = await db.Select<person>()
                                                .Fields(r => r.Field(x => x.name))
                                                .GroupBy<person_aggregate>(r => r.Field(x => x.name))
                                                .Fields2(r => r.Count(x => x.count))
                                                .OrderBy1(r => r.Field(x => x.name, ListSortDirection.Ascending))
                                                .Execute()
                                                .Then()
                                                .ToArray();

            greaterThanEqual_Asc1.Length.Should().Be(2);
            greaterThanEqual_Asc1[0].Item2.count.Should().Be(1);
            greaterThanEqual_Asc1[1].Item2.count.Should().Be(2);

            var greaterThanEqual_Desc1 = await db.Select<person>()
                                                 .Fields(r => r.Field(x => x.name))
                                                 .GroupBy<person_aggregate>(r => r.Field(x => x.name))
                                                 .Fields2(r => r.Count(x => x.count))
                                                 .OrderBy2(r => r.Field(x => x.count, ListSortDirection.Descending))
                                                 .Execute()
                                                 .Then()
                                                 .ToArray();
            greaterThanEqual_Desc1.Length.Should().Be(2);
            greaterThanEqual_Desc1[0].Item2.count.Should().Be(2);
            greaterThanEqual_Desc1[1].Item2.count.Should().Be(1);
//            var matts = await db.Select<person>()
//                                .GroupBy(rs=>rs.Field(x=>x.name))
//                                .Execute()
//                                .Then()
//                                .ToArray();

//            matts.Length.Should().Be(1);
        }

        private Task Delay() => this.delay.HasValue ? Task.Delay(this.delay.Value) : Task.FromResult(true);
    }
}