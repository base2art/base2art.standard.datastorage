﻿namespace Base2art.DataStorage.Specs
{
    using System;
    using System.Threading.Tasks;
    using Data;
    using DataDefinition;
    using FluentAssertions;
    using Meta;
    using Threading.Tasks;
    using Xunit.Abstractions;

    public class CreateTableSpec
    {
        private readonly TimeSpan? delay;

//        private readonly bool truncatesChar = false;

        private readonly Type exceptionType;

//        private readonly bool throwsExceptionOnOverflow = false;

//
//        private readonly bool autoexpandingColumns = false;

        public CreateTableSpec(
            Type exceptionType,
            TimeSpan? delay = null)
        {
//            this.autoexpandingColumns = autoexpandingColumns;
            this.exceptionType = exceptionType;
            this.delay = delay;
        }

//        private bool CanDropTables { get; }

        public async Task ShouldCreateTable(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person_with_attributes>().IfExists().Execute();
            }

            await dbms.CreateTable<person_with_attributes>().Execute();

            Func<Task> act = async () => await dbms.CreateTable<person_with_attributes>().Execute();
            Func<Task> act1 = () => Task.FromResult(true);

            if (dbms.Supports.DroppingTables)
            {
                act1 = async () => await dbms.CreateTable<person_with_attributes>().IfNotExists().Execute();
            }

            act.Should().Throw<Exception>();
            act1.Should().NotThrow();
            act.Should().Throw<Exception>();
            act1.Should().NotThrow();
        }

        public async Task ShouldBeAbleToDeclareFields(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r =>
                                  r.Field(x => x.name, false, Range.Create(0, 250))
                                   .Field(x => x.social_security_number, false, Range.Create(11, 11))
                                   .Field(x => x.living_parent_count))
                      .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Todd")
                                 .Field(x => x.living_parent_count, 0)
                                 .Field(x => x.social_security_number, "123-14-2112"))
                    .Execute();

            Func<Task> act = async () => await db.Insert<person>()
                                                 .Record(r =>
                                                             r.Field(x => x.name, new string('S', 255))
                                                              .Field(x => x.living_parent_count, 1)
                                                              .Field(x => x.social_security_number, "123-1235"))
                                                 .Execute();

            if (db.Supports.OverflowHandling == DbmsOverflowHandlingStyle.ThrowException)
            {
                act.Should().Throw<Exception>();
                await db.Insert<person>()
                        .Record(r =>
                                    r.Field(x => x.name, new string('S', 250))
                                     .Field(x => x.living_parent_count, 1)
                                     .Field(x => x.social_security_number, "123-1235"))
                        .Execute();
            }
            else
            {
                act.Should().NotThrow();
            }

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 2)
                                 .Field(x => x.social_security_number, "123-34-2313"))
                    .Execute();

            await db.Insert<person>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 3)
                                 .Field(x => x.social_security_number, "123-34-2135"))
                    .Execute();

            var data = await db.Select<person>()
                               .Fields(r => r.Field(x => x.name).Field(x => x.social_security_number))
                               .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                               .OrderBy(r =>
                                            r.Field(x => x.name, ListSortDirection.Descending)
                                             .Field(x => x.social_security_number, ListSortDirection.Descending))
                               .Execute()
                               .Then()
                               .ToArray();

            var truncatesChar = db.Supports.UnderflowHandling == DbmsUnderflowHandlingStyle.None;
            var autoexpandingColumns = db.Supports.OverflowHandling == DbmsOverflowHandlingStyle.Expand;
            data.Length.Should().Be(3);
            data[0].name.Should().Be(new string('S', autoexpandingColumns ? 255 : 250));
            data[0].social_security_number.Should().Be(truncatesChar ? "123-1235" : "123-1235   ");
            data[1].name.Should().Be("Matt");
            data[1].social_security_number.Should().Be("123-34-2313");
            data[2].name.Should().Be("Matt");
            data[2].social_security_number.Should().Be("123-34-2135");
        }

        public async Task ShouldBeAbleToInferFields(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person_with_attributes>().IfExists().Execute();
            }

            await dbms.CreateTable<person_with_attributes>()
                      .Execute();

            await db.Insert<person_with_attributes>()
                    .Record(r =>
                                r.Field(x => x.name, "Todd")
                                 .Field(x => x.living_parent_count, 0)
                                 .Field(x => x.social_security_number, "123-14-2112"))
                    .Execute();

            Func<Task> act = async () => await db.Insert<person_with_attributes>()
                                                 .Record(r =>
                                                             r.Field(x => x.name, new string('S', 255))
                                                              .Field(x => x.living_parent_count, 1)
                                                              .Field(x => x.social_security_number, "123-1235"))
                                                 .Execute();

            if (db.Supports.OverflowHandling == DbmsOverflowHandlingStyle.ThrowException)
            {
                act.Should().Throw<Exception>();
                await db.Insert<person_with_attributes>()
                        .Record(r =>
                                    r.Field(x => x.name, new string('S', 250))
                                     .Field(x => x.living_parent_count, 1)
                                     .Field(x => x.social_security_number, "123-1235"))
                        .Execute();
            }
            else
            {
                act.Should().NotThrow();
            }

            await db.Insert<person_with_attributes>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 2)
                                 .Field(x => x.social_security_number, "123-34-2313"))
                    .Execute();

            await db.Insert<person_with_attributes>()
                    .Record(r =>
                                r.Field(x => x.name, "Matt")
                                 .Field(x => x.living_parent_count, 3)
                                 .Field(x => x.social_security_number, "123-34-2135"))
                    .Execute();

            var data = await db.Select<person_with_attributes>()
                               .Fields(r => r.Field(x => x.name).Field(x => x.social_security_number))
                               .Where(r => r.Field(x => x.living_parent_count, x => x >= 1))
                               .OrderBy(r =>
                                            r.Field(x => x.name, ListSortDirection.Descending)
                                             .Field(x => x.social_security_number, ListSortDirection.Descending))
                               .Execute()
                               .Then()
                               .ToArray();

            var truncatesChar = db.Supports.UnderflowHandling == DbmsUnderflowHandlingStyle.None;
            var autoexpandingColumns = db.Supports.OverflowHandling == DbmsOverflowHandlingStyle.Expand;

            data.Length.Should().Be(3);
            data[0].name.Should().Be(new string('S', autoexpandingColumns ? 255 : 250));
            data[0].social_security_number.Should().Be(truncatesChar ? "123-1235" : "123-1235   ");
            data[1].name.Should().Be("Matt");
            data[1].social_security_number.Should().Be("123-34-2313");
            data[2].name.Should().Be("Matt");
            data[2].social_security_number.Should().Be("123-34-2135");
        }

        public async Task ShouldBeAbleToInferRequiredFields(IDbms dbms, IDataStore db, ITestOutputHelper helper = null)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person_with_attributes>().IfExists().Execute();
            }

            await dbms.CreateTable<person_with_attributes>()
                      .Execute();

            Func<Task> act = async () => await db.Insert<person_with_attributes>()
                                                 .Record(r => r.Field(x => x.name, null)
                                                               .Field(x => x.living_parent_count, 0)
                                                               .Field(x => x.social_security_number, "123-14-2112"))
                                                 .Execute();

            var exception = act.Should().Throw<Exception>();
            helper?.WriteLine(exception.And.StackTrace);
            
            exception.And.GetType().FullName.Should().Be(db.Supports.NativeExceptionTypeName);
        }

        public async Task ShouldBeAbleToUpgradeTables(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r => r.Field(x => x.name, false, Range.Create(0, 250)))
                      .Execute();

            await db.Insert<person>()
                    .Record(r => r.Field(x => x.name, "Scott"))
                    .Execute();

            var data = await db.Select<person>()
                               .Execute()
                               .Then()
                               .ToArray();

            data.Length.Should().Be(1);
            data[0].social_security_number.Should().BeNull();
            data[0].name.Should().Be("Scott");

            await dbms.CreateOrUpdateTable<person>()
                      .Fields(r => r.Field(x => x.name, false, Range.Create(0, 250))
                                    .Field(x => x.social_security_number, false, Range.Create(11, 11)))
                      .Execute();

            await db.Insert<person>()
                    .Record(r => r.Field(x => x.name, "Matt").Field(x => x.social_security_number, "874-23-2934"))
                    .Execute();

            data = await db.Select<person>()
                           .Execute()
                           .Then()
                           .ToArray();
            data.Length.Should().Be(2);
            data[0].social_security_number.Should().BeNull();
            data[0].name.Should().Be("Scott");
            data[1].social_security_number.Should().Be("874-23-2934");
            data[1].name.Should().Be("Matt");
        }

        public async Task ShouldBeAbleToAddKeyes(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r => r.Field(x => x.name, false, Range.Create(0, 250)))
                      .WithKey("name", r => r.Field(x => x.name))
                      .Execute();

            Func<Task> creator = async () => await db.Insert<person>()
                                                     .Record(r => r.Field(x => x.name, "Scott"))
                                                     .Execute();

            creator.Should().NotThrow();
            if (dbms.Supports.ConstraintEnforcement)
            {
                creator.Should().Throw<Exception>();
            }
            else
            {
                creator.Should().NotThrow();
            }

            await db.Insert<person>()
                    .Record(r => r.Field(x => x.name, "Matt"))
                    .Execute();

            await dbms.CreateOrUpdateTable<person>()
                      .Fields(r => r.Field(x => x.name, false, Range.Create(0, 250)))
                      .WithKey("name", r => r.Field(x => x.name))
                      .Execute();
        }

        public async Task ShouldBeAbleToAddIndexes(IDbms dbms, IDataStore db)
        {
            if (dbms.Supports.DroppingTables)
            {
                await dbms.DropTable<person>().IfExists().Execute();
            }

            await dbms.CreateTable<person>()
                      .Fields(r => r.Field(x => x.name, false, Range.Create(0, 250)))
                      .Fields(r => r.Field(x => x.social_security_number, false, Range.Create(0, 250)))
                      .WithIndex("name", r => r.Field(x => x.name))
                      .WithIndex("social_security_number", r => r.Field(x => x.social_security_number))
                      .Execute();

            Func<Task> creator = async () => await db.Insert<person>()
                                                     .Record(r => r.Field(x => x.name, "Scott"))
                                                     .Execute();

            creator.Should().NotThrow();
            creator.Should().NotThrow();

            await db.Insert<person>()
                    .Record(r => r.Field(x => x.name, "Matt"))
                    .Execute();

            await dbms.CreateOrUpdateTable<person>()
                      .Fields(r => r.Field(x => x.name, false, Range.Create(0, 250)))
                      .WithIndex("name", r => r.Field(x => x.name))
                      .Execute();
        }
    }
}