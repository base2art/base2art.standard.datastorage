﻿namespace Base2art.DataStorage
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Data;
    using Xunit;

    public class ApiFeature
    {
        private class MessageQueueSample
        {
            private readonly IDataStore connectionFactory = null;

            public Task AddItemsToQueue(QueueWorkItemRequest[] workItems)
            {
                var insert = this.connectionFactory.Insert<message_queue_message_v1>();
                foreach (var item in workItems)
                {
                    insert.Record(r =>
                                      r.Field(x => x.id, Guid.NewGuid())
                                       .Field(x => x.application_key, item.ApplicationKey)
                                       .Field(x => x.application_type, item.WorkItemType)
                                       .Field(x => x.application_data, item.WorkItemData)
                                       .Field(x => x.retries_remaining, item.RetryCount)
                                       .Field(x => x.process_timeout, item.Timeout)
                                       .Field(x => x.previous_message_id, null)
                                       .Field(x => x.requestor, item.RequestedBy)
                                       .Field(x => x.requested_at, item.RequestedAt.ToUniversalTime().DateTime));
                }

                return insert.Execute();
            }

            public Task AddItemsToQueueWithApproval(QueueWorkItemRequest[] workItems)
            {
                var inserter = this.connectionFactory.Insert<message_queue_message_v1>();
                foreach (var item in workItems)
                {
                    inserter.Record(r =>
                                        r.Field(x => x.id, Guid.NewGuid())
                                         .Field(x => x.application_key, item.ApplicationKey)
                                         .Field(x => x.application_type, item.WorkItemType)
                                         .Field(x => x.application_data, item.WorkItemData)
                                         .Field(x => x.retries_remaining, item.RetryCount)
                                         .Field(x => x.process_timeout, item.Timeout)
                                         .Field(x => x.previous_message_id, null)
                                         .Field(x => x.requestor, item.RequestedBy)
                                         .Field(x => x.requested_at, item.RequestedAt.ToUniversalTime().DateTime)
                                         .Field(x => x.approver, item.RequestedBy)
                                         .Field(x => x.approved_at, item.RequestedAt.ToUniversalTime().DateTime));
                }

                return inserter.Execute();
            }

            public Task FindUnapprovedItems(int count)
            {
                return this.connectionFactory.Select<message_queue_message_v1>()
                           .Where(r => r.Field(x => x.approver, a => a == null))
                           .Limit(count)
                           .Execute();
            }

            public Task FindUnapprovedItems(string workItemType, int count)
            {
                return this.connectionFactory.Select<message_queue_message_v1>()
                           .Where(r => r.Field(x => x.approver, a => a == null)
                                        .Field(x => x.application_type, workItemType, (a, b) => a == b))
                           .Limit(count)
                           .Execute();
            }

            public Task PeekItem(string workItemType)
            {
                return this.connectionFactory.SelectSingle<message_queue_message_v1>()
                           .Where(r => r.Field(x => x.application_type, workItemType, (a, b) => a == b)
                                        .Field(x => x.approver, a => a != null)
                                        .Field(x => x.processing_begun_at, x => x == null))
                           .Execute();
            }

            public async Task PollItem(
                string workItemType, string machine, string process, string thread)
            {
                var item = await this.connectionFactory.SelectSingle<message_queue_message_v1>()
                                     .Where(r => r.Field(x => x.application_type, workItemType, (a, b) => a == b)
                                                  .Field(x => x.processing_completed_at, x => x == null)
                                                  .Field(x => x.approver, a => a != null)
                                                  .Field(x => x.processing_lock, a => a == null))
                                     .Execute();

                if (item == null)
                {
                    return;
                }

                var lockId = Guid.NewGuid();
                await this.connectionFactory.Update<message_queue_message_v1>()
                          .Set(x => x.Field(y => y.processing_by_machine, machine)
                                     .Field(y => y.processing_by_process, process)
                                     .Field(y => y.processing_by_thread, thread)
                                     .Field(y => y.processing_begun_at, DateTime.UtcNow)
                                     .Field(y => y.processing_lock, lockId))
                          .Where(r => r.Field(x => x.id, item.id, (a, b) => a == b))
                          .Execute();

                await this.connectionFactory.SelectSingle<message_queue_message_v1>()
                          .Where(r => r.Field(x => x.id, item.id, (a, b) => a == b)
                                       .Field(x => x.processing_lock, lockId, (a, b) => a == b))
                          .Execute();
            }

            public Task GetMessageItem(Guid id)
            {
                return this.connectionFactory.SelectSingle<message_queue_message_v1>()
                           .Where(r => r.Field(x => x.id, id, (a, b) => a == b))
                           .Execute();
            }

            public Task ApproveItem(Guid id, string approver)
            {
                return this.connectionFactory.Update<message_queue_message_v1>()
                           .Set(x => x.Field(y => y.approver, approver)
                                      .Field(y => y.approved_at, DateTime.UtcNow))
                           .Where(r => r.Field(x => x.id, id, (a, b) => a == b))
                           .Execute();
            }

            public Task MarkItemSuccessful(Guid id, string details)
            {
                return this.connectionFactory.Update<message_queue_message_v1>()
                           .Set(x => x.Field(y => y.processing_completed_at, DateTime.UtcNow)
                                      .Field(y => y.processing_completed_details, details)
                                      .Field(y => y.processing_completed_successfully, true))
                           .Where(r => r.Field(x => x.id, id, (a, b) => a == b))
                           .Execute();
            }

            public Task MarkItemFailed(Guid id, string details)
            {
                return this.connectionFactory.Update<message_queue_message_v1>()
                           .Set(x => x.Field(y => y.processing_completed_at, DateTime.UtcNow)
                                      .Field(y => y.processing_completed_details, details)
                                      .Field(y => y.processing_completed_successfully, false))
                           .Where(r => r.Field(x => x.id, id, (a, b) => a == b))
                           .Execute();
            }
        }

        [Fact(Skip = "Compilation Only")]
        public async void Select_Example()
        {
            IDbms dbms = null;
            IDataStore db = null;

            await dbms.CreateTable<all_field_types_v1>()
                      .Fields(t => t.Field(x => x.Boolean_value)
                                    .Field(x => x.Decimal_value)
                                    .Field(x => x.Double_value)
                                    .Field(x => x.Float_value)
                                    .Field(x => x.int_value)
                                    .Field(x => x.long_value)
                                    .Field(x => x.short_value)
                                    .Field(x => x.string_value)
                                    .Field(x => x.binary_value)
                                    .Field(x => x.date_value)
                                    .Field(x => x.dateTimeOffset_value)
                                    .Field(x => x.datetime_value)
                                    .Field(x => x.interval_value)
                                    .Field(x => x.guid_value)
                                    .Field(x => x.xml1_value)
                                    .Field(x => x.xml2_value)
                                    .Field(x => x.json_value)
                                    .Field(x => x.Boolean_nullable_value)
                                    .Field(x => x.Decimal_nullable_value)
                                    .Field(x => x.Double_nullable_value)
                                    .Field(x => x.Float_nullable_value)
                                    .Field(x => x.int_nullable_value)
                                    .Field(x => x.long_nullable_value)
                                    .Field(x => x.short_nullable_value)
                                    .Field(x => x.date_nullable_value)
                                    .Field(x => x.dateTimeOffset_nullable_value)
                                    .Field(x => x.datetime_nullable_value)
                                    .Field(x => x.interval_nullable_value)
                                    .Field(x => x.guid_nullable_value))
                      .WithKey("Abc", rs => rs.Field(x => x.string_value).Field(x => x.Boolean_value))
                      .WithKey("string_value", rs => rs.Field(x => x.string_value))
                      .WithIndex("Abc", rs => rs.Field(x => x.string_value).Field(x => x.Boolean_value))
                      .WithIndex("string_value", rs => rs.Field(x => x.string_value))
                      .Execute();

            var g = Guid.NewGuid();
            await db.Insert<all_field_types_v1>()
                    .Record(t => t.Field(x => x.Boolean_value, true)
                                  .Field(x => x.Decimal_value, 10.0m)
                                  .Field(x => x.Double_value, double.MaxValue / 2)
                                  .Field(x => x.Float_value, float.MaxValue / 2)
                                  .Field(x => x.int_value, int.MaxValue / 2)
                                  .Field(x => x.long_value, int.MaxValue)
                                  .Field(x => x.short_value, short.MaxValue / 2)
                                  .Field(x => x.string_value, g.ToString("N"))
                                  .Field(x => x.binary_value, g.ToByteArray())
                                  .Field(x => x.date_value, DateTime.UtcNow.Date)
                                  //                        .Field(x => x.dateTimeOffset_value)
                                  .Field(x => x.datetime_value, DateTime.UtcNow)
                                  .Field(x => x.interval_value, DateTime.UtcNow.TimeOfDay)
                                  .Field(x => x.guid_value, g)
                                  .Field(x => x.xml1_value, null)
                                  .Field(x => x.xml2_value, null)
                                  .Field(x => x.json_value, new Dictionary<string, object> {{"a", 1}})
                                  .Field(x => x.Boolean_nullable_value, true)
                                  .Field(x => x.Decimal_nullable_value, 10.0m)
                                  .Field(x => x.Double_nullable_value, double.MaxValue / 2)
                                  .Field(x => x.Float_nullable_value, float.MaxValue / 2)
                                  .Field(x => x.int_nullable_value, int.MaxValue / 2)
                                  .Field(x => x.long_nullable_value, int.MaxValue)
                                  .Field(x => x.short_nullable_value, short.MaxValue / 2)
                                  .Field(x => x.date_nullable_value, DateTime.UtcNow.Date)
                                  .Field(x => x.datetime_nullable_value, DateTime.UtcNow)
                                  .Field(x => x.interval_nullable_value, DateTime.UtcNow.TimeOfDay)
                                  .Field(x => x.guid_nullable_value, g))
                    .Execute();

            var data = await db.SelectSingle<all_field_types_v1>().Execute();
        }
    }
}