using Base2art.Serialization.CodeGeneration;

[assembly: IncludeJsonSerializer("Base2art.DataStorage.Serialization", MakePublic = false)]