﻿namespace Base2art.DataStorage.Data
{
    using System;
    using System.ComponentModel;

    [DefaultValue("article_v2")]
    public class article
    {
        public Guid id { get; set; }
        public Guid category_id { get; set; }
        public string name { get; set; }
    }
}