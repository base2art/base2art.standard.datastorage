﻿namespace Base2art.DataStorage.Data
{
    using System;
    using System.ComponentModel;

    [DefaultValue("message_queue_message_v2")]
    public class message_queue_message_v1
    {
        public Guid id { get; set; }

        public string application_key { get; set; }

        // VARCHAR(256),
        public string application_type { get; set; }

        // VARCHAR(256),
        public string application_data { get; set; }

        // TEXT,
        public int retries_remaining { get; set; }

        // TEXT,
        public TimeSpan process_timeout { get; set; }

        // INT NOT NULL,
        public Guid previous_message_id { get; set; }
        //  BINARY(16),

        public string requestor { get; set; }

        // VARCHAR(256) NOT NULL,
        public DateTime requested_at { get; set; }
        // TIMESTAMP NOT NULL,

        public string approver { get; set; }

        // VARCHAR(256) NOT NULL,
        public DateTime? approved_at { get; set; }
        // TIMESTAMP NOT NULL,

        public Guid processing_lock { get; set; }

        // BINARY(16),
        public string processing_by_machine { get; set; }

        // VARCHAR(256),
        public string processing_by_process { get; set; }

        // VARCHAR(256),
        public string processing_by_thread { get; set; }

        // VARCHAR(256),
        public DateTime? processing_begun_at { get; set; }
        //  TIMESTAMP,

        public bool? processing_completed_successfully { get; set; }

        //  BOOL,
        public DateTime? processing_completed_at { get; set; }

        // TIMESTAMP,
        public string processing_completed_details { get; set; }
        // TEXT);
    }
}