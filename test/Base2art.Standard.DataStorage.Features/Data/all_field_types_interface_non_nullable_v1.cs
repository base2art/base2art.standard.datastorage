﻿namespace Base2art.DataStorage.Data
{
    using System;
    using System.ComponentModel;

    [DefaultValue("all_types_interface_non_nullable_v1")]
    public interface all_field_types_interface_non_nullable_v1
    {
        bool Boolean_value { get; }
        decimal Decimal_value { get; }
        double Double_value { get; }
        float Float_value { get; }
        int int_value { get; }
        long long_value { get; }
        short short_value { get; }
        string string_value { get; }
        DateTime date_value { get; }
        DateTimeOffset dateTimeOffset_value { get; }
        DateTime datetime_value { get; }
        TimeSpan interval_value { get; }
        Guid guid_value { get; }
    }
}