﻿namespace Base2art.DataStorage.Data
{
    using System;
    using System.ComponentModel;

    [DefaultValue("person_v2")]
    public class person
    {
        public string name { get; set; }

        public string social_security_number { get; set; }

        public int living_parent_count { get; set; }

        public bool? has_bool { get; set; }

        public DateTime when { get; set; }
    }
}