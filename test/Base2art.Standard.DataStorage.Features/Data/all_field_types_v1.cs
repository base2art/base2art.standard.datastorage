﻿namespace Base2art.DataStorage.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Xml;
    using System.Xml.Linq;

    [DefaultValue("all_types_v1")]
    public class all_field_types_v1 : all_field_types_non_nullable_v1
    {
        public byte[] binary_value { get; set; }
        public XmlDocument xml1_value { get; set; }
        public XElement xml2_value { get; set; }
        public Dictionary<string, object> json_value { get; set; }

        public bool? Boolean_nullable_value { get; set; }

        //        public byte? Byte_nullable_value { get; set; }
        public decimal? Decimal_nullable_value { get; set; }
        public double? Double_nullable_value { get; set; }
        public float? Float_nullable_value { get; set; }
        public int? int_nullable_value { get; set; }

        public long? long_nullable_value { get; set; }

        //        public Object? object_nullable_value { get; set; }
        public short? short_nullable_value { get; set; }

        //        public string? string_nullable_value { get; set; }
        //        public byte[]? binary_nullable_value { get; set; }
        public DateTime? date_nullable_value { get; set; }
        public DateTimeOffset? dateTimeOffset_nullable_value { get; set; }
        public DateTime? datetime_nullable_value { get; set; }
        public TimeSpan? interval_nullable_value { get; set; }

        public Guid? guid_nullable_value { get; set; }
        //        public XmlDocument? xml1_nullable_value { get; set; }
        //        public XElement? xml2_nullable_value { get; set; }
        //        public Object? Json_nullable { get; set; }
    }
}