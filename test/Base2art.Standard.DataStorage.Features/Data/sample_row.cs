﻿namespace Base2art.DataStorage.Data
{
    using System;
    using System.ComponentModel;

    public interface sample_row
    {
        Guid id { get; }
        string content { get; }
        string audit_who { get; }
        DateTime? audit_when { get; }
    }

    public interface ITableRow
    {
        Guid id { get; }
        DateTime? audit_when { get; }
    }

    public interface sample_row_implementing_interface : ITableRow
    {
        string content { get; }
        string audit_who { get; }
        DateTime? audit_when { get; }
    }

    [DefaultValue("sample_row")]
    public interface sample_row_OtherCased
    {
        Guid ID { get; }
        string Content { get; }
        string Audit_Who { get; }
        DateTime? Audit_When { get; }
    }

    public interface sample_row_state
    {
        int id { get; }
        Guid sample_id { get; }
        int? state { get; }
        string audit_who { get; }
        DateTime? audit_when { get; }
    }

    public interface sample_row_version
    {
        int id { get; }
        Guid sample_id { get; }
        string version_data { get; }
        string audit_who { get; }
        DateTime? audit_when { get; }
    }
}