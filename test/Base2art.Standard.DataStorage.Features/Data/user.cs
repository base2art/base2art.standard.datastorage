﻿namespace Base2art.DataStorage.Data
{
    using System;
    using System.ComponentModel;

    [DefaultValue("user_v2")]
    public class user
    {
        public Guid id { get; set; }
        public string name { get; set; }
        public string table { get; set; }
    }
}