﻿namespace Base2art.DataStorage.Data
{
    using System;

    public class QueueWorkItemRequest
    {
        public QueueWorkItemRequest(
            string applicationKey,
            string workItemType,
            string workItemData,
            string requestedBy,
            DateTimeOffset requestedAt,
            int retryCount,
            TimeSpan timeout)
        {
            this.Timeout = timeout;
            this.ApplicationKey = applicationKey;
            this.RequestedBy = requestedBy;
            this.RequestedAt = requestedAt;
            this.RetryCount = retryCount;
            this.WorkItemType = workItemType;
            this.WorkItemData = workItemData;
        }

        public string ApplicationKey { get; }

        public string RequestedBy { get; }

        public DateTimeOffset RequestedAt { get; }

        public int RetryCount { get; }

        public TimeSpan Timeout { get; }

        public string WorkItemType { get; }

        public string WorkItemData { get; }
    }
}