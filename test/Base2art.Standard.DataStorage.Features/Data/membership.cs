﻿namespace Base2art.DataStorage.Data
{
    using System;
    using System.ComponentModel;

    [DefaultValue("publishers_v2")]
    public class publishers
    {
        public Guid article_id { get; set; }
        public string person_id { get; set; }
        public DateTime published_on { get; set; }
    }
}