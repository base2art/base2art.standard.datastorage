﻿namespace Base2art.DataStorage.Data
{
    using System.ComponentModel;

    [DefaultValue("person_aggregate_v2")]
    public class person_aggregate
    {
        public long count { get; set; }
    }
}