﻿namespace Base2art.DataStorage.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Xml;
    using System.Xml.Linq;

    [DefaultValue("all_types_interface_v1")]
    public interface all_field_types_interface_v1 : all_field_types_interface_non_nullable_v1
    {
        byte[] binary_value { get; }
        XmlDocument xml1_value { get; }
        XElement xml2_value { get; }
        Dictionary<string, object> json_value { get; }

        bool? Boolean_nullable_value { get; }
        decimal? Decimal_nullable_value { get; }
        double? Double_nullable_value { get; }
        float? Float_nullable_value { get; }
        int? int_nullable_value { get; }
        long? long_nullable_value { get; }
        short? short_nullable_value { get; }
        DateTime? date_nullable_value { get; }
        DateTimeOffset? dateTimeOffset_nullable_value { get; }
        DateTime? datetime_nullable_value { get; }
        TimeSpan? interval_nullable_value { get; }
        Guid? guid_nullable_value { get; }
    }
}