﻿namespace Base2art.DataStorage.Data
{
    using System;
    using System.ComponentModel;

    [DefaultValue("employee_v2")]
    public interface employee
    {
        string manager_first_name { get; }
        string manager_last_name { get; }

        string first_name { get; }
        string last_name { get; }

        DateTime birth_date { get; }
    }
}