﻿namespace Base2art.DataStorage.Data
{
    using System;
    using System.ComponentModel;

    [DefaultValue("category_v2")]
    public class category
    {
        public Guid id { get; set; }
        public string name { get; set; }
    }
}