﻿namespace Base2art.DataStorage.Data
{
    using System.ComponentModel;
    using System.ComponentModel.DataAnnotations;

    [DefaultValue("person_with_attributes_v2")]
    public class person_with_attributes
    {
        [Required]
        [MaxLength(250)]
        public string name { get; set; }

        [MinLength(11)]
        [MaxLength(11)]
        public string social_security_number { get; set; }

        public int living_parent_count { get; set; }

        public bool? has_bool { get; set; }
    }
}