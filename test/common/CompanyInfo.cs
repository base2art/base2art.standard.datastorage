using System;
using System.Reflection;

[assembly: CLSCompliant(true)]
[assembly: AssemblyCompany("{companyName}")]
[assembly: AssemblyCopyright("Copyright © 2016")]
[assembly: AssemblyTrademark("{companyName} (c) 2016")]
[assembly: AssemblyCulture("")]